#!/usr/bin/env node

/**
 * After prepare, files are copied to the platforms/ios and platforms/android folders.
 * Lets clean up some of those files that arent needed with this hook.
 */
var path = require('path');
var mv = require('mv');



var iosPlatformsDir_dist_js = path.resolve(__dirname, '../../platforms/ios/www/build/dist');
var iosPlatformsDir_dist_index = path.resolve(__dirname, '../../platforms/ios/www/build/index.html');

var iosPlatformsDir_www_js = path.resolve(__dirname, '../../platforms/ios/www/dist');
var iosPlatformsDir_www_index = path.resolve(__dirname, '../../platforms/ios/www/index.html');

var iosPlatformsDir_www_main = path.resolve(__dirname, '../../platforms/ios/www/main.html');
var iosPlatformsDir_dist_main = path.resolve(__dirname, '../../platforms/ios/www/build/main.html');

console.log("Moving dist files to iOS platform");

mv(iosPlatformsDir_dist_js, iosPlatformsDir_www_js, {mkdirp: true}, function(err) {
  if(typeof err != 'undefined')
  {
    console.log("err");
    console.log(err);
    console.log("ERROR when moving JS folder to iOS platform");
  }
  else
  {
    console.log("JS folder moved OK to iOS platform");
  }
});

mv(iosPlatformsDir_dist_main, iosPlatformsDir_www_main, function(err) {
  if(typeof err != 'undefined')
  {
    console.log("err");
    console.log(err);
    console.log("ERROR when moving main.html file to iOS platform");
  }
  else
  {
    console.log("main.html file moved OK to iOS platform");
  }
});

mv(iosPlatformsDir_dist_index, iosPlatformsDir_www_index, function(err) {
  if(typeof err != 'undefined')
  {
    console.log("err");
    console.log(err);
    console.log("ERROR when moving index.html file to iOS platform");
  }
  else
  {
    console.log("index.html file moved OK to iOS platform");
  }
});

var androidPlatformsDir_dist_js = path.resolve(__dirname, '../../platforms/android/assets/www/build/dist');
var androidPlatformsDir_dist_index = path.resolve(__dirname, '../../platforms/android/assets/www/build/index.html');

var androidPlatformsDir_www_js = path.resolve(__dirname, '../../platforms/android/assets/www/dist');
var androidPlatformsDir_www_index = path.resolve(__dirname, '../../platforms/android/assets/www/index.html');

var androidPlatformsDir_dist_main = path.resolve(__dirname, '../../platforms/android/assets/www/build/main.html');
var androidPlatformsDir_www_main = path.resolve(__dirname, '../../platforms/android/assets/www/main.html');

console.log("Moving dist files to Android platform");

mv(androidPlatformsDir_dist_js, androidPlatformsDir_www_js, {mkdirp: true}, function(err) {
  if(typeof err != 'undefined')
  {
    console.log("err");
    console.log(err);
    console.log("ERROR when moving JS folder to Android platform");
  }
  else
  {
    console.log("JS folder moved OK to Android platform");
  }
});

mv(androidPlatformsDir_dist_index, androidPlatformsDir_www_index, function(err) {
  if(typeof err != 'undefined')
  {
    console.log("err");
    console.log(err);
    console.log("ERROR when moving index.html file to Android platform");
  }
  else
  {
    console.log("index.html file moved OK to Android platform");
  }
});

mv(androidPlatformsDir_dist_main, androidPlatformsDir_www_main, {mkdirp: true}, function(err) {
  if(typeof err != 'undefined')
  {
    console.log("err");
    console.log(err);
    console.log("ERROR when moving main.html folder to Android platform");
  }
  else
  {
    console.log("main.html moved OK to Android platform");
  }
});
