//
//  Requester.h
//  VideoCall
//
//  Created by Weizhe He on 2019/8/15.
//  Copyright © 2019 Wangli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

NS_ASSUME_NONNULL_BEGIN


@interface Requester :  NSObject

+ (void)getHttpRequestURL:(NSString *)url withParam:(NSDictionary *)param RequestSuccess:(void(^)(id repoes,NSURLSessionDataTask *task)) success RequestFaile:(void(^)(NSError *error))faile;

+ (void)postHttpRequestURL:(NSString *)url RequestPram:(id)pram RequestSuccess:(void(^)(id respoes))success RequestFaile:(void(^)(NSError *erro))faile;

+ (void)createDownloadFileWithURLString:(NSString *)URLString downloadFileProgress:(void (^)(NSProgress *))downloadFileProgress setupFilePath:(NSURL *(^)(NSURLResponse *))setupFilePath downloadCompletionHandler:(void (^)(NSURL *, NSError *))downloadCompletionHandler;
@end

NS_ASSUME_NONNULL_END
