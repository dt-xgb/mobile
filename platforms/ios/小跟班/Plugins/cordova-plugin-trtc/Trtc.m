//
//  TrtcPlugin.m
//
//  Created by 布丁丸子酱 on 2018/12/26.
//

#import "Trtc.h"


#define KEY_ALL_USER_ID         @"__all_userid__"
#define KEY_CURRENT_USERID      @"__current_userid__"
#import "TRTCMainViewController.h"
#import "Requester.h"
@implementation Trtc

-(void)requestVideoCall:(CDVInvokedUrlCommand *)command {
    NSLog(@"showCreatePage");
  CDVPluginResult* pluginResult = nil;

    NSString *requestId = [command.arguments objectAtIndex:0];

    NSString *debugStatus = [command.arguments objectAtIndex:1];

    NSString *roomId = [command.arguments objectAtIndex:2];

    NSString *userId = [command.arguments objectAtIndex:3];

    NSString *userName = [command.arguments objectAtIndex:4];

    NSString *userIcon = [command.arguments objectAtIndex:5];

    NSString *userSig = [command.arguments objectAtIndex:6];

   

    //   NSLog(@"receive --roomid :%@,--userId:%@,--userSig:%@",roomId,userId,userSig);
    //  RecallVC *recallVC = [[RecallVC alloc] init];
    //  recallVC.roomId = roomId;
    //  recallVC.userId = userId;
    //  recallVC.userSig = userSig;
    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
    UIViewController* currentViewController = window.rootViewController;
    //获取当前页面控制器
 
    BOOL runLoopFind = YES;




    if (roomId != nil && [roomId  length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:roomId];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    UIViewController *vc = [self xs_getCurrentViewController];
     if([vc isKindOfClass:[TRTCMainViewController class]]){
        NSLog(@"is video call view");
    }else{
        [self joinRoom:roomId withUserId:userId withUsersig:userSig withUserName:userName withUserIcon:userIcon withRequestId:requestId withDebugStatus:debugStatus];
    }
    
    
}
- (void)joinRoom:(NSString *)roomId1 withUserId:(NSString *)userId1 withUsersig:(NSString *)userSig1 withUserName:(NSString *)userName withUserIcon:(NSString *)userIcon withRequestId:(NSString *)requestId  withDebugStatus:(NSString *)debugStatus
{
    // 房间号，注意这里是32位无符号整型
    NSString *roomId = roomId1;//@"999";//_roomIdTextField.text;
    if (roomId.length == 0) {
        roomId = @"999";//_roomIdTextField.placeholder;
    }
    
    // 如果账号没填，为了简单起见，这里随机产生一个
    NSString* userId = userId1;//@"iOS_trtc_01";//self.userId;//@"32626414";//_userIdTextField.text;
    if(userId.length == 0) {
        double tt = [[NSDate date] timeIntervalSince1970];
        int user = ((uint64_t)(tt * 1000.0)) % 100000000;
        userId = [NSString stringWithFormat:@"%d", user];
    }
#pragma warning - "此处传入自己的代码"
    // 将当前userId保存，下次进来时会默认这个账号
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:KEY_CURRENT_USERID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // TRTC相关参数设置
    TRTCParams *param = [[TRTCParams alloc] init];
    param.sdkAppId = 1400231141 ;// _SDKAppID;
    param.userId = userId1;
    param.roomId = (UInt32)roomId.integerValue;
    param.userSig =  userSig1;//@"eJxlj0FPgzAAhe-8CsJ1RttCGZh4QGRRM9e5jUS5NB1tWVU6UurmMP53I1sixnf9vryX9*m4ruutpstzVpbbd22pPTTCcy9dD3hnv7BpFKfMUt-wf1B8NMoIyqQVpocQY4wAGDqKC22VVCdDkSW1xpYUwIHU8lfaLx1bAgCQD2HwR1FVDx*yPL273Qeraj16JOZi0k6vW9Otbxabcc5YVN7DHX8hWSpNnb5tokRlySgnFenmvND*80TqWRjKLn*SLGYxknFY86Yq5oeqnBXJ1WDSqlqcboV4jAEMogHdCdOqre4FBCCGyAc-8Zwv5xujJF9H";//[GenerateTestUserSig genTestUserSig:userId];
    param.privateMapKey = @"";
    //    param.bussInfo = @"{\"Str_uc_params\":{\"pure_audio_push_mod\":1}}"; //纯音频推流参数设置示例
    param.role = TRTCRoleAnchor;//_role;
    
    TRTCMainViewController *vc = [[TRTCMainViewController alloc] init];
    vc.param = param;
    vc.enableCustomVideoCapture = NO;
   // vc.customMediaAsset = _customSourceAsset;
    vc.appScene = TRTCAppSceneVideoCall;//self.appScene;

    vc.userName = userName;
    vc.userIcon = userIcon;
    vc.requestId = requestId;
    vc.debugStatus = debugStatus;

    
    //   [self.navigationController pushViewController:vc animated:YES];
    [self.viewController  presentViewController:vc animated:YES completion:nil];
}
//获取当前页面
-(UIViewController *)xs_getCurrentViewController{
UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
 
    NSAssert(window, @"The window is empty");
 
    //获取根控制器
 
    UIViewController* currentViewController = window.rootViewController;
 
   
 
    //获取当前页面控制器
 
    BOOL runLoopFind = YES;
 
    while (runLoopFind){
 
        if (currentViewController.presentedViewController) {
 
            
 
            currentViewController = currentViewController.presentedViewController;
 
            
 
        } else if ([currentViewController isKindOfClass:[UINavigationController class]]) {
 
            
 
            UINavigationController* navigationController = (UINavigationController* )currentViewController;
 
            currentViewController = [navigationController.childViewControllers lastObject];
 
            
 
        } else if ([currentViewController isKindOfClass:[UITabBarController class]]){
 
            
 
            UITabBarController* tabBarController = (UITabBarController* )currentViewController;
 
            currentViewController = tabBarController.selectedViewController;
 
            
 
        } else {
 
            NSUInteger childViewControllerCount = currentViewController.childViewControllers.count;
 
            if (childViewControllerCount > 0) {
 
                
 
                currentViewController = currentViewController.childViewControllers.lastObject;
 
                return currentViewController;
 
            } else {
 
                return currentViewController;
 
            }
 
        }
 
    }   
  return currentViewController;
 
}

-(void)finishVideoCall :(CDVInvokedUrlCommand *)command{
    CDVPluginResult* pluginResult = nil;

    NSString *requestId = [command.arguments objectAtIndex:0];

    NSString *debugStatus = [command.arguments objectAtIndex:1];


    if (requestId  != nil && [requestId  length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:requestId ];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ForeGroundClosedMusic" object:nil];

    // [self.viewController  dismissViewControllerAnimated:YES completion:nil];

    
  
   [self receiveVideoCallRequestRequestId:requestId withStatus:1 withDebug:debugStatus];

}
-(void)receiveVideoCallRequestRequestId:(NSString *)requestId withStatus:(NSInteger)status withDebug:(NSString*)debug{
    
    NSString *headUrl = [debug isEqualToString:@"1"]? @"https://test17.xgenban.com/sctserver/api/":@"https://xgenban.com/sctserver/api/";
    NSString *footUrl = @"ad/trtc/reply/" ;
    NSString *url = [NSString stringWithFormat:@"%@%@%@/%ld",headUrl,footUrl,requestId,status];
    NSDictionary *dic = @{};
    [Requester getHttpRequestURL:url withParam:dic
                  RequestSuccess:^(id  _Nonnull repoes, NSURLSessionDataTask * _Nonnull task) {
                      
                      NSLog(@"request succeed");
                      // [self dismissViewControllerAnimated:YES completion:nil];
                      
                  } RequestFaile:^(NSError * _Nonnull error) {
                      
                      NSLog(@"request failure");
                      // [self dismissViewControllerAnimated:YES completion:nil];
                  }];
}



@end
