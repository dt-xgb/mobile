//
//  Requester.m
//  VideoCall
//
//  Created by Weizhe He on 2019/8/15.
//  Copyright © 2019 Wangli. All rights reserved.
//

#import "Requester.h"


@implementation Requester

//1.Get请求
/**Get请求
 
 url 服务器请求地址
 
 success 服务器响应返回的结果
 
 faile  失败的信息
 
 */

+ (void)getHttpRequestURL:(NSString *)url withParam:(NSDictionary *)param RequestSuccess:(void(^)(id repoes,NSURLSessionDataTask *task)) success RequestFaile:(void(^)(NSError *error))faile{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    ///自定义http header 此处可省略
    
    /// [manager.requestSerializer setValue:@"application/json"forHTTPHeaderField:@"Accept"];
    
     [manager.requestSerializer setValue:@"application/json;charset=utf-8"forHTTPHeaderField:@"Content-Type"];
    //[manager.requestSerializer setValue:@"application/json;text/plain,*/*"forHTTPHeaderField:@"Accept"];
    manager.requestSerializer.timeoutInterval = 180;
    [manager GET:url parameters:param progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success) {
            
          
            NSLog(@"get ---task:%@",task);
            //
            
            success(responseObject,task);
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        //返回失败结果
        NSLog(@"error=====%@",error);
        faile(error);
        
    }];
    
}

//2.Post请求
/**Post请求
 
 url 服务器请求地址
 
 pram 请求参数
 
 success 服务器响应返回的结果
 
 faile  失败的信息
 
 */

+ (void)postHttpRequestURL:(NSString *)url RequestPram:(id)pram RequestSuccess:(void(^)(id respoes))success RequestFaile:(void(^)(NSError *erro))faile{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerialization = [AFHTTPRequestSerializer serializer];
    
    requestSerialization.timeoutInterval = 15;
    
    // 设置自动管理Cookies
    requestSerialization.HTTPShouldHandleCookies = YES;
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain",nil];
    [manager.requestSerializer setValue:@"application/json;charset=utf-8"forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json;text/plain,*/*"forHTTPHeaderField:@"Accept"];
    // [manager.requestSerializer setHTTPShouldHandleCookies:NO];
    
//    
//    NSHTTPCookieStorage *myCookie = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//    
//    for(NSHTTPCookie *cookie in [myCookie cookies]){
//        NSLog(@"cookie:%@",cookie);
//         [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie]; //保存
//    }
    
    
    [manager POST:url parameters:pram progress:^(NSProgress * _Nonnull uploadProgress) {
        
        ///这里面是进度
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success) {
            
            //返回成功结果
            // NSLog(@"post string:---%@",str);
            success(responseObject);
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        //返回失败结果
        
        faile(error);
        
    }];
    
}

//3.文件下载
+ (void)createDownloadFileWithURLString:(NSString *)URLString downloadFileProgress:(void (^)(NSProgress *))downloadFileProgress setupFilePath:(NSURL *(^)(NSURLResponse *))setupFilePath downloadCompletionHandler:(void (^)(NSURL *, NSError *))downloadCompletionHandler{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSURLSessionDownloadTask *dataTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        /**
         
         *  下载进度
         
         */
        
        downloadFileProgress(downloadProgress);
        
        
        
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        
        /**
         *  设置保存目录
         */
        
        return setupFilePath(response);
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        /**
         *  下载完成
         */
        
        downloadCompletionHandler(filePath,error);
        
    }];
    
    [dataTask resume];
    
}
@end
