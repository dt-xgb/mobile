angular.module('app.controllers')
    .controller('evaluateStudentCtrl', function ($scope, Constant, $state, ChatService, Requester, UserPreference, toaster, $ionicLoading, $timeout, $ionicPopup) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.footerShow = false; //底部
            $scope.evaluateBtnShow = true; //点评按钮
            $scope.getStudentAssessList();
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.classes = UserPreference.getArray('HeadTeacherClasses');
           // console.log($scope.classes);
            $scope.page = 1;
            $scope.selected = {
                status: ''
            };
            $scope.selected.class_id = $scope.recordClassId ? $scope.recordClassId : $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].id : '';
            $scope.selected.class_id = $scope.getMinOfArray();
           
        });

        //更新班级id
        $scope.loadData = function () {
            $scope.recordClassId = $scope.selected.class_id;
            $scope.getStudentAssessList();

        };

        //下拉刷新
        $scope.refreshData = function () {
            $scope.getStudentAssessList();
        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.loadMoreData();
        };

        //去选择评分项
        $scope.goToSelectEvaluateOption = function (item) {
            $state.go('evaluateOptions', {
                classId: $scope.selected.class_id,
                taskId: item.taskId,
                assessTitle:item.title
            });
        };

        // --request 老师获取评价班级列表
        $scope.getStudentAssessList = function () {
            $scope.page = 1;
            Requester.getStudentAssessList($scope.selected.class_id, $scope.page).then(function (resp) {
                if (resp.result) {
                    $scope.list = resp.data.content;
                   
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };
        //加载更多
        $scope.loadMoreData = function () {
            Requester.getStudentAssessList($scope.selected.class_id, $scope.page).then(function (resp) {
                if (resp.result) {
                    var data = resp.data.content;

                    for (var i = 0; i < data.length; i++) {
                        $scope.list.push(data[i]);
                    }
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };


        $scope.getMinOfArray = function () {
            var min = 0;
            if ($scope.classes && $scope.classes.length > 0) {
                min = $scope.classes[0].id;
                for (var i = 0; i < $scope.classes.length; i++) {
                    if ($scope.classes[i].id < min) {
                        min = $scope.classes[i].id;
                    }
                }
            }
            return min;
        };
    });
