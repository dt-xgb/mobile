angular.module('app.controllers')
    .controller('checkInHistoryCtrl', function ($scope, toaster, BroadCast, Constant, $ionicHistory, $state, Requester, $ionicModal, $stateParams, UserPreference, $ionicLoading, ionicImageView, $rootScope, ionicTimePicker, $ionicScrollDelegate, $ionicPopup) {
        function getDisplayTime(time) {
            return time.substr(11, 5);
        }

        $scope.$on("$ionicView.enter", function (event, data) {
            var user = UserPreference.getObject('user');
            $scope.activeIndex = 0;
            $scope.timeBtnTxt = '选择时间';
            $scope.checkInTypes = [{
                key: 0,
                name: '已到校'
            }, {
                key: 1,
                name: '已离校'
            }, {
                key: 2,
                name: '已请假'
            }];
            $scope.selectStudentArr = [];
            //$scope.hasLeaveSchool = useruser.AUTH_LEAVESCHOOL===1;
        });

        function handleCheckInRecordDetail(resp, type) {

            for (var i = 0; i < resp.data.length; i++) {
                var item = resp.data[i];
                if (type === 'l') {
                    $scope.listDetail.push({
                        id: item.studentId,
                        name: item.studentName,
                        icon: item.logo,
                        phoneNumber: item.phoneNumber,
                        time: '',
                        checked: type
                    });
                    $scope.lNum++;
                } else {
                    if (type === 'a')
                        $scope.aNum++;
                    else
                        $scope.nNum++;
                    $scope.listDetail.push({
                        id: item.id,
                        name: item.student_name,
                        icon: item.logo,
                        phoneNumber: item.phoneNumber,
                        time: getDisplayTime(item.arr_time),
                        checked: type
                    });
                }
            }
        }

        $scope.switchTime = function (t) {
            if (t)
                $scope.activeTime = t;
            else if ($scope.timeLists && $scope.timeLists.length > 0)
                $scope.activeTime = $scope.timeLists[0];
            Requester.getNormalClassCheckedIn($scope.selectDay.date, $scope.selected.class_id, $scope.activeTime.id).then(function (resp) {
                $scope.listNormal = resp.data;
                for (var i = 0; i < $scope.listNormal.length; i++) {
                    Array.prototype.push.apply($scope.selectStudentArr, $scope.listNormal[i].adAttendDetailVos);
                }

                $scope.listNormal.forEach(function (item) {
                    if (item.type === 1) {
                        $scope.notComeData = item;
                    } else if (item.type === 2) {
                        $scope.comeData = item;
                    } else if (item.type === 3) {
                        $scope.vacationData = item;
                    } else if (item.type === 5) {
                        $scope.notLeaveData = item;
                    } else if (item.type === 6) {
                        $scope.lateData = item;
                    }
                });
                $scope.recordListData = $scope.activeIndex === 0 ? $scope.notComeData : $scope.activeIndex === 1 ? $scope.lateData : $scope.activeIndex === 2 ? $scope.comeData : $scope.activeIndex === 3 ? $scope.vacationData : $scope.notComeData;
                // $scope.switchTab(0);
            });
        };
        $scope.switchTab = function (index) {
            $scope.activeIndex = index;

            // $scope.listDetail = $scope.listNormal[index].adAttendDetailVos;
            //type 1[未到校] 2[已到] 3[请假] 4[已离校] 5[未离校] 6[迟到]
            $scope.recordListData = index === 0 ? $scope.notComeData : index === 1 ? $scope.lateData : index === 2 ? $scope.comeData : index === 3 ? $scope.vacationData : $scope.notLeaveData;
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/checkInDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function (item) {

            // $timeout(function () {
            //     $ionicScrollDelegate.$getByHandle('attendance').scrollTo(130,0,true);
            // }, 100);
            $scope.selectDay = item;
            $scope.listDetail = [];
            $scope.notComeData = {}; //未到
            $scope.comeData = {}; //已到
            $scope.vacationData = {}; //请假
            $scope.notLeaveData = {}; //未离校
            $scope.lateData = {}; //迟到
            $scope.recordListData = {};
            $scope.activeIndex = 0;
            $scope.listFilter = {
                checked: 'n'
            };
            if ($scope.funcType === 'normal') {
                $scope.switchTime();
            } else if ($scope.funcType === 'kid') {
                Requester.getClassCheckedIn($scope.selectDay.daystr, $scope.selected.class_id).then(function (resp) {
                    $scope.aNum = 0;
                    handleCheckInRecordDetail(resp, 'a');
                });
                Requester.getClassUncheckedIn($scope.selectDay.daystr, $scope.selected.class_id).then(function (resp) {
                    $scope.nNum = 0;
                    handleCheckInRecordDetail(resp, 'n');
                });
                Requester.getClassLeave($scope.selectDay.daystr, $scope.selected.class_id).then(function (resp) {
                    $scope.lNum = 0;
                    handleCheckInRecordDetail(resp, 'l');
                });
            } else {
                Requester.getCourseCheckedIn($scope.selectDay.daystr, $scope.course.id).then(function (resp) {
                    $scope.aNum = 0;
                    handleCheckInRecordDetail(resp, 'a');
                });
                Requester.getCourseUncheckedIn($scope.selectDay.daystr, $scope.course.id).then(function (resp) {
                    $scope.nNum = 0;
                    handleCheckInRecordDetail(resp, 'n');
                });
            }
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/checkInStudentDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.detailModal = modal;
        });
        $scope.openDetailModal = function (sid) {
            var day;
            $scope.studId = sid;
            if ($scope.funcType === 'kid' || $scope.funcType === 'normal') {
                if ($scope.funcType === 'kid') {
                    day = $scope.selectDay.daystr;
                    $scope.detailTitle = day + '学生接送记录';
                    $scope.noContentTip = '暂无接送记录';
                } else if ($scope.funcType === 'normal') {
                    day = $scope.selectDay.date;
                    $scope.detailTitle = day + $scope.activeTime.attendName + '打卡记录';
                    $scope.noContentTip = '暂无打卡记录';
                }
                $scope.detail = {
                    logo: 'img/icon/default_student.png'//Constant.IM_USER_AVATAR
                };
                // $scope.addSignModal.hide();
                $scope.detailModal.show();
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
                });
                var timeId = $scope.activeTime ? $scope.activeTime.id : '';
                $scope.timeId = timeId;
                $scope.myDay = day;
                $scope.getStuInOutRecords();
            }
        };

        //request
        $scope.getStuInOutRecords = function () {
            Requester.getStuInOutRecords($scope.myDay, $scope.studId, undefined, $scope.funcType, $scope.timeId).then(function (resp) {
                $scope.detail = {
                    // list:[{attendDateTime:'2019-09-08 11:30',type:0,attendSource:2,imgUrl:'dada'},{attendDateTime:'2019-09-08 11:30',type:1,attendSource:0,imgUrl:'dada'},{attendDateTime:'2019-09-08 11:30',type:1,attendSource:1}],
                    list: resp.data.attendRecordDetailVos ? resp.data.attendRecordDetailVos : [],
                    studentName: resp.data.studentName,
                    askLeave: resp.data.askLeave,
                    logo: resp.data.logo,
                    id: resp.data.studentId
                };
            }).finally(function () {
                $ionicLoading.hide();
            });
        };

        $scope.closeDetailModal = function () {
            $scope.detailModal.hide();
        };
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
            $scope.detailModal.remove();
            // $scope.addSignModal.remove();
        });

        $scope.changeFilter = function (arg) {
            $scope.listFilter.checked = arg;
        };

        $scope.viewImg = function (url) {
            console.log('url:' + url);
            var newUrl = url.replace('/bre', '');
            if ($scope.largeImgUrl !== newUrl) {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
                });
            }
            $scope.largeImgUrl = newUrl;
            ionicImageView.showViewModal({
                allowSave: true
            }, [$scope.largeImgUrl]);
        };


        function initDate(start, end) {
            if (start && end) {

                $scope.fromDate = resetHMSM(new Date(start)).getTime();
                var endDate = resetHMSM(new Date(end));

                if (endDate.getTime() < $scope.currentDate.getTime()) {
                    $scope.currentDate = endDate;
                }
            } else {
                //学期始于2月和8月
                $scope.currentDate = resetHMSM(new Date());
                if ($scope.currentDate.getMonth() >= 1 && $scope.currentDate.getMonth() <= 6)
                    $scope.fromDate = resetHMSM(new Date($scope.currentDate.getFullYear(), 1, 1)).getTime();
                else if ($scope.currentDate.getMonth() === 0)
                    $scope.fromDate = resetHMSM(new Date($scope.currentDate.getFullYear() - 1, 7, 1)).getTime();
                else
                    $scope.fromDate = resetHMSM(new Date($scope.currentDate.getFullYear(), 7, 1)).getTime();
            }
            $scope.toDate = resetHMSM(new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth() + 1, 0).getDate())).getTime();
            onSelectedDateChange();
        }

        function onSelectedDateChange() {

            var firstDay = new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), 1).getDate();
            var lastDay = new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth() + 1, 0).getDate();
            var firstDayEpoch = resetHMSM(new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), firstDay)).getTime();
            var lastDayEpoch = resetHMSM(new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), lastDay)).getTime();

            $scope.disableGoPre = (firstDayEpoch - 86400000) <= $scope.fromDate;
            $scope.disableGoNext = (lastDayEpoch + 86400000) >= $scope.toDate;
            $scope.loadData();
        }


        $scope.onSelectionChange = function () {
            UserPreference.set("DefaultClassID", $scope.selected.class_id);
            //onSelectedDateChange();
            $scope.getSemesterInfo();
        };

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.funcType = $stateParams.type ? $stateParams.type : $rootScope.WXCheckInHistoryType;
            $scope.isWalkCourse = $stateParams.isWalkCourse ? $stateParams.isWalkCourse : $rootScope.WXIsWalkCourse;
            //console.log('funcType:' + $scope.funcType);
            if (isWeixin()) {
                $rootScope.WXIsWalkCourse = $scope.isWalkCourse;
                $rootScope.WXCheckInHistoryType = $rootScope.WXIsWalkCourse == 'yes' ? '' : $scope.funcType;
            }

            if ($scope.funcType === 'normal' || $scope.funcType === 'kid') {
                $scope.classes = UserPreference.getArray('class');
                $scope.selected = {
                    class_id: UserPreference.get('DefaultClassID', $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].key : '')
                };
                $scope.getSemesterInfo();
            } else {
                $scope.course = $stateParams.course ? $stateParams.course : $rootScope.WXCheckInHistoryCourse;
                $rootScope.WXCheckInHistoryCourse = $scope.course;

                initDate();
            }
        });

        //获取学期列表
        $scope.getSemesterInfo = function () {
            $scope.currentDate = resetHMSM(new Date());
            Requester.getSemesterInfo($scope.selected.class_id).then(function (resp) {
                if (resp.result) {
                    $scope.noCalendar = false;
                    $scope.schoolyear = resp.data.schoolyear;
                    $scope.schoolterm = resp.data.schoolterm;
                    $scope.schoolTime = resp.data.beginDateStr + ' ~ ' + resp.data.endDateStr;
                    var start = resp.data.beginDate && resp.data.beginDate.length >= 10 ? resp.data.beginDate.substr(0, 10) : void 0;
                    var end = resp.data.endDate && resp.data.endDate.length >= 10 ? resp.data.endDate.substr(0, 10) : void 0;
                    initDate(start, end);
                } else {
                    if (resp.code == -1)
                        $scope.noCalendar = true;
                }
            }).then(function () {
                //onSelectedDateChange();
            });
        };

        function handleCheckInRecords(resp) {
            if (resp.result) {
                $scope.list = resp.data;
                $scope.noCalendar = false;
            } else {
                if (resp.code == -1)
                    $scope.noCalendar = true;
                $scope.list = [];
            }
        }

        $scope.loadData = function () {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            console.log('funtion type:' + $scope.funcType);
            var date = $scope.currentDate.getFullYear() + '-' + ($scope.currentDate.getMonth() > 8 ? $scope.currentDate.getMonth() + 1 : '0' + ($scope.currentDate.getMonth() + 1));
            if ($scope.funcType === 'normal') {
                Requester.getClassCheckInInfo(date, $scope.selected.class_id).then(function (resp) {

                    console.log(resp);
                    resp.data = resp.data.content;
                    handleCheckInRecords(resp);
                }).finally(function () {
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
                Requester.getClassCheckInTimeList($scope.selected.class_id).then(function (resp) {
                    $scope.timeLists = resp.data;
                });
            } else if ($scope.funcType === 'kid') {
                Requester.getKidClassCheckInInfo(date, $scope.selected.class_id).then(function (resp) {
                    handleCheckInRecords(resp);
                }).finally(function () {
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
            } else {
                Requester.getCourseCheckInInfo(date, $scope.course.id).then(function (resp) {
                    handleCheckInRecords(resp);
                }).finally(function () {
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
            }
        };

        $scope.prevMonth = function () {
            if ($scope.currentDate.getMonth() === 1) {
                $scope.currentDate.setFullYear($scope.currentDate.getFullYear());
            }
            $scope.currentDate.setMonth($scope.currentDate.getMonth() - 1);
            onSelectedDateChange();
        };

        $scope.nextMonth = function () {
            if ($scope.currentDate.getMonth() === 11) {
                $scope.currentDate.setFullYear($scope.currentDate.getFullYear());
            }
            $scope.currentDate.setDate(1);
            $scope.currentDate.setMonth($scope.currentDate.getMonth() + 1);
            onSelectedDateChange();
        };


        $scope.goBack = function () {
            if ($ionicHistory.backView())
                $ionicHistory.goBack();
            else
                $state.go('tabsController.mainPage');
        };

        // if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/addSign.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.addSignModal = modal;

        });
        $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/addSignDetail.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.addSignDetailModal = modal;
        });

        //补签
        $scope.showAddSignModal = function (type) {
            $scope.signSelected = {
                resignType: 0,
                time: ''
            };
            $scope.timeBtnTxt = '选择时间';

            $scope.addSignType = type; //判断是否从考勤详情页进入
            if (type === 's') {
                $scope.addSignDetailModal.show();
            } else {
                $scope.addSignModal.show();
            }
        };
        //关闭补签弹框
        $scope.hideAddSignModal = function () {
            for (var i = 0; i < $scope.selectStudentArr.length; i++) {
                $scope.selectStudentArr[i].isChecked = false;
            }
            $scope.addSignModal.hide();

        };

        $scope.hideAddSignDetailModal = function () {
            // for (var i = 0; i < $scope.selectStudentArr.length; i++) {
            //     $scope.selectStudentArr[i].isChecked = false;
            // }
            $scope.addSignDetailModal.hide();
        };



        //时间选择器
        $scope.showTimePicker = function () {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {} else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        $scope.timeBtnTxt = time;
                        // var currentDate = formatTimeWithoutSecends(new Date().getTime() / 1000, "yyyy-MM-dd ");
                        $scope.signSelected.time =  $scope.selectDay.date+' ' + time + ':00';
                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

        $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/addSignChooseStudent.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            // modal.el.className = modal.el.className + " third-modal";
            $scope.stuModal = modal;
        });

        //选择学生弹框
        $scope.showChooseMode = function () {
            $scope.stuModal.show();
        };
        //关闭学生选择弹框
        $scope.closeChooseModal = function () {
            $scope.stuModal.hide();
        };

        //
        $scope.toggle = function (group, length) {
            if (length > 0) {
                group.show = !group.show;
                $ionicScrollDelegate.resize();
            }
        };

        $scope.getGroupName = function (type) {
            var result;
            if (type === 1) {
                result = '未到校';
            } else if (type === 2) {
                result = '已到校';
            } else if (type === 3) {
                result = '请假';
            } else if (type === 4) {
                result = '已离校';
            } else if (type === 5) {
                result = '未离校';
            } else if (type === 6) {
                result = '迟到';
            }
            return result;


        };


        //选择学生
        $scope.onLeaveStudentClicked = function (item) {
            if (item.askLeaved) {
                getStuLeaveInfo(item.studentId);
            }
        };



        function getStuLeaveInfo(sid) {
            Requester.getStudentTodayLeaveInfo(sid).then(function (resp) {
                if (resp.result) {
                    var confirmPopup = $ionicPopup.confirm({
                        title: '提示',
                        template: '确定要撤销该学生今日的请假吗？',
                        cancelText: '取消',
                        okText: '确认',
                        okType: 'button-balanced'
                    });
                    confirmPopup.then(function (res) {
                        if (res) {
                            Requester.removeLeaveReq(resp.data.id).then(function (response) {
                                if (response.result) {
                                    $scope.loadData();
                                    $scope.closeDetailModal();
                                } else
                                    toaster.error({
                                        title: response.message,
                                        body: ''
                                    });
                            });
                        }
                    });
                }
            });
        }


        //提交 补签
        $scope.commitAddSign = function (type) {
            if ($scope.signSelected.resignType !== 2 && !$scope.signSelected.time) {
                var title = $scope.signSelected.resignType == 0 ? '请选择到校时间' : '请选择离校时间';
                toaster.warning({
                    title: title,
                    body: ''
                });
                return;
            }
            if($scope.signSelected.resignType==2){
                $scope.signSelected.time = $scope.selectDay.date;
            }
            var to = [];
            if (type == 1) {
                //批量补签
                for (var i = 0; i < $scope.selectStudentArr.length; i++) {
                    if ($scope.selectStudentArr[i].isChecked)
                        to.push({
                            id: $scope.selectStudentArr[i].studentId,
                            time: $scope.signSelected.time,
                            type: $scope.signSelected.resignType,
                            bqtype: 1
                        });
                }
            } else {
                //单个补签
                to.push({
                    id: $scope.studId,
                    time: $scope.signSelected.time,
                    type: $scope.signSelected.resignType,
                    bqtype: 1
                });

            }
            if (to.length > 0) {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });

                Requester.reCheckIn(to).then(function (resp) {
                    if (resp.result) {
                        console.log('补签成功');
                        var count = 0;
                        for (var i = 0; i < resp.data.length; i++) {
                            if (resp.data[i].result==false) {
                                toaster.warning({
                                    title: "补签失败",
                                    timeout: 3000
                                });
                                break;
                            } else {
                                count++;
                            }
                        }
                        if (count != 0 && count == resp.data.length) {
                            toaster.success({
                                title: "补签成功",
                                timeout: 3000
                            });

                            if (type == 1) {
                                $scope.hideAddSignModal();
                            } else {
                                $scope.hideAddSignDetailModal();
                                $scope.getStuInOutRecords();
                            }
                            $scope.switchTime();
                            $scope.loadData();
                        }



                    } else
                        toaster.error({
                            title: resp.message,
                            body: ''
                        });
                }).finally(function () {
                    $ionicLoading.hide();
                });
            } else
                toaster.warning({
                    title: '请选择待补签学生',
                    body: ''
                });
        };

        //判断 数据来源
        $scope.attendanceSourceType = function (type) {
            var result = '';
            result = type == 0 ? '班牌刷卡' : type == 1 ? '人脸识别' : type == 2 ? '远距离考勤机' : type == 3 ? '校车' : type == 4 ? '补签' : '';
            return result;
        };

    });
