
angular.module('app.services', [])
    .factory('UserPreference', ['$window', function ($window) {
        return {
            set: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key) {
                return JSON.parse($window.localStorage[key] || '{}');
            },
            getArray: function (key) {
                return JSON.parse($window.localStorage[key] || '[]');
            },
            getBoolean: function (key) {
                return $window.localStorage[key] === true || $window.localStorage[key] === 'true';
            },
            clear: function () {
                $window.localStorage.clear();
            }
        };
    }])
    .service('scrollDelegateFix', function ($ionicScrollDelegate) {
        //fix a $ionicScrollDelegate bug in ionic v1,which exists in modals
        return {
            $getByHandle: function (name) {
                var instances = $ionicScrollDelegate.$getByHandle(name)._instances;
                return instances.filter(function (element) {
                    //return (element['$$delegateHandle'] === name);
                    return element.$$delegateHandle === name;
                })[0];
            }
        };
    })
    .factory('CameraHelper', function ($q, $ionicPopup, $ionicActionSheet, $rootScope, BroadCast, $cordovaCamera, $jrCrop, Constant) {
        var helper = {};
        helper.cropImage = function (imageURI, width, height, cropTitle) {
            if(iOSDevice()){
                var defer = $q.defer();
                return defer.promise;
            }else{
                $jrCrop.crop({
                    url: imageURI,
                    width: width,
                    height: height,
                    title: cropTitle,
                    cancelText: '取消',
                    chooseText: '确定'
                }).then(function (canvas) {
                    // success!
                    $rootScope.$broadcast(BroadCast.IMAGE_CROP, canvas.toDataURL());
                }, function () {
                    // User canceled or couldn't load image.
                    $rootScope.$broadcast(BroadCast.IMAGE_CROP, undefined);
                });
            }
           
        };
        helper.selectImage = function (which, imgOpt) {
            if (!navigator.camera) {
                alert("Camera API not supported");
                return;
            }
            if(iOSDevice()) imgOpt = {}; //使用wkwebview后 ，图片剪裁插件无法使用，故强制不让剪裁
            $ionicActionSheet.show({
                buttons: [{
                        text: '<i class="icon ion-camera"></i>相机'
                    },
                    {
                        text: '<i class="icon ion-ios-photos"></i>相册'
                    }
                ],
                titleText: '请选择图片来源(单张图片大小不得超过10M)',
                cancelText: '取消',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
                    if (index === 0) {
                        srcType = Camera.PictureSourceType.CAMERA;
                    }
                    var destinationType = Camera.DestinationType.DATA_URL;
                    if ((imgOpt && imgOpt.allowEdit) || which == 'chat') {
                        destinationType = Camera.DestinationType.FILE_URI;
                    }
                    if (index === 0 || index === 1) {
                        var defaultHeight = 300;
                        var defaultWidth = 300;
                        var cropTitle = '截取高亮区域';
                        var options = {
                            quality: which === 'portrait' ? 95 : 85,
                            destinationType: destinationType,
                            sourceType: srcType,
                            allowEdit: false,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: Constant.CAPTURE_IMAGE_RANGE,
                            targetHeight: Constant.CAPTURE_IMAGE_RANGE,
                            correctOrientation: true,
                            popoverOptions: CameraPopoverOptions,
                            saveToPhotoAlbum: false
                        };
                        if (imgOpt) {
                            if (imgOpt.width && imgOpt.height) {
                                defaultHeight = imgOpt.height;
                                defaultWidth = imgOpt.width;
                            } else {
                                options.targetWidth = defaultWidth * 2;
                                options.targetHeight = defaultHeight * 2;
                            }
                            if (imgOpt.title)
                                cropTitle = imgOpt.title;
                        }
                        console.log(imgOpt);
                        if (window.cordova) {
                            $cordovaCamera.getPicture(options).then(function (imageURI) {
                                if (imgOpt && imgOpt.allowEdit) {
                                   //console.log('imageURI:'+imageURI);
                                   // alert(imageURI);
                                    $jrCrop.crop({
                                        url: imageURI,
                                        width: defaultWidth,
                                        height: defaultHeight,
                                        title: cropTitle,
                                        cancelText: '取消',
                                        chooseText: '确定'
                                    }).then(function (canvas) {
                                        // success!
                                        var data = {
                                            which: which,
                                            source: canvas.toDataURL()
                                        };
                                        $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, data);
                                    }, function () {
                                        // User canceled or couldn't load image.
                                        $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, undefined);
                                    });
                                } else {
                                    if (which == 'chat') {
                                        window.resolveLocalFileSystemURL(imageURI, function (fileEntry) {
                                            fileEntry.file(function (fileObj) {
                                                var data = {
                                                    which: which,
                                                    source: fileObj
                                                };
                                                $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, data);
                                            });
                                        });
                                    } else {
                                        var data = {
                                            which: which,
                                            source: "data:image/jpeg;base64," + imageURI
                                        };
                                        $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, data);
                                    }
                                }

                            }, function (err) {
                                $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, undefined);
                                //alert("Read Photo Error: " + err);
                            });
                        }

                    }
                    return true;
                }
            });
        };
        helper.selectMultiImage = function (callback, limit,mult) {
            $ionicActionSheet.show({
                buttons: [{
                        text: '<i class="icon ion-camera"></i>相机'
                    },
                    {
                        text: '<i class="icon ion-ios-photos"></i>相册'
                    }
                ],
                titleText: '请选择图片来源(单张图片大小不得超过10M)',
                cancelText: '取消',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    if (index === 0) {
                        var options = {
                            quality: 85,
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: Camera.PictureSourceType.CAMERA,
                            allowEdit: false,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: Constant.CAPTURE_IMAGE_RANGE,
                            targetHeight: Constant.CAPTURE_IMAGE_RANGE,
                            correctOrientation: true,
                            saveToPhotoAlbum: false
                        };
                       
                        $cordovaCamera.getPicture(options).then(function (imageURI) {
                            callback(imageURI);
                           
                        }, function (err) {
                            console.log(err);
                        });
                    } else if (index === 1) {
                       // console.log('mult==='+mult);
                        window.imagePicker.getPictures(
                            function (results) {
                                if(mult&&mult==="mult"&&iOSDevice()){
                                    for(var k =0;k<results.length;k++){
                                        results[k] = dataURLtoFile(results[k], 'image.jpeg');
                                       }
                                       setTimeout(function(){
                                           callback(results);
                                       },100);
                                   
                                }else{
                                    callback(results);
                                }
                               
                            },
                            function (error) {
                                console.log('Error: ' + error);
                            }, {
                                maximumImagesCount: limit,
                                width: Constant.CAPTURE_IMAGE_RANGE
                            }
                        );

                    }
                    return true;
                }
            });
        };

        helper.selectSinglePicture = function (callback, index) {
            if (!navigator.camera) {
                alert("Camera API not supported");
                return;
            }
            //1 相册 0拍照
            if (window.cordova) {
                var srcType = Camera.PictureSourceType.PHOTOLIBRARY;
                if (index === 0) {
                    srcType = Camera.PictureSourceType.CAMERA;
                }
                var imgOpt = {
                    allowEdit: false
                };
                var destinationType = Camera.DestinationType.DATA_URL;
                if ((imgOpt && imgOpt.allowEdit)) {
                    destinationType = Camera.DestinationType.FILE_URI;
                }
                if (index === 0 || index === 1) {
                    var defaultHeight = 300;
                    var defaultWidth = 300;
                    var options = {
                        quality: 80,
                        destinationType: destinationType,
                        sourceType: srcType,
                        allowEdit: false,
                        encodingType: Camera.EncodingType.JPEG,
                        targetWidth: Constant.CAPTURE_IMAGE_RANGE,
                        targetHeight: Constant.CAPTURE_IMAGE_RANGE,
                        correctOrientation: true,
                        popoverOptions: CameraPopoverOptions,
                        saveToPhotoAlbum: false
                    };
                    if (imgOpt) {
                        if (imgOpt.width && imgOpt.height) {
                            defaultHeight = imgOpt.height;
                            defaultWidth = imgOpt.width;
                        } else {
                            options.targetWidth = defaultWidth * 2;
                            options.targetHeight = defaultHeight * 2;
                        }
                        if (imgOpt.title)
                            cropTitle = imgOpt.title;
                    }
                    $cordovaCamera.getPicture(options).then(function (imageURI) {
                        var base64Str = 'data:image/jpeg;base64,' + imageURI;
                        callback(base64Str);
                    }, function (err) {
                        alert("picture get failure");
                    });
                }
            }
        };

        return helper;
    })
    .factory('httpInterceptor', ['$q', '$injector', function ($q, $injector) {
        var regex = new RegExp('\.(html|js|css)$', 'i');
        var isAsset = function (url) {
            return regex.test(url);
        };
        var doLogin = function () {
            var state = $injector.get('$state');
            var user = $injector.get('UserPreference');
            var auth = $injector.get('AuthorizeService');
            if (!auth.logining && state.current.url !== '/login') {
                var loginModel = {
                    remember: true,
                    username: user.get('account', ''),
                    password: user.get('password', '')
                };
                if (loginModel.username !== '' && loginModel.password !== '') {
                    console.error('timeout, try auto login.');
                    auth.login(loginModel).then(function (res) {
                        if (res && res.result)
                            state.go('tabsController.mainPage');
                    });
                } else {
                    console.error('username or password not found');
                    state.go('login');
                }
            }
        };
        return {
            // optional method
            'request': function (config) {
                // do something on success
                //if(!isAsset(config.url)){            //if the call is not for an asset file
                //	config.url+= "?ts=" +  Date.now();     //append the timestamp
                //}
                if (!isAsset(config.url)) {
                    var Constant = $injector.get('Constant');
                    if (config.url.indexOf('login') > 0 || config.url.indexOf('logout') > 0)
                        config.timeout = Constant.serverTimeout;
                    else
                        config.timeout = Constant.heavyServerTimeout;
                }
                return config;
            },
            // optional method
            'requestError': function (rejection) {
                // do something on error
                return $q.reject(rejection);
            },
            // optional method
            'response': function (response) {
                // do something on success
                if (!isAsset(response.config.url)) {
                    if (response.data.code === -100) {
                       doLogin();
                    }
                }
                return response;
            },
            // optional method
            'responseError': function (rejection) {
                if (!isAsset(rejection.config.url)) {
                    var toaster = $injector.get('toaster');
                    var MESSAGES = $injector.get('MESSAGES');
                    var rootScope = $injector.get('$rootScope');
                    var BroadCast = $injector.get('BroadCast');
                    rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);


                    switch (rejection.status) {
                        case -1:
                        case 408:
                            // console.log('error code:' + rejection.status);
                            toaster.warning({
                                title: MESSAGES.REQUEST_ERROR,
                                body: MESSAGES.CONNECT_TIMEOUT_MSG
                            });
                            break;
                        case 401:
                            doLogin();
                            break;
                        case 404:
                            toaster.error({
                                title: MESSAGES.CONNECT_ERROR,
                                body: rejection.data.message
                            });
                            break;
                        case 500:
                            toaster.error({
                                title: MESSAGES.CONNECT_ERROR,
                                body: MESSAGES.SERVER_ERROR
                            });
                            break;
                        case 503:
                            toaster.error({
                                title: rejection.data.message,
                                body: ''
                            });
                            break;
                        default:

                            toaster.error({
                                title: MESSAGES.CONNECT_ERROR,
                                body: MESSAGES.CONNECT_ERROR_MSG
                            });
                            break;
                    }
                }
                return $q.reject(rejection);
            }
        };
    }])
    .service('closePopupService', [
        function () {
            var currentPopup;
            var htmlEl = angular.element(document.querySelector('html'));
            htmlEl.on('click', function (event) {
                if (event.target.nodeName === 'HTML') {
                    if (currentPopup) {
                        currentPopup.close();
                    }
                }
            });

            this.register = function (popup) {
                currentPopup = popup;
            };
        }
    ])
    .factory('PushService', function ($http, $window, Constant, $state, $ionicPopup, $rootScope, BroadCast, Requester, $cordovaInAppBrowser, UserPreference) {
        var push = {};
        //启动极光推送
        push.init = function (config) {
            var setTagsWithAliasCallback = function (event) {
                console.log(event);
                console.log("Broadcast Rev: Jpush tag and alias settled");
            };
            var debugStatus = Constant.debugMode ? "1" : "2";


            //处于前台
            var receiveNotificationCallback = function (event) {
                var notice;
                var userId = UserPreference.getObject('user').id;
                if (device.platform === "Android") {
                    notice = event.extras; //event.alert
                } else {
                    notice = event; //event.aps.alert
                }
                //后台或前台收到通知时 
                //调用视频插件
              
                var arr = notice.requestId.split("-");
                var pushTamp = arr[1] / 1000; // Date.parse(new Date(notice.createTime.replace(/\-/g, "/"))) / 1000;
                var cureentTamp = Date.parse(new Date()) / 1000;
                var isPass = cureentTamp - pushTamp > 30;
           
                if (notice.messageType === '100114') {
                 //if (device.platform === "Android")  push.clearAllNotification();
                   // $rootScope.pushNotice = notice;
                    var params = {
                        requestId: notice.requestId,
                        debug: debugStatus,
                        roomId: String(notice.userId),
                        userId: String(userId),
                        userName: notice.userName,
                        userIcon: notice.userIcon ? notice.userIcon : '',
                        userSig: notice.userSig //
                    };
                    if (!userId || isPass) {
                        return;
                    }
                    Trtc.requestVideoCall(params, function (success) {
                       // console.log('success');
                        $rootScope.throughStatus = '2';
                        push.clearAllNotification();

                    }, function (failure) {
                      //  console.log('failure');
                    });
                } else {
                    push.setBadgeNumber();
                    $rootScope.$broadcast(BroadCast.NEW_PUSH_REV, undefined);
                }

            };

            var receiveBackNotificationCallback = function(){
                var notice;
                var userId = UserPreference.getObject('user').id;
                
                if (device.platform === "Android") {
                    notice = event.extras; //event.alert   
                } else {
                    notice = event; //event.aps.alert
                }
                //后台或前台收到通知时 
                //调用视频插件
                var arr = notice.requestId.split("-");
                var pushTamp = arr[1] / 1000; // Date.parse(new Date(notice.createTime.replace(/\-/g, "/"))) / 1000;
                var cureentTamp = Date.parse(new Date()) / 1000;
                var isPass = cureentTamp - pushTamp > 30;
            
                if (notice.messageType === '100114') {
                    //if (device.platform === "Android")  push.clearAllNotification();
                    setTimeout(function () {
                        $rootScope.throughStatus = '1';
                    }, 2000);
                   // console.log('bankground troughStatus:' + $rootScope.throughStatus);
                    $rootScope.pushNotice = notice;
                    var params = {
                        requestId: notice.requestId,
                        debug: debugStatus,
                        roomId: String(notice.userId),
                        userId: String(userId),
                        userName: notice.userName,
                        userIcon: notice.userIcon ? notice.userIcon : '',
                        userSig: notice.userSig //
                    };
                    if (!userId || isPass) {
                        return;
                    }
                    Trtc.requestVideoCall(params, function (success) {
                        console.log('success');
                        $rootScope.throughStatus = '2';
                        //if(device.platform === "Android")
                         

                    }, function (failure) {
                        console.log('failure');
                    });
                } else {
                    push.setBadgeNumber();
                    $rootScope.$broadcast(BroadCast.NEW_PUSH_REV, undefined);
                }

            };

            //点击通知栏回调事件
            var openNotificationCallback = function (event) {
                var notice;
                console.log('点击通知栏');
                console.log(event);
                var userRole = UserPreference.getObject('user').rolename;

                if (device.platform === "Android") {
                    notice = event.extras;
                } else {
                    notice = event;
                    push.setBadgeNumber();
                }
                if (notice.messageType != '100114') {
                    //如果收到是视频聊天的 不清除
                    push.clearAllNotification();
                }

                switch (String(notice.messageType)) {
                    case '1':
                        /**
                         * todo
                         */
                    case '2':
                        notice.imageUrls = notice.imageUrls.split(',');
                        $state.go('noticeDetail', {
                            notice: notice
                        });
                        break;
                    case '99999':
                    case '99991':
                        $state.go('checkInNotice');
                        break;
                    case '99998':
                        $state.go('leaveRequest');
                        break;
                    case '99997':
                    case '99996':
                    case '99994':
                        $state.go('safeSchool');
                        break;
                    case '99995':
                        $state.go('checkInNoticeBaby');
                        break;
                    case '10000':
                        $state.go('tabsController.communicatePage');
                        break;
                    case '100100':
                        var userid = '?userId=' + UserPreference.getObject('user').id;
                        if (userid) {
                            var options = {
                                location: 'no',
                                clearcache: 'yes',
                                toolbar: 'no',
                                hidden: 'yes',
                                zoom: 'no'
                            };
                            $cordovaInAppBrowser.open(notice.url + userid, '_blank', options)
                                .then(function (event) {
                                    // success
                                    $cordovaInAppBrowser.show();
                                })
                                .catch(function (event) {
                                    // error
                                    console.log(event);
                                    $cordovaInAppBrowser.close();
                                });
                        }
                        break;
                    case '100101':
                        //用户反馈
                        $state.go('suggest');
                        break;

                    case '100102':
                        //接收考试的 通知
                        $state.go('parScores');
                        break;
                    case '100103':
                        //接收小孩留言
                        $state.go('leaveMessage');
                        break;
                    case '100106':
                        //报修记录关闭 发送给报修人(老师用户)
                        $state.go('repairDetail', {
                            repairRecordId: notice.orderId
                        });
                        break;

                    case '100108':
                        //审批通过 通知
                        $state.go('assetsDetail', {
                            assetsId: notice.orderId
                        });
                        break;

                    case '100109':
                        // 会议通知
                        $state.go('meetingNoticeList');
                        break;

                    case '100110':
                        //取消 会议通知
                        $state.go('meetingNoticeList');
                        break;
                    case '100111':
                        $state.go('approvalDetail', {
                            archiveId: notice.archiveId
                        });
                        break;
                    case '100112':
                        $state.go('dateArrange', {
                            arrangeDate: notice.date
                        });
                        break;

                    case '100113':
                        $state.go('exerciseNoticeDetail', {
                            itemId: notice.id,
                            title: notice.title
                        });
                        break;
                    case '373373':
                        if(userRole ===3){
                            $rootScope.$broadcast(BroadCast.NEW_PUSH_CLICK, undefined);
                            $state.go('covidDailySummery'); 
                        }else{
                           
                            $state.go('studentDailyDetail'); 
                        }
                          
                        break;

                    case '100114':
                        $rootScope.throughStatus = '2';
                        $rootScope.pushNotice = {};
                        var arr = notice.requestId.split("-");
                        var pushTamp = arr[1] / 1000; // Date.parse(new Date(notice.createTime.replace(/\-/g, "/"))) / 1000;
                        var cureentTamp = Date.parse(new Date()) / 1000;
                        var isPass = cureentTamp - pushTamp > 30;
                        var userId = UserPreference.getObject('user').id;
                        var params = {
                            requestId: notice.requestId,
                            debug: debugStatus,
                            roomId: String(notice.userId),
                            userId: String(userId),
                            userName: notice.userName,
                            userIcon: notice.userIcon ? notice.userIcon : '',
                            userSig: notice.userSig 
                        };
                        if (!userId || isPass) {
                            return;
                        }
                        Trtc.requestVideoCall(params, function (success) {
                            console.log('success');
                            // $rootScope.throughStatus = '2';
                            // $rootScope.pushNotice = {};
                        }, function (failure) {
                            console.log('failure');
                        });
                        //收到视频聊天通知
                        push.clearAllNotification();
                        break;

                    case '100115':
                        //收到对方挂断通知
                        var param = {
                            requestId: notice.requestId,
                            debug: debugStatus,
                        };
                        Trtc.finishVideoCall(param, function (success) {
                            console.log('----hang up push success');
                            push.clearAllNotification();
                        }, function (failure) {
                            console.log('----hang up push failure');
                        });
                        break;

                        case '100116':
                            //通知公告
                           // $rootScope.rootNoticeType = notice.key;
                          var  notice1 = {id:notice.id};
                            $state.go('noticeDetail', {
                                notice: angular.copy(notice1)
                            });

                        break;
                        
                     
                        

            

                }
            };

            $window.plugins.jPushPlugin.init();
            //JPushPlugin.init(); //初始化

         //   push.clearAllNotification();
            //设置tag和Alias触发事件处理
            document.addEventListener('jpush.setTagsWithAlias', setTagsWithAliasCallback, false);
            //打开推送消息事件处理
            document.addEventListener("jpush.openNotification", openNotificationCallback, false);
            document.addEventListener("jpush.receiveNotification", receiveNotificationCallback, false);
            if (ionic.Platform.isIOS()) {
                document.addEventListener("jpush.backgroundNotification", receiveBackNotificationCallback, false);
                push.setBadgeNumber();

            } else {
                $window.plugins.jPushPlugin.setLatestNotificationNum(2);
                //JPushPlugin.setLatestNotificationNum(2);
            }
            document.addEventListener("pause", function () {
                console.log('module pause');
                push.appBackground = true;
                push.clearAllNotification();
            }, false);
            document.addEventListener("resume", function () {
                // console.log('module resume');
                // console.log('--', $rootScope.throughStatus);
                var userId = UserPreference.getObject('user').id;
                setTimeout(function () {
                    if ($rootScope.throughStatus === '1' && device.platform !== "Android") {
                        console.log('调用video插件');
                        $rootScope.throughStatus = '2';
                        var notice = $rootScope.pushNotice;
                        var arr = notice.requestId.split("-");
                        var pushTamp = arr[1] / 1000; //Date.parse(new Date(notice.createTime.replace(/\-/g, "/"))) / 1000;
                        var cureentTamp = Date.parse(new Date()) / 1000;
                        var isPass = cureentTamp - pushTamp > 30;
                        if (isPass || !userId) {
                            push.appBackground = false;
                            //push.clearAllNotification();
                            return;
                        }

                        var params = {
                            requestId: notice.requestId,
                            debug: debugStatus,
                            roomId: String(notice.userId),
                            userId: String(userId),
                            userName: notice.userName,
                            userIcon: notice.userIcon ? notice.userIcon : '',
                            userSig: notice.userSig
                        };
                        Trtc.requestVideoCall(params, function (success) {
                            push.clearAllNotification();
                            console.log('success');
                        }, function (failure) {
                            console.log('failure');
                        });
                    }
                    push.appBackground = false;
                    push.clearAllNotification();
                    push.setBadgeNumber();
                }, 10);

            }, false);

            document.addEventListener("jpush.receiveMessage", function (event) {
                var notice;
                notice = event.extras;
                if (notice.messageType === '100115') {
                    var param = {
                        requestId: notice.requestId,
                        debug: debugStatus,
                    };
                    Trtc.finishVideoCall(param, function (success) {
                        console.log('----hang up push success');
                    }, function (failure) {
                        console.log('----hang up push failure');
                    });
                }

            }, false);
            $window.plugins.jPushPlugin.setDebugMode(Constant.debugMode);
            $window.plugins.jPushPlugin.setSilenceTime(22, 0, 7, 0);
            // JPushPlugin.setDebugMode(Constant.debugMode);
            // JPushPlugin.setSilenceTime(22, 0, 7, 0);

            push.getRegID();
        };

        //获取状态
        push.isPushStopped = function (fun) {
            $window.plugins.jPushPlugin.isPushStopped(fun);
            // JPushPlugin.isPushStopped(fun);
        };
        //停止极光推送
        push.stopPush = function () {
            $window.plugins.jPushPlugin.stopPush();
            // JPushPlugin.stopPush();
        };

        //清除通知
        push.clearAllNotification = function () {
            if (ionic.Platform.isIOS())
                $window.plugins.jPushPlugin.clearAllLocalNotifications();
            //JPushPlugin.clearAllLocalNotifications();
            else
                $window.plugins.jPushPlugin.clearAllNotification();
            //JPushPlugin.clearAllLocalNotifications();
        };

        push.getRegID = function () {
            setTimeout(function () {
                $window.plugins.jPushPlugin.getRegistrationID(function (data) {
                    console.log("JPushPlugin:registrationID is " + data);
                    if (data) {
                        push.regID = data;
                    } else {
                        push.getRegID();
                    }
                });

            }, 2000);
        };

        //重启极光推送
        push.resumePush = function () {
            $window.plugins.jPushPlugin.resumePush();
            // JPushPlugin.resumePush();
        };

        //设置标签和别名
        push.setTagsWithAlias = function (tags, alias) {
            console.log("Jpush xxxx tag alias ");

            push.setTags(tags);
            push.setAlias(alias);

        };

        //设置标签
        push.setTags = function (tags) {
            $window.plugins.jPushPlugin.setTags({
                sequence: 1,
                tags: tags
            }, function (success) {
                console.log('new set tags success');
            }, function (failure) {
                console.log('new set tags failure');
            });
            // JPushPlugin.setTags({sequence: 1, tags: tags},function(success){
            //     console.log('new set tags success');
            // },function(failure){
            //     console.log('new set tags failure');
            // });
        };

        //设置别名
        push.setAlias = function (alias) {
            $window.plugins.jPushPlugin.setAlias({
                sequence: 1,
                alias: alias
            }, function (success) {
                console.log('new set alias success ');
                console.log(success);
            }, function (failure) {
                console.log('new set alias failure ');
            });
        };

        // 清除指定标签和alias
        push.clearAliasAndTags = function () {
            var cureentTamp = Date.parse(new Date()) / 1000;

            $window.plugins.jPushPlugin.deleteAlias({
                    sequence: cureentTamp
                },
                function (success) {
                    console.log('delete alias success');
                },
                function (failure) {
                    console.log('delete alias failure');
                });

            $window.plugins.jPushPlugin.cleanTags({
                sequence: cureentTamp
            }, function (success) {
                console.log('delete tags sucess');
            }, function (failure) {
                console.log('delete tags failure');
            });
        };

        push.setAndroidPushTime = function (days, startHour, endHour) {
            $window.plugins.jPushPlugin.setPushTime(days, startHour, endHour);
            // JPushPlugin.setPushTime(days, startHour, endHour);
        };

        push.setAndroidSilenceTime = function (startHour, startMinute, endHour, endMinute) {
            $window.plugins.jPushPlugin.setSilenceTime(startHour, startMinute, endHour, endMinute);
            // JPushPlugin.setSilenceTime(startHour, startMinute, endHour, endMinute);
        };

        push.getUserNotificationSettings = function (fun) {
            $window.plugins.jPushPlugin.getUserNotificationSettings(fun);
            // JPushPlugin.getUserNotificationSettings(fun);
        };

        push.setBasicNotification = function (type) {
            $window.plugins.jPushPlugin.setBasicPushNotificationBuilder(type);
            // JPushPlugin.setBasicPushNotificationBuilder(type);
        };

        push.setBadgeNumber = function () {
            $window.plugins.jPushPlugin.getApplicationIconBadgeNumber(function (data) {
                if (data > 0)
                    $window.plugins.jPushPlugin.setApplicationIconBadgeNumber(0);
            });
            // JPushPlugin.getApplicationIconBadgeNumber(function (data) {
            //     if (data > 0) {
            //         JPushPlugin.setApplicationIconBadgeNumber(0);
            //     }
            // });

        };

        return push;
    })

    //结构待优化


    .factory('AuthorizeService', function ($http, Constant, $rootScope, BroadCast, UserPreference, PushService, $ionicPopup, $state, $q,$injector) {
        var auth = {};

        auth.login = function (user,success,faliure) {
            if (auth.logining) {
                console.log('request is processing.');
                return;
            }
            auth.logining = true;

            var reqUrl = Constant.ServerUrl + "/login";
            var params = {
                loginname: user.username.trim(),
                password: md5(user.password).toUpperCase()
            };
            if ($rootScope.openId) {
                reqUrl = Constant.ServerUrl + "/login/wechat";
                params.openId = $rootScope.openId;
            }
            if(iOSDevice()){
                var defer = {};
                 defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.clearCookies();
                cordova.plugin.http.setRequestTimeout(12.0);
                cordova.plugin.http.setDataSerializer('utf8');
                
                var header = { "Content-Type": 'application/json' }; 
                cordova.plugin.http.post(reqUrl,
                    JSON.stringify(params), header, function (response){
                    var cookies  = cordova.plugin.http.getCookieString(reqUrl);
                    if (cookies) {
                        UserPreference.set("IOSCookies", cookies);
                      cordova.plugin.http.setCookie(reqUrl, cookies);
                    }
                    try {
                        auth.logining = false;
                        var data = JSON.parse(response.data);
                        // console.log('xgb login data=====');
                        // console.log(data);
                        if (data.result) {
                            if (String(data.data.rolename) === Constant.USER_ROLES.SCHOOL) {
                                $ionicPopup.confirm({
                                    title: '温馨提示',
                                    template: '学校管理员请下载小跟班管理端APP再进行登录',
                                    cancelText: '取消',
                                    okText: '下载',
                                    okType: 'button-balanced'
                                }).then(function (res) {
                                    if (res) {
                                        auth.logout();
                                        window.open('http://xgenban.com/download.html', '_system');
                                    }
                                });
                                $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                                return;
                            }
                            var tags = [];
                            if (data.data.sex && data.data.sex !== '')
                                tags.push(data.data.sex);
                            tags.push("JS" + data.data.rolename);
                            if (data.data.school) {
                                tags.push("XX" + data.data.school.id);
                                if (data.data.school.area) {
                                    var area = data.data.school.area.id;
                                    tags.push("SF" + area.substr(0, 2) + "0000");
                                    tags.push("CS" + area.substr(0, 4) + "00");
                                    tags.push("DQ" + area.substr(0, 6));
                                }
                                UserPreference.set('DefaultSchoolName', data.data.school.schoolName);
                            }
                            if (data.data.class) {
                                tags.push("BJ" + data.data.class.id);
                                tags.push("NJ" + data.data.class.grade);
                                if (data.data.class.stage && data.data.class.stage.length > 0)
                                    tags.push("XD" + data.data.class.stage[0]);
                                UserPreference.set('DefaultClassID', data.data.class.id);
                                UserPreference.set('DefaultClassName', data.data.class.className);
                            }
        
                            if (data.data.rolename === 3) {
                                //t
                                UserPreference.set('DefaultSchoolID', data.data.school.id);
                            } else if (data.data.rolename === 4) {
                                //p
                                UserPreference.set('DefaultSchoolID', data.data.schauthmap[0].schoolid);
                            } else if (data.data.rolename === 2) {
                                //s
                                UserPreference.set('DefaultSchoolID', data.data.school.id);
                            }
        
                           // console.log(data.data);
        
                            var students = data.data.student;
                            if (students) {
                                var cid = UserPreference.get('DefaultChildID');
                                var index = 0;
                                for (var i = 0; i < students.length; i++) {
                                    if (students[i].school) {
                                        tags.push("XX" + students[i].school.id);
                                    }
                                    if (students[i].area_code) {
                                        var area2 = students[i].area_code;
                                        tags.push("SF" + area2.substr(0, 2) + "0000");
                                        tags.push("CS" + area2.substr(0, 4) + "00");
                                        tags.push("DQ" + area2.substr(0, 6));
                                    }
                                    if (students[i].classno) {
                                        tags.push("BJ" + students[i].classno.id);
                                        tags.push("NJ" + students[i].classno.grade);
                                        if (students[i].classno.stage && students[i].classno.stage.length > 0)
                                            tags.push("XD" + students[i].classno.stage[0]);
                                    }
                                    if (students[i].id == cid)
                                        index = i;
                                }
                                if (students[index]) {
                                    UserPreference.set('DefaultChildID', students[index].id);
                                    UserPreference.set('DefaultChildName', students[index].student_name);
                                    UserPreference.set('DefaultChildLogo', students[index].logo);
                                    UserPreference.set('DefaultSchoolID', students[index].school.id);
                                    UserPreference.set('DefaultSchoolName', students[index].school.schoolName);
                                    UserPreference.setObject('DefaultChild', students[index]);
                                    if (students[index].classno) {
                                        UserPreference.set('DefaultClassID', students[index].classno.id);
                                        UserPreference.set('DefaultClassName', students[index].classno.className);
                                    }
                                }
                            }
                            var headClassIds = [];
                            if (data.data.classes && data.data.classes.length > 0) {
                                UserPreference.set('isHeadTeacher', true);
                                data.data.classes.forEach(function (item) {
                                    headClassIds.push(item.id);
                                });
                                UserPreference.setObject('HeadTeacherClassId', headClassIds);
                                UserPreference.setObject('HeadTeacherClasses', data.data.classes);
                            } else
                                UserPreference.set('isHeadTeacher', false);
                            UserPreference.setObject("user", data.data);
                            $rootScope.$broadcast(BroadCast.LOGIN_RESULT_RECEIVED, undefined);
                            auth.isLogin = true;
                            //  console.log('tags');
                            // console.log(tags);
        
                            setTimeout(function () {
                                if (window.cordova) {
                                    MobclickAgent.onEvent('app_login');
                                    PushService.setTagsWithAlias(tags, user.username.toLowerCase());
                                }
                                if (Constant.debugMode) console.log(tags);
                            }, 5000);
                        } else {
                            UserPreference.set("password", '');
                            if ($state.current.url !== '/login')
                                $state.go('login');
                        }
                        success(data);
                        defer.resolve(data);
                    } catch (err) {
                        console.log('login parse error');
                    }
                },function(error){
                    auth.logining = false;
                    var status = JSON.parse(error.status);
                    if (status) {
                        tool.interceptors(status);
                    }
                    failure(error);
                     defer.reject(error);
                });
                return defer.promise;
            }else{
                return $http.post(reqUrl, params).then(function (response) {
                    //cache user
                    var data = response.data;
                    // console.log(data.data);
                    if (data.result) {
                        if (String(data.data.rolename) === Constant.USER_ROLES.SCHOOL) {
                            $ionicPopup.confirm({
                                title: '温馨提示',
                                template: '学校管理员请下载小跟班管理端APP再进行登录',
                                cancelText: '取消',
                                okText: '下载',
                                okType: 'button-balanced'
                            }).then(function (res) {
                                if (res) {
                                    auth.logout();
                                    window.open('http://xgenban.com/download.html', '_system');
                                }
                            });
                            $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                            return;
                        }
                        var tags = [];
                        if (data.data.sex && data.data.sex !== '')
                            tags.push(data.data.sex);
                        tags.push("JS" + data.data.rolename);
                        if (data.data.school) {
                            tags.push("XX" + data.data.school.id);
                            if (data.data.school.area) {
                                var area = data.data.school.area.id;
                                tags.push("SF" + area.substr(0, 2) + "0000");
                                tags.push("CS" + area.substr(0, 4) + "00");
                                tags.push("DQ" + area.substr(0, 6));
                            }
                            UserPreference.set('DefaultSchoolName', data.data.school.schoolName);
                        }
                        if (data.data.class) {
                            tags.push("BJ" + data.data.class.id);
                            tags.push("NJ" + data.data.class.grade);
                            if (data.data.class.stage && data.data.class.stage.length > 0)
                                tags.push("XD" + data.data.class.stage[0]);
                            UserPreference.set('DefaultClassID', data.data.class.id);
                            UserPreference.set('DefaultClassName', data.data.class.className);
                        }
    
                        if (data.data.rolename === 3) {
                            //t
                            UserPreference.set('DefaultSchoolID', data.data.school.id);
                        } else if (data.data.rolename === 4) {
                            //p
                            UserPreference.set('DefaultSchoolID', data.data.schauthmap[0].schoolid);
                        } else if (data.data.rolename === 2) {
                            //s
                            UserPreference.set('DefaultSchoolID', data.data.school.id);
                        }
    
                        console.log(data.data);
    
                        var students = data.data.student;
                        if (students) {
                            var cid = UserPreference.get('DefaultChildID');
                            var index = 0;
                            for (var i = 0; i < students.length; i++) {
                                if (students[i].school) {
                                    tags.push("XX" + students[i].school.id);
                                }
                                if (students[i].area_code) {
                                    var area2 = students[i].area_code;
                                    tags.push("SF" + area2.substr(0, 2) + "0000");
                                    tags.push("CS" + area2.substr(0, 4) + "00");
                                    tags.push("DQ" + area2.substr(0, 6));
                                }
                                if (students[i].classno) {
                                    tags.push("BJ" + students[i].classno.id);
                                    tags.push("NJ" + students[i].classno.grade);
                                    if (students[i].classno.stage && students[i].classno.stage.length > 0)
                                        tags.push("XD" + students[i].classno.stage[0]);
                                }
                                if (students[i].id == cid)
                                    index = i;
                            }
                            if (students[index]) {
                                UserPreference.set('DefaultChildID', students[index].id);
                                UserPreference.set('DefaultChildName', students[index].student_name);
                                UserPreference.set('DefaultChildLogo', students[index].logo);
                                UserPreference.set('DefaultSchoolID', students[index].school.id);
                                UserPreference.set('DefaultSchoolName', students[index].school.schoolName);
                                UserPreference.setObject('DefaultChild', students[index]);
                                if (students[index].classno) {
                                    UserPreference.set('DefaultClassID', students[index].classno.id);
                                    UserPreference.set('DefaultClassName', students[index].classno.className);
                                }
                            }
                        }
                        var headClassIds = [];
                        if (data.data.classes && data.data.classes.length > 0) {
                            UserPreference.set('isHeadTeacher', true);
                            data.data.classes.forEach(function (item) {
                                headClassIds.push(item.id);
                            });
                            UserPreference.setObject('HeadTeacherClassId', headClassIds);
                            UserPreference.setObject('HeadTeacherClasses', data.data.classes);
                        } else
                            UserPreference.set('isHeadTeacher', false);
                        UserPreference.setObject("user", data.data);
                        $rootScope.$broadcast(BroadCast.LOGIN_RESULT_RECEIVED, undefined);
                        auth.isLogin = true;
                        //  console.log('tags');
                        // console.log(tags);
    
                        setTimeout(function () {
                            if (window.cordova) {
                                MobclickAgent.onEvent('app_login');
                                PushService.setTagsWithAlias(tags, user.username.toLowerCase());
                            }
                            if (Constant.debugMode) console.log(tags);
                        }, 5000);
                    } else {
                        UserPreference.set("password", '');
                        if ($state.current.url !== '/login')
                            $state.go('login');
                    }
                    return data;
                }, function (error) {
                    return $q.reject(error);
                }).finally(function () {
                    setTimeout(function () {
                        auth.logining = false;
                    }, 5000);
                });
            }
            
        };

        auth.logout = function () {
            auth.isLogin = false;
            if (window.cordova) {
                // PushService.setTagsWithAlias([], "");
                PushService.clearAliasAndTags();
                MobclickAgent.onEvent('app_logout');
            }
            var params = {};
            if ($rootScope.openId) {
                params.openId = $rootScope.openId;
            }
            if(iOSDevice()){
                var defer = $q.defer();
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl + "/logout", cookie);
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
               cordova.plugin.http.get(Constant.ServerUrl + "/logout",{},header,function (response) { 
                cordova.plugin.http.clearCookies();
                 try {
                     var data = JSON.parse(response.data);
                 } catch (error) {
                     
                 }
                 $timeout(function () {
                     defer.resolve(data);
                });   
             }, function (response) {
                 $timeout(function () {
                     defer.reject(response);
                 });
             });
             return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/logout", {
                    params: params
                });
            }
           
        };

        return auth;
    })
    .factory('AccountService', function ($http, Constant, $rootScope, BroadCast,UserPreference,SavePhotoTool,$q,$timeout) {
        var account = {};

        account.getSmsCode = function (phone, type, usertype, username) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl + "/pass/sendmsg", cookie);
                 cordova.plugin.http.setDataSerializer('json');
                 var header = { "Content-Type": 'application/json' };
                 var options = getStringJson({
                    phone: phone,
                    smstype: type,
                    retype: usertype,
                    name: username
                });
                 cordova.plugin.http.get(Constant.ServerUrl + "/pass/sendmsg",options,header,function(response){
                      try {
                        var data = JSON.parse(response.data);
                        $rootScope.$broadcast(BroadCast.SMS_SENT, data);
                        if(data.result==false){
                            SavePhotoTool.defaultLogin(data.code); 
                        }
                      } catch (error) {
                          
                      }
                 },function(error){
                    var status = JSON.parse(error.status);
                    if (status) {
                        SavePhotoTool.interceptors(status);
                    }
                    $rootScope.$broadcast(BroadCast.SMS_SENT, undefined);
                 });
            }else{
                $http.get(Constant.ServerUrl + "/pass/sendmsg", {
                    params: {
                        phone: phone,
                        smstype: type,
                        retype: usertype,
                        name: username
                    }
                })
                .success(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.SMS_SENT, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.SMS_SENT, undefined);
                });
            }
           
        };

        account.resetPwd = function (opt) {
            opt.newpassword = md5(opt.newpassword).toUpperCase();
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl + "/pass/resetpassword", cookie);
                 cordova.plugin.http.setDataSerializer('json');
                 var header = { "Content-Type": 'application/json' };
                 var options = getStringJson(opt);
                 cordova.plugin.http.post(Constant.ServerUrl + "/pass/resetpassword",options,header,function(response){
                      try {
                        var data = JSON.parse(response.data);
                        $rootScope.$broadcast(BroadCast.PASSWORD_RESET, data);
                        if(data.result==false){
                            SavePhotoTool.defaultLogin(data.code); 
                        }
                      } catch (error) {
                          
                      }
                 },function(error){
                    var status = JSON.parse(error.status);
                    if (status) {
                        SavePhotoTool.interceptors(status);
                    }
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                 });
            }else{
                $http.post(Constant.ServerUrl + "/pass/resetpassword", opt)
                .success(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.PASSWORD_RESET, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
            
        };

        account.register = function (opt) {
            opt.password = md5(opt.password).toUpperCase();
            if(iOSDevice()){
                 var header = { "Content-Type": 'application/json' };
                 var options = getStringJson(opt);
                 cordova.plugin.http.post(Constant.ServerUrl + "/pass/resetpassword",options,header,function(response){
                      try {
                        var data = JSON.parse(response.data);
                        if (window.cordova) MobclickAgent.onEvent('app_register');
                        $rootScope.$broadcast(BroadCast.ACCOUNT_REGISTER_COMPLETE, data);
                      } catch (error) {
                          
                      }
                 },function(error){
                    var status = JSON.parse(error.status);
                    if (status) {
                        SavePhotoTool.interceptors(status);
                    }
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                 });
            }else{
              
                $http.post(Constant.ServerUrl + "/registerNew", opt)
                    .success(function (data, header, config, status) {
                        if (window.cordova) MobclickAgent.onEvent('app_register');
                        $rootScope.$broadcast(BroadCast.ACCOUNT_REGISTER_COMPLETE, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }
            
        };

        account.checkCode = function (opt) {
            if(iOSDevice()){
                var defer = $q.defer();
                var header = { "Content-Type": 'application/json' };
                var options = getStringJson(opt);
                cordova.plugin.http.post(Constant.ServerUrl + "/pass/checkcode",options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       defer.resolve(data);
                      
                     } catch (error) {
                         
                     }
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $timeout(function () {
                    defer.reject(error);
                });
                });
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/pass/checkcode", {
                    params: opt
                });
            }
            
        };

        return account;
    })
    .factory('SchoolService', function ($http, Constant, $rootScope, BroadCast, UserPreference,SavePhotoTool,$injector) {
        var school = {};
        school.newNews = function (reqData) {
            var role = String(UserPreference.getObject('user').rolename);
            if (role === Constant.USER_ROLES.PARENT)
                reqData.stuId = UserPreference.get('DefaultChildID');
                if(iOSDevice()){
               var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl + "/campusview/issue", cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options = getStringJson(reqData);
                cordova.plugin.http.post(Constant.ServerUrl + "/campusview/issue",options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       $rootScope.$broadcast(BroadCast.NEW_NEWS, data);
                       if (data.result){
                        school.getNewsList(1);
                       }else{
                        SavePhotoTool.defaultLogin(data.code); 
                       }
                          
                       if (role === Constant.USER_ROLES.PARENT || role === Constant.USER_ROLES.STUDENT) {
                           if (window.cordova) MobclickAgent.onEvent('app_new_news');
                       } else {
                           if (window.cordova) MobclickAgent.onEvent('app_new_news_teacher');
                       }
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
                }else{
                    $http.post(Constant.ServerUrl + "/campusview/issue", reqData)
                    .success(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.NEW_NEWS, data);
                        if (data.result)
                            school.getNewsList(1);
                        if (role === Constant.USER_ROLES.PARENT || role === Constant.USER_ROLES.STUDENT) {
                            if (window.cordova) MobclickAgent.onEvent('app_new_news');
                        } else {
                            if (window.cordova) MobclickAgent.onEvent('app_new_news_teacher');
                        }
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
                }
            
        };

        school.allowPublish = function (reqData) {
            if (reqData && reqData.id) {
                if(iOSDevice()){
                    var cookie = UserPreference.get('IOSCookies');
                    cordova.plugin.http.setCookie(Constant.ServerUrl + "/campusview/pass", cookie);
                     cordova.plugin.http.setDataSerializer('json');
                    var header = { "Content-Type": 'application/json' };
                    var options = getStringJson(reqData);
                    cordova.plugin.http.post(Constant.ServerUrl + "/campusview/pass",options,header,function(response){
                         try {
                           var data = JSON.parse(response.data);
                           data.request = {
                            type: BroadCast.ALLOW_PUBLISH_RST_REV,
                            id: reqData.id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                        if(data.result==false){
                            SavePhotoTool.defaultLogin(data.code); 
                        }
                         } catch (error) {}
                    },function(error){
                       var status = JSON.parse(error.status);
                       if (status) {
                           SavePhotoTool.interceptors(status);
                       }
                       $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
                }else{
                    $http.post(Constant.ServerUrl + "/campusview/pass", reqData)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.ALLOW_PUBLISH_RST_REV,
                            id: reqData.id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
                }
               
            }
        };

        school.ignorePublish = function (id) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl + "/campusview/ignore/" + id, cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =  {};//getStringJson(reqData);
                cordova.plugin.http.put(Constant.ServerUrl + "/campusview/ignore/" + id,options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       data.request = {
                        type: BroadCast.IGNORE_RST_REV,
                        id: id
                    };
                    $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    if(data.result==false){
                        SavePhotoTool.defaultLogin(data.code); 
                    }
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http.put(Constant.ServerUrl + "/campusview/ignore/" + id)
                .success(function (data, header, config, status) {
                    data.request = {
                        type: BroadCast.IGNORE_RST_REV,
                        id: id
                    };
                    $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);

                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
           
        };

        school.deleteNews = function (id) {
            var stuid = '';
            if (UserPreference.getObject('user').rolename == Constant.USER_ROLES.PARENT)
                stuid = '&stuId=' + UserPreference.get('DefaultChildID');
                if(iOSDevice()){
                    var cookie = UserPreference.get('IOSCookies');
                    cordova.plugin.http.setCookie(Constant.ServerUrl + "/campusview/delete?id=" + id + stuid, cookie);
                     cordova.plugin.http.setDataSerializer('json');
                    var header = { "Content-Type": 'application/json' };
                    var options =  {};//getStringJson(reqData);
                    cordova.plugin.http.delete(Constant.ServerUrl + "/campusview/delete?id=" + id + stuid,options,header,function(response){
                         try {
                           var data = JSON.parse(response.data);
                           data.request = {
                            type: BroadCast.DELETE_NEWS_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                        if(data.result==false){
                            SavePhotoTool.defaultLogin(data.code); 
                        }
                         } catch (error) {}
                    },function(error){
                       var status = JSON.parse(error.status);
                       if (status) {
                           SavePhotoTool.interceptors(status);
                       }
                       $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
                }else{
                    $http.delete(Constant.ServerUrl + "/campusview/delete?id=" + id + stuid)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.DELETE_NEWS_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
                }
            
        };

        school.cancelPublish = function (id) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/campusview/cancelIssue/" + id, cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =  {};//getStringJson(reqData);
                cordova.plugin.http.get(Constant.ServerUrl + "/campusview/cancelIssue/" + id,options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       data.request = {
                        type: BroadCast.CANCEL_PUBLISH_REV,
                        id: id
                    };
                    $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    if(data.result==false){
                        SavePhotoTool.defaultLogin(data.code); 
                    }
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http.get(Constant.ServerUrl + "/campusview/cancelIssue/" + id)
                .success(function (data, header, config, status) {
                    data.request = {
                        type: BroadCast.CANCEL_PUBLISH_REV,
                        id: id
                    };
                    $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
           
        };

        school.send2Admin = function (id) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/campusview/recommend/" + id, cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =  {};//getStringJson(reqData);
                cordova.plugin.http.put(Constant.ServerUrl + "/campusview/recommend/"+ id,options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       data.request = {
                        type: BroadCast.SEND_TO_ADMIN_RST,
                        id: id
                    };
                    $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    if(data.result==false){
                        SavePhotoTool.defaultLogin(data.code); 
                    }
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http.put(Constant.ServerUrl + "/campusview/recommend/" + id)
                .success(function (data, header, config, status) {
                    data.request = {
                        type: BroadCast.SEND_TO_ADMIN_RST,
                        id: id
                    };
                    $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
           
        };

        school.getNewsList = function (page, key, reqId) {

            var req = {
                page: page,
                rows: Constant.reqLimit
            };
            if (String(UserPreference.getObject('user').rolename) === Constant.USER_ROLES.PARENT)
                req.stuId = UserPreference.get('DefaultChildID');
            angular.extend(req, key);
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl + "/campusview/list", cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =  getStringJson(req);
                cordova.plugin.http.get(Constant.ServerUrl +  "/campusview/list",options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       if (data.result) {
                        var arr = data.data;
                        var myId = UserPreference.getObject('user').id;
                        for (var i = 0; i < arr.length; i++) {
                            arr[i].isMine = myId === arr[i].authorId;
                            arr[i].isPublish = String(arr[i].state) !== Constant.NEWS_STATUS.TEACHER_REVIEW.key;
                        }
                        if (page && page > 1)
                            Array.prototype.push.apply(school.list, arr);
                        else {
                            school.list = [];
                            school.list = arr;

                            school.list.forEach(function (item) {
                                item.picUrls = [];
                                item.imageUrls.forEach(function (url) {
                                    var urlItem = {
                                        thumb: '',
                                        src: ''
                                    };
                                    urlItem.thumb = url;
                                    urlItem.src = url;
                                    item.picUrls.push(urlItem);
                                });
                            });
                            if (!key) {
                                UserPreference.setObject("news_list_cache", school.list);
                            }

                        }
                        school.listHasMore = arr.length >= Constant.reqLimit;
                        if (Constant.debugMode) {
                            console.log('news data rev, page:' + page);
                            console.log(arr);
                        }
                        data.reqId = reqId;
                    }
                    $rootScope.$broadcast(BroadCast.NEWS_LIST_REV, data);
                    if(data.result==false){
                        SavePhotoTool.defaultLogin(data.code); 
                    }
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http.get(Constant.ServerUrl + "/campusview/list", {
                    params: req
                })
                .success(function (data, header, config, status) {
                    if (data.result) {
                        var arr = data.data;
                        var myId = UserPreference.getObject('user').id;
                        for (var i = 0; i < arr.length; i++) {
                            arr[i].isMine = myId === arr[i].authorId;
                            arr[i].isPublish = String(arr[i].state) !== Constant.NEWS_STATUS.TEACHER_REVIEW.key;
                        }
                        if (page && page > 1)
                            Array.prototype.push.apply(school.list, arr);
                        else {
                            school.list = [];
                            school.list = arr;

                            school.list.forEach(function (item) {
                                item.picUrls = [];
                                item.imageUrls.forEach(function (url) {
                                    var urlItem = {
                                        thumb: '',
                                        src: ''
                                    };
                                    urlItem.thumb = url;
                                    urlItem.src = url;
                                    item.picUrls.push(urlItem);
                                });
                            });
                            if (!key) {
                                UserPreference.setObject("news_list_cache", school.list);
                            }

                        }
                        school.listHasMore = arr.length >= Constant.reqLimit;
                        if (Constant.debugMode) {
                            console.log('news data rev, page:' + page);
                            console.log(arr);
                        }
                        data.reqId = reqId;
                    }
                    $rootScope.$broadcast(BroadCast.NEWS_LIST_REV, data);
                }).error(function (data, header, config, status) {
                    school.listHasMore = false;
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    //toaster.error({title: MESSAGES.CONNECT_ERROR, body: MESSAGES.CONNECT_ERROR_MSG});
                });
            }
            
                
        };

        school.getBannerList = function () {
            var req = {};
            if (String(UserPreference.getObject('user').rolename) === Constant.USER_ROLES.PARENT)
                req.stuId = UserPreference.get('DefaultChildID');
                if(iOSDevice()){
                    var cookie = UserPreference.get('IOSCookies');
                    cordova.plugin.http.setCookie(Constant.ServerUrl +"/campusview/focuslist", cookie);
                     cordova.plugin.http.setDataSerializer('json');
                    var header = { "Content-Type": 'application/json' };
                    var options =  getStringJson(req);
                    cordova.plugin.http.get(Constant.ServerUrl + "/campusview/focuslist",options,header,function(response){
                         try {
                           var data = JSON.parse(response.data);
                           if (data.result) {
                            school.bannerList = data.data;
                            if (Constant.debugMode) {
                                // console.log('banner data rev');
                                // console.log(school.bannerList);
                            }
                            UserPreference.setObject("news_banner_cache", school.bannerList);
                        }
                        $rootScope.$broadcast(BroadCast.BANNER_LIST_REV, data);
                        if(data.result==false){
                            SavePhotoTool.defaultLogin(data.code); 
                        }
                         } catch (error) {}
                    },function(error){
                       var status = JSON.parse(error.status);
                       if (status) {
                           SavePhotoTool.interceptors(status);
                       }
                       $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
                }else{
                    $http.get(Constant.ServerUrl + "/campusview/focuslist", {
                        params: req
                    })
                    .success(function (data, header, config, status) {
                        if (data.result) {
                            school.bannerList = data.data;
                            if (Constant.debugMode) {
                                // console.log('banner data rev');
                                // console.log(school.bannerList);
                            }
                            UserPreference.setObject("news_banner_cache", school.bannerList);
                        }
                        $rootScope.$broadcast(BroadCast.BANNER_LIST_REV, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body: MESSAGES.CONNECT_ERROR_MSG});
                    });
                }
           
        };

        school.getNewsType = function () {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/getdic/show-type/new", cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =  {};//getStringJson(req);
                cordova.plugin.http.get(Constant.ServerUrl + "/getdic/show-type/new",options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       if (data.result) {
                        var list = data.data;

                        var selectList = [];
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].parentKey === 3) {
                                selectList.push(list[i]);
                            }
                        }
                        UserPreference.setObject("news_type", selectList);
                        $rootScope.$broadcast(BroadCast.NEWS_TYPE_REV, data);
                        if(data.result==false){
                            SavePhotoTool.defaultLogin(data.code); 
                        }
                    }else{
                        var auth = $injector.get('AuthorizeService');
                        var loginModel = {
                            //remember: true,
                            username: UserPreference.get('username', ''),
                            password: UserPreference.get('password', '')
                        };
                        if (data.result == false && (data.code == -1||data.code == -100) && loginModel.password && loginModel.username) {
                            console.log('重新登录');
                            auth.logining = false;
                            auth.login(loginModel, function () { }, function () { });
                        }
                    }
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http.get(Constant.ServerUrl + "/getdic/show-type/new")
                .success(function (data, header, config, status) {
                    if (data.result) {
                        var list = data.data;

                        var selectList = [];
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].parentKey === 3) {
                                selectList.push(list[i]);
                            }
                        }
                        UserPreference.setObject("news_type", selectList);
                        $rootScope.$broadcast(BroadCast.NEWS_TYPE_REV, data);
                    }
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
           
        };


        return school;
    })
    .factory('SettingService', function ($http, Constant, $rootScope, BroadCast, UserPreference, toaster, MESSAGES, $ionicLoading, $ionicPopup, $cordovaFileTransfer, $cordovaFileOpener2, $timeout, ChatService,SavePhotoTool) {
        var setting = {};

        setting.sendFeedback = function (text) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/opnionup", cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =  getStringJson(text);
                cordova.plugin.http.post(Constant.ServerUrl + "/opnionup",options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       $rootScope.$broadcast(BroadCast.FEEDBACK, data);
                       if (window.cordova) MobclickAgent.onEvent('app_suggest');
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                   if(data.result==false){
                    SavePhotoTool.defaultLogin(data.code); 
                }
                });
            }else{
                $http.post(Constant.ServerUrl + "/opnionup", text)
                .success(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.FEEDBACK, data);
                    if (window.cordova) MobclickAgent.onEvent('app_suggest');
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                });
            }
          
        };

        setting.changePwd = function (oldPass, newPass) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/changepassword", cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =   {
                    curpassword: md5(oldPass).toUpperCase(),
                    newpassword: md5(newPass).toUpperCase()
                };// getStringJson(text);
                cordova.plugin.http.post(Constant.ServerUrl + "/changepassword",options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       $rootScope.$broadcast(BroadCast.PASSWORD_CHANGE, data);
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                   if(data.result==false){
                    SavePhotoTool.defaultLogin(data.code); 
                }
                });
            }else{
                $http.post(Constant.ServerUrl + "/changepassword", {
                    curpassword: md5(oldPass).toUpperCase(),
                    newpassword: md5(newPass).toUpperCase()
                })
                .success(function (data, header, config, status) {

                    $rootScope.$broadcast(BroadCast.PASSWORD_CHANGE, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                });
            }
           
        };

        setting.getChildInfo = function () {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/getchildinfo", cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =   {};//getStringJson(text);
                cordova.plugin.http.get(Constant.ServerUrl +"/getchildinfo",options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       if (data.result) {
                        var user = UserPreference.getObject('user');
                        user.child = data.data;
                        UserPreference.setObject('user', user);
                        if (Constant.debugMode) console.log(user.child);
                    }
                    $rootScope.$broadcast(BroadCast.CHILD_INFO_REV, data);
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                   if(data.result==false){
                    SavePhotoTool.defaultLogin(data.code); 
                }
                });
            }else{
                $http.get(Constant.ServerUrl + "/getchildinfo")
                .success(function (data, header, config, status) {
                    if (data.result) {
                        var user = UserPreference.getObject('user');
                        user.child = data.data;
                        UserPreference.setObject('user', user);
                        if (Constant.debugMode) console.log(user.child);
                    }
                    $rootScope.$broadcast(BroadCast.CHILD_INFO_REV, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                });
            }
           
        };

        setting.editInfo = function (key, value) {
            var param = {};
            if (!key || !value)
                return;
            param.key = key;
            param.value = value;

            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/infoedit", cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =  getStringJson(param);
                cordova.plugin.http.post(Constant.ServerUrl + "/infoedit",options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       if (data.result) {
                        var user = UserPreference.getObject('user');
                        if (key === 'name') {
                            user.name = value;
                            if (window.cordova) MobclickAgent.onEvent('app_set_nick');
                        } else if (key === 'logo') {
                            user.logo = 'data:image/jpeg;base64,' + value;
                            if (ChatService.loginInfo)
                                ChatService.loginInfo.image = user.logo;
                            if (window.cordova) MobclickAgent.onEvent('app_set_avata');
                        } else if (key === 'sex') {
                            user.sex = value;
                            if (window.cordova) MobclickAgent.onEvent('app_set_sex');
                        } else if (key === 'instruction') {
                            user.instruction = value;
                            if (window.cordova) MobclickAgent.onEvent('app_set_instruction');
                        }
                        UserPreference.setObject('user', user);
                        $rootScope.$broadcast(BroadCast.EDIT_INFO, data);
                    }
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                   if(data.result==false){
                    SavePhotoTool.defaultLogin(data.code); 
                }
                });
            }else{
                $http.post(Constant.ServerUrl + "/infoedit", param)
                .success(function (data, header, config, status) {
                    if (data.result) {
                        var user = UserPreference.getObject('user');
                        if (key === 'name') {
                            user.name = value;
                            if (window.cordova) MobclickAgent.onEvent('app_set_nick');
                        } else if (key === 'logo') {
                            user.logo = 'data:image/jpeg;base64,' + value;
                            if (ChatService.loginInfo)
                                ChatService.loginInfo.image = user.logo;
                            if (window.cordova) MobclickAgent.onEvent('app_set_avata');
                        } else if (key === 'sex') {
                            user.sex = value;
                            if (window.cordova) MobclickAgent.onEvent('app_set_sex');
                        } else if (key === 'instruction') {
                            user.instruction = value;
                            if (window.cordova) MobclickAgent.onEvent('app_set_instruction');
                        }
                        UserPreference.setObject('user', user);
                    }

                    $rootScope.$broadcast(BroadCast.EDIT_INFO, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                });
            }          
        };

        setting.checkUpdate = function (silent) {

            var os = ionic.Platform.isIOS() ? 'ios' : 'android';

            function hasNewVersion(v) {
                var vr = v.split('.');
                var vl = Constant.version.split('.');
                if (vr && vr.length === vl.length) {
                    for (var i = 0; i < vr.length; i++) {
                        if (Number(vr[i]) > Number(vl[i]))
                            return true;
                        else if (Number(vr[i]) < Number(vl[i]))
                            return false;
                    }
                }
                return false;
            }

            function showProcessDialog(url) {
                $ionicLoading.show({
                    template: "请稍候，正在下载更新包：0%"
                });
                var targetPath = "file:///sdcard/Xgenban/update.apk"; //APP下载存放的路径，可以使用cordova file插件进行相关配置
                var trustHosts = true;
                var options = {};
                $cordovaFileTransfer.download(url, targetPath, options, trustHosts).then(function (result) {
                    // 打开下载下来的APP
                    $cordovaFileOpener2.open(targetPath, 'application/vnd.android.package-archive').then(function () {
                        // 成功
                    }, function (err) {
                        // 错误
                        console.log(err);
                    });
                    $ionicLoading.hide();
                }, function (err) {
                    toaster.error({
                        title: MESSAGES.DOWNLOAD_ERROR,
                        body: err.exception
                    });
                    $ionicLoading.hide();
                }, function (progress) {
                    $timeout(function () {
                        var downloadProgress = (progress.loaded / progress.total) * 100;
                        $ionicLoading.show({
                            template: "请稍候，正在下载更新包：" + Math.floor(downloadProgress) + "%"
                        });
                        if (downloadProgress > 99) {
                            $ionicLoading.hide();
                        }
                    });
                });
            }

            if(iOSDevice()){
                // var cookie = UserPreference.get('IOSCookies');
                // cordova.plugin.http.setCookie(Constant.ServerUrl +"/opnionup", cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =  getStringJson({
                    ostype: os,
                    version: Constant.version
                });
                cordova.plugin.http.get(Constant.ServerUrl + "/checkversion",options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       if (data.data && data.data.version) {
                        console.log('当前服务器版本：' + data.data.version);
                        if (!hasNewVersion(data.data.version)) {
                            if (!silent) $ionicLoading.show({
                                template: '已是最新版应用',
                                noBackdrop: true,
                                duration: 1000
                            });
                        } else {
                            if (Constant.debugMode) console.log(data.data);
                            var confirmPopup = $ionicPopup.confirm({
                                title: '更新提示',
                                template: data.data.content,
                                cancelText: '取消',
                                okText: '升级',
                                okType: 'button-balanced'
                            });
                            if (ionic.Platform.isIOS()) {
                                confirmPopup.then(function (res) {
                                    if (res) {
                                        window.open(data.data.url, '_system');
                                    }
                                });
                            } else if (ionic.Platform.isAndroid()) {

                                confirmPopup.then(function (res) {
                                    if (res) {
                                        window.open('market://details?id=com.sct.xgenban', '_system');
                                    }
                                });
                            }
                        }

                    } else if (!silent) $ionicLoading.show({
                        template: '已是最新版应用',
                        noBackdrop: true,
                        duration: 1000
                    });
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http.get(Constant.ServerUrl + "/checkversion", {
                    params: {
                        ostype: os,
                        version: Constant.version
                    }
                })
                .success(function (data, header, config, status) {
                    if (data.data && data.data.version) {
                        console.log('当前服务器版本：' + data.data.version);
                        if (!hasNewVersion(data.data.version)) {
                            if (!silent) $ionicLoading.show({
                                template: '已是最新版应用',
                                noBackdrop: true,
                                duration: 1000
                            });
                        } else {
                            if (Constant.debugMode) console.log(data.data);
                            var confirmPopup = $ionicPopup.confirm({
                                title: '更新提示',
                                template: data.data.content,
                                cancelText: '取消',
                                okText: '升级',
                                okType: 'button-balanced'
                            });
                            if (ionic.Platform.isIOS()) {
                                confirmPopup.then(function (res) {
                                    if (res) {
                                        window.open(data.data.url, '_system');
                                    }
                                });
                            } else if (ionic.Platform.isAndroid()) {

                                confirmPopup.then(function (res) {
                                    if (res) {
                                        window.open('market://details?id=com.sct.xgenban', '_system');
                                        // var permissions = cordova.plugins.permissions;
                                        // permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, function (status) {
                                        //     if (!status.hasPermission) {
                                        //         var errorCallback = function () {
                                        //             toaster.error({
                                        //                 title: MESSAGES.DOWNLOAD_ERROR,
                                        //                 body: '无储存空间写入权限。请重试或在设置、应用中对小跟班放开权限。'
                                        //             });
                                        //         };
                                        //         permissions.requestPermission(
                                        //             permissions.READ_EXTERNAL_STORAGE,
                                        //             function (status) {
                                        //                 if (!status.hasPermission) {
                                        //                     errorCallback();
                                        //                 } else {
                                        //                     showProcessDialog(data.data.url);
                                        //                 }
                                        //             }, errorCallback);
                                        //     }
                                        //     else
                                        //         showProcessDialog(data.data.url);
                                        // }, null);
                                    }
                                });
                            }
                        }

                    } else if (!silent) $ionicLoading.show({
                        template: '已是最新版应用',
                        noBackdrop: true,
                        duration: 1000
                    });
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }

           
        };

        return setting;
    })
    .factory('NoticeService', function ($http, Constant, $rootScope, BroadCast, UserPreference,SavePhotoTool,Requester,$injector) {
        var notice = {};

        notice.newNotice = function (reqData) {
            if(iOSDevice()){
                Requester.advancedHttpRequest(Constant.ServerUrl + "/notice/issue",'post',1,reqData).then(function(data){
                       if(data.result){
                        $rootScope.$broadcast(BroadCast.NEW_NOTICE, data);
                          if (window.cordova) MobclickAgent.onEvent('app_new_notice');
                       }else{
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                         SavePhotoTool.defaultLogin(data.code); 
                       }
                });
              
            }else{
                $http.post(Constant.ServerUrl + "/notice/issue", reqData)
                .success(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.NEW_NOTICE, data);
                    if (window.cordova) MobclickAgent.onEvent('app_new_notice');
                    
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
            
        };

        notice.deleteNotice = function (id) {
            if(iOSDevice()){  
                Requester.advancedHttpRequest(Constant.ServerUrl +"/notice/delete/" + id,'delete',1).then(function(data){
                    if(data.result){
                        data.request = {
                            type: BroadCast.DELETE_NOTICE_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.DELETE_NOTICE_REV, data);
                    }else{
                     $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        SavePhotoTool.defaultLogin(data.code); 
                    }
             });
               
            }else{
                $http.delete(Constant.ServerUrl + "/notice/delete/" + id)
                .success(function (data, header, config, status) {
                    data.request = {
                        type: BroadCast.DELETE_NOTICE_REV,
                        id: id
                    };
                    $rootScope.$broadcast(BroadCast.DELETE_NOTICE_REV, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
           
        };

        notice.getDic = function (dictype) {
            if ($rootScope.dicReqCount > 5) {
                //console.log('not da yue 5');
                return;
            }
            if(iOSDevice()){
                 Requester.advancedHttpRequest(Constant.ServerUrl + "/getdic",'get',1,{
                    dictype: dictype
                }).then(function(data){
                    if(data.result){
                        UserPreference.setObject(dictype, data.data);
                        $rootScope.dicReqCount++;
                        $rootScope.$broadcast(BroadCast.DIC_REV, dictype);
                    }else{
                       
                      SavePhotoTool.defaultLogin(data.code); 
                     $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    }
             });
                
            }else{
                $http.get(Constant.ServerUrl + "/getdic", {
                    params: {
                        dictype: dictype
                    }
                })
                .success(function (data, header, config, status) {
                    if (data.result) {
                        UserPreference.setObject(dictype, data.data);
                        $rootScope.dicReqCount++;
                        $rootScope.$broadcast(BroadCast.DIC_REV, dictype);
                    }
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
            
        };

        notice.getNoticeList = function (page, type) {
            var req = {
                page: page,
                rows: Constant.reqLimit
            };
            var url =  Constant.ServerUrl + "/notice/list" +"?page="+page+"&rows=10";
            if (String(UserPreference.getObject('user').rolename) === Constant.USER_ROLES.PARENT){
                req.stuId = UserPreference.get('DefaultChildID');
                url = url + "&stuId=" + req.stuId;
            }
               
            if (type){
                req.noticeKey = type;
                url = url + "&noticeKey="+type;
            }
               

                if(iOSDevice()){
                    Requester.advancedHttpRequest(url,'get',1).then(function(data){
                        if(data.result){
                            var arr = data.data;
                            for (var j = 0; j < arr.length; j++) {
                                arr[j].time = getWeekday(arr[j].createTime);
                            }
    
                            if (page && page > 1)
                                Array.prototype.push.apply(notice.list, arr);
                            else {
                                notice.list = arr;
                                UserPreference.setObject("notice_list_cache" + type, notice.list);
                            }
                            if (arr.length < Constant.reqLimit)
                                notice.listHasMore = false;
                            else
                                notice.listHasMore = true;
                            if (Constant.debugMode) {
                                // console.log('notice data rev, page:' + page);
                                // console.log(arr);
                            }
                            $rootScope.$broadcast(BroadCast.NOTICE_LIST_REV, data);
                        }else{
                         $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                            SavePhotoTool.defaultLogin(data.code); 
                        }
                 });
                }else{
                    $http.get(Constant.ServerUrl + "/notice/list", {
                        params: req
                    })
                    .success(function (data, header, config, status) {
                        if (data.result) {
                            var arr = data.data;
                            for (var j = 0; j < arr.length; j++) {
                                arr[j].time = getWeekday(arr[j].createTime);
                            }
    
                            if (page && page > 1)
                                Array.prototype.push.apply(notice.list, arr);
                            else {
                                notice.list = arr;
                                UserPreference.setObject("notice_list_cache" + type, notice.list);
                            }
                            if (arr.length < Constant.reqLimit)
                                notice.listHasMore = false;
                            else
                                notice.listHasMore = true;
                            if (Constant.debugMode) {
                                console.log('notice data rev, page:' + page);
                                console.log(arr);
                            }
                        }
                        $rootScope.$broadcast(BroadCast.NOTICE_LIST_REV, data);
                    })
                    .error(function (data, header, config, status) {
                        notice.listHasMore = false;
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                    });
                }
            
        };

        notice.getTodayClass = function (page, startTime, endTime, deviceID) {
            var opt = {
                page: page,
                rows: Constant.reqLimit
            };
            if (String(UserPreference.getObject('user').rolename) === Constant.USER_ROLES.PARENT)
                opt.stuId = UserPreference.get('DefaultChildID');
            else if (String(UserPreference.getObject('user').rolename) === Constant.USER_ROLES.STUDENT)
                opt.stuId = UserPreference.getObject('user').id;
            if (startTime)
                opt.timestart = startTime;
            else
                opt.timestart = '00:00:00';
            if (endTime)
                opt.timeend = endTime;
            else
                opt.timeend = '23:59:59';
            if (deviceID && deviceID !== '')
                opt.tid = deviceID;
            if (Constant.debugMode) console.log(opt);

            if(iOSDevice()){

                Requester.advancedHttpRequest(Constant.ServerUrl + "/todayclass",'get',1,opt).then(function(data){
                    if (data.data) {
                        if (page && page > 1)
                            Array.prototype.push.apply(notice.todayClassList, data.data.content);
                        else
                            notice.todayClassList = data.data.content;
                        if (notice.todayClassList.length < data.data.totalElements)
                            notice.todayListHasMore = true;
                        else
                            notice.todayListHasMore = false;
                        notice.deviceList = data.data.terminals;
                        notice.currentDeviceID = data.data.current;
                        if (Constant.debugMode) {
                            console.log('today class data rev, page:' + page);
                            console.log('today has more: ' + notice.todayListHasMore);
                        }
                        notice.todayClassCount = data.data.totalElements;
                        UserPreference.set("today_class_count", notice.todayClassCount);
                        UserPreference.setObject("today_class_list_cache", notice.todayClassList);
                        if (window.cordova) MobclickAgent.onEvent('app_today_class');
                    } else {
                        notice.todayClassList = [];
                        notice.todayListHasMore = false;
                        notice.todayClassCount = 0;
                        UserPreference.set("today_class_count", 0);
                        UserPreference.setObject("today_class_list_cache", notice.todayClassList);
                    }

                    $rootScope.$broadcast(BroadCast.TODAY_CLASS_LIST_REV, data);
                    if(data.result==false){
                        SavePhotoTool.defaultLogin(data.code); 
                    }
             });
              
            }else{
                $http.get(Constant.ServerUrl + "/todayclass", {
                    params: opt
                })
                .success(function (data, header, config, status) {
                    if (data.data) {
                        if (page && page > 1)
                            Array.prototype.push.apply(notice.todayClassList, data.data.content);
                        else
                            notice.todayClassList = data.data.content;
                        if (notice.todayClassList.length < data.data.totalElements)
                            notice.todayListHasMore = true;
                        else
                            notice.todayListHasMore = false;
                        notice.deviceList = data.data.terminals;
                        notice.currentDeviceID = data.data.current;
                        if (Constant.debugMode) {
                            console.log('today class data rev, page:' + page);
                            console.log('today has more: ' + notice.todayListHasMore);
                        }
                        notice.todayClassCount = data.data.totalElements;
                        UserPreference.set("today_class_count", notice.todayClassCount);
                        UserPreference.setObject("today_class_list_cache", notice.todayClassList);
                        if (window.cordova) MobclickAgent.onEvent('app_today_class');
                    } else {
                        notice.todayClassList = [];
                        notice.todayListHasMore = false;
                        notice.todayClassCount = 0;
                        UserPreference.set("today_class_count", 0);
                        UserPreference.setObject("today_class_list_cache", notice.todayClassList);
                    }

                    $rootScope.$broadcast(BroadCast.TODAY_CLASS_LIST_REV, data);
                })
                .error(function (data, header, config, status) {
                    notice.todayListHasMore = false;
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
            
        };

        notice.getClassSchedule = function (classID) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/course?classId=" + classID, cookie);
                 cordova.plugin.http.setDataSerializer('json');
                 cordova.plugin.http.setRequestTimeout(12.0);
                var header = { "Content-Type": 'application/json' };
                var options =  {};//getStringJson(text);
                cordova.plugin.http.get(Constant.ServerUrl +"/course?classId=" + classID,options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       $rootScope.$broadcast(BroadCast.CLASS_SCHEDULE_REV, data);
                       if(data.result==false){
                        SavePhotoTool.defaultLogin(data.code); 
                    }
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http.get(Constant.ServerUrl + "/course?classId=" + classID)
                .success(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CLASS_SCHEDULE_REV, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
           
        };

        notice.getClassDuty = function (classID) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/duty/students/" + classID, cookie);
                 cordova.plugin.http.setDataSerializer('json');
                // cordova.plugin.http.setRequestTimeout(12.0);
                var header = { "Content-Type": 'application/json' };
                var options =  {};//getStringJson(text);
                cordova.plugin.http.get(Constant.ServerUrl +"/duty/students/" + classID,options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       $rootScope.$broadcast(BroadCast.CLASS_DUTY_REV, data);
                       if(data.result==false){
                        SavePhotoTool.defaultLogin(data.code); 
                    }
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http.get(Constant.ServerUrl + "/duty/students/" + classID)
                .success(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CLASS_DUTY_REV, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
           
        };

        notice.getClassMembers = function (classID) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/class/members/" + classID, cookie);
                 cordova.plugin.http.setDataSerializer('json');
                 cordova.plugin.http.setRequestTimeout(12.0);
                var header = { "Content-Type": 'application/json' };
                var options = {}; //getStringJson(text);
                cordova.plugin.http.get(Constant.ServerUrl + "/class/members/" + classID,options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       $rootScope.$broadcast(BroadCast.CLASS_MEMBER_REV, data);
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http.get(Constant.ServerUrl + "/class/members/" + classID)
                .success(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CLASS_MEMBER_REV, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
           
        };

        notice.updateClassMembers = function (reqData) {
           
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/duty/students", cookie);
                 cordova.plugin.http.setDataSerializer('urlencoded');
                 cordova.plugin.http.setRequestTimeout(12.0);
                var header = { "Content-Type": 'application/x-www-form-urlencoded' };
                var options =   {};//getStringJson(reqData);
              
                var endUrl = CustomParam(reqData);
                // if(reqData.stu_ids&&reqData.stu_ids.length>0){
                   
                //     for(var h=0;h<reqData.stu_ids.length;h++){
                //          endUrl = endUrl+"&stu_ids="+reqData.stu_ids[h];
                //     }
                // }
               
                var URL = Constant.ServerUrl + "/duty/students"+'?'+ endUrl;
               // console.log("URL===="+URL);
                cordova.plugin.http.post(URL,{},header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       $rootScope.$broadcast(BroadCast.CLASS_DUTY_MODIFIED, data);
                    if (window.cordova) MobclickAgent.onEvent('app_set_duty');
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http({
                    method: "post",
                    url: Constant.ServerUrl + "/duty/students",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam(reqData)
                }).success(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CLASS_DUTY_MODIFIED, data);
                    if (window.cordova) MobclickAgent.onEvent('app_set_duty');
                }).error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
            
        };

        notice.getClassBanxun = function (classID) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/class/banxun/" + classID, cookie);
                 cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                var options =  {};//getStringJson(text);
                cordova.plugin.http.get(Constant.ServerUrl + "/class/banxun/" + classID,options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       $rootScope.$broadcast(BroadCast.CLASS_BANXUN_REV, data);
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http.get(Constant.ServerUrl + "/class/banxun/" + classID)
                .success(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CLASS_BANXUN_REV, data);
                })
                .error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
           
        };

        notice.updateClassBanxun = function (reqData) {
            if(iOSDevice()){
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/class/banxun", cookie);
                 cordova.plugin.http.setDataSerializer('urlencoded');
                 cordova.plugin.http.setRequestTimeout(12.0);
                var header = { "Content-Type": 'application/x-www-form-urlencoded' };
                var options =  getStringJson(reqData);
                cordova.plugin.http.post(Constant.ServerUrl + "/class/banxun",options,header,function(response){
                     try {
                       var data = JSON.parse(response.data);
                       if (window.cordova) MobclickAgent.onEvent('app_class_motto');
                     $rootScope.$broadcast(BroadCast.CLASS_BANXUN_UPDATE, data);
                     } catch (error) {}
                },function(error){
                   var status = JSON.parse(error.status);
                   if (status) {
                       SavePhotoTool.interceptors(status);
                   }
                   $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }else{
                $http({
                    method: "post",
                    url: Constant.ServerUrl + "/class/banxun",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam(reqData)
                }).success(function (data, header, config, status) {
                    if (window.cordova) MobclickAgent.onEvent('app_class_motto');
                    $rootScope.$broadcast(BroadCast.CLASS_BANXUN_UPDATE, data);
                }).error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
           
        };

        notice.getMainBannerList = function () {
            if(iOSDevice()){
                Requester.advancedHttpRequest(Constant.ServerUrl + "/article/top",'get',1,{
                    userId: UserPreference.getObject('user').id
                }).then(function(data){
                    if(data.result){
                        UserPreference.setObject('MainBanners', data.data);
                         $rootScope.$broadcast(BroadCast.Main_BANNER_LIST_REV, data);
                    }else{
                     $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    }
             });
               
            }else{
                $http.get(Constant.ServerUrl + "/article/top", {
                    params: {
                        userId: UserPreference.getObject('user').id
                    }
                }).success(function (data, header, config, status) {
                    if (data.result) {
                        UserPreference.setObject('MainBanners', data.data);
                    }
                    $rootScope.$broadcast(BroadCast.Main_BANNER_LIST_REV, data);
    
                }).error(function (data, header, config, status) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            }
            
        };

        return notice;
    })
    .factory('ChatService', function (Constant, UserPreference, $rootScope, BroadCast, $http, $q, $ionicPopup, $window, $state,$injector, $timeout,AuthorizeService) {
        var chat = {};
        /**
         * 联系人缓存
         */
        chat.infoMap = UserPreference.getObject('ChatUserInfoMap');
        /**
         * 通讯录缓存
         */
        chat.friendsMap = UserPreference.getObject('ChatContactListMap');
        chat.retryCount = 0;
        chat.getTabUnreadCount = function () {
            var count = 0;
            for (var i = 0; i < chat.conversations.length; i++) {
                count += chat.conversations[i].UnreadMsgCount;
            }
            $rootScope.$broadcast(BroadCast.BADGE_UPDATE, {
                type: 'im',
                count: count
            });
        };

        chat.sendPush = function (to, title, message) {
            if(iOSDevice()){
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/im/push", cookie);
                cordova.plugin.http.setDataSerializer('json');
                var options = getStringJson({
                    to: to,
                    title: title,
                    message: message
                }); 
                var header = { "Content-Type": 'application/json' };

                cordova.plugin.http.post(Constant.ServerUrl + "/im/push",
                    options, header, function (response) {
                        
                        try {
                            var data = JSON.parse(response.data);
                            var loginModel = {
                                username: UserPreference.get('username', ''),
                                password: UserPreference.get('password', '')
                            };
                            if(data.result==false&&data.code==-1&&loginModel.password&&loginModel.username){ 
                                AuthorizeService.logining = false; 
                                AuthorizeService.login(loginModel,function(){ },function(){});
                            }
                            $timeout(function () {
                                defer.resolve(data);
                            });

                        } catch (error) {

                        }

                    }, function (error) {
                        var status = JSON.parse(error.status);
                    if (status) {
                        tool.interceptors(status);
                    }
                        $timeout(function () {
                             defer.reject(error);
                        });
                    });

                return defer.promise;
            }else{
                return $http.post(Constant.ServerUrl + "/im/push", {
                    to: to,
                    title: title,
                    message: message
                })
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        chat.init = function () {
            var user = UserPreference.getObject("user");
            if (chat.loginInfo || !user.usersig) {
                return;
            }
            var loginInfo = {
                sdkAppID: Constant.IMAppID,
                accountType: Constant.IMAccountType,
                identifier: user.id + '',
                userSig: user.usersig,
                image: user.logo
            };

            //console.log(loginInfo);

            var onKickedEventCall = function () {
                var confirmPopup = $ionicPopup.confirm({
                    title: '温馨提示',
                    template: '账号在其它地方登录，您已下线。',
                    cancelText: '取消',
                    okText: '重新登录',
                    okType: 'button-balanced'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        $state.go('tabsController.mainPage');
                        chat.logout();
                    }
                });
            };

            var onGroupSystemNotifys = function () {
                chat.getContacts();
            };

            var listeners = {
                "onConnNotify": function (resp) {
                    var info;
                    switch (resp.ErrorCode) {
                        case webim.CONNECTION_STATUS.ON:
                            webim.Log.warn('建立连接成功: ' + resp.ErrorInfo);
                            break;
                        case webim.CONNECTION_STATUS.OFF:
                            info = '连接已断开，无法收到新消息，请检查下你的网络是否正常: ' + resp.ErrorInfo;
                            webim.Log.warn(info);
                            break;
                        case webim.CONNECTION_STATUS.RECONNECT:
                            info = '连接状态恢复正常: ' + resp.ErrorInfo;
                            webim.Log.warn(info);
                            break;
                        default:
                            webim.Log.error('未知连接状态: =' + resp.ErrorInfo);
                            break;
                    }
                }, //监听连接状态回调变化事件,必填
                "onMsgNotify": function onMsgNotify(newMsgList) {
                    console.warn(newMsgList);
                    for (var j in newMsgList) { //遍历新消息
                        var newMsg = newMsgList[j];
                        if (newMsg.fromAccount === '@TIM#SYSTEM') {
                            chat.getContacts();
                            continue;
                        }
                        var selSess = newMsg.getSession();
                        var selSessID = selSess.id();
                        var selType = selSess.type();
                        var headUrl = Constant.IM_GROUP_AVATAR,
                            nickName = selSess.name();
                        if (selType == webim.SESSION_TYPE.C2C) {
                            var c2cInfo = chat.getMemberInfoFromMap(selType, selSessID);
                            if (c2cInfo && c2cInfo.name) {
                                nickName = c2cInfo.name;
                            } else {
                                nickName = selSessID;
                            }
                            if (c2cInfo && c2cInfo.image) {
                                headUrl = c2cInfo.image;
                            } else {
                                headUrl = Constant.IM_USER_AVATAR;
                            }
                        }
                        var con = {
                            SessionId: selSessID,
                            SessionImage: headUrl,
                            SessionType: selType,
                            SessionNick: nickName,
                            SessionTime: selSess.time(),
                            MsgTimeStamp: getChatTimeLabel(selSess.time()),
                            UnreadMsgCount: selSess.unread(),
                            MsgShow: delHtmlTag(convertMsgtoPushStr(newMsg))
                        };
                        var k = 0;
                        for (; chat.conversations && k < chat.conversations.length; k++) {
                            if (chat.conversations[k].SessionId == selSess.id()) {
                                chat.conversations[k] = con;
                                break;
                            }
                        }
                        if (!chat.conversations)
                            chat.conversations = [];
                        if (k === chat.conversations.length) {
                            chat.conversations.push(con);
                        }
                    }
                    chat.getTabUnreadCount();
                    $rootScope.$broadcast(BroadCast.IM_NEW_MESSAGE, newMsgList);
                }, //监听新消息(私聊，普通群(非直播聊天室)消息，全员推送消息)事件，必填
                "onGroupSystemNotifys": onGroupSystemNotifys,
                "onKickedEventCall": onKickedEventCall
            };
            var opts = {
                isLogOn: false
            };

            function showLoginError() {
                var confirmPopup = $ionicPopup.confirm({
                    template: '通讯服务器连接失败，请重试！',
                    cancelText: '取消',
                    okText: '重试',
                    okType: 'button-balanced'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        chat.init();
                    }
                });
            }

            webim.login(loginInfo, listeners, opts,
                function (resp) {
                    //console.log('--resp:'+loginInfo);
                    chat.loginInfo = loginInfo;
                    chat.retryCount = 0;
                    chat.getRecentContacts();
                    chat.getContacts();
                },
                function (err) {
                    if (chat.retryCount > 2) {
                        chat.retryCount = 0;
                        console.log(err);
                        console.log('error:');
                        showLoginError();
                    } else {
                        setTimeout(function () {
                            chat.retryCount++;
                            chat.init();
                        }, 1000);
                    }
                });
        };

        chat.getRecentContacts = function (classid) {
          //  console.log('===class id:'+classid);
            if (!chat.loginInfo) {
                chat.init();
                console.log("try login firstly..");
                return;
            }
            webim.getRecentContactList({
                'Count': 100
            }, function (resp) {
                //console.log(resp);
                var data = [];
                var tempSess, tempSessMap = {}; //临时会话变量
                if (resp.SessionItem && resp.SessionItem.length > 0) {
                    for (var i in resp.SessionItem) {
                        var item = resp.SessionItem[i];
                        var type = item.Type; //接口返回的会话类型
                        var sessType, typeZh, sessionId, sessionNick = '',
                            sessionImage = '',
                            senderId = '',
                            senderNick = '';
                        if (type == webim.RECENT_CONTACT_TYPE.C2C) { //私聊
                            typeZh = '私聊';
                            sessType = webim.SESSION_TYPE.C2C; //设置会话类型
                            sessionId = item.To_Account; //会话id，私聊时为好友ID或者系统账号（值为@TIM#SYSTEM，业务可以自己决定是否需要展示），注意：从To_Account获取,

                            if (sessionId === '@TIM#SYSTEM') { //先过滤系统消息，，
                                webim.Log.warn('过滤好友系统消息,sessionId=' + sessionId);
                                continue;
                            }
                            var c2cInfo = chat.getMemberInfoFromMap(sessType, sessionId, classid);
                            if (c2cInfo && c2cInfo.name) { //从infoMap获取c2c昵称
                                sessionNick = c2cInfo.name; //会话昵称，私聊时为好友昵称，接口暂不支持返回，需要业务自己获取（前提是用户设置过自己的昵称，通过拉取好友资料接口（支持批量拉取）得到）
                            } else { //没有找到或者没有设置过
                                sessionNick = sessionId; //会话昵称，如果昵称为空，默认将其设成会话id
                            }
                            if (c2cInfo && c2cInfo.image) { //从infoMap获取c2c头像
                                sessionImage = c2cInfo.image; //会话头像，私聊时为好友头像，接口暂不支持返回，需要业务自己获取（前提是用户设置过自己的昵称，通过拉取好友资料接口（支持批量拉取）得到）
                            } else { //没有找到或者没有设置过
                                sessionImage = Constant.IM_USER_AVATAR; //会话头像，默认
                            }
                            senderId = senderNick = ''; //私聊时，这些字段用不到，直接设置为空

                        } else if (type == webim.RECENT_CONTACT_TYPE.GROUP) { //群聊
                            typeZh = '群聊';
                            sessType = webim.SESSION_TYPE.GROUP; //设置会话类型
                            sessionId = item.ToAccount; //会话id，群聊时为群ID，注意：从ToAccount获取
                            sessionNick = item.GroupNick; //会话昵称，群聊时，为群名称，接口一定会返回

                            if (item.GroupImage) {
                                sessionImage = item.GroupImage;
                            } else
                                sessionImage = Constant.IM_GROUP_AVATAR;
                            senderId = item.MsgGroupFrom_Account; //群消息的发送者id

                            if (!senderId) { //发送者id为空
                                webim.Log.warn('群消息发送者id为空,senderId=' + senderId + ",groupid=" + sessionId);
                                continue;
                            }
                            // if (senderId == '@TIM#SYSTEM') {//先过滤群系统消息，因为接口暂时区分不了是进群还是退群等提示消息，
                            // 	webim.Log.warn('过滤群系统消息,senderId=' + senderId + ",groupid=" + sessionId);
                            // 	continue;
                            // }
                        } else {
                            typeZh = '未知类型';
                            sessionId = item.ToAccount;
                        }

                        if (!sessionId) { //会话id为空
                            webim.Log.warn('会话id为空,sessionId=' + sessionId);
                            continue;
                        }

                        if (sessionId === '@TLS#NOT_FOUND') { //会话id不存在，可能是已经被删除了
                            webim.Log.warn('会话id不存在,sessionId=' + sessionId);
                            continue;
                        }

                        if (sessionNick.length > 10) { //帐号或昵称过长，截取一部分
                            sessionNick = sessionNick.substr(0, 10) + "...";
                        }

                        tempSess = tempSessMap[sessType + "_" + sessionId];
                        if (!tempSess) { //先判断是否存在（用于去重），不存在增加一个
                            tempSessMap[sessType + "_" + sessionId] = true;
                            data.push({
                                SessionType: sessType, //会话类型
                                SessionTypeZh: typeZh, //会话类型中文
                                SessionId: webim.Tool.formatText2Html(sessionId), //会话id
                                SessionNick: webim.Tool.formatText2Html(sessionNick), //会话昵称
                                SessionImage: sessionImage, //会话头像
                                C2cAccount: webim.Tool.formatText2Html(senderId), //发送者id
                                C2cNick: webim.Tool.formatText2Html(senderNick), //发送者昵称
                                UnreadMsgCount: item.UnreadMsgCount, //未读消息数
                                MsgSeq: item.MsgSeq, //消息seq
                                MsgRandom: item.MsgRandom, //消息随机数
                                MsgTimeStamp: getChatTimeLabel(item.MsgTimeStamp), //消息时间戳
                                MsgShow: item.MsgShow //消息内容
                            });
                        }
                    }
                }
                chat.conversations = data;

                function initUnreadMsgCount() {
                    var sess;
                    var sessMap = webim.MsgStore.sessMap();
                    for (var i in sessMap) {
                        sess = sessMap[i];
                        for (var j = 0; j < data.length; j++) {
                            if (sess.id() == data[j].SessionId) {
                                data[j].UnreadMsgCount = sess.unread();
                                break;
                            }
                        }
                    }
                    chat.getTabUnreadCount();
                }

                webim.syncMsgs(initUnreadMsgCount);
                $rootScope.$broadcast(BroadCast.IM_RECENT_CONTACTS, data);
            }, function (error) {
                $rootScope.$broadcast(BroadCast.IM_RECENT_CONTACTS, undefined);
                console.log('im error');
                console.error(error);
            });
        };

        chat.getContacts = function () {
            var options = {
                'Member_Account': chat.loginInfo.identifier,
                'Offset': 0,
                'GroupBaseInfoFilter': [
                    'Type',
                    'Name',
                    'Introduction',
                    'Notification',
                    'FaceUrl',
                    'CreateTime',
                    'Owner_Account',
                    'LastInfoTime',
                    'LastMsgTime',
                    'NextMsgSeq',
                    'MemberNum',
                    'MaxMemberNum',
                    'ApplyJoinOption'
                ],
                'SelfInfoFilter': [
                    'Role',
                    'JoinTime',
                    'MsgFlag',
                    'UnreadMsgNum'
                ]
            };
            webim.getJoinedGroupListHigh(
                options,
                function (resp) {
                    chat.retryCount = 0;
                    if (!resp.GroupIdList || resp.GroupIdList.length === 0) {
                        console.log('你目前还没有加入任何群组');
                        return;
                    }
                    var data = [];
                    // console.log('group Id list');
                    // console.log(resp);
                    for (var i = 0; i < resp.GroupIdList.length; i++) {
                        var item = {
                            'SortField': webim.Tool.groupTypeEn2Ch(resp.GroupIdList[i].Type),
                            'GroupId': resp.GroupIdList[i].GroupId,
                            'Name': webim.Tool.formatText2Html(resp.GroupIdList[i].Name),
                            'TypeEn': resp.GroupIdList[i].Type,
                            'Type': webim.Tool.groupTypeEn2Ch(resp.GroupIdList[i].Type),
                            'RoleEn': resp.GroupIdList[i].SelfInfo.Role,
                            'Role': webim.Tool.groupRoleEn2Ch(resp.GroupIdList[i].SelfInfo.Role),
                            'MsgFlagEn': webim.Tool.groupMsgFlagEn2Ch(resp.GroupIdList[i].SelfInfo.MsgFlag),
                            'MsgFlag': webim.Tool.groupMsgFlagEn2Ch(resp.GroupIdList[i].SelfInfo.MsgFlag),
                            'MemberNum': resp.GroupIdList[i].MemberNum,
                            'Notification': webim.Tool.formatText2Html(resp.GroupIdList[i].Notification),
                            'Introduction': webim.Tool.formatText2Html(resp.GroupIdList[i].Introduction),
                            'JoinTime': webim.Tool.formatTimeStamp(resp.GroupIdList[i].SelfInfo.JoinTime),
                            'NextMsgSeq': resp.GroupIdList[i].NextMsgSeq,
                            'FaceUrl': resp.GroupIdList[i].FaceUrl
                        };

                        var classid, needCache = false;
                        var arr = item.GroupId.split('_');
                        if (arr && arr.length > 1) {
                            classid = item.ClassID = arr[arr.length - 1];
                            if (item.TypeEn === 'Public' && (arr[0] === 'all' || arr[0] === 'school')) {
                                needCache = true;
                                if (arr[0] === 'school') {
                                    classid = item.ClassID = 'schoolTeacher';
                                    data.push(item);
                                }
                            } else
                                data.push(item);
                            chat.getGroupMemberInfo(item, needCache, classid);
                        }
                    }
                    chat.groups = data;
                },
                function (err) {
                    console.log(err.ErrorInfo);
                    if (chat.retryCount > 2) {
                        chat.retryCount = 0;
                        $rootScope.$broadcast(BroadCast.IM_REV_CONTACTS, undefined);
                    } else {
                        setTimeout(function () {
                            chat.retryCount++;
                            chat.getContacts();
                        }, 5000);
                    }
                }
            );
        };

        chat.getMemberInfoFromMap = function (sessType, sessionId, classid) {
            var key = sessType + "_" + sessionId;
            if (sessType == webim.SESSION_TYPE.C2C) {
                var ckey = sessType + "_" + sessionId + "_" + classid;
                if (chat.infoMap[ckey])
                    return chat.infoMap[ckey];
                else
                    return chat.infoMap[key];
            }
            return chat.infoMap[key];
        };

        chat.getGroupMemberInfo = function (group, needCache, classid) {
            var options = {
                'GroupId': group.GroupId,
                'Offset': 0,
                'MemberInfoFilter': [
                    'Account',
                    'Role',
                    'NameCard',
                    'JoinTime',
                    'LastSendMsgTime',
                    'ShutUpUntil',
                    'AppMemberDefinedData'
                ]
            };
            webim.getGroupMemberInfo(
                options,
                function (resp) {
                    chat.retryCount = 0;
                    if (resp.MemberNum <= 0) {
                        console.log(group.Name + '目前没有成员');
                        return;
                    }
                    // console.log('resp--');
                    // console.log(resp.MemberList);
                    var data = [];
                    for (var i in resp.MemberList) {
                        var account = resp.MemberList[i].Member_Account;
                        var role = webim.Tool.groupRoleEn2Ch(resp.MemberList[i].Role);
                        var join_time = webim.Tool.formatTimeStamp(
                            resp.MemberList[i].JoinTime);
                        var shut_up_until = webim.Tool.formatTimeStamp(
                            resp.MemberList[i].ShutUpUntil);
                        if (shut_up_until === 0) {
                            shut_up_until = '-';
                        }
                        var nick, icon, category;
                        var extra = resp.MemberList[i].AppMemberDefinedData;
                        for (var j = 0; j < extra.length; j++) {
                            if (extra[j].Key === 'Category')
                                category = extra[j].Value;
                            else if (extra[j].Key === 'Nick')
                                nick = extra[j].Value;
                            else if (extra[j].Key === 'Icon')
                                icon = extra[j].Value;
                        }

                        var key = webim.SESSION_TYPE.C2C + "_" + account;
                        var pre = chat.infoMap[key];
                        if (!pre || (pre && nick !== ""))
                            chat.infoMap[key] = {
                                name: nick,
                                image: icon,
                                type: category
                            };
                        if (needCache) {
                            chat.infoMap[key + "_" + classid] = chat.infoMap[key];
                            data.push({
                                SortField: nick,
                                GroupId: group.GroupId,
                                Member_Account: account,
                                Role: role,
                                JoinTime: join_time,
                                ShutUpUntil: shut_up_until,
                                Icon: icon,
                                Name: nick,
                                Category: category,
                                ClassID: classid
                            });
                        }
                    }
                    if (needCache) {
                      
                        chat.friendsMap[classid] = data;
                        UserPreference.setObject('ChatUserInfoMap', chat.infoMap);
                        UserPreference.setObject('ChatContactListMap', chat.friendsMap);
                        $rootScope.$broadcast(BroadCast.IM_REV_CONTACTS, data);
                    }
                },
                function (err) {
                    if (needCache) {
                        $rootScope.$broadcast(BroadCast.IM_REV_CONTACTS, undefined);
                        if (chat.retryCount > 2) {
                            chat.retryCount = 0;
                        } else {
                            setTimeout(function () {
                                chat.retryCount++;
                                chat.getContacts();
                            }, 5000);
                        }
                    }
                    console.log(err.ErrorInfo);
                }
            );
        };

        chat.getC2CHistoryMsgs = function (id, lastMsgTime, msgKey) {
            var options = {
                'Peer_Account': id, //好友帐号
                'MaxCnt': 10, //拉取消息条数
                'LastMsgTime': lastMsgTime, //最近的消息时间，即从这个时间点向前拉取历史消息
                'MsgKey': msgKey
            };
            webim.getC2CHistoryMsgs(
                options,
                function (resp) {
                    $rootScope.$broadcast(BroadCast.IM_C2C_HISTORY_MESSAGE, resp);
                },
                function (error) {
                    $rootScope.$broadcast(BroadCast.IM_C2C_HISTORY_MESSAGE, error);
                }
            );
        };

        chat.getGroupHistoryMsgs = function (id, ReqMsgSeq) {
            if (ReqMsgSeq === 0)
                ReqMsgSeq = undefined;
            var options = {
                'GroupId': id,
                'ReqMsgNumber': 10,
                'ReqMsgSeq': ReqMsgSeq
            };
            webim.syncGroupMsgs(
                options,
                function (resp) {
                    $rootScope.$broadcast(BroadCast.IM_GROUP_HISTORY_MESSAGE, resp);
                },
                function (error) {
                    $rootScope.$broadcast(BroadCast.IM_GROUP_HISTORY_MESSAGE, error);
                }
            );
        };

        chat.quitGroup = function (group_id) {
            var defer = $q.defer();
            var options = null;
            if (group_id) {
                options = {
                    'GroupId': group_id
                };
            }
            webim.quitGroup(
                options,
                function (resp) {
                    defer.resolve(resp);
                },
                function (err) {
                    defer.reject(err);
                }
            );
            return defer.promise;
        };

        //解散群 destroyGroup
        chat.destroyGroup = function (groupId, ownerAccount) {
            // var defer = $q.defer();
            // var options = null;
            // if (group_id) {
            //     options = {
            //         'GroupId': group_id
            //     };
            // }
            // webim.destroyGroup(
            //     options,
            //     function (resp) {
            //         defer.resolve(resp);
            //     },
            //     function (err) {
            //         defer.reject(err);
            //     }
            // );
            // return defer.promise;
            if(iOSDevice()){
                  var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                var cookie = UserPreference.get('IOSCookies');
               cordova.plugin.http.setCookie(Constant.ServerUrl +"/im/destroyGroup", cookie);
                cordova.plugin.http.setDataSerializer('json');
                var option = getStringJson( {
                    groupId: groupId,
                    ownerAccount: ownerAccount
                });
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/im/destroyGroup",
                    option, header, function (response) {
                        
                        try {
                            var data = JSON.parse(response.data);
                            $timeout(function () {
                                return defer.resolve(data);
                            });

                        } catch (error) {

                        }
                    }, function (error) {
                        var status = JSON.parse(error.status);
                        if (status) {
                            tool.interceptors(status);
                        }
                        $timeout(function () {
                            return defer.reject(error);
                        });
                    });

                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/im/destroyGroup", {
                    params: {
                        groupId: groupId,
                        ownerAccount: ownerAccount
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        chat.getGroupInfo = function (id) {
            var defer = $q.defer();
            var options = {
                'GroupIdList': [
                    id
                ],
                'GroupBaseInfoFilter': [
                    'Type',
                    'Name',
                    'Introduction',
                    'Notification',
                    'FaceUrl',
                    'CreateTime',
                    'Owner_Account',
                    'LastInfoTime',
                    'LastMsgTime',
                    'NextMsgSeq',
                    'MemberNum',
                    'MaxMemberNum',
                    'ApplyJoinOption'
                ],
                'MemberInfoFilter': [
                    'Account',
                    'Role',
                    'NameCard',
                    'JoinTime',
                    'LastSendMsgTime',
                    'ShutUpUntil',
                    'AppMemberDefinedData',
                    'Category'
                ]
            };
            webim.getGroupInfo(
                options,
                function (resp) {
                    defer.resolve(resp);
                },
                function (err) {
                    defer.reject(err);
                }
            );
            return defer.promise;
        };

        chat.modifyGroupInfo = function (gid, name) {
            var defer = $q.defer();
            var options = {
                'GroupId': gid,
                'Name': name
            };
            webim.modifyGroupBaseInfo(
                options,
                function (resp) {
                    defer.resolve(resp);
                },
                function (err) {
                    defer.reject(err);
                }
            );
            return defer.promise;
        };

        chat.modifyGroupMember = function (gid, members, deleteMode) {
            if (deleteMode) {
                var defer = $q.defer();
                var options = {
                    'GroupId': gid,
                    'MemberToDel_Account': members
                };
                webim.deleteGroupMember(
                    options,
                    function (resp) {
                        defer.resolve(resp);
                    },
                    function (err) {
                        defer.reject(err);
                    }
                );
                return defer.promise;
            } else {
                if(iOSDevice()){
                    var defer1 = $q.defer();
                    var tool = $injector.get('SavePhotoTool');
                    var cookie = UserPreference.get('IOSCookies');
                   cordova.plugin.http.setCookie(Constant.ServerUrl +"/im/add-members", cookie);
                    cordova.plugin.http.setDataSerializer('urlencoded');
                    var option = {
                       
                    };
                    var header = { "Content-Type": 'application/x-www-form-urlencoded' };
                    var URL = Constant.ServerUrl + "/im/add-members" + '?'+ CustomParam(option);
                    cordova.plugin.http.post(URL,
                        option, header, function (response) {
                            
                            try {
                                var data = JSON.parse(response.data);
                                $timeout(function () {
                                    return defer1.resolve(data);
                                });

                            } catch (error) {

                            }
                        }, function (error) {
                            var status = JSON.parse(error.status);
                            if (status) {
                                tool.interceptors(status);
                            }
                            $timeout(function () {
                                return defer1.reject(error);
                            });
                        });

                    return defer1.promise;
                }else{
                    return $http({
                        method: "post",
                        url: Constant.ServerUrl + "/im/add-members",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: CustomParam({
                            'groupId': gid,
                            'members': members
                        })
                    });
                }
                
            }
        };

        chat.createCustomGroup = function (name, sclass, members) {
            var uid = chat.loginInfo.identifier;
            var gid = new Date().getTime() + uid;
            var options = {
                'groupId': gid + "_" + sclass.key,
                'owner': chat.loginInfo.identifier,
                'groupName': name,
                'intro': sclass.value,
                'members': members
            };
            if(iOSDevice()){
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('urlencoded');
                var cookie = UserPreference.get('IOSCookies');
              cordova.plugin.http.setCookie(Constant.ServerUrl +"/im/create-group", cookie);
                
                var endUrl = CustomParam(options);
                var option = {};
                
                var URL = Constant.ServerUrl + "/im/create-group" + '?'+endUrl;
                var header = { "Content-Type": 'application/x-www-form-urlencoded;utf-8' };
                cordova.plugin.http.post(URL,
                    option, header, function (response) {    
                        try {
                            var data = JSON.parse(response.data);
                            console.log('chat data ');
                            console.log(data);
                            $timeout(function () {
                                 defer.resolve(data);
                            });

                        } catch (error) {

                        }
                    }, function (error) {
                        console.log(error);
                        var status = JSON.parse(error.status);
                        if (status) {
                            tool.interceptors(status);
                        }
                        $timeout(function () {
                             defer.reject(error);
                        });
                    });

                return defer.promise;
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/im/create-group",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam(options)
                });
            }  
        };

        chat.uploadImageAndSend = function (selSess, img, onProgressCallBack) {
            var selType = selSess.type();
            var businessType; //业务类型，1-发群图片，2-向好友发图片
            if (selType == webim.SESSION_TYPE.C2C) { //向好友发图片
                businessType = webim.UPLOAD_PIC_BUSSINESS_TYPE.C2C_MSG;
            } else if (selType == webim.SESSION_TYPE.GROUP) { //发群图片
                businessType = webim.UPLOAD_PIC_BUSSINESS_TYPE.GROUP_MSG;
            }
            var opt = {
                'file': img, //图片对象
                'onProgressCallBack': onProgressCallBack, //上传图片进度条回调函数
                //'abortButton': document.getElementById('upd_abort'), //停止上传图片按钮
                'To_Account': selSess.id(), //接收者
                'businessType': businessType //业务类型
            };
            var msg = new webim.Msg(selSess, true, -1, -1, -1, chat.loginInfo.identifier, 0, chat.loginInfo.identifierNick);
            var rsp = {
                msg: msg,
                type: 'image'
            };
            $rootScope.$broadcast(BroadCast.IM_MSG_SENDING, rsp);
            //上传图片
            webim.uploadPic(opt,
                function (images) {
                    var images_obj = new webim.Msg.Elem.Images(images.File_UUID);
                    for (var i in images.URL_INFO) {
                        var img = images.URL_INFO[i];
                        var newImg;
                        var type;
                        switch (img.PIC_TYPE) {
                            case 1: //原图
                                type = 1; //原图
                                break;
                            case 2: //小图（缩略图）
                                type = 3; //小图
                                break;
                            case 4: //大图
                                type = 2; //大图
                                break;
                        }
                        newImg = new webim.Msg.Elem.Images.Image(type, img.PIC_Size, img.PIC_Width, img.PIC_Height, img.DownUrl);
                        images_obj.addImage(newImg);
                    }
                    msg.addImage(images_obj);
                    rsp = {
                        msg: msg,
                        type: 'image'
                    };
                    webim.sendMsg(msg, function (resp) {
                        if (selType == webim.SESSION_TYPE.C2C) { //私聊时，在聊天窗口手动添加一条发的消息，群聊时，长轮询接口会返回自己发的消息
                            $rootScope.$broadcast(BroadCast.IM_MSG_SENT, rsp);
                        }
                    }, function (err) {
                        console.error(err.ErrorInfo);
                        $rootScope.$broadcast(BroadCast.IM_MSG_SEND_FAIL, rsp);
                    });
                },
                function (err) {
                    console.error(err.ErrorInfo);
                    $rootScope.$broadcast(BroadCast.IM_IMAGE_SEND_FAIL, rsp);
                }
            );
        };

        chat.sendMessage = function (session, msgtosend) {
            var msgLen = webim.Tool.getStrBytes(msgtosend);
            var selType = session.type();
            var maxLen, errInfo;
            if (selType == webim.SESSION_TYPE.C2C) {
                maxLen = webim.MSG_MAX_LENGTH.C2C;
                errInfo = "消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)";
            } else {
                maxLen = webim.MSG_MAX_LENGTH.GROUP;
                errInfo = "消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)";
            }
            if (msgLen > maxLen) {
                alert(errInfo);
                return;
            }
            if (!session) {
                alert("消息发送失败");
            }
            var isSend = true; //是否为自己发送
            var seq = -1; //消息序列，-1表示sdk自动生成，用于去重
            var random = Math.round(Math.random() * 4294967296); //消息随机数，用于去重
            var msgTime = Math.round(new Date().getTime() / 1000); //消息时间戳
            var subType; //消息子类型
            if (selType == webim.SESSION_TYPE.C2C) {
                subType = webim.C2C_MSG_SUB_TYPE.COMMON;
                if (window.cordova) MobclickAgent.onEvent('app_send_c2c_msg');
            } else {
                //webim.GROUP_MSG_SUB_TYPE.COMMON-普通消息,
                //webim.GROUP_MSG_SUB_TYPE.LOVEMSG-点赞消息，优先级最低
                //webim.GROUP_MSG_SUB_TYPE.TIP-提示消息(不支持发送，用于区分群消息子类型)，
                //webim.GROUP_MSG_SUB_TYPE.REDPACKET-红包消息，优先级最高
                subType = webim.GROUP_MSG_SUB_TYPE.COMMON;
                if (window.cordova) MobclickAgent.onEvent('app_send_group_msg');
            }
            var msg = new webim.Msg(session, isSend, seq, random, msgTime, chat.loginInfo.identifier, subType, chat.loginInfo.identifierNick);

            var text_obj, face_obj, tmsg, emotionIndex, emotion, restMsgIndex;
            //解析文本和表情
            var expr = /\[[^[\]]{1,3}\]/mg;
            var emotions = msgtosend.match(expr);
            if (!emotions || emotions.length < 1) {
                text_obj = new webim.Msg.Elem.Text(msgtosend);
                msg.addText(text_obj);
            } else {

                for (var i = 0; i < emotions.length; i++) {
                    tmsg = msgtosend.substring(0, msgtosend.indexOf(emotions[i]));
                    if (tmsg) {
                        text_obj = new webim.Msg.Elem.Text(tmsg);
                        msg.addText(text_obj);
                    }
                    emotionIndex = webim.EmotionDataIndexs[emotions[i]];
                    emotion = webim.Emotions[emotionIndex];

                    if (emotion) {
                        face_obj = new webim.Msg.Elem.Face(emotionIndex, emotions[i]);
                        msg.addFace(face_obj);
                    } else {
                        text_obj = new webim.Msg.Elem.Text(emotions[i]);
                        msg.addText(text_obj);
                    }
                    restMsgIndex = msgtosend.indexOf(emotions[i]) + emotions[i].length;
                    msgtosend = msgtosend.substring(restMsgIndex);
                }
                if (msgtosend) {
                    text_obj = new webim.Msg.Elem.Text(msgtosend);
                    msg.addText(text_obj);
                }
            }
            var rsp = {
                msg: msg,
                type: 'text'
            };
            $rootScope.$broadcast(BroadCast.IM_MSG_SENDING, rsp);
            webim.sendMsg(msg, function (resp) {
                $rootScope.$broadcast(BroadCast.IM_MSG_SENT, rsp);
            }, function (err) {
                console.error(err.ErrorInfo);
                $rootScope.$broadcast(BroadCast.IM_MSG_SEND_FAIL, rsp);
            });
        };

        chat.logout = function () {
            if (chat.loginInfo) {
                webim.logout(
                    function (resp) {
                        chat.loginInfo = null;
                        $window.location.reload();
                    }
                );
            }
        };

        chat.getUserDetail = function (id) {
            if(iOSDevice()){
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('json');
                var cookie = UserPreference.get('IOSCookies');
                cordova.plugin.http.setCookie(Constant.ServerUrl +"/getinfo", cookie);
                var option =  getStringJson({ id: id }) ;
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/getinfo",
                    option, header, function (response) {
                       
                        try { 
                            var data = JSON.parse(response.data);
                            $timeout(function () {
                                 defer.resolve(data);
                            });

                        } catch (error) {

                        }
                    }, function (error) {
                        var status = JSON.parse(error.status);
                        if (status) {
                            tool.interceptors(status);
                        }
                        $timeout(function () {
                             defer.reject(error);
                        });
                    });

                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/getinfo", {
                    params: {
                        id: id
                    }
                })
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        return chat;
    })
    .factory('SavePhotoTool', function ($ionicLoading, $injector) {
        var tool = {};
        tool.savePhoto = function (url) {
            var config = {
                allowSave: true,
                album: 'Xgenban'
            };
            tool.config = angular.extend({}, config, {
                allowSave: true
            });
            if (window.cordova) {
                cordova.plugins.photoLibrary.getAlbums(
                    function (result) {
                        tool.saveImgUrl(url);
                    },
                    function (err) {
                        if (err.startsWith('Permission')) {
                            cordova.plugins.photoLibrary.requestAuthorization(
                                function () {
                                    tool.saveImgUrl(url);
                                },
                                function (err) {
                                    // User denied the access
                                    console.log(err);
                                }, {
                                    read: true,
                                    write: true
                                }
                            );
                        }
                    }
                );

            }
        };

        tool.saveImgUrl = function (url) {
            if (window.cordova) {
                cordova.plugins.photoLibrary.saveImage(url + '?ext=.jpg', tool.config.album, function (libraryItem) {
                    $ionicLoading.show({
                        template: '保存成功',
                        duration: 1500
                    });
                }, function (err) {
                    console.log(err);
                });
            }

        };
        var doLogin = function () {
            var state = $injector.get('$state');
            var user = $injector.get('UserPreference');
            var auth = $injector.get('AuthorizeService');
            if (!auth.logining && state.current.url !== '/login') {
                var loginModel = {
                    remember: true,
                    username: user.get('username', ''),
                    password: user.get('password', '')
                };
                if (loginModel.username !== '' && loginModel.password !== '') {
                    console.error('timeout, try auto login.');
                    auth.login(loginModel).then(function (res) {
                        if (res && res.result)
                            state.go('tabsController.mainPage');
                    });
                } else {
                    console.error('username or password not found');
                    state.go('login');
                }
            }
        };

        tool.interceptors = function (status) {

            var toaster = $injector.get('toaster');
            var MESSAGES = $injector.get('MESSAGES');
            var rootScope = $injector.get('$rootScope');
            var BroadCast = $injector.get('BroadCast');
           // var ionic
            $ionicLoading.hide();
            // console.log(' response ---:' + rejection.status);
            rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
            switch (status) {
                case -1:
                   
                case -4:
                    toaster.warning({
                        title: MESSAGES.REQUEST_ERROR,
                        body: MESSAGES.CONNECT_TIMEOUT_MSG
                    });
                    break;

                case 408:
                    toaster.warning({
                        title: MESSAGES.REQUEST_ERROR,
                        body: MESSAGES.CONNECT_TIMEOUT_MSG
                    });
                    break;
                case 401:
                    doLogin();
                    break;
                case 404:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,
                        body: MESSAGES.CONNECT_ERROR,//rejection.data.message
                    });
                    break;
                case 500:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,
                        body: MESSAGES.SERVER_ERROR
                    });
                    break;
                case 503:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,//rejection.data.message,
                        body: ''
                    });
                    break;
                default:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,
                        body: MESSAGES.CONNECT_ERROR_MSG
                    });
                    break;
            }

        };
        tool.defaultLogin = function(code){
            var user = $injector.get('UserPreference');
            var auth = $injector.get('AuthorizeService');
            var loginModel = {
                username: user.get('username', '')?user.get('username', ''):user.get('account', ''),
                password: user.get('password', '')
            };
            if((code==-1||code==-100)&&loginModel.password&&loginModel.username){ 
                auth.logining = false; 
                auth.login(loginModel,function(){ },function(){});
            }
        };

        return tool;
    })
    .factory('DownloadFile', function ($ionicLoading, $timeout, $cordovaFileTransfer, $cordovaFileOpener2, toaster, MESSAGES) {
        var file = {};
        file.readFile = function (url) {
            $ionicLoading.show({
                template: "请稍候，正在读取：0%"
            });


            var fileType = url.slice(url.lastIndexOf('.') + 1);

            var mimeType = '';
            switch (fileType.toLowerCase()) {
                case 'txt':
                    mimeType = 'text/plain';
                    break;
                case 'docx':
                    mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                    break;
                case 'doc':
                    mimeType = 'application/msword';
                    break;
                case 'pptx':
                    mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
                    break;
                case 'ppt':
                    mimeType = 'application/vnd.ms-powerpoint';
                    break;
                case 'xlsx':
                    mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    break;
                case 'xls':
                    mimeType = 'application/vnd.ms-excel';
                    break;
                case 'zip':
                    mimeType = 'application/x-zip-compressed';
                    break;
                case 'rar':
                    mimeType = 'application/octet-stream';
                    break;
                case 'pdf':
                    mimeType = 'application/pdf';
                    break;
                case 'jpg':
                    mimeType = 'image/jpeg';
                    break;
                case 'png':
                    mimeType = 'image/png';
                    break;
                default:
                    mimeType = 'application/' + fileType;
                    break;
            }

            var targetPath;
            var isIos = ionic.Platform.isIOS();
            if (window.cordova) {
                if (isIos) {
                    targetPath = cordova.file.tempDirectory + fileType;
                } else {
                    targetPath = cordova.file.dataDirectory + fileType;
                }
            }

            //console.log('targetpath:'+targetPath);
            var trustHosts = true;
            var options = {};
            $cordovaFileTransfer.download(url, targetPath, options, trustHosts).then(function (result) {
                //  alert('succeed');
                // 打开下载下来的文件
                if (window.cordova) {
                    // if(!isIos){
                    //     $cordovaFileOpener2.appIsInstalled('com.adobe.reader').then(function(res) {
                    //         if (res.status === 0) {
                    //           alert('未检测到相应的软件能打开此文档');
                    //         } else {
                    //             // Adobe Reader is installed.
                    //         }
                    //     });
                    // }
                    $cordovaFileOpener2.open(targetPath, mimeType).then(function () {
                        console.log('open succeed');
                        // alert('open succeed');
                        // 成功
                    }, function (err) {
                        console.log('open file failure');
                        // 错误
                        // alert('open error');
                        console.log(err);
                    });
                }

                $ionicLoading.hide();
            }, function (err) {
                toaster.error({
                    title: MESSAGES.DOWNLOAD_ERROR,
                    body: err.exception
                });
                $ionicLoading.hide();
            }, function (progress) {
                $timeout(function () {
                    var downloadProgress = (progress.loaded / progress.total) * 100;
                    $ionicLoading.show({
                        template: "请稍候，正在读取：" + Math.floor(downloadProgress) + "%"
                    });
                    if (downloadProgress > 99) {
                        $ionicLoading.hide();
                    }
                });
            });
        };
        return file;
    });
    
