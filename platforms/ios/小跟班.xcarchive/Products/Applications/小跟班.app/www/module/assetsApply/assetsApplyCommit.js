angular.module('app.controllers')
    .controller('assetsApplyCommitCtrl', function ($scope, $ionicModal, Constant, $state, toaster, $ionicLoading, $ionicScrollDelegate, $window, Requester, $ionicHistory) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.isIos = ionic.Platform.isIOS() && !isWeixin();
        });
        //即将进入
        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.navTitles = ['全部'];
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            console.log('load');
            $scope.select = {};
            $scope.recordAssetsCategorys = []; //当前页面数组;
            $scope.allFloorCategorys = []; //存放所有层级的数组
            $scope.getAssetsCategoryRequest();
        
            // $scope.assetsNames = ['中电一体机', '电子班牌', '投影仪'];

        });

        $scope.commit = function () {
            if (!$scope.select.category) {
                toaster.warning({
                    title: "",
                    body: "您还没有选择资产类别",
                    timeout: 2500
                });
                return;
            }

            if (!$scope.select.name) {
                toaster.warning({
                    title: "",
                    body: "您还没有选择资产名称",
                    timeout: 2500
                });
                return;
            }

            //提交资产申请
            Requester.commitAssetsApply($scope.select).then(function (res) {
               if(res.result){
                toaster.success({
                    title: "成功申请",
                    body: "成功申请，请耐心等待管理员处理",
                    timeout: 3000
                });
               
              $ionicHistory.goBack();
               }
            });
        };

        /**
         * modal 资产类别及资产名称
         */
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'assetsApply/assetsCategory.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {

            $scope.modal = modal;


        });

        $ionicModal.fromTemplateUrl(UIPath + 'assetsApply/assetsCategoryName.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.categoryNameModal = modal;
        });



        $scope.openModal = function (type) {

            if (type == 2) {
                if (!$scope.select.category) {
                    toaster.warning({
                        title: "",
                        body: "请先选择资产类别",
                        timeout: 2500
                    });
                    return;
                }

                $scope.getAssetsName($scope.select.categoryId);
                $scope.categoryNameModal.show();

            } else {
                $scope.recordAssetsCategorys = $scope.assetsCategorys;
                $scope.navTitles = ['全部'];
                $scope.navIndex = 0; //$scope.navTitles.length-1;
                $ionicScrollDelegate.$getByHandle('navBarScroll').scrollTo(0, 0);
                $ionicScrollDelegate.$getByHandle('navBarScroll').resize();
                $scope.modal.show();
            }
            $scope.recordType = type;


        };

        $scope.closeModal = function () {
            $scope.modal.hide();

        };
        $scope.closeCategoryNameModal = function () {
            $scope.categoryNameModal.hide();
        };


        //选择资产类别
        $scope.chooseCategory = function (category, index) {

            $scope.select = {};
            $scope.select.category = category.name;
            $scope.select.categoryId = category.id;
            $scope.modal.hide();
        };

        //选择资产名称
        $scope.chooseAssetsName = function (name, index) {

            $scope.select.name = name.assetInfoName;
            $scope.select.nameId = name.id;
            $scope.recordNameIndex = index + 1;
            $scope.categoryNameModal.hide();
        };

        //类别子分类选择 下一级
        $scope.nextFlooor = function (category) {

            //$scope.recordAssetsCategorys = category.child;

            $scope.navTitles.push(category.name);
            $scope.navIndex = $scope.navTitles.length - 1;
            $scope.allFloorCategorys.push(category.child);
            $scope.recordAssetsCategorys = $scope.allFloorCategorys[$scope.allFloorCategorys.length - 1];

            //计算下一个 btn长度 
            var nextBtnWidth = textWidth(category.name, 16) + 30;
            $scope.offSetWidthScroll(nextBtnWidth);



        };

        //点击导航按钮 返回上一级
        $scope.clickNavBtn = function (navIndex) {
            //将导航栏标题从数组中 移除
            var length = $scope.navTitles.length;
            var length1 = $scope.allFloorCategorys.length;


            $scope.navTitles.splice(navIndex + 1, length - navIndex - 1);
            $scope.navIndex = $scope.navTitles.length - 1;
            $scope.allFloorCategorys.splice(navIndex + 1, length1 - navIndex - 1);
            $scope.recordAssetsCategorys = $scope.allFloorCategorys[$scope.allFloorCategorys.length - 1];

            $scope.offSetWidthScroll(0);


        };

        //偏移量计算
        $scope.offSetWidthScroll = function (nextBtnWidth) {
            // //获取屏幕宽度
            var screenWidth = $window.innerWidth;

            //计算当前的长btn的长度
            var currentBtnWidth = 0;
            for (var i = 0; i < $scope.navTitles.length; i++) {
                currentBtnWidth = currentBtnWidth + textWidth($scope.navTitles[i], 16) + (i + 1) * 24 - 18;
            }

            if (currentBtnWidth >= screenWidth) {
                var offWidth = nextBtnWidth + currentBtnWidth - screenWidth;
                $ionicScrollDelegate.$getByHandle('navBarScroll').scrollTo(offWidth, 0);
                $ionicScrollDelegate.$getByHandle('navBarScroll').resize();
            } else {
                $ionicScrollDelegate.$getByHandle('navBarScroll').scrollTo(0, 0);
                $ionicScrollDelegate.$getByHandle('navBarScroll').resize();
            }
        };

        //获取资产类别（request）
        $scope.getAssetsCategoryRequest = function () {
            Requester.assetsCategory().then(function (res) {
                if (res.result) {
                    $scope.assetsCategorys = res.data;
                    $scope.recordAssetsCategorys = $scope.assetsCategorys; //当前选中的数组为第一层级
                    $scope.allFloorCategorys.push($scope.assetsCategorys);
                }
            });
        };
        //根据资产类别获取资产名称 （request）
        $scope.getAssetsName = function (categoryId) {
            Requester.queryAssetsName(categoryId).then(function (res) {
                $scope.assetsNames = res.data;
            });
        };


    });