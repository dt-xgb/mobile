angular.module('app.controllers')
    .controller('chargeDetailCtrl', function ($scope, Constant, UserPreference, $stateParams, $rootScope, $ionicPopup, Requester, toaster, $ionicLoading) {
        $scope.$on("$ionicView.beforeEnter", function (event, data) {

        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.classes = UserPreference.getArray('class');
            $scope.funcType = $stateParams.type ? $stateParams.type : $rootScope.rootWxFuncType;
            $scope.classId = $stateParams.classId ? $stateParams.classId : $rootScope.rootWxFeeClassId;
            $scope.feeId = $stateParams.feeId ? $stateParams.feeId : $rootScope.rootWxFeeId;
            $scope.studentId = $stateParams.studentId ? $stateParams.studentId : $rootScope.rootWxFeeStudentId;
            $scope.title = $stateParams.title ? $stateParams.title : $rootScope.rootWxFeeTitle;

            if (isWeixin()) {
                $rootScope.rootWxFuncType = $scope.funcType;
                $rootScope.rootWxFeeClassId = $scope.classId;
                $rootScope.rootWxFeeId = $scope.feeId;
                $rootScope.rootWxFeeStudentId = $scope.studentId;
                $rootScope.rootWxFeeTitle = $scope.title;
            }

            $scope.isParent = UserPreference.getObject('user').rolename === 4;
            if ($scope.isParent) $scope.studentId = UserPreference.get('DefaultChildID');
            if ($scope.funcType === 'cla') {
                $scope.getClassFeeStudentDetail();
            } else {
                $scope.getOneStudentFeeDetail();
            }
            console.log('functype:' + $scope.funcType);


        });

        //减免说明
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $scope.deductIntroduction = function (item) {
            var deductaccount = item.deductaccount; //$scope.funcType==='cla'?item.feePorject.deductaccount:item.deductaccount;
            if ((deductaccount === 0 || !deductaccount)) return;
             // $scope.deRemark = item.remark;
             if(item.remark) $scope.deRemarks = item.remark.split(",");
            $scope.deductPopup = $ionicPopup.show({
                //templateUrl: UIPath + '/others/chargeManage/introductionAlert.html',
                template:'<div style="color:#222;font-size:15px;margin-top: -8px;"><div style="text-align:left;width:calc(100% - 20px);margin:7px 10px 0px 10px" ng-repeat="deRemark in deRemarks" >{{deRemark}} </div> </div><div style="width:100%;height:36px;line-height:36px;font-size:17px;text-align:center;margin-top:20px;padding-top:4px;border-top:1px solid #ddd;font-weight:500;color:#2bcab2;"ng-click="closeCustomAlert(\'d\')">知道了</div>',
                title: '<p >减免说明</p>',
                cssClass: 'approval_alert', //'customAlert', // String, The custom CSS class name
                scope: $scope
            });
        };

        //收费说明
        $scope.chargeIntroduction = function (item) {
            var account = item.feePorject.account;
            $scope.feeRemark = item.feePorject.remark;
            if (account === 0 || !account) return;
            $scope.chargePopup = $ionicPopup.show({
                // templateUrl: UIPath + '/others/chargeManage/introductionAlert.html',
                template: '<div style="color:#222;font-size:15px;"><div style="text-align:center;width:100%;">{{feeRemark ?feeRemark :"暂无说明"}}</div> </div><div style="width:100%;height:36px;line-height:36px;font-size:17px;text-align:center;margin-top:20px;padding-top:4px;border-top:1px solid #ddd;font-weight:500;color:#2bcab2;" ng-click="closeCustomAlert(\'c\')">知道了</div>',
                title: '<p >缴费说明</p>',
                cssClass: 'approval_alert', //'customAlert', // String, The custom CSS class name
                scope: $scope
            });

        };

        $scope.closeCustomAlert = function (type) {
            if(type==='d'){
                $scope.deductPopup.close();
            }else {
                $scope.chargePopup.close();
            }
          
           
        };
        //获取标题
        $scope.getTitle = function () {
            var title = '';
            if ($scope.isParent && $scope.funcType === 'stu') {
                title = '缴费明细';
            } else {
                title = $scope.title;
            }
            return title;

        };

        //request --某收费项班级收费详情
        $scope.getClassFeeStudentDetail = function () {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            Requester.getClassFeeStudentDetail($scope.feeId, $scope.classId).then(function (resp) {
                if (resp.result) {
                    $scope.studentFeeList = resp.data;
                    if ($scope.studentFeeList && $scope.studentFeeList.length > 0)
                    $scope.classFeeRemark =$scope.studentFeeList[0].feePorject.remark;
                    $ionicLoading.hide();
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                }, 5000);
            });
        };

        //学生收费详情
        $scope.getOneStudentFeeDetail = function () {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            Requester.getOneStudentFeeDetail($scope.studentId).then(function (resp) {
                if (resp.result) {
                    $scope.myFeeDetailList = resp.data;
                    $ionicLoading.hide();
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                }, 5000);
            });
        };
    });