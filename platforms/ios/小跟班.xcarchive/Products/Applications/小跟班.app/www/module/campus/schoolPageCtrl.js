 /**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('schoolPageCtrl', function ($q, $scope, $ionicModal, CameraHelper, BroadCast, UserPreference, SchoolService, MESSAGES, toaster, $state, Constant, $ionicLoading, $ionicPopup, $ionicListDelegate, ionicImageView, closePopupService, Requester, $rootScope, $ionicSlideBoxDelegate, $timeout) {

        //收到通知 保存图片
        $rootScope.$on('saveImgUrl', function (event, url) {
            var config = {
                allowSave: true,
                album: 'Xgenban'
            };
            $scope.config = angular.extend({}, config, {
                allowSave: true
            });
            if (window.cordova) {
                cordova.plugins.photoLibrary.getAlbums(
                    function (result) {
                        save(url);
                    },
                    function (err) {
                        if (err.startsWith('Permission')) {
                            cordova.plugins.photoLibrary.requestAuthorization(
                                function () {
                                    save(url);
                                },
                                function (err) {
                                    // User denied the access
                                    console.log(err);
                                }, {
                                    read: true,
                                    write: true
                                }
                            );
                        }
                    }
                );

            }
        });

        function save(url) {
            if (window.cordova) {
                cordova.plugins.photoLibrary.saveImage(url + '?ext=.jpg', $scope.config.album, function (libraryItem) {
                    $ionicLoading.show({
                        template: '保存成功',
                        duration: 1500
                    });
                }, function (err) {
                    console.log(err);
                });
            }

        }

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.schoolName = UserPreference.get('DefaultSchoolName');
            if ($scope.currentSID && $scope.currentSID != UserPreference.get('DefaultChildID')) {
                $scope.currentSID = UserPreference.get('DefaultChildID');
                $scope.loadData(true);
                SchoolService.getBannerList();
            }
        });
        $scope.$on('$ionicView.loaded', function (event, username) {
           // console.log('loaded');
        });
        $scope.$on("$ionicView.enter", function (event, data) {
           // console.log('enter');


            if (!$scope.newsTypeSelectable || $scope.newsTypeSelectable.length === 0) {
                SchoolService.getNewsType();
            }

            $ionicSlideBoxDelegate.$getByHandle('delegateHandler').update();
            $ionicSlideBoxDelegate.$getByHandle('delegateHandler').start();
            $ionicSlideBoxDelegate.loop(true);
        });

        if (window.cordova) MobclickAgent.onEvent('app_school_page');
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'campus/newPost.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            if ($scope.isTeacher() && (!$scope.classes || $scope.classes.length == 0)) {
                toaster.warning({
                    title: "温馨提示",
                    body: "您还未关联班级，请先设置班级"
                });
                return;
            }
            if ($scope.isTeacher() && $scope.classes.length === 1) {
                $scope.selected.class_id = $scope.classes[0].key;
            }
            $scope.newsTypeSelectable = UserPreference.getArray('news_type');
            if ($scope.newsTypeSelectable)
                SchoolService.getNewsType();
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.isTeacher = function () {
            return String($scope.userRole) === Constant.USER_ROLES.TEACHER;
        };

        $scope.canBeFocus = function (item) {
            return (item.focus || item.role === 'superadmin' || item.role === 'area_admin' || item.role === 'province_admin' || item.role === 'city_admin');

        };

        $scope.canCancel = function (item) {
            if (item.authorId == $scope.userId)
                return false;
            return $scope.isTeacher() && (item.state == Constant.NEWS_STATUS.TEACHER_REVIEW_PASS.key || item.state == Constant.NEWS_STATUS.ADMIN_REVIEW.key || item.state == Constant.NEWS_STATUS.TEACHER_IGNORE.key);
        };

        $scope.canDelete = function (item) {
            if (item.authorId == $scope.userId) {
                // if ($scope.isTeacher() && item.state != Constant.NEWS_STATUS.ADMIN_REVIEW_PASS.key)
                return true;
                // else if (!$scope.isTeacher() && item.state == Constant.NEWS_STATUS.TEACHER_REVIEW.key)
                //     return true;
            }
            return false;
        };

        $scope.openTypeSelectModal = function () {
            $scope.typeChooser = $ionicPopup.show({
                template: '<div class="item" ng-repeat="item in newsTypeQueryable" ng-click="onTypeSelected(item)" style="padding: 10px 20px;border-radius: 0">{{item.name}}</div>',
                title: '风采类型',
                scope: $scope
            });
            closePopupService.register($scope.typeChooser);
        };

        $scope.onTypeSelected = function (item) {
            $scope.queryParam.key = item.key;
            $scope.queryParam.parentKey = item.parentKey;
            $scope.selectedTypeame = item.name;
            $scope.page = 1;
            SchoolService.getNewsList($scope.page, $scope.queryParam, $scope.reqId);
            if ($scope.typeChooser)
                $scope.typeChooser.close();
        };


        function getFilterList() {
            $scope.newsTypeSelectable = UserPreference.getArray('news_type');
           // console.log($scope.newsTypeSelectable);
            $scope.newsTypeQueryable = angular.copy($scope.newsTypeSelectable);
            $scope.newsTypeQueryable.unshift({
                key: '',
                parentKey: '2',
                name: '校园风采'
            });
            $scope.newsTypeQueryable.unshift({
                key: undefined,
                parentKey: undefined,
                name: '全部类别'
            });
        }

        $scope.changeFilter = function (arg) {
            if ($scope.activeTab === arg) {
                return;
            }
            $scope.activeTab = arg;
            $scope.queryParam = {};
            if (arg === 'mine') {
                $scope.queryParam.authorId = $scope.userId;
                $scope.disableTypeSelect = true;
            } else if (arg === 'pending') {
                $scope.queryParam.status = Constant.NEWS_STATUS.TEACHER_REVIEW.key;
                $scope.disableTypeSelect = true;
            } else {
                $scope.disableTypeSelect = false;
            }
            $scope.reqId++;
            $scope.onTypeSelected({
                key: undefined,
                parentKey: undefined,
                name: '全部类别'
            });
        };

        $scope.attachments = [];
        $scope.selected = {};
        $scope.queryParam = {};
        $scope.activeTab = 'all';
        $scope.selectedTypeame = '全部类别';
        $scope.currentSID = UserPreference.get('DefaultChildID');
        $scope.userRole = UserPreference.getObject('user').rolename;
        $scope.userRole = String($scope.userRole);
        $scope.userId = UserPreference.getObject('user').id;
        $scope.reqId = 0;
        $scope.page = 1;
        $scope.newsList = UserPreference.getArray('news_list_cache');


        $scope.listHasMore = SchoolService.listHasMore;
        $scope.bannerList = UserPreference.getArray('news_banner_cache');
        $scope.classes = UserPreference.getArray('class');

        getFilterList();
        $scope.loadData = function (reset) {
            if (Constant.debugMode) console.log('load news restart ' + reset);
            if (reset) {
                $scope.page = 1;
                SchoolService.getBannerList();
            } else if ($scope.listHasMore)
                $scope.page++;
            else
                return;

            SchoolService.getNewsList($scope.page, $scope.queryParam);
            $scope.getCampusUnread();//查看是否已读
        };

        SchoolService.getNewsType();
        SchoolService.getBannerList();
        SchoolService.getNewsList(1);

        // console.log('news list :');
        // console.log($scope.newsList);

        $scope.getDisplayTime = function (time) {
            var now = new Date().Format("yyyy-MM-dd");
            if (now === time.substr(0, 10))
                return time.substr(11, 5);
            else
                return time.substr(5, 11);
        };

        $scope.getNewsType = function (key) {
            for (var i = 0; i < $scope.newsTypeQueryable.length; i++) {
                if (key === $scope.newsTypeQueryable[i].key)
                    return $scope.newsTypeQueryable[i].name;
            }
            return '';
        };

        //根据key值确定风采类型的背景色
        $scope.getBgColorByKey = function (key) {
            var bgColor = '';
            switch (key) {

                case 32:
                    bgColor = '#2bcab2'; //班级活动
                    break;

                case 36:
                    bgColor = '#556fb5'; //德育安全
                    break;

                case 35:
                    bgColor = '#80c269'; //电子板报
                    break;

                case 34:
                    bgColor = '#448aca'; //作品展示
                    break;

                case 33:
                    bgColor = '#ec6941'; //班级荣誉
                    break;

                case 31:
                    bgColor = '#f6b37f'; //师生风采
                    break;

                default:
                    bgColor = '#8f3adf'; //其它  校园风采 2
                    break;
            }
            return bgColor;
        };

        $scope.delHtmlTag = function (str) {
            return delHtmlTag(str);
        };

        $scope.deleteItem = function (item, $event) {
            $event.stopPropagation();
            var pre = '';
            if (item.focus)
                pre = '该风采已被设为校园焦点。';
            var confirmPopup = $ionicPopup.confirm({
                title: '删除确认',
                template: pre + '确认删除?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    SchoolService.deleteNews(item.id);
                    $ionicListDelegate.closeOptionButtons();
                }
            });
        };
        $scope.cancelItem = function (item, $event) {
            $event.stopPropagation();
            var pre = '';
            if (item.focus)
                pre = '该风采已被设为校园焦点。';
            var confirmPopup = $ionicPopup.confirm({
                title: '撤回确认',
                template: pre + '撤回后该条消息将被忽略，确认撤回?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    SchoolService.cancelPublish(item.id);
                    $ionicListDelegate.closeOptionButtons();
                }
            });
        };

        //点赞
        $scope.favorNews = function (item, $event) {
            $event.stopPropagation();
            if (item.isFavor === true) {
                Requester.unfavorCampus(item.id);
                if (item.praiseNum > 0)
                    item.praiseNum--;
            } else {
                Requester.favorCampus(item.id);
                item.praiseNum++;
            }
            item.isFavor = !item.isFavor;
        };

        $scope.$on(BroadCast.DIC_REV, function (a, rst) {
            if (rst === 'class')
                $scope.classes = UserPreference.getArray(rst);
        });

        $scope.$on(BroadCast.NEWS_LIST_REV, function (a, rst) {
            if (rst && rst.result) {
                if (!rst.reqId || (rst.reqId && rst.reqId === $scope.reqId)) {
                    $scope.newsList = SchoolService.list;
                    // console.log('call back list ');
                    // console.log($scope.newsList);
                    //    if($scope.userRole===Constant.USER_ROLES.TEACHER){
                    //        if(!$scope.classes||($scope.classes&&$scope.classes.length<=0)){

                    //        }
                    //    }


                    $scope.newsList.forEach(function (item) {
                        item.picUrls = [];
                        item.imageUrls.forEach(function (url) {
                            $scope.urlItem = {
                                thumb: '',
                                src: ''
                            };
                            $scope.urlItem.thumb = url;
                            $scope.urlItem.src = url;
                            item.picUrls.push($scope.urlItem);
                        });
                    });

                } else {
                    if (Constant.debugMode) console.log('req is deprecated');
                }
            }
            $scope.listHasMore = SchoolService.listHasMore;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });

        function onImageResized(resp) {
            var start = resp.indexOf('base64,');
            $scope.selected.picdatas.push(resp.substr(start + 7));
        }

        $scope.save1 = function () {
            if ($scope.isTeacher()) {
                $scope.selected.classIds = [];
                if ($scope.selected.class_id && $scope.selected.class_id !== "")
                    $scope.selected.classIds.push($scope.selected.class_id);
                else {
                    toaster.warning({
                        title: MESSAGES.OPERATE_ERROR,
                        body: MESSAGES.NO_TARGET_CLASS
                    });
                    return;
                }
            }
            if ($scope.isTeacher() && (!$scope.selected.key || $scope.selected.key === '')) {
                toaster.warning({
                    title: MESSAGES.OPERATE_ERROR,
                    body: MESSAGES.NO_NEWS_TYPE
                });
                return;
            }
            if ($scope.attachments.length === 0) {
                toaster.warning({
                    title: MESSAGES.OPERATE_ERROR,
                    body: MESSAGES.AT_LEAST_IMAGE
                });
                return;
            }
            $scope.selected.picdatas = [];
            var promiseArr = [];
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $scope.attachments.forEach(function (libraryItem) {
                if (libraryItem.data) {
                    onImageResized(libraryItem.data);
                } else {
                    promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, libraryItem).then(onImageResized));
                }
            });
            $q.all(promiseArr).then(function () {
                SchoolService.newNews($scope.selected);
            });
        };

        $scope.$on(BroadCast.NEW_NEWS, function (a, rst) {
            if (rst && rst.result) {
                if ($scope.isTeacher())
                    toaster.success({
                        title: MESSAGES.REMIND,
                        body: MESSAGES.NEW_NEWS_ADMIN
                    });
                else
                    toaster.success({
                        title: MESSAGES.REMIND,
                        body: MESSAGES.NEW_NEWS
                    });
                $scope.closeModal();
                $scope.selected = {};
                $scope.attachments = [];
                $scope.activeTab = 'all';
            } else
                toaster.error({
                    title: MESSAGES.REMIND,
                    body: rst.message
                });
            $ionicLoading.hide();
        });

        $scope.$on(BroadCast.BANNER_LIST_REV, function (a, rst) {
            if (rst && rst.result)
                $scope.bannerList = SchoolService.bannerList;
            $ionicSlideBoxDelegate.update(); //解决图片加载不出来的问题
            $ionicSlideBoxDelegate.loop(true); //解决滚动到最后一张再循环滚动（暂时方案）
        });

        $scope.$on(BroadCast.NEWS_TYPE_REV, function (a, rst) {
            if (rst && rst.result) {
                getFilterList();
            }
        });

        $scope.selectImg = function () {
            //if(!isWeixin())
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.attachments, resp);
                        $scope.$apply();
                    } else {
                        $scope.attachments.push({
                            data: "data:image/jpeg;base64," + resp
                        });
                    }
                }, 9 - $scope.attachments.length);
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }

        };

        // input形式打开系统系统相册
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.attachments.push({
                        data: $scope.testImg
                    });

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.attachments.splice(index, 1);
        };


        $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
            $ionicLoading.hide();
        });

        $scope.favor = function (item, $event) {
            $event.stopPropagation();
        };

        $scope.send2Admin = function (item, $event) {
            $event.stopPropagation();
            var confirmPopup = $ionicPopup.confirm({
                title: '推荐确认',
                template: '推荐成功后将在全校展示，确认推荐?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    SchoolService.send2Admin(item.id);
                }
            });
        };

        $scope.setIgnore = function (item, $event) {
            $event.stopPropagation();
            var confirmPopup = $ionicPopup.confirm({
                title: '忽略确认',
                template: '确认忽略此风采?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    SchoolService.ignorePublish(item.id);
                }
            });
        };

        $scope.setPublish = function (item, $event) {
            $event.stopPropagation();
            if ($scope.isTeacher()) {
                $scope.publishData = {
                    key: item.key,
                    id: item.id
                };
                $ionicPopup.show({
                    title: '发布确认',
                    scope: $scope,
                    template: '<p>请选择发布风采的类型：</p> <div style="padding: 1px 15px !important;"> <button class="button button-outline " ng-repeat="btn in newsTypeSelectable" ng-click="publishData.key=btn.key" ng-class="publishData.key==btn.key?\'button-positive\':\'button-stable\'" style="margin: 2px 3px;">{{btn.name}} </button> </div>',
                    buttons: [{
                            text: '取消'
                        },
                        {
                            text: '<b>确认</b>',
                            type: 'button-balanced',
                            onTap: function (e) {
                                if ($scope.publishData.key)
                                    SchoolService.allowPublish($scope.publishData);
                                else {
                                    e.preventDefault();
                                    toaster.warning({
                                        title: MESSAGES.NO_NEWS_TYPE,
                                        body: ''
                                    });
                                }
                            }
                        }
                    ]
                });
            }
        };

        $scope.goDetail = function (item) {
            $state.go('postDetail', {
                post: item,
                index: 'p' + item.id
            });
        };

        $scope.viewImages = function (urls, index, $event) {
            $event.stopPropagation();
            ionicImageView.showViewModal({
                allowSave: true
            }, urls, index);
        };

        $scope.preViewImages = function (index) {
            var origin = [];
            $scope.attachments.forEach(function (libraryItem) {
                if (libraryItem.data)
                    origin.push(libraryItem.data);
                else
                    origin.push(libraryItem);
            });
            ionicImageView.showViewModal({
                allowSave: false
            }, origin, index);
        };

        $scope.$on(BroadCast.NEWS_STATE_CHANGED, function (a, rst) {
            if (Constant.debugMode) console.log(rst);
            if (rst && rst.result) {
                var i = 0;
                for (; i < $scope.newsList.length; i++) {
                    if ($scope.newsList[i].id === rst.request.id) {
                        switch (rst.request.type) {
                            case BroadCast.IGNORE_RST_REV:
                                $scope.newsList.splice(i, 1);
                                break;
                            case BroadCast.ALLOW_PUBLISH_RST_REV:
                                $scope.newsList[i].isPublish = true;
                                $scope.newsList[i].key = $scope.publishData.key;
                                if ($scope.isTeacher())
                                    $scope.newsList[i].state = Constant.NEWS_STATUS.TEACHER_REVIEW_PASS.key;
                                else
                                    $scope.newsList[i].state = Constant.NEWS_STATUS.ADMIN_REVIEW_PASS.key;
                                break;
                            case BroadCast.SEND_TO_ADMIN_RST:
                                $scope.newsList[i].state = Constant.NEWS_STATUS.ADMIN_REVIEW.key;
                                break;
                            case BroadCast.DELETE_NEWS_REV:
                                $scope.newsList.splice(i, 1);
                                SchoolService.getBannerList();
                                break;
                            case BroadCast.CANCEL_PUBLISH_REV:
                                $scope.newsList.splice(i, 1);
                                SchoolService.getBannerList();
                                break;
                        }
                        break;
                    }
                }
                $scope.getCampusUnread();//拒绝或同意会刷新状态
            } else
                toaster.error({
                    title: MESSAGES.OPERATE_ERROR,
                    body: rst.message
                });
        });


        $scope.getCampusUnread = function () {
            Requester.getCampusUnread().then(function (resp) {
                if (resp.result) {
                    var count = resp.data.myCampusViewUnrendNum + resp.data.statePendingAuditNum;
                    $scope.unreadCount = resp.data.myCampusViewUnrendNum;
                    $scope.unreadList = resp.data.newsCampusViewCommentsVoList;
                    $scope.pendingPublishCount = resp.data.statePendingAuditNum;
                    if (count < 0 || !Number.isInteger(count)) {
                        count = 0;
                    }
                    $rootScope.$broadcast(BroadCast.BADGE_UPDATE, {
                        type: 'campus',
                        count: count
                    });
                }
            });
        };


        //判断是否为iphonex及以上
        $scope.isIphoneX = function() {
            var result = false;
            $scope.isIos = ionic.Platform.isIOS() && !isWeixin();
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = true;
                } else {
                    result = false;
                }
        
            } else {
             result = false;
            }
            return result;
        };
    });