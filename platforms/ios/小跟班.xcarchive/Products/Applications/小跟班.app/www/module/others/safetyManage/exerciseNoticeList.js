angular.module('app.controllers')
    .controller('exerciseNoticeListCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, BroadCast) {
        
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.page = 1;
            $scope.programList = []; //方案列表
            $scope.selected = {
                programName: '',
                exerciseTime: '',
                title: '',
                address: '',
                content: '',
                programId :0,
               
            };

          $scope.getDrillRecordList();
       
        });

        $scope.$on("$ionicView.loaded", function (event, data) {

        });

        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getDrillRecordList();
        };
        //上拉记载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.getMoreDataList();
        };

        //详情或记录
        $scope.goToNextPage = function (type,item) {
            if (type === 'detail') {
                $state.go('exerciseNoticeDetail',{itemId:item.id,title:item.title});
            } else {
                $state.go('exerciseRecord',{itemId:item.id,startTime:item.drillStartTime,endTime:item.drillEndTime});
            }
        };

        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.isMoreData = true;
           // $ionicLoading.hide();
        });

          //request --获取列表
          $scope.getDrillRecordList = function () {
            Requester.getDrillRecordList($scope.page).then(function (rest) {
                if (rest.result) {
                   
                    $scope.list = rest.data.content;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //request --加载更多
        $scope.getMoreDataList = function () {
            Requester.getDrillRecordList($scope.page).then(function (rest) {
                if (rest.result) {
                    var data = rest.data.content;
                    for (var i = 0; i < data.length; i++) {
                        $scope.list.push(data[i]);
                    }
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

       
    });