angular.module('app.controllers')
    .controller('classroomMonitListCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {

        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.studentId = UserPreference.get('DefaultChildID');
            $scope.list = [1, 2, 3];
            $scope.getChildrenClassroomLiveList();
        });

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'others/tourClass/classroomLive.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.classLiveModal = modal;
        });

        //open
        $scope.openClassroomLive = function (item) {
            $scope.falgDay = 0;
            if ($scope.falgDay === 1) {
                toaster.warning({
                    title: "",
                    body: '非上学日不能查看'
                });     
            } else {
                //var url = 'rtsp://admin:sct123456@192.168.20.114:554/h264/ch1/sub/av_stream';
                var url = 'rtsp://admin:sct123456@192.168.1.193:554/h264/ch1/sub/av_stream';
                if (window.cordova) {
                    var options = {
                        autoPlay: true,
                        hideControls: false
                    };
                    window.VideoPlayerVLC.play(url,  function (success) {
                        //alert('open success');
                        console.log('success');
                        console.log(success);
                    }, function (failure) {
                        console.log('failure');
                        console.log(failure);
                    });
                } else {
                    //alert('is not cordova');
                }
            }

            //$scope.classLiveModal.show();
        };
        // close
        $scope.hideClassroomLiveModal = function () {
            if (window.cordova) {
                libVLCPlayer.stop(function (success) {
                    alert('close sucess');
                }, function (failure) {
                    alert('close failure');
                });
            }
            // $scope.classLiveModal.hide();
        };

        //--request 巡课列表
        $scope.getChildrenClassroomLiveList = function () {
            Requester.getChildrenClassroomLiveList($scope.studentId).then(function (resp) {
                if (resp.result) {
                    $scope.list = resp.data.cameraManageList;
                    $scope.list = [{
                        siteName: '场所1',
                        terminalUrl: ''
                    }, {
                        siteName: '场所2',
                        terminalUrl: ''
                    }, {
                        siteName: '场所3',
                        terminalUrl: ''
                    }];
                    $scope.falgDay = resp.data.falgDay; //是否是上学日 0 上学日 1：节假日
                } else {
                    toaster.error({
                        title: resp.message,
                        body: ''
                    });
                }
            });
        };
    });