/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('safeSchoolCtrl', function ($scope, BroadCast, Constant, UserPreference, $state, Requester, $ionicModal, $ionicHistory, toaster, ionicTimePicker, $ionicLoading, ngIntroService) {

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.isIOS = ionic.Platform.isIOS();
            $scope.userRole = String(UserPreference.getObject("user").rolename);
            if ($scope.userRole === Constant.USER_ROLES.PARENT) {
                $scope.isParent = true;
                $scope.children = UserPreference.getObject("user").student;
                $scope.activeIndex = 0;
                $scope.needCardFee = UserPreference.getBoolean('NeedCardFee');
            } else {
                $scope.isParent = false;
                $scope.classes = UserPreference.getObject("user").classes;
                $scope.selected = {
                    id: $scope.classes[0].id
                };
                $scope.list = [];
                $scope.isHeadTeacher = UserPreference.getBoolean('isHeadTeacher');
                if (!UserPreference.getBoolean("ShowRemindSettingTip")) {
                    setTimeout(function () {
                        var IntroOptions = {
                            steps: [
                                {
                                    element: document.querySelector('#focusItem'),
                                    intro: "<strong>点击这里，可以设置每日考勤推送的时间</strong>"
                                }],
                            showStepNumbers: false,
                            showBullets: false,
                            showButtons: false,
                            exitOnOverlayClick: true,
                            exitOnEsc: true
                        };
                        ngIntroService.setOptions(IntroOptions);
                        ngIntroService.start();
                        UserPreference.set("ShowRemindSettingTip", true);
                    }, 300);
                }
            }
            $scope.loadData();
        });

        function fillTimes() {
            if (!$scope.arriveSchoolWarningTime) $scope.arriveSchoolWarningTime = '08:00';
            if (!$scope.leaveSchoolWarningTime) $scope.leaveSchoolWarningTime = '19:00';
            if (!$scope.arriveNoticeTime) $scope.arriveNoticeTime = '08:00';
            if (!$scope.leaveNoticeTime) $scope.leaveNoticeTime = '19:00';
        }

        $scope.goDetail = function (item) {
            var currentDate = formatTimeWithoutSecends(new Date().getTime() / 1000, "yyyy-MM-dd");
            if (item.pushDate == currentDate) {
                $state.go('attendanceToday');
            } else {
                $state.go('attendanceRecords', {day: item.pushDate});
            }
        };

        $scope.onSelectionChange = function () {
            Requester.getSafetyWarnings('CHAR_TEACHER_MOR_ARR_TIME,CHAR_TEACHER_AFT_LEA_TIME,FAMMOR_ARR_END_TIME,FAMAFT_LEA_ENT_TIME', $scope.selected.id)
                .then(function (resp) {
                    if (resp.result) {
                        for (var j = 0; j < resp.data.length; j++) {
                            if (resp.data[j]) {
                                if ('CHAR_TEACHER_MOR_ARR_TIME' === resp.data[j].parameterName) {
                                    $scope.arriveNoticeTime = resp.data[j].parameterValue;
                                } else if ('CHAR_TEACHER_AFT_LEA_TIME' === resp.data[j].parameterName) {
                                    $scope.leaveNoticeTime = resp.data[j].parameterValue;
                                } else if ('FAMMOR_ARR_END_TIME' === resp.data[j].parameterName) {
                                    $scope.arriveSchoolWarningTime = resp.data[j].parameterValue;
                                } else if ('FAMAFT_LEA_ENT_TIME' === resp.data[j].parameterName) {
                                    $scope.leaveSchoolWarningTime = resp.data[j].parameterValue;
                                }
                            }
                        }
                    }
                }).finally(function () {
                fillTimes();
            });
        };

        $scope.loadPushList = function (reset) {
            if (reset)
                $scope.page = 1;
            else
                $scope.page++;
            Requester.getSafeSchoolPushs(UserPreference.getObject("user").id, $scope.page).then(function (resp) {
                $scope.listHasMore = !resp.data.last;
                if (reset) {
                    $scope.list = resp.data.content;
                } else {
                    for (var i = 0; i < resp.data.content.length; i++)
                        $scope.list.push(resp.data.content[i]);
                }
            }).finally(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        $scope.loadData = function () {
            if ($scope.isParent) {
                $scope.listArray = [2];
                Requester.getChildArriveInfo()
                    .then(function (resp) {
                        $scope.list = resp.data;
                        $scope.listArray[0] = resp.data;
                        $scope.arrReport = [];
                        for (var i = 0; i < resp.data.length; i++) {
                            if (resp.data[i].isread === 0) {
                                $scope.arrReport.push({
                                    id: resp.data[i].id,
                                    arrTime: resp.data[i].arrTime
                                });
                            }
                        }
                        if ($scope.arrReport.length > 0)
                            Requester.setChildArriveInfoRead($scope.arrReport);
                    });
                Requester.getChildSaftyWarnings()
                    .then(function (resp) {
                        $scope.listArray[1] = resp.data;
                        $scope.arr2Report = [];
                        var count = 0;
                        for (var i = 0; i < resp.data.length; i++) {
                            if (resp.data[i].isread === 0) {
                                $scope.arr2Report.push({
                                    id: resp.data[i].id,
                                    timeStr: resp.data[i].timeStr
                                });
                                count++;
                            }
                        }
                        $scope.warningUnreadCount = count;
                    });
            } else {
                $scope.onSelectionChange();
                $scope.loadPushList(true);
            }
        };

        $scope.getMessage = function (msg, warning) {
            var text = '';
            var student;
            for (var i = 0; i < $scope.children.length; i++) {
                if (msg.studentId == $scope.children[i].id || msg.stuid == $scope.children[i].id) {
                    student = $scope.children[i];
                    break;
                }
            }
            if (!warning) {
                var who;
                if (String(msg.source) === '2')
                    who = student.classno.className + '班主任';
                else
                    who = '小跟班';
                text = who + '提醒您：' + student.student_name;
                if (msg.instatus === 0)
                    text += '已到学校';
                else
                    text += '已出学校';
            }
            else {
                text = student.student_name;
            }

            return text;
        };

        $scope.createOrder = function () {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            var childID = UserPreference.get('DefaultChildID');
            var childName = UserPreference.get('DefaultChildName');
            Requester.isUserPurchased(1, childID).then(function (resp) {
                if (resp.result) {
                    if (resp.data.isContains)
                        toaster.success({title: '您的小孩' + childName + '已交过押金', body: ''});
                    else {
                        $state.go('orderPayView', {goods: [{id: 1, count: 1, categoryId: 1}]});
                    }
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                }, 300);
            });
        };

        $scope.switchTab = function (index) {
            $scope.activeIndex = index;
            $scope.list = $scope.listArray[index];
            if (index === 1 && $scope.warningUnreadCount !== 0) {
                $scope.warningUnreadCount = 0;
                Requester.setChildSafetyWarningsRead($scope.arr2Report);
            }
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'safeSchool/safeSchoolSetting.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            ngIntroService.exit();
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.showTimePicker = function (arg) {
            var hm;
            switch (arg) {
                case 0:
                    hm = $scope.arriveNoticeTime.split(':');
                    break;
                case 1:
                    hm = $scope.leaveNoticeTime.split(':');
                    break;
                case 2:
                    hm = $scope.arriveSchoolWarningTime.split(':');
                    break;
                case 3:
                    hm = $scope.leaveSchoolWarningTime.split(':');
                    break;
            }
            var ipObj1 = {
                inputTime: (parseInt(hm[1]) >= 30 ? parseInt(hm[0]) - 1 : parseInt(hm[0])) * 60 * 60 + parseInt(hm[1]) * 60,
                callback: function (val) {
                    if (typeof (val) === 'undefined') {
                    } else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        switch (arg) {
                            case 0:
                                $scope.arriveNoticeTime = time;
                                $scope.commit('CHAR_TEACHER_MOR_ARR_TIME', time);
                                break;
                            case 1:
                                $scope.leaveNoticeTime = time;
                                $scope.commit('CHAR_TEACHER_AFT_LEA_TIME', time);
                                break;
                            case 2:
                                if ($scope.arriveNoticeTime && time < $scope.arriveNoticeTime) {
                                    toaster.warning({title: '不能早于班主任提醒时间', body: ''});
                                    return;
                                }
                                $scope.arriveSchoolWarningTime = time;
                                $scope.commit('FAMMOR_ARR_END_TIME', time);
                                break;
                            case 3:
                                if ($scope.leaveNoticeTime && time < $scope.leaveNoticeTime) {
                                    toaster.warning({title: '不能早于班主任提醒时间', body: ''});
                                    return;
                                }
                                $scope.leaveSchoolWarningTime = time;
                                $scope.commit('FAMAFT_LEA_ENT_TIME', time);
                                break;
                        }
                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

        $scope.commit = function (key, value) {
            Requester.setSafetySettings($scope.selected.id, key, value).then(function (resp) {
                if (resp.result) {
                    toaster.success({title: '设置已保存', body: ''});
                }
                else
                    toaster.error({title: resp.message, body: ''});
            });
        };

        $scope.goBack = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('tabsController.mainPage');
        };

    });