// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app.controllers', []);
angular.module('app', ['ionic', 'ngCordova', 'app.constants', 'app.controllers', 'app.routes', 'app.services', 'app.directives', 'app.requester','templates',
    'jrCrop', 'monospaced.elastic', 'angular-intro', 'ionicImgCache', 'toaster', 'ionic-timepicker', 'ionic-datepicker', 'angular-timeline', 'pdfjsViewer', 'ionic-image-view','ion-gallery','ionicLazyLoad'])
    .config(function(ionGalleryConfigProvider) {
        ionGalleryConfigProvider.setGalleryConfig({
                                action_label: 'Done',
                                toggle: false,
                                row_size: 3,
                                fixed_row_size: true
        });
      })
    .run(function ($ionicPlatform, PushService, $ionicPopup, $state, $ionicHistory, SettingService, $rootScope,$injector) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
                cordova.plugins.Keyboard.disableScroll(true);
                //if(ionic.Platform.isIOS())cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                window.addEventListener('native.keyboardshow', function (keyboardParameters) {
                    document.body.classList.add('keyboard-open');
                    console.log(keyboardParameters);
                    $rootScope.keyboardHeight = keyboardParameters.keyboardHeight;
                });
            }
            if(window.cordova){
                window.MobileAccessibility.usePreferredTextZoom(false);
                if(iOSDevice()){
                    cordova.plugin.htp.setSSLCertMode('pinned');
                }
                
                
            }
            document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
                WeixinJSBridge.call('hideToolbar');
                WeixinJSBridge.call('hideOptionMenu');
            });

           
                              
            setTimeout(function () {
                if (navigator.splashscreen)
                    navigator.splashscreen.hide();
                if (window.StatusBar) {
                    // org.apache.cordova.statusbar required
                    StatusBar.styleLightContent();
                    // navigator.globalization.getPreferredLanguage(function(res){
                    //     console.log('language is :');
                    //     console.log(res);
                    //     //alert(res.value);
                    //   }, function(error){
                    //     console.log('error is :');
                    //     console.log(error);
                    //   });
                }
                if (window.cordova) {
                    if (ionic.Platform.isIOS()) {
                        cordova.plugins.Keyboard.disableScroll(true);
                        MobclickAgent.init('57eb2f31e0f55ab9bd001a53', 'SCT-Official');
                    }else {
                        MobclickAgent.init('57eb2ec467e58eb35e00025f', 'SCT-Official');
                    }
                    PushService.init();
                }
            }, 500);


            

            if(!isWeixin()) {
               // SettingService.checkUpdate(true);
            }
            //更换网络请为插件无法提供拦截器，默认登录一次
            if(iOSDevice()){
                var user = $injector.get('UserPreference');
                var auth = $injector.get('AuthorizeService');
                var loginModel = {
                    username: user.get('username', '')?user.get('username', ''):user.get('account', ''),
                    password: user.get('password', '')
                };
                if(loginModel.password&&loginModel.username){ 
                    auth.logining = false; 
                    auth.login(loginModel,function(){ },function(){});
                }
            }
             // Disable BACK button on home
             $ionicPlatform.registerBackButtonAction(function (event) {
                if ($state.current.name.indexOf('tabsController') === 0 || $state.current.name === "login") {
                    var confirmPopup = $ionicPopup.confirm({
                        title: '温馨提示',
                        template: '确认退出应用吗？',
                        cancelText: '取消',
                        okText: '确认',
                        okType: 'button-balanced'
                    });
                    confirmPopup.then(function (res) {
                        if (res) {
                            navigator.app.exitApp();
                        }
                    });
                }
                else {
                    $ionicHistory.goBack();
                }
            }, 100);
        });
    })
    .config(function ($ionicConfigProvider, $httpProvider, msdElasticConfig, ionicTimePickerProvider, ionicImgCacheProvider, ionicDatePickerProvider, $compileProvider) {
        if (!ionic.Platform.isIOS()) {
            $ionicConfigProvider.scrolling.jsScrolling(false);
        }
        $ionicConfigProvider.views.swipeBackEnabled(false);
        $ionicConfigProvider.views.maxCache(15);
        $httpProvider.defaults.withCredentials = true;
        //enable cors
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.headers.post["Content-Type"] = "application/json";
        $httpProvider.interceptors.push('httpInterceptor');
        $httpProvider.useApplyAsync(true);

        // note that you can also chain configs
        //$ionicConfigProvider.backButton.text('返回').icon('ion-chevron-left');
        $ionicConfigProvider.tabs.position("bottom");
        $ionicConfigProvider.navBar.alignTitle("center");

       // $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob|chrome-extension|ms-appx-web|ms-appx|cdvphotolibrary|ionic):|data:image/);
       $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob|ionic):|data:image/);
        msdElasticConfig.append = '\n';

        var timePickerObj = {
            inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
            format: 24,
            step: 1,
            setLabel: '确定',
            closeLabel: '取消'
        };
        ionicTimePickerProvider.configTimePicker(timePickerObj);
        var datePickerObj = {
            inputDate: new Date(),
            titleLabel: '请选择日期',
            setLabel: '确定',
            closeLabel: '关闭',
            mondayFirst: true,
            weeksList: ["日", "一", "二", "三", "四", "五", "六"],
            monthsList: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            templateType: 'popup',
            showTodayButton: false,
            dateFormat: 'yyyy-MM-dd',
            closeOnSelect: false,
            disableWeekdays: []
        };
        ionicDatePickerProvider.configDatePicker(datePickerObj);

        ionicImgCacheProvider.debug(false);
        ionicImgCacheProvider.quota(200);
        ionicImgCacheProvider.folder('Xgenban');
        ionicImgCacheProvider.cacheClearSize(200);
    })
    .filter('nl2br', ['$filter',
        function ($filter) {
            return function (data) {
                if (!data) return data;
                return data.replace(/\n\r?/g, '<br />');
            };
        }
    ])
    .filter('orderContactBy', function () {
        return function (items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field].localeCompare(b[field]));
            });
            if (reverse) filtered.reverse();
            return filtered;
        };
    })
    .filter('parseUrl', function() {
        var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim;
        //var emails = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/gim

        return function(text) {
            if(text.match(urls)) {
                text = text.replace(urls, "<a href=\"$1\" target=\"_system\">$1</a>");
            }
            // if(text.match(emails)) {
            //     text = text.replace(emails, "<a href=\"mailto:$1\">$1</a>");
            // }
            return text;
        };
    })
    .filter('filterweekday', [function () {
        return function (list, day) {
            var i;
            var tempList = [];
            var temp;

            if (angular.isDefined(list) &&
                list.length > 0) {
                for (temp = list[i = 0]; i < list.length; temp = list[++i]) {
                    if (temp.weekNum % 7 == day)
                        tempList.push(temp);
                }
            }
            return tempList;
        };
    }]);
