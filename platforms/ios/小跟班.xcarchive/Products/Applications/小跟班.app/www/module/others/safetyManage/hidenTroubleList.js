angular.module('app.controllers')
    .controller('hidenTroubleListCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, BroadCast, UserPreference, $ionicLoading,$rootScope) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            // console.log('enter');
            // console.log($scope.select.selectIndex0);
          
            $scope.waitDealCount = 0;
            $scope.dealingCount = 0;
            $scope.closedCount = 0;
            $scope.page = 1;
            $scope.getHidenTroubleList();
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.select = {
                selectIndex0: 1,
                selectIndex1: ''
            };
            console.log('load');
            $scope.assignForMe = false;

             
          
        });

        $rootScope.$on('ADD_HidenTrouble_SUCCEED', function (arg) {
            // $scope.isMoreData = true;
            $scope.select = {
                selectIndex0: 1,
                selectIndex1: ''
            };
            $scope.assignForMe = false;
        });

        //
        $scope.switchView = function (bol) {
            $scope.assignForMe = bol;
            $scope.select.selectIndex0 = bol === false ? 1 : 0;
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $scope.page = 1;
            $scope.isMoreData = false;
            $scope.getHidenTroubleList();
        };

        //隐患上报
        $scope.troubleReport = function () {
            $state.go('hidenTroubleReport');
        };
        $scope.tab1Click = function (index) {
            $scope.select.selectIndex1 = index;
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $scope.page = 1;
            $scope.isMoreData = false;
            $scope.getHidenTroubleList();
        };

        $scope.getDeviceType = function () {
            var result = 'android';
            $scope.isIos = ionic.Platform.isIOS();
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }

            } else {
                result = 'android';
            }
            return result;
        };

        //详情
        $scope.goToDetail = function (item) {
            $state.go('hidenTroubleDetail', {
                itemId: item.id
            });
        };

        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getHidenTroubleList();
        };
        //上拉记载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.getMoreDataList();
        };

        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.isMoreData = true;
            $ionicLoading.hide();
        });

        //request 获取列表
        $scope.getHidenTroubleList = function () {
            Requester.getHidenTroubleList($scope.select.selectIndex0, $scope.select.selectIndex1, $scope.page).then(function (rest) {
                if (rest.result) {
                    $ionicLoading.hide();
                    $scope.list = rest.data.content;
                    $scope.getHidenTroubleCount();//获取数量
                    $scope.list.forEach(function (item) {
                        var picUrls = [];
                        item.picUrls = [];
                        picUrls = item.imgUrlList? item.imgUrlList : [];
                        picUrls.forEach(function (url) {
                            item.picUrls.push({
                                thumb: url,
                                src: url
                            });
                        });
                    });
                   
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                }, 5000);
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };
        //request --加载更多
        $scope.getMoreDataList = function () {
            Requester.getHidenTroubleList($scope.select.selectIndex0, $scope.select.selectIndex1, $scope.page).then(function (rest) {
                if (rest.result) {
                    var data = rest.data.content;
                    for (var i = 0; i < data.length; i++) {
                        $scope.list.push(data[i]);
                    }
                    $scope.list.forEach(function (item) {
                        var picUrls = [];
                        item.picUrls = [];
                        picUrls = item.imgUrlList? item.imgUrlList : [];
                        picUrls.forEach(function (url) {
                            item.picUrls.push({
                                thumb: url,
                                src: url
                            });
                        });
                    });
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };


        //request-获取数量
        $scope.getHidenTroubleCount = function () {
            Requester.getHidenTroubleCount($scope.select.selectIndex0).then(function (rest) {
                if (rest.result) {
                    for(var i=0;i<rest.data.length;i++){

                        if(rest.data[i].status===1){
                            $scope.waitDealCount = rest.data[i].count; 
                        }else if(rest.data[i].status===2){
                            $scope.dealingCount = rest.data[i].count; 
                        }else{
                            $scope.closedCount= rest.data[i].count;    
                        }
                    }
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

    });