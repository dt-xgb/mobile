/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('registerCtrl', function ($scope, AccountService, BroadCast, UserPreference, Constant, $state, $ionicPopup, toaster, MESSAGES, AuthorizeService, $ionicLoading, $ionicModal, Requester, $ionicHistory,$stateParams,$rootScope) {

        $scope.input = {
            retype: undefined
        };
        $scope.btnText = '获取验证码';

        function updateCoolDownText() {
            if ($scope.cd > 0) {
                $scope.cd--;
                $scope.btnText = $scope.cd;
            }
            else if ($scope.cooldown) {
                clearInterval($scope.cooldown);
                $scope.canReqSMS = true;
                $scope.cd = Constant.SMS_REQ_INTERVAL;
                $scope.btnText = '获取验证码';
            }
            $scope.$digest();
        }

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.input.retype = $stateParams.status?$stateParams.status:  $rootScope.rootRegistStatus;
           if (isWeixin())
            $rootScope.rootRegistStatus = $scope.input.retype;
        });

        function onFinishRegister() {
            $scope.modal.hide();
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            UserPreference.set("account", $scope.username);
            var opt = {
                username: $scope.username,
                password: $scope.input.password
            };
            AuthorizeService.login(opt)
                .then(function (data) {
                    if (data.result) {
                        UserPreference.set("password", $scope.input.password);
                        $state.go('tabsController.mainPage');
                    }
                    else {
                        toaster.error({title: MESSAGES.LOGIN_ERROR, body: data.message});
                        $state.go('login');
                    }
                    $scope.input = {
                        retype: 't'
                    };
                })
                .finally(function () {
                    $ionicLoading.hide();
                });
        }

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'open/registerStudentsSelect.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.input.smscode = '';
            $scope.input.password = '';
            $scope.input.pwd = '';
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        var lastSmsTime = UserPreference.get("LastSendSMSTime", 0);
        var diff = Math.round(new Date().getTime() / 1000) - lastSmsTime;
        $scope.canReqSMS = (diff >= Constant.SMS_REQ_INTERVAL);
        if ($scope.canReqSMS)
            $scope.cd = Constant.SMS_REQ_INTERVAL;
        else {
            $scope.cd = Constant.SMS_REQ_INTERVAL - diff;
            $scope.cooldown = setInterval(updateCoolDownText, 1000);
        }

        $scope.getSmsCode = function () {
            if ($scope.canReqSMS) {
                $scope.canReqSMS = false;
                AccountService.getSmsCode($scope.input.phone, 'register', $scope.input.retype, $scope.input.name);
            }
        };

        $scope.doRegister = function (check) {
            var reg = /^[a-zA-Z0-9]+$/;
            if (!$scope.input.password.match(reg) || !$scope.input.pwd.match(reg)) {
                toaster.warning({title: MESSAGES.REMIND, body: MESSAGES.LOGIN_PASSWORD_PATTERN});
                return;
            }
            if ($scope.input.password != $scope.input.pwd) {
                toaster.warning({title: MESSAGES.REMIND, body: MESSAGES.PASSWORD_CONFIRM_ERROR});
                return;
            }
            var params = angular.copy($scope.input);
            params.bCheck = check;
            AccountService.register(params);
        };

        $scope.registerAs = function (child) {
            Requester.selectExistedStudent2Register(child.stuId, $scope.input.phone, $scope.input.smscode, $scope.input.password)
                .then(function (resp) {
                    if (resp && resp.result) {
                        $scope.username = resp.data.fusername;
                        onFinishRegister();
                    } else {
                        toaster.error({title: '注册失败', body: resp.message});
                    }
                });
        };

        $scope.$on(BroadCast.SMS_SENT, function (event, data) {
            if (data) {
                if (data.result) {
                    UserPreference.set("LastSendSMSTime", Math.round(new Date().getTime() / 1000));
                    $scope.cooldown = setInterval(updateCoolDownText, 1000);
                } else {
                    $scope.canReqSMS = true;
                    toaster.warning({title: MESSAGES.REMIND, body: data.message});
                }
            }
            if (Constant.debugMode) console.log(data);
        });

        $scope.$on(BroadCast.ACCOUNT_REGISTER_COMPLETE, function (event, data) {
            if (data && data.result) {
                var text = '';
                if ($scope.input.retype === 'sf') {
                    if (data.data.tag === 0) {
                        text = '成功注册家长和学生账号各一个，家长和学生均可使用注册手机号登录，家长与学生的初始密码一致，感谢你的支持。';
                        $scope.username = data.data.fusername;
                    }
                    else {
                        text = '成功注册学生账号，使用注册手机号即可登录，感谢你的支持。';
                        $scope.username = undefined;
                    }
                }
                else {
                    text = '成功注册教师账号，使用注册手机号即可登录，感谢你的支持。';
                    $scope.username = data.data.username;
                }
                $ionicPopup.alert({
                    title: '注册成功',
                    template: text,
                    okText: '确定',
                    okType: 'button-balanced'
                }).then(function () {
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                    if($scope.username)
                        onFinishRegister();
                    else {
                        $state.go('login');
                    }
                });
            } else {
                if (data.code === 10001) {
                    $scope.stuList = data.data;
                    $scope.openModal();
                }
                else
                    toaster.error({title: '注册失败', body: data.message});
            }
            if (Constant.debugMode) console.log(data);
        });

        //用户协议
        // if (Constant.debugMode) UIPath = 'module/';
        // $ionicModal.fromTemplateUrl(UIPath + 'open/userProtocol.html', {
        //     scope: $scope,
        //     animation: 'slide-in-up'
        // }).then(function (modal) {
        //     $scope.protocolModal = modal;
        // });
       
        
        $scope.protocol = function(){
           $state.go('userProtocol');
        };

        // $scope.closeProtocolModal= function(){
        //     $scope.protocolModal.hide();
        // };
    });
