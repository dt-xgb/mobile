/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('tabsCtrl', function ($scope, BroadCast) {
        $scope.$on("$ionicView.beforeEnter", function (event, data){
            $scope.isPhone = isWeixin();
        });
        $scope.badge = 0;
        $scope.hasContactPage = true;
       

        $scope.$on(BroadCast.BADGE_UPDATE, function (event, data) {
            if (data.type === 'im') {
                $scope.badge = data.count;
                $scope.$digest();
            } else {
                $scope.campus = data.count;
            }
        });



    });