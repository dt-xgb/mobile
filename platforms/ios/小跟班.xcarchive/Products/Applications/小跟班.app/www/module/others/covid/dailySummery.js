angular.module('app.controllers')
    .controller('covidDailySummeryCtrl', function ($scope, Constant, $state, Requester, toaster, UserPreference, BroadCast) {

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.initLocalData();
        });
        //初始化
        $scope.initLocalData = function () {
            var user = UserPreference.getObject('user');
            //console.log(user);
            $scope.classes = UserPreference.getArray('HeadTeacherClasses');
            $scope.selected = {
                class_id:  $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].id : ''//UserPreference.get('DefaultClassID', $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].id : '')
            };
            $scope.isTeacher = user.rolename == '3' ? true : false;
            $scope.selectedStatus = '';
            $scope.activeTab = 'all';
            $scope.warningList = [];
        };

        $scope.$on(BroadCast.NEW_PUSH_CLICK, function () {
            $scope.initLocalData();
        });

        function setProcess(val) {
            var process = document.querySelector("#circle-process");
            var circleLength = Math.floor(2 * Math.PI * 50);
            val = Math.max(0, val);
            val = Math.min(100, val);
            process.setAttribute("stroke-dasharray", "" + circleLength * val + ",10000");
        }

        $scope.$on("$ionicView.enter", function (event, data) {
           
            $scope.classes = UserPreference.getArray('HeadTeacherClasses');
            $scope.selected = {
                class_id: $scope.selected.class_id?$scope.selected.class_id:UserPreference.get('DefaultClassID', $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].id : '')
            };
            setProcess(0);
            $scope.loadData();
        });

        $scope.loadData = function () {
            Requester.getCovidSummary($scope.selected.class_id).then(function (res) {
                $scope.summary = res.data;
                if ($scope.summary.attendanceNum && $scope.summary.attendanceNum != 0)
                    setProcess($scope.summary.abnormalNum / $scope.summary.attendanceNum);
                else
                    setProcess(0);
            });
            Requester.getWarningList($scope.selected.class_id).then(function (res) {
                if (res.data && res.data.content) {
                    $scope.warningList = res.data.content;
                    Requester.covidWarningRead($scope.selected.class_id);
                } else
                    $scope.warningList = [];
            });
        };

        $scope.changeFilter = function (arg) {
            if ($scope.activeTab === arg) {
                return;
            }
            $scope.activeTab = arg;
            if (arg === 'clear') {
                $scope.selectedStatus = '2';
            } else if (arg === 'pending') {
                $scope.selectedStatus = '1';
            } else {
                $scope.selectedStatus = '';
            }
        };


        $scope.goFunction = function (where, args) {
            if (where) {
                if (args)
                    $state.go(where, {
                        id: args
                    });
                else
                    $state.go(where);
            }
        };
    });