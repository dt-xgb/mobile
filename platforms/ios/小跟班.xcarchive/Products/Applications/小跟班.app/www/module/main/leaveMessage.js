angular.module('app.controllers')
    .controller('leaveMessageCtrl', function ($scope, Requester, $ionicLoading ,  $timeout, $state, UserPreference, $ionicScrollDelegate) {

        $scope.$on("$ionicView.enter", function (event, data) {
            //获取用户id
            $scope.user = UserPreference.getObject('user');
            $scope.child = UserPreference.getObject('DefaultChild');
            $scope.childName = UserPreference.get('DefaultChildName');
            $scope.childId = UserPreference.get('DefaultChildID');
            $scope.isIos = ionic.Platform.isIOS();


            if(window.cordova&& $scope.isIos){
                //cordova.plugins.Keyboard.disableScroll(false);
            }

            for (var i = 0; i < $scope.user.student.length; i++) {
                if ($scope.user.student[i].id == $scope.childId) {
                    $scope.child = $scope.user.student[i];
                    break;
                }
            }
            $scope.leaveMsgKey = "leaveMsg_list_" + $scope.childId;
            $scope.sendSucceed = false;
            $scope.userName = String($scope.user.name);
            $scope.userHead = $scope.user.logo;
            $scope.screenHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

            $scope.input = {
                message: '',
                messageType: 0
            };
            $scope.input.message = '';

            $scope.recordMsg = ""; //判断是否为发送消息

            //获取留言消息列表

            $scope.leaveMsgList = UserPreference.getArray($scope.leaveMsgKey) ? UserPreference.getArray($scope.leaveMsgKey) : [];
            //  UserPreference.setObject($scope.leaveMsgKey, []);
            //$scope.sendLeaveMsg();  
            $scope.getUnreadLeaveMsgList();

                        

        });

        $scope.blur = function () {
            
            setTimeout(function () {
                var scrollHeight = document.documentElement.scrollTop || document.body.scrollTop || 0;
                window.scrollTo(0, Math.max(scrollHeight - 1, 0));
            }, 100);

        };

        $scope.isShow = false; //是否弹出底部视图 相机
        $scope.emotionShow = false; //是否显示表情


        //监听 键盘弹出 隐藏事件
        window.addEventListener('native.keyboardshow', function (e) {
            $scope.isShow = false;
            $scope.emotionShow = false;
            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            }, 100);

        });

        window.addEventListener('native.keyboardhide', function (e) {
            // todo 进行键盘不可用时操作
            console.log('关闭时');
            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            }, 100);

        });


        //离开页面时  将所有未读的 标记为已读状态
        $scope.$on("$ionicView.leave", function (event, data) {
            //$scope.markAllLeaveMsgReaded(); 
        });

        //发送消息
        $scope.sendMessage = function () {
            $scope.recordMsg = 'sendMsg';
            if(isEmojiCharacter($scope.input.message)){
                $ionicLoading.show({
                    template: '请不要输入表情字符',
                    duration: 1500
                  });
                return;
            }
            $scope.getUnreadLeaveMsgList(); //发送消息前 先判断是否有新留言传入 防止时间错乱
        };

        //选择表情
        $scope.setValue = function (e) {
            console.log(e);
            if ($scope.input.message)
                $scope.input.message += e;
            else
                $scope.input.message = e;

            var tObj = document.getElementById("suggestTextinput");
            var sPos = tObj.value.length;
            setCaretPosition(tObj, sPos);
        };

        //弹出底部视图
        $scope.selectImg = function () {
            $timeout(function () {
                if (window.cordova)
                    cordova.plugins.Keyboard.close();
            }, 10);
            var resp = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollPosition();
            $scope.isShow = !$scope.isShow;
            $scope.emotionShow = false;
            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            }, 100);
        };
        //底部弹出表情
        $scope.showEmotions = function () {
            var resp = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollPosition();
            $scope.emotionShow = !$scope.emotionShow;
            $scope.isShow = false;

            if ($scope.emotionShow) {
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollTo(0, resp.top + 200, true);
                }, 100);

            } else {
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
                }, 100);
            }

        };
        //隐藏footView
        $scope.hideFootView = function () {
            if ($scope.emotionShow === true || $scope.isShow === true) {
                $scope.emotionShow = false;
                $scope.isShow = false;
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
                }, 100);
            }

        };

        $scope.insertTimeLabelToMsg = function (i) {
            for (; i >= 0; i--) {
                var msg = $scope.leaveMsgList[i];
                var time1 = msg.inserttime.replace(/\-/g, "/");
                var timestamp = Date.parse(new Date(time1)) / 1000;

                var msgDay = msg.inserttime.substr(8, 2);
                var now = getNowFormatDate1().substr(8, 2);

                if (i > 0) {
                    var preMsg = $scope.leaveMsgList[i - 1];
                    var timestamp1 = Date.parse(new Date(preMsg.inserttime.replace(/\-/g, "/"))) / 1000;
                    var preMsgDay = preMsg.inserttime.substr(8, 2);
                    if (now != msgDay) {
                        if (msgDay != preMsgDay)
                            msg.timeLabel = msg.inserttime; //formatTimeWithoutSecends(timestamp);
                        else
                            msg.timeLabel = undefined;
                    } else if (timestamp - timestamp1 >= 60 * 5) {
                        msg.timeLabel = msg.inserttime.substr(11, 5); //formatTimeWithoutSecends(timestamp).substr(11, 5);
                    } else {
                        msg.timeLabel = undefined;
                    }

                } else {
                    if (now != msgDay) {
                        msg.timeLabel = msg.inserttime; //formatTimeWithoutSecends(timestamp);
                    } else
                        msg.timeLabel = msg.inserttime.substr(11, 5); //formatTimeWithoutSecends(timestamp).substr(11, 5);
                }
            }
        };

        //转为html标签
        $scope.convertMsgtoHtml = function (msg) {
            return encodeHtml(msg);
        };

        //发送留言
        $scope.sendLeaveMsg = function () {
            Requester.sendClassLeaveMessage($scope.input.message, $scope.childId, $scope.user).then(function (resp) {
                if (resp.result) {
                    //发送成功
                    console.log('发送留言成功');
                    console.log(resp.data);
                    $scope.sendSucceed = resp.result;
                    return resp.data;

                } else {

                    console.log('发送留言失败');
                }
            }).then(function (result) {
              
                $scope.leaveMsgList.push({
                    userType: 0,
                    msg: $scope.input.message,
                    inserttime: result
                });
                $scope.insertTimeLabelToMsg($scope.leaveMsgList.length - 1);
               // $scope.saveHistoryData();
                UserPreference.setObject($scope.leaveMsgKey, $scope.leaveMsgList);
                $scope.input.message = ''; //清空输入框
                $scope.recordMsg = '';
            });
        };


        //获取未读留言消息列表
        $scope.getUnreadLeaveMsgList = function () {
            Requester.getUnreadLeaveMsgList($scope.user.id, $scope.childId).then(function (resp) {
                if (resp.result) {
                    console.log(resp.data);
                    resp.data.forEach(function (message) {
                        $scope.leaveMsgList.push(message);
                    });
                    $scope.insertTimeLabelToMsg($scope.leaveMsgList.length - 1);
                    //$scope.saveHistoryData(); //最多保留200条历史数据
                    $scope.saveDataNearlyMonth();//保留近30天
                    UserPreference.setObject($scope.leaveMsgKey, $scope.leaveMsgList);
                    console.log(UserPreference.getArray($scope.leaveMsgKey));
                    console.log('未读的留言消息列表');
                }
                return resp.result;

            }).then(function (result) {
                if (result && $scope.recordMsg == 'sendMsg') {
                    console.log('发送消息执行');
                    $scope.sendLeaveMsg();
                }

            }).then(function () {
                //标记为已读
                $timeout(function () {
                    $scope.markAllLeaveMsgReaded();
                }, 100);
            });
        };

        //将所有消息标记为已读
        $scope.markAllLeaveMsgReaded = function () {
            Requester.markLeaveMsgReaded($scope.user.id, $scope.childId).then(function (resp) {
                if (resp.result) {
                    console.log('所有留言标记为已读');
                    console.log(resp.data);
                }
            });
        };

        //只保留最新200条历史数据
        $scope.saveHistoryData = function () {

            if ($scope.leaveMsgList.length > 200) {
                $scope.leaveMsgList = $scope.leaveMsgList.splice($scope.leaveMsgList.length - 200, 200);
            }
        };

        //只保留近30天的
        $scope.saveDataNearlyMonth = function(){

           
          var currentTime = getNowFormatDate1();
          var currentTimestamp = Date.parse(new Date(currentTime.replace(/\-/g, "/"))) / 1000;
          //如果数组中第一个为 在30天内，就不用遍历数组
          if ($scope.leaveMsgList&&$scope.leaveMsgList.length > 0) {
              var timeStamp = Date.parse(new Date($scope.leaveMsgList[0].inserttime.replace(/\-/g, "/"))) / 1000;
              if ((timeStamp - currentTimestamp) <= 60 * 60 * 24 * 30) {
                    return;
              }
          }
          for (var i = 0; i < $scope.leaveMsgList.length; i++) {
              var msgTime = $scope.leaveMsgList[i].inserttime;
              var msgTimestamp = Date.parse(new Date(msgTime.replace(/\-/g, "/"))) / 1000;
            
              if ((msgTimestamp - currentTimestamp) > 60 * 60 * 24 * 30) {
                  $scope.leaveMsgList = $scope.leaveMsgList.splice(i, 1);
                  continue;
              }
          }
        };


    });