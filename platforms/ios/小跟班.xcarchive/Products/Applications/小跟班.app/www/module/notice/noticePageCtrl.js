/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('noticePageCtrl', function ($stateParams, $scope, $ionicModal, $state, CameraHelper, BroadCast, NoticeService, Constant, UserPreference, toaster, MESSAGES, $ionicLoading, $ionicPopup, $ionicListDelegate, ionicImageView, $q,$rootScope, $timeout) {
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'notice/newNotice.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.goDetail = function (notice) {
            if (notice) {
                $state.go('noticeDetail', {notice: angular.copy(notice)});
                notice.alreadyRead = true;
            }
        };

        $scope.goFunction = function (where, args) {
            if (where) {
                if (args)
                    $state.go(where, {type: args});
                else
                    $state.go(where);
            }
        };

        $scope.itemTracker= function(item) {
            return item.id + '' + item.studentByFimalyList.length;
        };
        
        $scope.$on("$ionicView.enter", function (event, data) {
           
            $scope.isWeiXin = isWeixin();
            $scope.noticeType = $stateParams.type?$stateParams.type:  $rootScope.rootNoticeType;
           
           if (isWeixin())
            $rootScope.rootNoticeType = $scope.noticeType;
            if(isWeixin()){
                // if(!$scope.noticeType){
                //     $state.go('tabsController.mainPage');
                //     alert('请点击上方返回按钮返回');
                //     return;
                // }
            }
            if ($scope.currentSID && $scope.currentSID != UserPreference.get('DefaultChildID')) {
                $scope.noticeList = [];
                $scope.currentSID = UserPreference.get('DefaultChildID');
                $scope.loadData(true);
            }
            $scope.userId = UserPreference.getObject('user').id;
           // console.log(' $scope.userId:'+ $scope.userId);
           $scope.noticeList = UserPreference.getArray('notice_list_cache' + $scope.noticeType);
            $scope.listHasMore = true;
            $scope.userRole = UserPreference.getObject('user').rolename;
            $scope.isTeacher = String($scope.userRole) === Constant.USER_ROLES.TEACHER;
            $scope.classes = UserPreference.getArray('class');
            $scope.subjects = UserPreference.getArray('subject');
            $scope.categories = UserPreference.getArray('category');
            $scope.currentSID = UserPreference.get('DefaultChildID');
            for (var i = 0; i < $scope.categories.length; i++) {
                if (String($scope.categories[i].key) === $scope.noticeType) {
                    $scope.noticePageTitle = $scope.categories[i].value;
                    break;
                }
            }
            $scope.dataInit();
            $scope.loadData(true);
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
           
        });

        // $scope.$on("$ionicView.enter", function (event, data) {
           
        // });

        $scope.deleteItem = function (item, $event) {
            $event.stopPropagation();
            var confirmPopup = $ionicPopup.confirm({
                title: '删除确认',
                template: '确认删除 ' + item.title + ' ?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    NoticeService.deleteNotice(item.id);
                    $ionicListDelegate.closeOptionButtons();
                }
            });
        };

        $scope.removeHTMLTag = function (str) {
            str = str.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
            str = str.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
            str = str.replace(/\n[\s| | ]*\r/g, '\n'); //去除多余空行
            str = str.replace(/&nbsp;/ig, '');//去掉&nbsp;
            return str;
        };

        $scope.$on(BroadCast.DELETE_NOTICE_REV, function (a, rst) {
            if (rst && rst.result) {
                var i = 0;
                for (; i < $scope.noticeList.length; i++) {
                    if ($scope.noticeList[i].id == rst.request.id) {
                        switch (rst.request.type) {
                            case BroadCast.DELETE_NOTICE_REV:
                                $scope.noticeList.splice(i, 1);
                                break;
                        }
                        break;
                    }
                }
            }
            else
                toaster.error({title: MESSAGES.OPERATE_ERROR, body: rst.message});
        });

        $scope.dataInit = function () {
            $scope.attachments = [];
            $scope.selected = {
                key: $scope.noticeType
            };
            $scope.suggestTitle = {
                class: '',
                subject: '',
                type: ''
            };
            if ($scope.classes.length === 1) {
                $scope.selected.class_id = $scope.classes[0].key;
                $scope.suggestTitle.class = $scope.classes[0].value;
            }
            if ($scope.subjects.length === 1) {
                $scope.selected.subjectId = $scope.subjects[0].key;
                if ($scope.selected.key === '12' || $scope.selected.key === '13')
                    $scope.suggestTitle.subject = $scope.subjects[0].value;
            }
            $scope.selected.title = $scope.suggestTitle.class + $scope.suggestTitle.subject + $scope.noticePageTitle;

        };

        $scope.onSelectionChange = function (which) {
            if ($scope.selected.title !== ($scope.suggestTitle.class + $scope.suggestTitle.subject + $scope.noticePageTitle))
                return;
            switch (which) {
                case 'class':
                    for (var j = 0; j < $scope.classes.length; j++) {
                        if ($scope.classes[j].key === $scope.selected.class_id) {
                            $scope.suggestTitle.class = $scope.classes[j].value;
                            break;
                        }
                    }
                    if (j === $scope.classes.length)
                        $scope.suggestTitle.class = '';
                    break;
                case 'subject':
                    for (var k = 0; k < $scope.subjects.length; k++) {
                        if ($scope.subjects[k].key === $scope.selected.subjectId) {
                            $scope.suggestTitle.subject = $scope.subjects[k].value;
                            break;
                        }
                    }
                    if (k === $scope.subjects.length)
                        $scope.suggestTitle.subject = '';
                    break;
            }
            $scope.selected.title = $scope.suggestTitle.class + $scope.suggestTitle.subject + $scope.noticePageTitle;
        };

        $scope.loadData = function (reset) {
            if (Constant.debugMode) console.log('load notice restart ' + reset);
            if (reset) {
                $scope.page = 1;
            }
            else if (NoticeService.listHasMore)
                $scope.page++;
            else
                return;
            $scope.loading = true;
            NoticeService.getNoticeList($scope.page, $scope.noticeType);
        };


        $scope.$on(BroadCast.NOTICE_LIST_REV, function (a, rst) {
            if (rst && rst.result) {
                $scope.noticeList = NoticeService.list;
                if(Constant.debugMode)  console.log('notice data has more:' + $scope.listHasMore);
            }
            $scope.loading = false;
            $scope.listHasMore = NoticeService.listHasMore;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });

        $scope.$on(BroadCast.DIC_REV, function (a, rst) {
            switch (rst) {
                case 'class':
                    $scope.classes = UserPreference.getArray(rst);
                    if ($scope.classes.length === 1) {
                        $scope.selected.class_id = $scope.classes[0].key;
                        $scope.suggestTitle.class = $scope.classes[0].value;
                    }
                    break;
                case 'subject':
                    $scope.subjects = UserPreference.getArray(rst);
                    if ($scope.subjects.length === 1) {
                        $scope.selected.subjectId = $scope.subjects[0].key;
                        if ($scope.selected.key === '12' || $scope.selected.key === '13')
                            $scope.suggestTitle.subject = $scope.subjects[0].value;
                    }
                    break;
                case 'category':
                    $scope.categories = UserPreference.getArray(rst);
                    break;
            }
            $scope.selected.title = $scope.suggestTitle.class + $scope.suggestTitle.subject + $scope.noticePageTitle;
        });

        $scope.selectImg = function () {
            if(window.cordova){
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.attachments, resp);
                        $scope.$apply();
                    } else {
                        $scope.attachments.push({
                            data: "data:image/jpeg;base64," + resp
                        });
                    }
                }, 9 - $scope.attachments.length);
            }else{
                var input = document.getElementById("capture");
                input.click();
            }
           
        };

         // input形式打开系统系统相册
         $scope.getFile = function(files){
            var file = files[0];
            var reader = new FileReader();
             reader.readAsDataURL(file);
               reader.onload  = function(theFile){
                 $timeout(function () {
                   $scope.testImg = theFile.target.result;//设置一个中间值
                   $scope.attachments.push({
                    data: $scope.testImg
                });
                  
               }, 100);
             };
           };

        $scope.preViewImages = function (index) {
            var origin = [];
            $scope.attachments.forEach(function(libraryItem) {
                if(libraryItem.data)
                    origin.push(libraryItem.data);
                else
                    origin.push(libraryItem);
            });
            ionicImageView.showViewModal({
                allowSave: false
            }, origin, index);
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.attachments.splice(index, 1);
        };

        $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
            if (rst && rst.which === 'notice') {
                $scope.attachments.push(rst);
                //if(Constant.debugMode) console.log($scope.attachments);
            }
        });

        function onImageResized(resp) {
            var start = resp.indexOf('base64,');
            $scope.selected.picdatas.push(resp.substr(start + 7));
        }

        $scope.save = function () {
            if ($scope.isTeacher) {
                $scope.selected.classIds = [];
                if ($scope.selected.class_id && $scope.selected.class_id !== "")
                    $scope.selected.classIds.push($scope.selected.class_id);
                else {
                    toaster.warning({title: MESSAGES.OPERATE_ERROR, body: MESSAGES.NO_TARGET_CLASS});
                    return;
                }
            }
            $scope.selected.picdatas = [];
            var promiseArr = [];
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $scope.attachments.forEach(function(libraryItem) {
                if (libraryItem.data) {
                    onImageResized(libraryItem.data);
                }
                else {
                    promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, libraryItem).then(onImageResized));
                }
            });
            $q.all(promiseArr).then(function () {
                NoticeService.newNotice($scope.selected);
             setTimeout(function(){
              
                $scope.loading = false;
                $scope.listHasMore = false;
                $ionicLoading.hide();
             },120000);
            });
        };

        $scope.$on(BroadCast.NEW_NOTICE, function (a, rst) {
            $ionicLoading.hide();
            if (rst && rst.result) {
                toaster.success({title: MESSAGES.REMIND, body: MESSAGES.NEW_NOTICE});
                $scope.closeModal();
                $scope.dataInit();
                $scope.loadData(true);
            }
            else
                toaster.error({title: MESSAGES.REMIND, body: rst.message});
            //$ionicLoading.hide();
        });

        $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
            $scope.loading = false;
            $scope.listHasMore = false;
            $ionicLoading.hide();
        });

        $scope.checkIncludeStr = function(str){
            var str1 = "<p";
            var str2 = "</p>";
            var str3 = "</span>";
            var str4 = "<img";
            if(str)
            result = (str.indexOf(str1)!=-1&&str.indexOf(str2)!=-1)||str.indexOf(str3)!=-1||str.indexOf(str4)!=-1;
            return result;
        };

    });