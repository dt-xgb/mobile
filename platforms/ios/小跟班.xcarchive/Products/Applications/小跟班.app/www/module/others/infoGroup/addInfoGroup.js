angular.module('app.controllers')
    .controller('addInfoGroupCtrl', function ($scope, Constant, $state, BroadCast, $ionicModal, CameraHelper, $q, Requester, toaster, UserPreference, $ionicHistory, $timeout, $ionicActionSheet, $rootScope, $ionicLoading) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.select = {
                selectClassCount: 0,
                isAllChecked: false,
                farCategoryName: '',
                farCategoryId: null,
                subCategoryName: '',
                subCategoryId: null, //细分类型id
                classIds: [],
                title: '',
                content: '',
                picdatas: []
            };
            $scope.farList = []; //父类
            $scope.subList = []; //子类型数组
            $scope.classList = []; //班级列表
            $scope.reportImgs = [];
            $scope.loadingCount = 0; //加载次数
            // $scope.classes = UserPreference.getArray('class');
            $scope.getFarInfoCategoryList();

        });

        $scope.$on("$ionicView.loaded", function (event, data) {

        });

        // 选择大类型
        $scope.showType = function (type) {

            if (type === 2 && !$scope.select.farCategoryId) {
                toaster.warning({
                    // title: "温馨提示",
                    body: '请先选择信息类型'
                });
                return;
            }

            var types = type === 1 ? $scope.farList : $scope.subList;
            // Show the action sheet
            var hideSheet = $ionicActionSheet.show({
                buttons: types,
                titleText: type === 1 ? '信息类型' : '细分类型',
                cancelText: '取消',
                cssClass:'customSheet',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index, event) {
                    if (type == 1) {
                        $scope.select.farCategoryName = types[index].text;
                        $scope.select.farCategoryId = types[index].id;
                        if (!$scope.recordParentId || $scope.recordParentId !== $scope.select.farCategoryId)
                            $scope.getSubInfoCategoryList($scope.select.farCategoryId);
                    } else {
                        $scope.select.subCategoryName = types[index].text;
                        $scope.select.subCategoryId = types[index].id;
                    }

                    //   console.log()
                    return true;
                }
            });
        };

        //选择图片
        $scope.selectImg = function () {

            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.reportImgs, resp);
                        $scope.$apply();
                    } else {
                        $scope.reportImgs.push(
                            "data:image/jpeg;base64," + resp
                        );
                    }
                }, 9 - $scope.reportImgs.length);
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }
        };

        // input形式打开系统系统相册
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.reportImgs.push($scope.testImg);

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.reportImgs.splice(index, 1);
        };

        function onImageResized(resp) {
            $scope.select.picdatas.push(resp);
        }

        //确定选择班级
        $scope.commitSelectClass = function () {
            $scope.select.selectClassCount = 0;
            $scope.select.classIds = [];
            for (var i = 0; i < $scope.classList.length; i++) {
                if ($scope.classList[i].checked) {
                    $scope.select.selectClassCount++;
                    $scope.select.classIds.push($scope.classList[i].id);
                }
            }
            $scope.selectClassModal.hide();
        };
        //提交   
        $scope.commit = function () {
            //console.log($scope.select);
            if (!$scope.select.farCategoryId) {
                toaster.warning({
                    // title: "温馨提示",
                    body: '请选择信息类型'
                });
                return;
            }
            if (!$scope.select.subCategoryId) {
                toaster.warning({
                    // title: "温馨提示",
                    body: '请选择细分类型'
                });
                return;
            }
            if ($scope.select.selectClassCount === 0) {
                toaster.warning({
                    // title: "温馨提示",
                    body: '请选择发布范围'
                });
                return;
            }
            $scope.isLoading = true;
            $scope.select.picdatas = [];
            var promiseArr = [];
            // $scope.reportImgs.forEach(function (img) {
            //     promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized));
            // });
            for (var j = 0; j < $scope.reportImgs.length; j++) {
                var img = $scope.reportImgs[j];
                promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized));
            }

            $q.all(promiseArr).then(function () {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });

                if ($scope.loadingCount === 0) {
                    $scope.addClassInfoGroup();
                    //    console.log('----count:'+$scope.loadingCount);
                    $scope.loadingCount++;
                }

            });



        };

        //选择发布范围
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'others/infoGroup/selectClassModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.selectClassModal = modal;
        });
        $scope.chooseClassCount = function () {
            $scope.selectClassModal.show();
        };
        //
        $scope.hideSelectClassModal = function () {
            $scope.select.selectClassCount = 0;
            $scope.select.classIds = [];
            if($scope.select.isAllChecked) $scope.select.isAllChecked = false;
            for (var i = 0; i < $scope.classList.length; i++) {
                if ($scope.classList[i].checked) {
                    $scope.classList[i].checked = false;    
                }
            }
            $scope.selectClassModal.hide();
        };

        //选择所有班级
        $scope.selectAllFiltered = function () {
            // console.log('is all checked :' + $scope.select.isAllChecked);  
            for (var i = 0; i < $scope.classList.length; i++) {
                $scope.classList[i].checked = $scope.select.isAllChecked;
            }
        };
        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.loadingCount = 0;
            $ionicLoading.hide();
        });

        //request --获取信息列表
        $scope.getFarInfoCategoryList = function () {
            Requester.getFarInfoCategoryList().then(function (rest) {
                if (rest.result) {
                    $scope.farList = [];
                    for (var i = 0; i < rest.data.length; i++) {
                        var data = rest.data[i];
                        $scope.farList.push({
                            text: data.categoryName,
                            id: data.id,
                            key: data.key,
                            parentKey: data.key
                        });
                    }

                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                //查看班级
                $scope.getGroupAttendanceClassList();
            });

        };

        //request -- 子类型列表
        $scope.getSubInfoCategoryList = function (parentId) {
            Requester.getSubInfoCategoryList(parentId).then(function (rest) {
                if (rest.result) {
                    $scope.subList = [];
                    $scope.recordParentId = parentId;
                    $scope.select.subCategoryName = '';
                    $scope.select.subCategoryId = '';
                    for (var i = 0; i < rest.data.length; i++) {
                        var data = rest.data[i];
                        $scope.subList.push({
                            text: data.categoryName,
                            id: data.id,
                            key: data.key
                        });
                    }
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //request --获取发布范围班级列表
        $scope.getGroupAttendanceClassList = function () {
            Requester.getGroupAttendanceClassList().then(function (rest) {
                if (rest.result) {

                    $scope.classList = rest.data;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //requester --发布信息类型
        $scope.addClassInfoGroup = function () {
            Requester.addClassInfoGroup($scope.select).then(function (rest) {
                if (rest.result) {
                    $scope.loadingCount = 0;
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    $scope.reportImgs = [];
                    toaster.success({
                        title: "发布成功",
                        timeout: 3000
                    });
                    //$rootScope.$broadcast('ADD_GroupInfo_SUCCEED', 'succeed');

                    $ionicHistory.goBack();
                } else {
                    $scope.loadingCount = 0;
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    $scope.loadingCount = 0;
                }, 5000);
            });

        };


    });