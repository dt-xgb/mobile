/**
 * 设置班训
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('classMottoCtrl', function ($scope, NoticeService, BroadCast, MESSAGES, UserPreference, toaster,$ionicPopup) {
        $scope.save = function () {
            if ($scope.selected.banxun.length > 0) {
               NoticeService.updateClassBanxun($scope.selected);
            //    toaster.pop({
            //     type: 'success',
            //     title: '设置成功',
            //     body: MESSAGES.BANXUN_UPDATE_OK,
            //     showCloseButton: false
            //  });
            }
        };

        $scope.onSelectionChange = function () {
            NoticeService.getClassBanxun($scope.selected.class_id);
        };

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            //$scope.classes = UserPreference.getArray('class');
             
            $scope.classes = [];
            $scope.headClasses = UserPreference.getArray('HeadTeacherClasses');
            $scope.selected = {};

            for (var i = 0; i < $scope.headClasses.length; i++) {
                $scope.classes.push({
                    key: $scope.headClasses[i].id,
                    value: $scope.headClasses[i].className
                });
            }

            $scope.selected.class_id = $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].key : '';
            $scope.selected.class_name = $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].value : '';
            if ($scope.selected.class_id && $scope.selected.class_id !== '') {
                NoticeService.getClassBanxun($scope.selected.class_id);
            }

        });

        $scope.$on(BroadCast.CLASS_BANXUN_REV, function (arg, data) {
            if (data.result){
               
                $scope.selected.banxun = data.data;
            }else{
                $scope.selected.banxun = MESSAGES.DEFAULT_BANXUN;
            }
               
        });

        $scope.$on(BroadCast.CLASS_BANXUN_UPDATE, function (arg, data) {
            //console.log(data);
            if (data.result){
                
                setTimeout(function(){
                    toaster.pop({
                        type: 'success',
                        title: '设置成功',
                        body: MESSAGES.BANXUN_UPDATE_OK,
                        showCloseButton: false
                     });
                },50);
               
            }else
                toaster.error({
                    title: MESSAGES.BANXUN_UPDATE_FAIL
                });
        });

         //弹出班级选择框
         $scope.onSelectionChange = function (item) {

            $scope.classAlert = $ionicPopup.show({
                template: '<div ng-repeat="item in classes " ng-click="onSelectClass(item)" ng-style="{\'border-bottom\':$index==classes.length-1?\'none\':\' 1px solid #ddd\'}" style="padding: 10px 0px; border-radius: 0;text-align:center;">{{item.value}}</div>',
                scope: $scope,
                title: '选择班级',
                cssClass: 'project_alert',
            });

        };

        $scope.onSelectClass = function (classes) {
            $scope.selected.class_id = classes.key;
            $scope.selected.class_name = classes.value;
            $scope.classAlert.close();
            NoticeService.getClassBanxun($scope.selected.class_id);
        };

    });