/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('loginCtrl', function ($scope, $state, $ionicLoading,  PushService, AuthorizeService, BroadCast, UserPreference, toaster, MESSAGES, ngIntroService, Requester, $ionicPopup, $rootScope) {

        $scope.$on("$ionicView.enter", function (event, data) {
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard&&ionic.Platform.isIOS()){
                cordova.plugins.Keyboard.disableScroll(true);
            }
               
            var ua = navigator.userAgent.toLowerCase();
           
            $scope.loginModel = {
                username: '',
                phone: '',
                password: UserPreference.get('password', ''),
                userType: 't'
            };
            $scope.isWeiXin = isWeixin();
       
            if (window.location.href.indexOf('openId=') > 0) {
                $rootScope.openId = getURLParameter("openId");
                var bind = getURLParameter("isbind");
                if (bind == 1) {
                    AuthorizeService.login($scope.loginModel);
                    $state.go('tabsController.mainPage');
                    return;
                }
            } else {
                $rootScope.openId = undefined;
            }
            var username = UserPreference.get('account', '');
            console.log('username :'+username);
            if (username.length >= 12) {
                $scope.loginModel.phone = username.substr(username.length - 11, username.length);
                $scope.loginModel.userType = username.substr(0, 1).toLowerCase();
                if ($scope.loginModel.password !== '') {
                    $scope.loginModel.username = $scope.loginModel.userType + $scope.loginModel.phone;
                    if(iOSDevice()){
                        AuthorizeService.logining = false;
                        AuthorizeService.login($scope.loginModel,function(resp){},function(error){});
                    }else{
                        AuthorizeService.login($scope.loginModel);
                    }
                   
                    $state.go('tabsController.mainPage');
                }
            } else {
                username = UserPreference.get('username', '');
                if (username.length >= 12) {
                    $scope.loginModel.phone = username.substr(username.length - 11, username.length);
                    $scope.loginModel.userType = username.substr(0, 1).toLowerCase();
                    if ($scope.loginModel.password !== '') {
                        AuthorizeService.logining = false;
                        $scope.loginModel.username = $scope.loginModel.userType + $scope.loginModel.phone;
                        if(iOSDevice()){
                            AuthorizeService.login($scope.loginModel,function(resp){},function(error){});
                        }else{
                            AuthorizeService.login($scope.loginModel);
                        }
                        $state.go('tabsController.mainPage');
                    } else {
                        setTimeout(function () {
                            var IntroOptions = {
                                steps: [{
                                    element: document.querySelector('#focusItem'),
                                    intro: "<strong>改版后直接输入手机号即可登录，无需输入字母</strong>"
                                }],
                                showStepNumbers: false,
                                showBullets: false,
                                exitOnOverlayClick: false,
                                exitOnEsc: false,
                                doneLabel: '<strong style="color:green">知道了</strong>'
                            };
                            ngIntroService.setOptions(IntroOptions);
                            ngIntroService.start();
                            ngIntroService.onComplete(function () {
                                UserPreference.set('account', username);
                                UserPreference.set('username', '');
                                ngIntroService.exit();
                            });
                        }, 100);
                    }
                }
            }
           
        });
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
        
          $scope.loginModel = {
            username: '',
            phone: '',
            password: UserPreference.get('password', ''),
            userType: 't'
        };
        });



        $scope.$watch('loginModel.userType', function () {
            $scope.getChildren();
        });

        $scope.isIOS = ionic.Platform.isIOS();
        $scope.showSelectBox = function () {
            $scope.stuPopup = $ionicPopup.show({
                template: '<ion-radio ng-model="loginModel.username" ng-value="item.username" ng-repeat="item in children" ng-click="closeSelectBox($index)">{{item.classname}} {{item.name}}</ion-radio>',
                title: '请选择要登录的学生',
                scope: $scope
            });
        };

        $scope.closeSelectBox = function (i) {
            if ($scope.stuPopup)
                $scope.stuPopup.close();
            $scope.selectLabel = $scope.children[i].classname + ' ' + $scope.children[i].name;
        };

        $scope.getChildren = function () {
            console.log('usertype:'+$scope.loginModel.userType );
            if ($scope.loginModel.userType === 's' && isPhoneNumber($scope.loginModel.phone)) {
                $scope.loginModel.username = undefined;
                Requester.getChildren($scope.loginModel.phone).then(function (resp) {
                    $scope.children = resp.data;
                    if ($scope.children && $scope.children.length > 0) {
                        $scope.loginModel.username = $scope.children[0].username;
                        $scope.selectLabel = $scope.children[0].classname + ' ' + $scope.children[0].name;
                    } else
                        $scope.loginModel.username = undefined;
                });
            }
        };

        $scope.login = function () {
          // $state.go('tabsController.mainPage');

            if (this.loginForm.$valid) {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                if ($scope.loginModel.userType !== 's' || !$scope.loginModel.username)
                    $scope.loginModel.username = $scope.loginModel.userType + $scope.loginModel.phone;
               
                if(iOSDevice()){
                    AuthorizeService.logining = false;
                    AuthorizeService.login($scope.loginModel,function(data){
                        $scope.isLogin = true;
                        $ionicLoading.hide();
                        if (data.result) {
                            UserPreference.set("account", $scope.loginModel.username);
                            console.log(data);
                                UserPreference.set("password", $scope.loginModel.password);
                                if(window.cordova){
                                    PushService.init(); 
                                }
                            $state.go('tabsController.mainPage');
                        } else
                            toaster.error({
                                title: MESSAGES.LOGIN_ERROR,
                                body: data.message
                            });

                    },function(error){
                        $scope.isLogin = true;
                        $ionicLoading.hide();
                    });
                    setTimeout(function(){
                        if(!$scope.isLogin&& AuthorizeService.logining===false)
                        //toaster.warning({title: MESSAGES.LOGIN_ERROR, body: '登录超时'});
                        AuthorizeService.logining = false;
                        $ionicLoading.hide();
                    },20000);
                }else{
                    var rst = AuthorizeService.login($scope.loginModel);
                    if (rst) {
                        rst.then(function (data) {
                            if (data.result) {
                                UserPreference.set("account", $scope.loginModel.username);
                                console.log(data);
                               // if (!$scope.isWeiXin)
                                    UserPreference.set("password", $scope.loginModel.password);
                                   // UserPreference.set('userId',)
                                    if(window.cordova){
                                        PushService.init(); 
                                    }
                                $state.go('tabsController.mainPage');
                            } else
                                toaster.error({
                                    title: MESSAGES.LOGIN_ERROR,
                                    body: data.message
                                });
                        }).finally(function () {
                            $ionicLoading.hide();
                        });
                    } else
                        $ionicLoading.hide();
                }
               
            } else {
                if (this.loginForm.input_user.$error.required) {
                    toaster.warning({
                        title: MESSAGES.LOGIN_ERROR,
                        body: MESSAGES.LOGIN_NO_USERNAME
                    });
                } else if (this.loginForm.input_user.$error.pattern) {
                    toaster.warning({
                        title: MESSAGES.LOGIN_ERROR,
                        body: MESSAGES.LOGIN_USERNAME_PATTERN
                    });
                } else if (this.loginForm.input_pwd.$error.required) {
                    toaster.warning({
                        title: MESSAGES.LOGIN_ERROR,
                        body: MESSAGES.LOGIN_NO_PASSWORD
                    });
                } else if (this.loginForm.input_pwd.$error.pattern) {
                    toaster.warning({
                        title: MESSAGES.LOGIN_ERROR,
                        body: MESSAGES.LOGIN_PASSWORD_PATTERN
                    });
                } else
                    toaster.error({
                        title: MESSAGES.LOGIN_ERROR,
                        body: 'Unknown Error'
                    });
            }
        };

        $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
            $ionicLoading.hide();
        });

        //注册
        $scope.regist = function () {
            $state.go('register');
        };
    });