angular.module('app.controllers')
    .controller('bannerArticleCtrl', function ($stateParams, $scope, $state, $ionicPopup, Requester, toaster, UserPreference, $rootScope, $ionicLoading) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {

        });

        $scope.$on("$ionicView.enter", function (event, data){
            console.log('enter');
            $scope.isLike = false; //是否喜欢，初始值应从服务器获取
            $scope.bannerCode = $stateParams.code ? $stateParams.code : $rootScope.WXBannerCode;
            $scope.bannerId = $stateParams.Id ? $stateParams.Id : $rootScope.WXBannerId;
            $rootScope.WXBannerCode = $scope.bannerCode; 
            $rootScope.WXBannerId = $scope.bannerId;
            UserPreference.set('WX_BannerId', $scope.bannerId);
            $scope.article = {};
            $scope.data = {
                article: {},
                articleCommentList: [],
                bPraise: false, //是否点赞
                praiseNum: 0, //喜欢数量
                pv: 0 //阅读数量
            };
           // $scope.getArticleDetail();
           $scope.getNewArticleDetail();
           $scope.operateData2();
           


        });

        $scope.$on("$ionicView.loaded", function (event, data) {

            console.log('load');
          
        });

        //**新增接口  运营操作数据 */
        $scope.operateData2 = function(){
            Requester.getArticleOperateData2($scope.bannerCode).then(function(res){
                if (res.result) {
                    $scope.data = res.data;
                }
            });
        };

        /**新增接口 文章详情 */
        $scope.getNewArticleDetail = function(){
            Requester.getNewArticleDetail($scope.bannerCode).then(function(res){
                $scope.article = res.data;
                if (window.cordova) {
                    MobclickAgent.onEvent('app_funs_click');
                }
            });
        };

        //获取文章详情
        // $scope.getArticleDetail = function () {
        //     Requester.getArticleDetail($scope.bannerCode).then(function (res) {
        //         $scope.result = res.result;
        //         if (res.result) {
        //             $scope.data = res.data;
        //             console.log(res);
        //             //友盟统计
        //             if (window.cordova) {
        //                 MobclickAgent.onEvent('app_funs_click');
        //             }
        //         }

        //     });
        // };

        //给article点赞
        $scope.giveLikeClick = function (code) {

            Requester.giveArticleLikeOrUnlike($scope.bannerCode).then(function (res) {

                if (res.result){
                    $scope.data.bPraise = res.data.tag;
                }
                   
            }).then(function () {
                $scope.operateData2();
            });
        };

        //编辑评论
        $scope.editComments = function (code) {
            $scope.article = {};
            $ionicPopup.show({
                template: '<textarea type="text" ng-model="article.comment" style="min-height: 100px;">',
                title: '请输入评论内容',
                cssClass: 'commentClass',
                scope: $scope,
                buttons: [{
                        text: '取消'
                    },
                    {
                        text: '<b>评论</b>',
                        type: 'button-xgreen',
                        onTap: function (e) {

                            if (!$scope.article.comment) {

                                $ionicLoading.show({
                                    template: '评论内容不能为空',
                                    duration: 1500
                                });
                                e.preventDefault();
                            } else {
                                $scope.addUserComment($scope.bannerCode, $scope.article.comment);
                                return e;

                            }

                        }
                    }
                ]
            }).then(function (res) {

            });
        };

        //添加用户评论
        $scope.addUserComment = function (articleId, content) {
            Requester.giveArticleComment(articleId, content).then(function (res) {
                if (res.result) {
                    $scope.operateData2();
                } else {
                    console.log('comment failure');
                }
            });
        };

        //给用户评论点赞
        $scope.commentLike = function (commentId, index) {

            Requester.giveCommentsLikeOrUnlike(commentId).then(function (res) {
                if (res.result) {
                    console.log('like comment  succeed');
                    $scope.praiseNum++;

                    if ($scope.data.articleCommentList[index].bPraise) {
                        $scope.data.articleCommentList[index].praiseNum--;
                    } else {
                        $scope.data.articleCommentList[index].praiseNum++;
                    }
                    $scope.data.articleCommentList[index].bPraise = !$scope.data.articleCommentList[index].bPraise;



                }
            });
        };

        //查看更多文章
        $scope.goToDetailList = function () {
            $state.go('bannerArticleList');
        };
    });