angular.module('app.controllers')
.controller('attendanceRecordsCtrl', function ($scope, BroadCast, Constant, UserPreference, $state, Requester, $ionicModal, $ionicHistory, $ionicPopup, $ionicLoading, $stateParams, $ionicScrollDelegate, toaster, ngIntroService,$rootScope) {
    if (window.cordova) MobclickAgent.onEvent('app_checkin_records');
    var UIPath = '';
    if (Constant.debugMode) UIPath = 'module/';
    $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/attendanceRecordsDetail.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        modal.el.className = modal.el.className + " first-modal";
        $scope.modal = modal;
    });
    $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/chooseStudentHistoryCheckIn.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        modal.el.className = modal.el.className + " fourth-modal";
        $scope.stuModal = modal;
    });
    $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/changeHistoryCheckInRecord.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        modal.el.className = modal.el.className + " third-modal";
        $scope.editModal = modal;
    });

    $scope.loadDetail = function () {
        Requester.getDailyAttendance($scope.selected.class_id, $scope.selectDay).then(function (resp) {
            $scope.listDetailArr = [];
            $scope.listDetailArr.push(resp.data.normal);
            $scope.listDetailArr.push(resp.data.leave);
            $scope.listDetailArr.push(resp.data.late);
            $scope.listDetailArr.push(resp.data.leaveEarly);
            $scope.listDetailArr.push(resp.data.absent);
            $scope.listDetailArr.push(resp.data.tbd);
            $scope.listDetail = $scope.listDetailArr[$scope.activeIndex];
            $scope.list = angular.copy(resp.data.late);
            for (var i1 = 0; i1 < $scope.list.length; i1++) {
                $scope.list[i1].LOL = true;
            }
            for (var i = 0; i < resp.data.leaveEarly.length; i++) {
                var stu = resp.data.leaveEarly[i];
                for (var j = 0; j < $scope.list.length; j++) {
                    if (stu.studentId == $scope.list[j].studentId)
                        break;
                }
                if (j == $scope.list.length) {
                    stu.LOL = true;
                    $scope.list.push(stu);
                }
            }
            for (var i0 = 0; i0 < resp.data.normal.length; i0++) {
                resp.data.normal[i0].normal = true;
                $scope.list.push(resp.data.normal[i0]);
            }
            for (var i2 = 0; i2 < resp.data.leave.length; i2++) {
                resp.data.leave[i2].leave = true;
                $scope.list.push(resp.data.leave[i2]);
            }
            for (var i3 = 0; i3 < resp.data.absent.length; i3++) {
                resp.data.absent[i3].absent = true;
                $scope.list.push(resp.data.absent[i3]);
            }
            for (var i4 = 0; i4 < resp.data.tbd.length; i4++) {
                resp.data.tbd[i4].tbd = true;
                $scope.list.push(resp.data.tbd[i4]);
            }
            $scope.groups = [{key: 0, name: '正常', index: 4, filter: {normal: true}},
                {key: 1, name: '请假', index: 3, filter: {leave: true}},
                {key: 3, name: '旷课', index: 2, filter: {absent: true}},
                {
                    key: -1,
                    name: '迟到早退',
                    sub: [{key: 4, name: '迟到'}, {key: 5, name: '早退'}],
                    index: 1,
                    filter: {LOL: true}
                },
                {name: '待确认', index: 0, filter: {tbd: true}}
            ];
        });
    };

    $scope.openModal = function (item) {
        $scope.selectDay = item.attendDate.substr(0, 10);
        $scope.listDetail = [];
        $scope.activeIndex = 0;
        $scope.list = [];
        $scope.loadDetail();
       $scope.modal.show();
        // setTimeout(function () {
        //     if ($scope.isHeadTeacher && !UserPreference.getBoolean("ShowAttendanceRecordsTip")) {
        //         var IntroOptions = {
        //             steps: [
        //                 {
        //                     element: document.querySelector('#focusItem'),
        //                     intro: "<strong>点击这里，可对学生的考勤状态进行变更</strong></br><strong>您也可以在学生列表中点击学生信息进入修改</strong>"
        //                 }],
        //             showStepNumbers: false,
        //             showBullets: false,
        //             showButtons: false,
        //             exitOnOverlayClick: true,
        //             exitOnEsc: true
        //         };
        //         ngIntroService.setOptions(IntroOptions);
        //         ngIntroService.start();
        //         UserPreference.set("ShowAttendanceRecordsTip", true);
        //     }
        // }, 300);
    };
    $scope.closeModal = function () {
        $scope.modal.hide();
    };
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
        $scope.editModal.remove();
        $scope.stuModal.remove();
        $scope.detailModal.remove();
    });
    $scope.closeChooseModal = function () {
        $scope.stuModal.hide();
    };
    $scope.showChooseMode = function () {
        $scope.stuModal.show();
    };
    $scope.closeEditModal = function () {
        $scope.selected.resignType = {key: 0};
        $scope.editModal.hide();
        $scope.closeDetailModal();
        for (var i = 0; i < $scope.list.length; i++) {
            $scope.list[i].isChecked = false;
        }
    };
    $scope.showEditMode = function (sid) {
        if ($scope.isHeadTeacher) {
            ngIntroService.exit();
            for (var i = 0; sid && i < $scope.list.length; i++) {
                if ($scope.list[i].studentId == sid) {
                    $scope.list[i].isChecked = true;
                    break;
                }
            }
            $scope.editModal.show();
        }
    };

    $scope.toggle = function (group, length) {
        if (length > 0) {
            group.show = !group.show;
            $ionicScrollDelegate.resize();
        }
    };

    $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/checkInStudentDetail.html', {
        scope: $scope
    }).then(function (modal) {
        modal.el.className = modal.el.className + " second-modal";
        $scope.detailModal = modal;
    });
    $scope.openDetailModal = function (item) {
        ngIntroService.exit();
        $scope.detailTitle = $scope.selectDay + '学生进出校明细';
        $scope.noContentTip = '暂无进出校记录';
        $scope.detail = {
            logo: Constant.IM_USER_AVATAR
        };
        $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
        });
        Requester.getStuInOutRecords($scope.selectDay, item.studentId, $scope.selected.class_id).then(function (resp) {
            $scope.detail = {
                list: resp.data.attendRecords,
                studentName: resp.data.studentName,
                askLeave: resp.data.askLeave,
                logo: resp.data.logo,
                id: resp.data.studentId
            };
        }).finally(function () {
            $ionicLoading.hide();
        });
        $scope.detailModal.show();
    };
    $scope.closeDetailModal = function () {
        $scope.detailModal.hide();
    };

    $scope.onHeadTeacherClicked = function (leave, sid) {
        $scope.showEditMode(sid);
    };

    $scope.getBtnLabel = function () {
        return '修改';
    };

    $scope.save = function () {
        var key = '';
        if ($scope.selected.resignType.key == -1) {
            for (var i = 0; i < $scope.groups[3].sub.length; i++) {
                if ($scope.groups[3].sub[i].isChecked) {
                    key += $scope.groups[3].sub[i].key;
                    key += ',';
                }
            }
            if (key.length === 0) {
                toaster.warning({title: '请选择考勤状态', body: ''});
                return;
            } else
                key = key.substr(0, key.length - 1);
        }
        else {
            key = $scope.selected.resignType.key;
        }
        var ids = '';
        for (var j = 0; j < $scope.list.length; j++) {
            if ($scope.list[j].isChecked) {
                ids += $scope.list[j].studentId;
                ids += ',';
            }
        }
        if (ids.length > 0) {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            Requester.modifyCheckIn(key, $scope.selectDay, ids.substr(0, ids.length - 1)).then(function (resp) {
                if (resp.result) {
                    $scope.loadDetail();
                    $scope.loadData();
                    $scope.closeEditModal();
                } else
                    toaster.error({title: resp.message, body: ''});
            }).finally(function () {
                $ionicLoading.hide();
            });
        } else
            toaster.warning({title: '请选择待修改学生', body: ''});
    };

    $scope.switchTab = function (arg) {
        $scope.activeIndex = arg;
        $scope.listDetail = $scope.listDetailArr[arg];
    };

    function onSelectedDateChange() {
        var firstDay = new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), 1).getDate();
        var lastDay = new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth() + 1, 0).getDate();

        var firstDayEpoch = resetHMSM(new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), firstDay)).getTime();
        var lastDayEpoch = resetHMSM(new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), lastDay)).getTime();

        $scope.disableGoPre = (firstDayEpoch - 86400000) <= $scope.fromDate;
        $scope.disableGoNext = (lastDayEpoch + 86400000) >= $scope.toDate;
        $scope.loadData();
    }

    $scope.onSelectionChange = function () {
        $scope.isHeadTeacher = false;
        var classes = UserPreference.getObject('user').classes;
        for (var j = 0; classes && j < classes.length; j++) {
            if ($scope.selected.class_id == classes[j].id) {
                $scope.isHeadTeacher = true;
                break;
            }
        }
        onSelectedDateChange();
    };

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
        $scope.classes = UserPreference.getArray('class');
        $scope.selected = {resignType: {key: 0}};
        $scope.selected.class_id = $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].key : '';

        $scope.currentDate = resetHMSM(new Date());
        //学期始于2月和8月
        if ($scope.currentDate.getMonth() >= 1 && $scope.currentDate.getMonth() <= 6)
            $scope.fromDate = resetHMSM(new Date($scope.currentDate.getFullYear(), 1, 1)).getTime();
        else if ($scope.currentDate.getMonth() === 0)
            $scope.fromDate = resetHMSM(new Date($scope.currentDate.getFullYear() - 1, 7, 1)).getTime();
        else
            $scope.fromDate = resetHMSM(new Date($scope.currentDate.getFullYear(), 7, 1)).getTime();

        $scope.toDate = resetHMSM(new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth() + 1, 0).getDate())).getTime();

        $scope.onSelectionChange();

        $scope.jumpArg = $stateParams.day?$stateParams.day:  $rootScope.WXAttendanceRecordDay;
        if(isWeixin())
        $rootScope.WXAttendanceRecordDay =  $scope.jumpArg;
       
    });

    $scope.loadData = function () {
        $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
        });
        var date = $scope.currentDate.getFullYear() + '-' + ($scope.currentDate.getMonth() > 8 ? $scope.currentDate.getMonth() + 1 : '0' + ($scope.currentDate.getMonth() + 1));
        Requester.getMonthlyAttendance($scope.selected.class_id, date).then(function (resp) {
            if (String(resp.code) === '70000') {
                $scope.monthList = undefined;
                $scope.noCalendar = true;
            }
            else if (resp.result) {
                $scope.noCalendar = false;
                $scope.monthList = resp.data.content;
            }
        }).finally(function () {
            $scope.$broadcast('scroll.refreshComplete');
            $ionicLoading.hide();
            if ($scope.jumpArg) {
                $scope.openModal({attendDate: $scope.jumpArg});
                $scope.jumpArg = undefined;
            }
        });
    };
    $scope.prevMonth = function () {
        if ($scope.currentDate.getMonth() === 1) {
            $scope.currentDate.setFullYear($scope.currentDate.getFullYear());
        }
        $scope.currentDate.setMonth($scope.currentDate.getMonth() - 1);
        onSelectedDateChange();
    };

    $scope.nextMonth = function () {
        if ($scope.currentDate.getMonth() === 11) {
            $scope.currentDate.setFullYear($scope.currentDate.getFullYear());
        }
        $scope.currentDate.setDate(1);
        $scope.currentDate.setMonth($scope.currentDate.getMonth() + 1);
        onSelectedDateChange();
    };

    $scope.about = function () {
        $ionicPopup.alert({
            title: '考勤状态说明',
            template: '1、正常：当日有进出校记录，并且没有迟到和早退的学生</br>2、请假：当日有请假记录的学生</br>3、迟到：当日最早入校记录晚于第一节上课开始时间</br>4、早退：当日最晚出校记录早于最后一节课结束时间</br>5、旷课：当日没有打卡记录</br>6、待确认：包括以下3类情况：当日进校时间晚于出校时间；当日最后一条打卡记录是进校记录；当日第一条打卡记录晚于放学时间',
            okText: '确认',
            okType: 'btn-follower'
        });
    };

    $scope.goBack = function () {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('tabsController.mainPage');
    };

});