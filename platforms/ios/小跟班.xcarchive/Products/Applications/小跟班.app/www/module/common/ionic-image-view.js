/**
 * Created by hewz on 2018/4/2.
 */
angular.module('ionic-image-view', [])
    .provider('ionicImageView', function () {

        var config = {
            allowSave: true,
            album: 'Xgenban'
        };

        this.config = function (options) {
            angular.extend(config, options);
        };

        this.$get = ['$rootScope', '$ionicModal', '$ionicLoading', 'scrollDelegateFix', 'Constant', function ($rootScope, $ionicModal, $ionicLoading, scrollDelegateFix, Constant) {

            var provider = {};
            var $scope = $rootScope.$new();

            provider.showViewModal = function (options, urls, defaultIndex) {
                $scope.config = angular.extend({}, config, options);
                if (!urls && urls.length === 0) {
                    return;
                }
                if (!defaultIndex || defaultIndex >= urls.length || defaultIndex < 0)
                    $scope.index = 0;
                else
                    $scope.index = defaultIndex;
                $scope.image = urls[$scope.index];

                function applyImage() {
                    $scope.image = urls[$scope.index];
                }

                $scope.onSwipeLeft = function () {
                    if (!$scope.disableSwipe && $scope.index < urls.length - 1) {
                        $scope.index++;
                        applyImage();
                    }
                    else if ($scope.index === urls.length - 1)
                        $scope.closeImageModal();
                };

                $scope.onSwipeRight = function () {
                    if (!$scope.disableSwipe && $scope.index > 0) {
                        $scope.index--;
                        applyImage();
                    } else if ($scope.index === 0)
                        $scope.closeImageModal();
                };

                $scope.onRelease = function() {
                    var scrollDelegate = scrollDelegateFix.$getByHandle('Handle');
                    var zoom = scrollDelegate.getScrollPosition().zoom;
                    $scope.disableSwipe = zoom !== 1;
                };

                var UIPath = '';
                if (Constant.debugMode) UIPath = 'module/';
                $ionicModal.fromTemplateUrl(UIPath + 'common/ionic-image-view.html', {
                    scope: $scope,
                    animation: 'none'
                }).then(function (modal) {
                    $scope.modal = modal;
                    $scope.modal.show();
                });

                $scope.closeImageModal = function () {
                    $scope.modal.hide();
                    $scope.modal.remove();
                };

                function save(url) {
                    if(window.cordova){
                        cordova.plugins.photoLibrary.saveImage(url + '?ext=.jpg', $scope.config.album, function (libraryItem) {
                            $ionicLoading.show({template: '保存成功', duration: 1500});
                        }, function (err) {
                            console.log(err);
                        });
                    }
                   
                }
                $scope.saveImg2Gallery = function (url) {
                    if(window.cordova){  
                        cordova.plugins.photoLibrary.getAlbums(
                            function (result) {
                                save(url);
                            },
                            function (err) {
                                if (err.startsWith('Permission')) {
                                    cordova.plugins.photoLibrary.requestAuthorization(
                                        function () {
                                            save(url);
                                        }, function (err) {
                                            // User denied the access
                                            console.log(err);
                                        }, {
                                            read: true,
                                            write: true
                                        }
                                    );
                                }
                            }
                        );

                    }
                   
                };
            };
            

            return provider;

        }];

    });