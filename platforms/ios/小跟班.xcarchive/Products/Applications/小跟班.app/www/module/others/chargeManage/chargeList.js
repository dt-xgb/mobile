angular.module('app.controllers')
    .controller('chargeListCtrl', function ($scope, $state, UserPreference, Requester, toaster, Constant, $ionicLoading) {
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            //  $scope.list = [1, 2, 3];
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.classes = UserPreference.getArray('HeadTeacherClasses');
           
            console.log($scope.classes);
           
            $scope.selected = {
                tabIndex: 1
            };
            $scope.selected.class_id = $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].id : '';
            $scope.page = 1;
            $scope.getClassFeeProject();
        });

        //更新班级id
        $scope.loadData = function () {
            // console.log('$scope.class_id:'+ $scope.selected.class_id);
           // $scope.recordClassId = $scope.class_id;
            $scope.page = 1;
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            if ($scope.selected.tabIndex === 1) {
                $scope.getClassFeeProject();
            } else {
                $scope.getListStudentStatic();
            }

        };

        $scope.goToDetail = function (item, type) {
            console.log(item);
            if (type === 'cla') {
                $state.go('chargeDetail', {
                    type: type,
                    feeId: item.id,
                    classId: $scope.selected.class_id,
                    studentId: null,
                    title: item.feename
                });
            } else {
                $state.go('chargeDetail', {
                    type: type,
                    feeId: null,
                    clasId: null,
                    studentId: item.student.id,
                    title: item.student.student_name + '(' + item.student.phoneNumber + ')'
                });
            }

        };


        //切换tab
        $scope.selectTab = function (index) {
            $scope.selected.tabIndex = index;
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            if ($scope.selected.tabIndex === 1) {
                $scope.getClassFeeProject();
            } else {
                $scope.getListStudentStatic();
            }
        };

        //刷新
        $scope.refreshData = function () {
            $scope.page = 1;
            if ($scope.selected.tabIndex === 1) {
                $scope.getClassFeeProject();
            } else {
                $scope.getListStudentStatic();
            }
        };
        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.loadMoreClassFeeProject();
        };

        //request --班级收费统计
        $scope.getClassFeeProject = function () {
            Requester.getClassFeeProject( $scope.selected.class_id, $scope.page).then(function (resp) {
                if (resp.result) {
                    $scope.classFeeList = resp.data;
                    $ionicLoading.hide();
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
                setTimeout(function(){
                    $ionicLoading.hide();
                },5000);
            });
        };

        //加载更多
        $scope.loadMoreClassFeeProject = function () {
            Requester.getClassFeeProject( $scope.selected.class_id, $scope.page).then(function (resp) {
                if (resp.result) {
                    var data = resp.data;
                    for (var i = 0; i < data.length; i++) {
                        $scope.classFeeList.push(data[i]);
                    }
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }

                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
                setTimeout(function () {
                    $ionicLoading.hide();
                }, 3000);
            });
        };

        //学生收费统计 
        $scope.getListStudentStatic = function () {
            Requester.getListStudentStatic( $scope.selected.class_id).then(function (resp) {
                if (resp.result) {
                    $scope.stuFeeList = resp.data.content;
                    $ionicLoading.hide();
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
                setTimeout(function () {
                    $ionicLoading.hide();
                }, 10000);
            });
        };

    });