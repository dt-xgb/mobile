angular.module('app.controllers')
     .controller('infoGroupDetailCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference, $stateParams, $rootScope) {

          $scope.$on("$ionicView.beforeEnter", function (event, data) {
               $scope.classes = [];
               $scope.picUrls = [];
               $scope.itemId = $stateParams.itemId ? $stateParams.itemId : $rootScope.WXGroupInfoItemId;
               if (isWeixin()) {
                    $rootScope.WXGroupInfoItemId = $scope.itemId;
               }
               $scope.getGroupNewsInfoDetail();
          });

          $scope.$on("$ionicView.loaded", function (event, data) {

          });


          //requester
          $scope.getGroupNewsInfoDetail = function () {
               Requester.getGroupNewsInfoDetail($scope.itemId).then(function (rest) {
                    if (rest.result) {
                         $scope.detail = rest.data;
                         if(rest.data.newsImages&&rest.data.newsImages.length>0){
                              for(var i=0;i<rest.data.newsImages.length;i++)
                              $scope.picUrls.push({
                                   src: rest.data.newsImages[i].url
                              });
                         }
                         $scope.classes = $scope.detail.classNames.split(',');
                    } else {
                         // $scope.isMoreData = true;
                         toaster.warning({
                              title: "温馨提示",
                              body: rest.message
                         });
                    }
               });
          };
     });