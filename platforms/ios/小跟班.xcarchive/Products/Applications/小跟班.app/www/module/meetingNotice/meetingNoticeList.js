angular.module('app.controllers')
  .controller('meetingNoticeListCtrl', function ($scope, $ionicModal, Constant, $window, BroadCast, $ionicLoading, Requester) {

    //即将进入
    $scope.$on("$ionicView.enter", function (event, data) {

      $scope.page = 1;
      $scope.meetingNoticeList();

    });



    $scope.$on(BroadCast.CONNECT_ERROR, function () {
      $ionicLoading.hide();
      $scope.isMoreData = false;
    });

    //下拉刷新
    $scope.refreshData = function () {
      $scope.page = 1;
      $scope.meetingNoticeList();
    };

    //上拉加载
    $scope.loadMore = function () {
      $scope.page++;
      Requester.meetingNocticeList($scope.page).then(function (res) {
        if (res.result) {
          if (res.data.content && res.data.content.length > 0) {
            for (var i = 0; i < res.data.content.length; i++) {
              $scope.meetingNocticeList.push(res.content[i]);
            }
          }

          //关闭刷新
          if (!res.data.content || res.data.content.length < Constant.reqLimit) {
            $scope.isMoreData = false;
          }


        } else {
          $scope.isMoreData = false;
        }

      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    };

    $scope.showAllContent = function (meetingNotice) {
      meetingNotice.isHidden = !meetingNotice.isHidden;
    };


    //会议通知（request）
    $scope.meetingNoticeList = function () {
      Requester.meetingNocticeList($scope.page).then(function (res) {
        if (res.result) {
          console.log('get meeting list succeed');
          console.log(res);
          $scope.meetingNocticeList = res.data.content;

        } else {
          $scope.isMoreData = false;
        }
        return res;

      }).then(function (res) {
        if (res.result)
          $scope.markMeetingNoticeRead();
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    };

    //标记为已读

    $scope.markMeetingNoticeRead = function () {
      Requester.markMeetingReadStatus().then(function () {

      });
    };
    //判断字符长度是否超过一行
    $scope.judgeTextIsPassLine = function (text) {
      var result;
      var txtWidth = textWidth(text, 15);
      var screenWidth = $window.innerWidth;
      if (txtWidth >= screenWidth - 110) {
        result = true;
      } else {
        result = false;
      }

      return result;
    };

  });
