cordova.define("cordova-plugin-trtc.trtc", function(require, exports, module) {
var exec = require('cordova/exec');

function Trtc() {}

Trtc.prototype.requestVideoCall = function(params, successCallback, errorCallback) {
    exec(successCallback, errorCallback, 'Trtc', 'requestVideoCall', [params.requestId, params.debug,params.roomId, params.userId, params.userName, params.userIcon, params.userSig]);
};

Trtc.prototype.finishVideoCall = function(params, successCallback, errorCallback) {
    exec(successCallback, errorCallback, 'Trtc', 'finishVideoCall', [params.requestId, params.debug]);
};

module.exports = new Trtc();
});
