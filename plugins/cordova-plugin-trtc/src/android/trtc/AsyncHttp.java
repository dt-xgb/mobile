package hewz.plugins.trtc;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 *
 * Created by hewz on 16/10/11.
 */
public class AsyncHttp extends AsyncTask<String, Integer, Object> {
    private AsyncResponse delegate;

    public AsyncHttp(AsyncResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected Object doInBackground(String... args) {
        if (args != null && args.length == 1)
            return HttpGet(args[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        //super.onPostExecute(jsonObject);
        if (delegate != null) {
            delegate.processFinish(result);
        }
        if (result != null)
            Log.d("TRTC", result.toString());
    }

    private Object HttpGet(String reqUrl) {
        InputStream inputStream = null;
        HttpURLConnection urlConnection = null;
        Object json = null;
        try {
            URL url = new URL(Trtc.BaseUrl + reqUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");

            int i = urlConnection.getResponseCode();
            if (HttpURLConnection.HTTP_OK == i) {
                inputStream = new BufferedInputStream(urlConnection.getInputStream());
                String response = getStringFromInputStream(inputStream);
                json = new JSONTokener(response).nextValue();
                Log.e("Get Success", reqUrl);
                Log.e("Get Success", "Rst: " + response);
            } else {
                Log.e("Get Fail", "Rsp Code: " + i);
                Log.e("Get Fail", reqUrl);
            }

        } catch (Exception e) {
            Log.e("Get Exception", reqUrl);
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return json;
    }

    private String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    public interface AsyncResponse {
        void processFinish(Object output);
    }
}
