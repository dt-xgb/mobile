package hewz.plugins.trtc;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.util.Log;


import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;


public class Trtc extends CordovaPlugin {

    private static String TAG = "TRTCPlugin";

    static final String REQUEST_START = "requestVideoCall";
    static final String REQUEST_FINISH = "finishVideoCall";

    public static String BaseUrl;
    private static final String DebugBaseUrl = "https://test17.xgenban.com/sctserver/api/";

    private static String reqId;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Activity activity = this.cordova.getActivity();

        try {
            if ("1".equals(args.getString(1))) {
                BaseUrl = DebugBaseUrl;
            } else {
                BaseUrl = DebugBaseUrl.replace("test17.", "");
            }

            String req = args.getString(0);

            if (req == null) {
                callbackContext.error("Request Id is null");
                return false;
            }

            if (REQUEST_START.equals(action)) {
                ActivityManager mngr = (ActivityManager) activity.getSystemService( ACTIVITY_SERVICE );

                assert mngr != null;
                List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

                String topActivity = taskList.get(0).topActivity.getClassName();
                if (topActivity.equals("hewz.plugins.trtc.TRTCMainActivity")) {
                    Log.e(TAG, "Video view is in front");
                    return false;
                }

                Intent intent = new Intent(this.cordova.getActivity(), TRTCNewActivity.class);
                reqId = req;
                intent.putExtra("roomId", args.getInt(2));
                intent.putExtra("userId", args.getString(3));
                intent.putExtra("userName", args.getString(4));
                intent.putExtra("userIcon", args.getString(5));
                intent.putExtra("userSig", args.getString(6));
                intent.putExtra("requestId", reqId);

                if (topActivity.equals("hewz.plugins.trtc.TRTCNewActivity")) {
                    intent.setAction("VideoUpdate");
                    activity.sendBroadcast(intent);
                } else
                    activity.startActivity(intent);
            } else if (REQUEST_FINISH.equals(action) && reqId != null && reqId.equals(args.getString(0))) {
                activity.sendBroadcast(new Intent(REQUEST_FINISH));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

}