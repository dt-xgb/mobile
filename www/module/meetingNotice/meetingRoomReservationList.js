angular.module('app.controllers')
    .controller('meetingRoomReservationListCtrl', function ($scope, $ionicModal, toaster, Constant, ionicDatePicker, ionicTimePicker, Requester, $ionicLoading, BroadCast) {

        //即将进入
        $scope.$on("$ionicView.enter", function (event, data) {

            $scope.select = {};
            $scope.page = 1;
            $scope.getMyMeetingReservationList();

        });


        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $ionicLoading.hide();
            $scope.isMoreData = false;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });

        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getMyMeetingReservationList();
        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            Requester.myMeetingReservationList($scope.page).then(function (res) {
                if (res.result) {
                    if (res.data.content && res.data.content.length > 0) {
                        for (var i = 0; i < res.data.content.length; i++) {
                            $scope.meetingReservationList.push(res.content[i]);
                        }
                    }

                    //关闭刷新
                    if (!res.data.content || res.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }


                } else {
                    $scope.isMoreData = false;
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };
        //上拉加载当前会议室会议
        $scope.refreshCurrentMeetingData = function () {
            $scope.currentMeetingRoomReservation();
        };

        //会议预约列表(request)
        $scope.getMyMeetingReservationList = function () {
            Requester.myMeetingReservationList($scope.page).then(function (res) {
                if (res.result) {

                    $scope.meetingReservationList = res.data.content;


                } else {
                    $scope.isMoreData = false;
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };


        //查看当前会议室的会议(request)
        $scope.currentMeetingRoomReservation = function (meetroomId) {
            Requester.meetingRoomHaveReservationList(meetroomId, 1).then(function (res) {
                if (res.result) {
                    $scope.currentMeeting = res.data.content;
                    $scope.currentReservationModal.show();
                } else {
                    toaster.warning({
                        title: "",
                        body: res.message,
                        timeout: 1500
                    });
                }
            });
        };

        //查看会议室(request)
        $scope.getMeetingRoomsList = function () {
            Requester.getMeetingRooms().then(function (res) {
                if (res.result) {
                    $scope.meetingRooms = res.data;
                }
            });
        };
        //提交会议室预约（request）
        $scope.commitMeetingReservation = function () {
            Requester.meetingAppointment($scope.select).then(function (res) {
                if (res.result) {
                    toaster.success({
                        title: "预约成功",
                        body: "已成功预约该会议室",
                        timeout: 3000
                    });
                    $scope.closeModal();

                } else {
                    toaster.warning({
                        title: "",
                        body: res.message,
                        timeout: 2500
                    });
                }
                return res;
            }).then(function (res) {
              
                if (res.result) {
                    $scope.getMyMeetingReservationList();
                }
            });
        };


        $scope.commit = function () {
            console.log('starttime:' + $scope.select.startStemp + "--endTime:" + $scope.select.endStemp);
            if (!$scope.select.meetingRoomName) {
                toaster.warning({
                    title: "",
                    body: "您还没有选择会议室",
                    timeout: 2500
                });
                return;
            }
            if (!$scope.select.meetingDate) {
                toaster.warning({
                    title: "",
                    body: "您还没有选择会议日期",
                    timeout: 2500
                });
                return;
            }
            if (!$scope.select.startTime) {
                toaster.warning({
                    title: "",
                    body: "您还没有选择开始日期",
                    timeout: 2500
                });
                return;
            }
            if (!$scope.select.endTime) {
                toaster.warning({
                    title: "",
                    body: "您还没有选择结束日期",
                    timeout: 2500
                });
                return;
            }
            if ($scope.select.startStemp >= $scope.select.endStemp) {
                toaster.warning({
                    title: "",
                    body: "结束时间不能小于开始时间",
                    timeout: 2500
                });
                return;
            }
            $scope.commitMeetingReservation();
        };


        /**
         * meeting modal
         */
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'meetingNotice/meetingRoomReservation.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {

            $scope.modal = modal;
        });

        //meeting room modal
        $ionicModal.fromTemplateUrl(UIPath + 'meetingNotice/chooseMeetingRoom.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {

            $scope.meetingRoomModal = modal;
        });

        //current reservation modal
        $ionicModal.fromTemplateUrl(UIPath + 'meetingNotice/currentReservationList.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {

            $scope.currentReservationModal = modal;
        });

        //开始预约会议室
        $scope.openModal = function () {
            $scope.select = {};
            $scope.modal.show();
        };


        /**关闭modal */
        $scope.closeModal = function () {
            $scope.modal.hide();
        };

        $scope.closeMeetingRoomModal = function () {
            $scope.meetingRoomModal.hide();
        };
        $scope.closeCurrentReservationRoomModal = function () {
            $scope.currentReservationModal.hide();
        };

        /**弹出选择会议室的modal */
        $scope.openChooseMeetingRoomModal = function () {
            //获取会议室
            $scope.getMeetingRoomsList();
            $scope.meetingRoomModal.show();
        };

        //确定 当前选择的会议
        $scope.confirmMeetingRoom = function () {
            $scope.select.meetingRoomName = $scope.meetingRooom.site_name;
            $scope.select.meetingRoomId = $scope.meetingRooom.id;
            $scope.meetingRoomModal.hide();
        };



        /**查看当前预约 modal */
        $scope.readCurrentReservation = function (room, event) {
            event.stopPropagation();
            $scope.select.currentReservationRoom = room;
            console.log('会议室id：'+room.id);
            $scope.currentMeetingRoomReservation(room.id);


        };

        //选择会议室 
        $scope.chooseMeetingRoom = function (room, index) {

            $scope.recordIndex = index + 1;
            $scope.meetingRooom = room;
            $scope.select.meetingRoomId = room.id;

        };


        //选择会议日期
        $scope.selectMeetingDate = function () {
            $scope.textareaShow = true;
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {
                        $scope.select.meetingDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                    }


                    setTimeout(function () {
                        $scope.textareaShow = false;
                        if (window.cordova) cordova.plugins.Keyboard.close();
                    }, 100);
                }
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        //选择会议时间 1：开始时间  2：结束时间
        $scope.selectMeetingTime = function (type) {
            $scope.textareaShow = true;
            var ipObj1 = {
                callback: function (val) { //Mandatory
                    $scope.textareaShow = false;
                    if (typeof (val) === 'undefined') {

                    } else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        if (type == 1) {

                            $scope.select.startTime = time;
                            $scope.select.startStemp = val * 1000;
                            $scope.select.startDate = $scope.select.meetingDate + ' ' + time;
                        } else {
                            $scope.select.endStemp = val * 1000;
                            $scope.select.endTime = time;
                            $scope.select.endDate = $scope.select.meetingDate + ' ' + time;
                        }
                    }
                    setTimeout(function () {
                        if (window.cordova) cordova.plugins.Keyboard.close();
                    }, 100);
                },

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };


    });