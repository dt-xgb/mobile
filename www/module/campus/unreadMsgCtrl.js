/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
.controller('unreadMsgCtrl', function ($scope, $stateParams, $state, Requester,$rootScope) {

    $scope.$on("$ionicView.enter", function (event, data) {
        $scope.list = $stateParams.list?$stateParams.list:$rootScope.WXRootUnreadMsgLIst;
        if(isWeixin())
        $rootScope.WXRootUnreadMsgLIst = $scope.list; 
      
        var toRead = [];
        for (var i = 0; i < $scope.list.length; i++) {
            toRead.push({
                commentsId: $scope.list[i].id,
                newsId: $scope.list[i].newsId,
                type: $scope.list[i].type
            });
        }
        Requester.setCampusReadStatus(toRead);
    });

    $scope.goPostDetail = function (comment) {
        comment.hide = true;
        $state.go('postDetail', {comment: comment, index: 'c' + comment.id});
    };

});