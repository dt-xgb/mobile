/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('myDynamicCourseCtrl', function ($scope, BroadCast, Constant, UserPreference, $state, Requester, $ionicModal, $ionicHistory) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.userRole = UserPreference.getObject('user').rolename;
            $scope.isTeacher = (String($scope.userRole) === Constant.USER_ROLES.TEACHER);
            $scope.loadData();
        });
        if (window.cordova) MobclickAgent.onEvent('app_dynamic_course');
        $scope.loadData = function () {
            Requester.getMyDynamicCourse($scope.userRole)
                .then(function (resp) {
                    console.log(resp);
                    $scope.list = [];
                    $scope.disableChooseIDs = [];
                    for (var j = 0; j < resp.data.length; j++) {
                        var c = resp.data[j];
                        var times = c.walkingCourseSub.walkingCourse.walkingCourseTimes;
                        var timeStr = '';
                        for (var k = 0; k < times.length; k++) {
                            timeStr += (getWeekdayByIndex(times[k].weekNum % 7)+ '  ' + times[k].begintime + '-' + times[k].endtime);
                            if (k < times.length - 1)
                                timeStr += '、';
                        }
                        $scope.list.push({
                            id: c.walkingCourseSub.id,
                            subjectName:c.walkingCourseSub.walkingCourse.subject.name,
                            walkingClassName:c.walkingCourseSub.walkingClassName,
                            courseName: c.walkingCourseSub.walkingCourseName,
                            teacherName: c.walkingCourseSub.teacher.teacherName,
                            classroom: c.walkingCourseSub.site.site_name,
                            classTime: timeStr,
                            classDuration: c.walkingCourseSub.walkingCourse.beginDate.substr(0, 10) + '~' + c.walkingCourseSub.walkingCourse.endDate.substr(0, 10),
                            studentNumber: c.walkingCourseSub.stunum,
                            introduction: c.walkingCourseSub.introduction,
                            teacherLogo: c.walkingCourseSub.teacher.logo
                        });

                        $scope.disableChooseIDs.push(c.walkingCourseSub.walkingCourse.id);
                    }
                })
                .finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'electiveCourse/courseInfo.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function (item, disable) {
            if ($scope.isTeacher && disable)
                return;
            $scope.selectedCourse = item;
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.showChoosableCourse = function () {
            $state.go('chooseDynamicCourse', {disableID: $scope.disableChooseIDs});
        };

        $scope.goCheckInView = function (item) {
          
            $state.go('checkInHistory', {course: item,isWalkCourse:'yes'});
        };

        $scope.goBack = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('tabsController.mainPage');
        };

    });