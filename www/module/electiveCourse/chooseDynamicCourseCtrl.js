/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('chooseDynamicCourseCtrl', function ($scope, Constant, UserPreference, $state, Requester, $ionicModal, $ionicPopup, toaster, $ionicHistory, $stateParams,$rootScope) {

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.disableChooseIDs = $stateParams.disableID?$stateParams.disableID:$rootScope.WXDynamicCourseDisableId;
            $rootScope.WXDynamicCourseDisableId = $scope.disableChooseIDs;
            console.log($scope.disableChooseIDs);
            $scope.showEnrollBtn = true;
            $scope.userRole = UserPreference.getObject('user').rolename;
            if (String($scope.userRole) === Constant.USER_ROLES.PARENT)
                $scope.userId = UserPreference.get('DefaultChildID');
            else
                $scope.userId = UserPreference.getObject('user').id;
            $scope.selected = {};
            $scope.loadData();
        });

        $scope.onCourseChanged = function () {
            $scope.loadSubData();
        };

        $scope.loadData = function () {
            Requester.getSelectableDynamicCourse($scope.userId).then(function (resp) {
                $scope.selectableCourses = [];
                var i = 0;
                for (; i < resp.data.length; i++) {
                    var c = {
                        id: resp.data[i].id,
                        name: resp.data[i].subject.name
                    };
                    if (i === 0) {
                        $scope.selected.course = c;
                        $scope.loadSubData();
                    }
                    $scope.selectableCourses.push(c);
                }
                if (i === 0)
                    $scope.selectableSubCourses = [];
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        $scope.loadSubData = function () {
            Requester.getSubDynamicCourse($scope.selected.course.id).then(function (resp) {
                console.log(resp);
                $scope.selectableSubCourses = [];
                for (var j = 0; j < resp.data.length; j++) {
                    var c = resp.data[j];
                    var times = c.walkingCourseSub.walkingCourse.walkingCourseTimes;
                    var timeStr = '';
                    for (var k = 0; k < times.length; k++) {
                        timeStr += (getWeekdayByIndex(times[k].weekNum % 7) + times[k].begintime + '-' + times[k].endtime);
                        if (k < times.length - 1)
                            timeStr += '、';
                    }
                    var choosable = true;
                    if ($scope.disableChooseIDs) {
                        for (var i = 0; i < $scope.disableChooseIDs.length; i++) {
                            if ($scope.disableChooseIDs[i] === $scope.selected.course.id) {
                                choosable = false;
                                break;
                            }
                        }
                    }
                    if (c.walkingCourseSub.maxCapacity <= c.walkingCourseSub.stunum)
                        choosable = false;
                    $scope.selectableSubCourses.push({
                        id: c.walkingCourseSub.id,
                        choosable: choosable,
                        courseName: c.walkingCourseSub.walkingCourseName,
                        teacherName: c.walkingCourseSub.teacher.teacherName,
                        classroom: c.walkingCourseSub.site.site_name,
                        classTime: timeStr,
                        classDuration: c.walkingCourseSub.walkingCourse.beginDate.substr(0, 10) + '~' + c.walkingCourseSub.walkingCourse.endDate.substr(0, 10),
                        studentNumber: c.walkingCourseSub.stunum,
                        introduction: c.walkingCourseSub.introduction
                    });
                }
            });
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'electiveCourse/courseInfo.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function (item) {
            console.log(item);
            $scope.selectedCourse = item;
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.goBack = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('myDynamicCourse');
        };

        $scope.enroll = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: '选课报名确认',
                template: '一个走班课只能选择一个，报名成功后不可更改，确定选择这门课吗？',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    Requester.courseEnroll(parseInt($scope.userId), $scope.selectedCourse.id).then(function (resp) {
                        if (resp.result) {
                            toaster.success({title: '您已选课成功！', body: ''});
                            if (window.cordova) MobclickAgent.onEvent('app_dynamic_course_enroll');
                        }
                        else
                            toaster.error({title: resp.message, body: ''});
                    });
                }
            });
        };

    });