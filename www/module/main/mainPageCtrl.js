/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('mainPageCtrl', function ($scope, BroadCast, Constant, UserPreference, NoticeService, MESSAGES, $state, Requester, AuthorizeService, $cordovaInAppBrowser, $rootScope, ChatService, $ionicSlideBoxDelegate, $ionicPopup,$timeout, $q) {

        $scope.goFunction = function (where, args) {
            if (where) {
                if ($scope.isTeacher &&
                    (!$scope.classes || ($scope.classes && $scope.classes.length == 0)) &&
                    where != 'myClassSchedule' &&
                    where != 'equipmentRepairList' &&
                    where != 'approvalList' &&
                    where != 'assetsList' &&
                    where != 'myDynamicCourse' &&
                    where != 'meetingRoomReservationList' &&
                    where != 'meetingNoticeList' &&
                    where != 'myPortrait' &&
                    where != 'infoGroupList' &&
                    where != 'exerciseNoticeList' &&
                    where != 'hidenTroubleList' &&
                    where != 'teaReservationList' &&
                    (!args || (args && args != '11'))
                ) {
                    myAlert();
                    return;
                }

                if (args)
                    $state.go(where, {
                        type: args,
                        isWalkCourse: 'no'
                    });
                else
                    $state.go(where);
            }
        };
        $scope.goExternal = function (articleCode, articleId, url) {
            var userid = '?userId=' + UserPreference.getObject('user').id;
          
            var result = url.indexOf(articleCode);
            console.log('result:'+result);
            if (result >= 0) {
                $state.go('bannerArticle', {
                    code: articleCode,
                    Id: articleId
                });
            } else {
                // var options = {
                //     statusbar: {
                //         color: '#2bcab2'
                //     },
                //     toolbar: {
                //         height: 44,
                //         color: '#2bcab2'
                //     },
                //     title: {
                //         color: '#FFFFFF',
                //         staticText: '查看文章',
                //         showPageTitle: true
                //     },
                //     backButton: {
                //         image: 'back',
                //         imagePressed: 'back_pressed',
                //         align: 'left',
                //         event: 'backPressed'
                //     },
                //     backButtonCanClose: true
                // };
                // if (window.cordova) {
                //     cordova.ThemeableBrowser.open(url, '_blank', options).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function (e) {
                //         if (e.code === cordova.ThemeableBrowser.ERR_CRITICAL) {
                //             // TODO: Handle critical error.
                //         } else if (e.code === cordova.ThemeableBrowser.ERR_LOADFAIL) {
                //             // TODO: Image failed to load.
                //         }

                //         console.error(e.message);
                //     }).addEventListener(cordova.ThemeableBrowser.EVT_WRN, function (e) {
                //         if (e.code === cordova.ThemeableBrowser.WRN_UNDEFINED) {
                //             // TODO: Some property undefined in config.
                //         } else if (e.code === cordova.ThemeableBrowser.WRN_UNEXPECTED) {
                //             // TODO: Something strange happened. But no big deal.
                //         }

                //         console.log(e.message);
                //     });
                // }
            }

        };

        $rootScope.$on('$cordovaInAppBrowser:loadstart', function (e, event) {
            var url = event.url;
            var iOut = url.indexOf("CloseNewsTab");
            if (iOut >= 0) {
                $cordovaInAppBrowser.close();
            }
        });

        function init() {
        //   var json1 = {key1:111,key2:'aaa',key3:[11,12,13]};
        //   var json2 =  CustomParam(json1);
        //   console.log(json2);
            $timeout(function () {
               $scope.bannerList = UserPreference.getArray('MainBanners');
            }, 100);
           
            NoticeService.getMainBannerList();
            $scope.getItemList();
            $scope.currentSID = UserPreference.get('DefaultChildID');
            $scope.classes = UserPreference.getArray('class');
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            if (AuthorizeService.isLogin || Constant.debugMode) {
                var user = UserPreference.getObject('user');
                // console.log('main user ===');
                // console.log(user);
                $scope.userRole = String(user.rolename);
                $scope.isHeadTeacher = UserPreference.getBoolean('isHeadTeacher');

                $scope.babyVideoManage = user.AUTH_KINDERGARTEN_STATUS ===1;//宝宝视频权限
                $scope.covidProvent = user.authVirusProtect ===1 ; //疫情防控权限
                
                var schoolId;
                if ($scope.userRole === Constant.USER_ROLES.TEACHER) {
                    $scope.isTeacher = true;
                    $scope.isParent = false;
                    NoticeService.getDic('subject');
                    NoticeService.getDic('class');
                    schoolId = user.school.id;
                } else if ($scope.userRole === Constant.USER_ROLES.PARENT) {
                    $scope.isParent = true;
                    schoolId = UserPreference.get('DefaultSchoolID');
                    $scope.banxunHeight = '26px';
                    $scope.banxunTxt = MESSAGES.DEFAULT_BANXUN;
                     $scope.IsParameterValueAuth = user.parameterValue===1;//0： 关闭 1 打开
                    // $scope.getItemList();
                } else if ($scope.userRole === Constant.USER_ROLES.STUDENT) {
                    // $scope.getItemList();
                }
                if ($scope.isParent) {
                    var authMap = user.schauthmap;
                    for (var i = 0; i < authMap.length; i++) {
                        if ($scope.currentSID == authMap[i].id) {
                            $scope.hasCheckInPermission = authMap[i].AUTH_ATTEND === 1;
                            $scope.hasSaftSchoolPermission = authMap[i].AUTH_SAFESCHOOL === 1;
                            $scope.hasBabyCheckInPermission = authMap[i].AUTH_KINDERGARTEN === 1;
                            $scope.authOA = authMap[i].AUTH_SCHOOL_OA === 1;
                            $scope.authTeachingManage = authMap[i].AUTH_EDUCATION_MANAGE === 1;//教务管理
                            $scope.authAssetsManage = authMap[i].AUTH_ASSET_MANAGE === 1;
                            $scope.authBanPai = authMap[i].AUTH_ADTERMINAL_MANAGE === 1;
                            $scope.safeManage =  authMap[i].AUTH_SAFETY_MANAGE ===1;//安全管理
                            $scope.chargeManage =  authMap[i].AUTH_CHARGE ===1;//收费管理
                            $scope.walkCourseManage =  authMap[i].AUTH_WALK ===1;//走班管理
                            $scope.babyVideoManage = authMap[i].AUTH_KINDERGARTEN_STATUS ===1;//宝宝视频权限
                            $scope.covidProvent = authMap[i].authVirusProtect ===1 ; //疫情防控权限
                        }
                    }
                    if ($scope.hasCheckInPermission) {
                        $scope.noticeTypeList.push({
                            icon: 'img/icon/main_jstz.png',
                            name: '到校通知',
                            url: 'checkInNotice',
                            type: '99991'
                        });
                    }
                    if ($scope.hasBabyCheckInPermission) {
                        $scope.noticeTypeList.push({
                            icon: 'img/icon/main_jstz.png',
                            name: '接送通知',
                            url: 'checkInNoticeBaby',
                            type: '99995'
                        });
                    }

                   
                } else {
                    $scope.hasCheckInPermission = user.AUTH_ATTEND === 1;
                    $scope.hasSaftSchoolPermission = user.AUTH_SAFESCHOOL === 1;
                    $scope.hasBabyCheckInPermission = user.AUTH_KINDERGARTEN === 1;
                    $scope.authOA = user.AUTH_SCHOOL_OA === 1;//校园oa权限
                    $scope.authTeachingManage = user.AUTH_EDUCATION_MANAGE === 1;//教务管理权限
                    $scope.authAssetsManage = user.AUTH_ASSET_MANAGE === 1;//资产管理
                    $scope.authBanPai = user.AUTH_ADTERMINAL_MANAGE === 1;
                    $scope.safeManage = user.AUTH_SAFETY_MANAGE ===1;//安全管理
                    $scope.groupManage = user.isClassGroupingManage ===1;//分组管理员
                    $scope.chargeManage =  user.AUTH_CHARGE ===1;//收费管理
                    $scope.walkCourseManage =  user.AUTH_WALK ===1;//走班管理
                    $scope.babyVideoManage = user.AUTH_KINDERGARTEN_STATUS ===1;//宝宝视频权限
                    $scope.covidProvent = user.authVirusProtect ===1 ; //疫情防控权限

                }
                if ($scope.userRole !== Constant.USER_ROLES.STUDENT) {
                    Requester.getSchoolCardPrice(schoolId).then(function (resp) {
                        if (resp.result) {
                            var price = resp.data.price;
                            $scope.needCardFee = price > 0;
                            UserPreference.set('NeedCardFee', $scope.needCardFee);
                        }
                    });
                }
                NoticeService.getDic('category');

                $scope.loadData();


                //  是否有新的考试增加
                $scope.hasNewExam = false;
                $scope.stuId = $scope.userRole === Constant.USER_ROLES.PARENT ? UserPreference.get('DefaultChildID') : String(user.id);
                if ($scope.userRole == Constant.USER_ROLES.PARENT || $scope.userRole == Constant.USER_ROLES.STUDENT)
                    $scope.checkHasNewExam();

                // 获取未读的留言消息数目
                if ($scope.userRole === Constant.USER_ROLES.PARENT)
                    $scope.getUnreadLeaveMsgCount(user.id, $scope.currentSID);

            } else {
                setTimeout(function () {
                    init();
                }, 1000);
            }

        }

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            
            setTimeout(function () {
                $ionicSlideBoxDelegate.$getByHandle('delegateHandler').update();
                $ionicSlideBoxDelegate.$getByHandle('delegateHandler').start();
                $ionicSlideBoxDelegate.loop(true);
            }, 100);

            if (window.cordova&&ionic.Platform.isIOS()) {
                cordova.plugins.Keyboard.disableScroll(true);
            }


        });
        $scope.$on("$ionicView.enter", function (event, data) {
            init();
           
        });

        
        $scope.convertMsgtoHtml = function (msg) {
            return encodeHtml(msg);
        };

        $scope.$on("$ionicView.loaded", function (event, data) {
            $rootScope.dicReqCount = 0;
            Requester.getCampusUnread().then(function (resp) {
                if (resp.result) {
                    var count = resp.data.myCampusViewUnrendNum + resp.data.statePendingAuditNum;
                    if (count > 0) {
                        $rootScope.$broadcast(BroadCast.BADGE_UPDATE, {
                            type: 'campus',
                            count: count
                        });
                    }
                }
            });
            if (!isWeixin())
                ChatService.init();
        });

        $scope.loadData = function () {
            Requester.getUnreadTotal($scope.currentSID).then(function (resp) {
                if (resp.data) {

                    $scope.paxyBadge = 0;
                    for (var i = 0; i < resp.data.length; i++) {
                        if (String(resp.data[i].key) === '99998') {
                            $scope.xsqjBadge = resp.data[i].value>99?'...':resp.data[i].value;
                            continue;
                        } else if (String(resp.data[i].key) === '99997') {
                            $scope.paxyBadge += resp.data[i].value;
                            continue;
                        } else if (String(resp.data[i].key) === '99996') {
                            $scope.paxyBadge += resp.data[i].value;
                            continue;
                        } else if (String(resp.data[i].key) === '99990') {
                            $scope.meetingNoticeBadge = resp.data[i].value>99?'...':resp.data[i].value;
                            continue;
                        } else if (String(resp.data[i].key) === '373373') {
                            $scope.covidWarningBadge = resp.data[i].value>99?'...':resp.data[i].value;
                            continue;
                        }

                        for (var j = 0; j < $scope.noticeTypeList.length; j++) {
                            if (String(resp.data[i].key) === $scope.noticeTypeList[j].type) {
                                if (resp.data[i].value > 0 && resp.data[i].value <= 99)
                                    $scope.noticeTypeList[j].badge = resp.data[i].value;
                                else if (resp.data[i].value > 99)
                                    $scope.noticeTypeList[j].badge = '...';
                                else
                                    $scope.noticeTypeList[j].badge = undefined;
                                break;
                            }
                        }
                    }
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
            // $scope.bannerList = UserPreference.getArray('MainBanners');
             $scope.getBannerList();
            $scope.slideImageIndex = 0;

        };

        $scope.$on(BroadCast.LOGIN_RESULT_RECEIVED, function (arg, data) {
            $scope.currentSID = UserPreference.get('DefaultChildID');
            init();
        });

        $scope.$on(BroadCast.CLASS_BANXUN_REV, function (arg, data) {
            if (data.result)
                $scope.banxunTxt = data.data;
            else
                $scope.banxunTxt = MESSAGES.DEFAULT_BANXUN;
        });

        $scope.$on(BroadCast.NEW_PUSH_REV, function (arg, data) {
            $scope.loadData();
        });

        $scope.$on(BroadCast.Main_BANNER_LIST_REV,function(arg,data){
        //    console.log(' banner data');
        //    console.log(data.data);
           $scope.bannerList = data.data;
        });

        //班级列表
        $scope.$on(BroadCast.DIC_REV, function (arg, dictype) {
            if ($scope.userRole === Constant.USER_ROLES.TEACHER) {
                $scope.classes = UserPreference.getArray('class');
                // if (!$scope.classes || ($scope.classes && $scope.classes.length <= 0)) {
                //     $scope.noticeTypeList = [{
                //         icon: 'img/icon/main_campus_notification.png',
                //         name: '学校通知',
                //         url: 'noticePage',
                //         type: '11'
                //     }];
                // } else {
                //     $scope.getItemList();
                // }
            }

        });

        //获取轮播图
        $scope.getBannerList = function () {
            Requester.getTopSlides().then(function (resp) {
                $scope.bannerList = resp.data;

                //alert($scope.bannerList.length);
                UserPreference.setObject('MainBanners', $scope.bannerList);
                setTimeout(function () {
                    $ionicSlideBoxDelegate.update(); //解决图片加载不出来的问题
                    $ionicSlideBoxDelegate.loop(true); //解决滚动到最后一张再循环滚动（暂时方案）
                }, 100);

            });
        };

        //  家长查看有无未读的消息
        $scope.checkHasNewExam = function () {
            $scope.examArr = [];
            Requester.selectTestScoresList($scope.userRole, '0', 1, $scope.stuId).then(function (resp) {
                if (resp.data) {
                    $scope.examArr = resp.data.content;
                    $scope.examArr.forEach(function (exam) {
                        if (exam.readed === false) {
                            $scope.hasNewExam = true;
                        }
                    });
                }

            });
        };

        //家长获取未读学生留言消息数目
        $scope.getUnreadLeaveMsgCount = function (parentId, childId) {
            var promiseArr = [];
            $q.all(promiseArr).then(function () {
                Requester.getUnreadLeaveMsgList(parentId, childId).then(function (resp) {
                    if (resp.result) {
                       if(resp.data&&resp.data.length>0&&resp.data.length<100){
                        $scope.leaveMsgBadge = resp.data.length;
                       }else if(resp.data.length>99){
                        $scope.leaveMsgBadge = '...';
                       }else{
                        $scope.leaveMsgBadge = '';
                       }
                       
                    } else {
                        console.log('get unreaded leave msg failure');
                    }

                });
            });

        };

        $scope.showMarket = function () {
          
            $state.go('market');
        };

        $scope.getItemList = function () {
            $scope.noticeTypeList = [{
                    icon: 'img/icon/main_campus_notification.png',
                    name: '学校通知',
                    url: 'noticePage',
                    type: '11'
                },
                {
                    icon: 'img/icon/class_announcement.png',
                    name: '班级公告',
                    url: 'noticePage',
                    type: '15'
                },
                {
                    icon: 'img/icon/class_notification.png',
                    name: '作业通知',
                    url: 'noticePage',
                    type: '12'
                },
                {
                    icon: 'img/icon/main_test_results.png',
                    name: '成绩公告',
                    url: 'noticePage',
                    type: '13'
                },
                {
                    icon: 'img/icon/main_school_performance.png',
                    name: '在校表现',
                    url: 'noticePage',
                    type: '14'
                }

            ];
        };


        function myAlert() {
            $scope.customPopup = $ionicPopup.show({
                template: ' <div style="color:#222;font-size:1em;text-align:center;">您还未关联班级，请先设置班级</div>' + '<div style="width:100%;height:36px;line-height:36px;font-size:17px;text-align:center;margin-top:30px;padding-top:4px;border-top:1px solid #ddd;font-weight:500;color:#2bcab2;" ng-click="closeCustomAlert()">知道了</div>',
                title: '<p style="color:white;">温馨提示</p>',
                cssClass:  'approval_alert',//'customAlert', // String, The custom CSS class name
                scope: $scope

            });
        }

        $scope.closeCustomAlert = function () {
            $scope.customPopup.close();
        };

        //查看更多功能
        // $scope.moreTools = function(){
        //   $state.go('moreToolsList');
        // };



    });
