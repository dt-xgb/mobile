angular.module('app.controllers')
    .controller('approvalListCtrl', function ($scope, Constant, $state, Requester, UserPreference, $ionicPopup, $timeout, toaster, $rootScope) {

        $scope.$on("$ionicView.enter", function (event, data) {
            console.log('will enter');
            $scope.page = 1;
            //我的申请
            if ($scope.select.selectIndex1 == 0) {
                $scope.getApprovalApplyList();
            } else if ($scope.select.selectIndex1 == 1) {
                $scope.getWaitApprovalList();
            } else if ($scope.select.selectIndex1 == 2) {
                $scope.getApprovaledOfMineList();
            } else if ($scope.select.selectIndex1 == 3) {
                $scope.getApprovelNotifyList();
            }
        });

        $rootScope.$on('ADD_APPROVAL_SUCCEED', function (arg) {
            $scope.isMoreData = true;
            $scope.page = 1;
            $scope.getApprovalApplyList();
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            console.log('load');
            $scope.isMoreData = true;
            $scope.page = 1;
            $scope.progress = '';
            $scope.recordList = [];
            $scope.allList = [];
            $scope.passList = [];
            $scope.failList = [];
            $scope.checkList = [];
            $scope.myAllList = [];
            $scope.myPassList = [];
            $scope.myFailList = [];
            $scope.myCheckList = [];
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.select = {
                selectIndex1: 0,
                selectIndex2: '',
            };
            $scope.userId = UserPreference.getObject('user').id;


        });
        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
            if ($scope.select.selectIndex1 == 0) {
                $scope.getApprovalApplyList();
            } else if ($scope.select.selectIndex1 == 1) {
                $scope.getWaitApprovalList();
            } else if ($scope.select.selectIndex1 == 2) {
                $scope.getApprovaledOfMineList();
            } else if ($scope.select.selectIndex1 == 3) {
                $scope.getApprovelNotifyList();
            }

        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.isLoading = true;
            $scope.isMoreData = true;
            if ($scope.select.selectIndex1 == 0) {
                $scope.loadMoreMyApply();
            } else if ($scope.select.selectIndex1 == 1) {
                $scope.loadMoreWaitApprovalList();
            } else if ($scope.select.selectIndex1 == 2) {
                $scope.loadMoreApprovaledOfMineList();
            } else if ($scope.select.selectIndex1 == 3) {
                $scope.loadMoreApprovelNotifyList();
            }
        };


        //tab choose
        $scope.tab1Click = function (index1) {
            $scope.page = 1;
            $scope.select.selectIndex2 = '';
            $scope.select.selectIndex1 = index1;
            $scope.isMoreData = true;
            $scope.recordList = [];
            if ($scope.select.selectIndex1 == 0) {
                $scope.getApprovalApplyList();
            } else if ($scope.select.selectIndex1 == 1) {
                $scope.getWaitApprovalList();
            } else if ($scope.select.selectIndex1 == 2) {
                $scope.getApprovaledOfMineList();
            } else if ($scope.select.selectIndex1 == 3) {
                $scope.getApprovelNotifyList();
            }
        };

        //审批状态选择
        $scope.tab2Click = function (progress) {
            $scope.select.selectIndex2 = progress;
            $scope.progress = progress == 1 ? 'PASS' : progress == 2 ? 'APPROVAL' : progress == 3 ? 'FAIL' : '';
            $scope.page = 1;
            $scope.isMoreData = true;
            if ($scope.select.selectIndex1 == 0) {
                $scope.getApprovalApplyList();
            } else {
                $scope.getApprovaledOfMineList();
            }

        };

        //新建审批
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $scope.openModal = function () {
            $scope.sampleList = [];
            $scope.getTeacherApprovalTemplateList(); //教师审批模板
            $scope.processAlert = $ionicPopup.show({
                templateUrl: UIPath + 'approvalAdmin/processAlert.html',
                title: '<p style="color:white;">请选择审批流程</p>',
                scope: $scope,
                cssClass: 'approval_alert'
            });

        };

        //open 弹框
        $scope.showApprovalModal = function (type) {
            console.log('type:' + type);
            $scope.processAlert.close();
            $state.go('addApproval', {
                templateId: type
            });
        };


        //关闭模板弹框
        $scope.cancelChoose = function () {
            $scope.processAlert.close();
        };


        //查看审批详情
        $scope.goToDetail = function (archiveId) {
            if ($scope.select.selectIndex1 == 1) {
                return;
            }
            $state.go('approvalDetail', {
                archiveId: archiveId,
                selectIndex: $scope.select.selectIndex1
            });
        };

        $scope.getList = function (type, arr) {
            if (type === 1) {
                $scope.passList = arr;
            } else if (type === 2) {
                $scope.checkList = arr;
            } else if (type === 3) {
                $scope.failList = arr;
            } else {
                $scope.allList = arr;
            }
        };

        $scope.getRecordList = function (type) {
            if (type === 1) {
                $scope.recordList = $scope.passList;
            } else if (type === 2) {
                $scope.recordList = $scope.checkList;
            } else if (type === 3) {
                $scope.recordList = $scope.failList;
            } else {
                $scope.recordList = $scope.allList;
            }

        };


        // request-我的申请
        $scope.getApprovalApplyList = function () {
            // $scope.select.selectIndex2 = '';
            Requester.getApprovalApplyList($scope.page, $scope.select.selectIndex2, $scope.schoolId, $scope.userId).then(function (data) {
                if (data.result) {
                    $scope.getList($scope.select.selectIndex2, data.data.content);
                    $scope.getRecordList($scope.select.selectIndex2);
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //加载更多我的申请
        $scope.loadMoreMyApply = function () {
            Requester.getApprovalApplyList($scope.page, $scope.select.selectIndex2, $scope.schoolId, $scope.userId).then(function (data) {
                if (data.result) {
                    for (var i = 0; i < data.data.content.length; i++) {
                        $scope.recordList.push(data.data.content[i]);
                        if ($scope.select.selectIndex2 === 1) {
                            $scope.passList.push(data.data.content[i]);
                        } else if ($scope.select.selectIndex2 === 2) {
                            $scope.checkList.push(data.data.content[i]);
                        } else if ($scope.select.selectIndex2 === 3) {
                            $scope.failList.push(data.data.content[i]);
                        } else {
                            $scope.allList.push(data.data.content[i]);
                        }
                    }
                    //关闭刷新
                    if (!data.data.content || data.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                    $scope.getRecordList($scope.select.selectIndex2);
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //request-待我审批
        $scope.getWaitApprovalList = function () {
            Requester.getWaitApprovalList($scope.page, $scope.userId).then(function (data) {
                if (data.result) {
                    $scope.recordList = data.data.content;
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //刷新待我审批
        $scope.loadMoreWaitApprovalList = function () {
            Requester.getWaitApprovalList($scope.page, $scope.userId).then(function (data) {
                if (data.result) {
                    for (var i = 0; i < data.data.content.length; i++) {
                        $scope.recordList.push(data.data.content[i]);
                    }
                    //关闭刷新
                    if (!data.data.content || data.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        // request-经我审批
        $scope.getApprovaledOfMineList = function () {
            Requester.getApprovaledOfMineList($scope.page, $scope.select.selectIndex2, $scope.userId).then(function (data) {
                if (data.result) {
                    // $scope.getList($scope.select.selectIndex2, data.data.content);
                    // $scope.getRecordList($scope.select.selectIndex2);

                    if ($scope.select.selectIndex2 === 1) {
                        $scope.myPassList = data.data.content;
                    } else if ($scope.select.selectIndex2 === 2) {
                        $scope.myCheckList = data.data.content;
                    } else if ($scope.select.selectIndex2 === 3) {
                        $scope.myFailList = data.data.content;
                    } else {
                        $scope.myAllList = data.data.content;
                    }
                    $scope.recordList = data.data.content;

                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //加载更多经我审批
        $scope.loadMoreApprovaledOfMineList = function () {
            Requester.getApprovaledOfMineList($scope.page, $scope.select.selectIndex2, $scope.userId).then(function (data) {
                if (data.result) {
                    for (var i = 0; i < data.data.content.length; i++) {
                        $scope.recordList.push(data.data.content[i]);
                        if ($scope.select.selectIndex2 === 1) {
                            $scope.myPassList.push(data.data.content[i]);
                        } else if ($scope.select.selectIndex2 === 2) {
                            $scope.myCheckList.push(data.data.content[i]);
                        } else if ($scope.select.selectIndex2 === 3) {
                            $scope.myFailList.push(data.data.content[i]);
                        } else {
                            $scope.myAllList.push(data.data.content[i]);
                        }

                    }
                    //关闭刷新
                    if (!data.data.content || data.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                    $scope.recordList = $scope.select.selectIndex2 == 1 ? $scope.myPassList : $scope.select.selectIndex2 == 2 ? $scope.myCheckList : $scope.select.selectIndex2 == 3 ? $scope.myFailList : $scope.myAllList;

                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //request -通知我的
        $scope.getApprovelNotifyList = function () {
            Requester.getApprovelNotifyList($scope.page, $scope.userId).then(function (data) {
                if (data.result) {
                    $scope.recordList = data.data.content;
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //加载更多通知我的
        $scope.loadMoreApprovelNotifyList = function () {
            Requester.getApprovelNotifyList($scope.page, $scope.userId).then(function (data) {
                if (data.result) {
                    for (var i = 0; i < data.data.content.length; i++) {
                        $scope.recordList.push(data.data.content[i]);
                    }
                    //关闭刷新
                    if (!data.data.content || data.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //requst - 审批模板列表
        $scope.getTeacherApprovalTemplateList = function () {
            Requester.getTeacherApprovalTemplateList($scope.userId).then(function (data) {
                console.log('template list');
                console.log(data);
                if (data.result) {
                    $scope.sampleList = data.data;
                    console.log($scope.sampleList);
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
            });
        };


        //审批-审批详情
        $scope.approvalApply = function (archiveId) {
            $state.go('approvalDetail', {
                archiveId: archiveId,
                selectIndex: $scope.select.selectIndex1

            });
        };


        $scope.currentApprovalStatus = function (progress) {
            var result;
            result = progress == 'PASS' ? '通过' : progress == 'APPROVAL' ? '审批中' : progress == 'FAIL' ? '不通过' : '';
            return result;
        };

        
    });
