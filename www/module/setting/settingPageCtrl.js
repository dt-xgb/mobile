/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
.controller('settingPageCtrl', function ($scope,$state, $ionicLoading, AuthorizeService, SettingService, UserPreference, BroadCast, Constant, ngIntroService, ChatService,  Requester,$ionicHistory) {

    function getUserRoleCN(role) {
        var arr = {};
        arr[Constant.USER_ROLES.TEACHER] = '老师';
        arr[Constant.USER_ROLES.PARENT] = '家长';
        arr[Constant.USER_ROLES.STUDENT] = '学生';
        return arr[role];
        
    }

    $scope.showContent = function (i) {
        if (i === 1)
            $ionicLoading.show({template: $scope.schoolName, noBackdrop: true, duration: 1000});
        else if (i === 2)
            $ionicLoading.show({template: $scope.className, noBackdrop: true, duration: 1000});
    };

    function initViewData() {
        $scope.user = UserPreference.getObject("user");
        $scope.currentSID = UserPreference.get('DefaultChildID');
        console.log($scope.user);
        if (!$scope.user.logo || $scope.user.logo.trim() === '')
            $scope.user.logo = $scope.user.rolename===2?'img/icon/default_student.png':$scope.user.rolename===4?'img/icon/default_parent.png':'img/icon/default_teacher.png'; //Constant.IM_USER_AVATAR;
        $scope.childName = UserPreference.get('DefaultChildName');
        $scope.needCardFee = UserPreference.getBoolean('NeedCardFee');
        var authMap = $scope.user.schauthmap;
        if(authMap){
            for (var j = 0; j < authMap.length; j++){
                if ($scope.currentSID == authMap[j].id){
                    $scope.babyVideoManage = authMap[j].AUTH_KINDERGARTEN_STATUS ===1;
                }
            }
        }
       
        $scope.showOrder = $scope.needCardFee || $scope.babyVideoManage;
        $scope.role = getUserRoleCN($scope.user.rolename);
        $scope.schoolName = UserPreference.get('DefaultSchoolName');
        $scope.isHeadTeacher = UserPreference.getBoolean('isHeadTeacher');
        if ($scope.user.rolename == Constant.USER_ROLES.TEACHER) {
            $scope.classes = UserPreference.getArray('class');
            var n = '';
            for (var i = 0; i < $scope.classes.length; i++) {
                n += ($scope.classes[i].value + ' ');
            }
            $scope.className = n;
        }
        else
            $scope.className = UserPreference.get('DefaultClassName');
        $scope.isParent = String($scope.user.rolename) === Constant.USER_ROLES.PARENT;
    }

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      $scope.isWeixin = isWeixin();
       if( UserPreference.get('VersionIntroduce')){
        //console.log('exit:'+UserPreference.get('VersionIntroduce'));
        $scope.isRead = UserPreference.get('VersionIntroduce')=='UNREAD'?false:true;
       }else{
       // console.log('not exit');
        UserPreference.set('VersionIntroduce','UNREAD');
        $scope.isRead = false;
       }
        if (window.cordova&&ionic.Platform.isIOS()) {
            cordova.plugins.Keyboard.disableScroll(true);
        }
      
    });


    $scope.$on("$ionicView.enter", function (event, data) {
        initViewData();

        //获取意见反馈的条数
        $scope.getFeedbackNotreadNum();
    });

   //获取意见反馈未读的条数
   $scope.getFeedbackNotreadNum = function(){
       $scope.noReadNum = 0;
    Requester.getNoreadNewsNumber().then(function(resp){
        if(resp.result){
            console.log('获取反馈未读数成功');
            $scope.noReadNum = resp.data.size;
            
        }
    });
   };

    initViewData();
    if (($scope.user.logo === Constant.IM_USER_STU_AVATAR||$scope.user.logo === Constant.IM_USER_PAR_AVATAR||$scope.user.logo === Constant.IM_USER_TEA_AVATAR) && !UserPreference.getBoolean("ShowSettingTip")) {
        var IntroOptions = {
            steps: [
                {
                    element: document.querySelector('#focusItem'),
                    intro: "<strong>点击头像可以修改个人信息和重置密码</strong>",
                    position:'center'
                }],
            showStepNumbers: false,
            cssClass:'headPic-alert',
            showBullets: false,
            showButtons: false,
            exitOnOverlayClick: true,
            exitOnEsc: true
        };
        ngIntroService.onHintClick(function () {
            ngIntroService.exit();
        });
        ngIntroService.setOptions(IntroOptions);
        ngIntroService.start();
        UserPreference.set("ShowSettingTip", true);
    }
    $scope.isAndroid = ionic.Platform.isAndroid();

    $scope.checkUpdate = function () {
        SettingService.checkUpdate(false);
    };

    $scope.hideTip = function () {
        ngIntroService.exit();
    };

    function logout() {
        $ionicLoading.hide();
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        var currentVersion = Constant.version;
        var rst = UserPreference.getBoolean(currentVersion);
        var lastUser = UserPreference.get('username', '');
        var lastUser2 = UserPreference.get('account', '');
        var tip = UserPreference.getBoolean("ShowSettingTip");
        var stip = UserPreference.getBoolean("ShowRemindSettingTip");
        var ctip = UserPreference.getBoolean("ShowChatTip");
        var srtip = UserPreference.getBoolean("ShowAttendanceRecordsTip");
        var sttip = UserPreference.getBoolean("ShowAttendanceTodayTip");
        var readStatue =  UserPreference.get('VersionIntroduce');
        var isReadMarkArr = UserPreference.getArray('RemarkReadStatus');//人像采集介绍是否已读

        var leaveArr = [];
        //班牌留言 取
        if (String($scope.user.rolename) == Constant.USER_ROLES.PARENT){
            for (var i = 0; i < $scope.user.student.length; i++) {
                var getLeaveMsgKey = "leaveMsg_list_" + $scope.user.student[i].id;
                var getLeaveMsgList = UserPreference.getArray(getLeaveMsgKey);
                leaveArr.push(getLeaveMsgList);
            }
        }
      
        UserPreference.clear();
        UserPreference.set(currentVersion, rst);
        UserPreference.set("username", lastUser);
        UserPreference.set("account", lastUser2);
        UserPreference.set("ShowSettingTip", tip);
        UserPreference.set("ShowChatTip", ctip);
        UserPreference.set("ShowRemindSettingTip", stip);
        UserPreference.set("ShowAttendanceRecordsTip", srtip);
        UserPreference.set("ShowAttendanceTodayTip", sttip);
        UserPreference.set('VersionIntroduce',readStatue);
        UserPreference.setObject('RemarkReadStatus', isReadMarkArr);
        //在存储
        if (String($scope.user.rolename) == Constant.USER_ROLES.PARENT){
            for (var j = 0; j< $scope.user.student.length; j++) {
                var saveLeaveMsgKey = "leaveMsg_list_" + $scope.user.student[j].id;
                   UserPreference.setObject(saveLeaveMsgKey, leaveArr[j]);
            }
        }

        if(isWeixin()){
            var url = window.location.href.replace('isbind=1', 'isbind=0');
            window.location.href = url;
        }
        else {
            ChatService.logout();
        }

        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('login');
    }
   
    $scope.doLogout = function () {
        $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
        });

        AuthorizeService.logout().finally(logout());
    };

    $scope.clearCache = function () {
        $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
        });
        setTimeout(function () {
            $ionicLoading.show({
                noBackdrop: true,
                template: '清理完成'
            });
            setTimeout(function () {
                $ionicLoading.hide();
            }, 1000);
        }, 1000);
    };

    $scope.openEditView = function (content) {
        $state.go('editProfile', {content: content});
    };

    $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
        $ionicLoading.hide();
    });

    $scope.getDeviceHeight = function () {
        var result = '96px';
        $scope.isIos = ionic.Platform.isIOS() && !isWeixin();
        if ($scope.isIos) {
            if (window.screen.width >= 375 && window.screen.height >= 812) {
                result = '118px';
            } else {
                result = '96px';
            }

        } else {
            result = '96px';
        }
        return result;
    };
});