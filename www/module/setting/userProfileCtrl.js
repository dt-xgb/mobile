/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('userProfileCtrl', function ($scope, $ionicHistory, $ionicActionSheet, $ionicPopup, CameraHelper, $state, BroadCast, UserPreference, SettingService, MESSAGES, $ionicLoading, toaster, $timeout) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.user = UserPreference.getObject("user");
            console.log($scope.user);
            $scope.userRole = String($scope.user.rolename);
            if (!$scope.user.logo || $scope.user.logo.trim() === '')
                $scope.user.logo = $scope.user.rolename === 2 ? 'img/icon/default_student.png' : $scope.user.rolename === 4 ? 'img/icon/default_parent.png' : 'img/icon/default_teacher.png';//'img/icon/person.png';
        });

        $scope.openEditView = function (content) {
            $state.go('editProfile', { content: content });
        };

        $scope.goBack = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('tabsController.settingPage');
        };

        $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
            if (rst && rst.which === 'av') {
                $scope.user.logo = rst.source;
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                SettingService.editInfo('logo', rst.source.substr(rst.source.indexOf('base64,') + 7));
            }
        });

        $scope.$on(BroadCast.EDIT_INFO, function (a, rst) {
            $ionicLoading.hide();
            if (rst && rst.result)
                toaster.success({ title: MESSAGES.SAVE_SUCCESS, body: MESSAGES.INFO_UPDATED });
            else
                toaster.error({ title: MESSAGES.REMIND, body: rst.message });

        });

        $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
            $ionicLoading.hide();
        });

        $scope.selectImage = function (index) {
            if (index == 1) {
                // if(!isWeixin()){
                // var input = document.getElementById("capture");
                // input.click();
                if (window.cordova) {

                    CameraHelper.selectImage('av', { allowEdit: true });

                } else {
                    var input = document.getElementById("capture");
                    input.click();
                }
            } else {
                //人像采集
            }


        };


        $scope.onSexChange = function (sex) {
            SettingService.editInfo('sex', sex);
        };

        //web端选择文件
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result;
                    CameraHelper.cropImage($scope.testImg, 300, 300, 'gaoliangqu');
                    $scope.user.logo = $scope.testImg;
                    $ionicLoading.show({
                        noBackdrop: true,
                        template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                    });
                    SettingService.editInfo('logo', $scope.testImg.substr($scope.testImg.indexOf('base64,') + 7));
                    setTimeout(function () {
                        $ionicLoading.hide();
                    }, 10000);
                }, 100);
            };
        };


    });