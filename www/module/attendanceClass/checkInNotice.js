angular.module('app.controllers')
.controller('checkInNoticeCtrl', function ($scope, Constant, $ionicHistory, $state, Requester, UserPreference,ionicImageView, $ionicLoading) {
    //到校通知
    $scope.goBack = function () {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('tabsController.mainPage');
    };

    $scope.$on("$ionicView.enter", function (event, data) {
        var stuid = UserPreference.get('DefaultChildID');
        $scope.user =  UserPreference.getObject('user');
        console.log('user id:'+$scope.user.id);
       //$scope.events = [{class:'tm-title',content:'2018-06-14  星期四'},{class:'tm-item',content:'上午', attendDesc:'到校',label:'09:00',read:false},{class:'tm-item',content:'下午', attendDesc:'出校',label:'17:00',read:false},{class:'tm-item',content:'下午', attendDesc:'返校',label:'19:00',read:false}];
        $scope.getComeSchoolNotice(stuid);
        // Requester.getChildCheckInRecords2(stuid).then(function (resp) {
        //     $scope.events = [];
        //     var toset = [];
        //     var list = [];
        //     var day = '';
        //     // resp.data = [{isread:1,daystr:'2018-06-04',studentName:'张三',walktimestr:'12:30',weekstr:'星期一',imgUrl:'https://test17.xgenban.com/wmdp/comup/attenddir/201805/07/bre/D40E139920180507044422.jpg'},
        //     // {isread:0,daystr:'2018-06-05',studentName:'张四',walktimestr:'13:40',weekstr:'星期二',imgUrl:''},
        //     // {isread:0,daystr:'2018-06-06',studentName:'张无',walktimestr:'15:40',weekstr:'星期三',imgUrl:'https://test17.xgenban.com/wmdp/comup/attenddir/201805/07/bre/D40E139920180507044422.jpg'},
        //     // {isread:1,daystr:'2018-06-07',studentName:'张六',walktimestr:'17:30',weekstr:'星期四',imgUrl:''}];
        //     for (var i = 0; i < resp.data.length; i++) {
        //         if (resp.data[i].isread === 0)
        //             toset.push(resp.data[i]);
        //         if (day != resp.data[i].daystr) {
        //             day = resp.data[i].daystr;
        //             list.push({
        //                 class: 'tm-title',
        //                 content: day + '\t' + resp.data[i].weekstr
        //             });
        //         }
        //         list.push({
        //             class: 'tm-item',
        //             content: resp.data[i].studentName,
        //             label: resp.data[i].walktimestr,
        //             read: resp.data[i].isread !== 0,
        //             imgUrl:resp.data[i].imgUrl,
        //             sub:'打卡照片',
        //             src :'img/attendance/Pictures_Day.png'
        //         });
        //     }
        //     console.log(list);
        //     $scope.events = list;
        //     Requester.setCheckInNoticeRead(toset);
        // });
    });

    //获取到校通知
    $scope.getComeSchoolNotice = function(stuid){
        Requester.getChildCheckInRecords3(stuid).then(function (resp) {
            // console.log('到校通知列表');
            // console.log(resp.data);
            $scope.events = [];
            var toset = [];
            var list = [];
            var day = '';
            if(resp.data&&resp.data.length>0){
                for (var i = 0; i < resp.data.length; i++) {
                    //  if (resp.data[i].isread === 0)
                    //      toset.push(resp.data[i]);
                     if (day != resp.data[i].date) {
                         day = resp.data[i].date;
                         list.push({
                             class: 'tm-title',
                             content: day + '\t' + resp.data[i].week
                         });
                     }
                     for(var j=0;j<resp.data[i].notices.length;j++){
                        if (resp.data[i].notices[j].isread ==false)
                         //toset.push(resp.data[i].notices[j]);
                         toset.push({id:resp.data[i].notices[j].attendId,stuid:stuid,userid:$scope.user.id});
                        list.push({
                            class: 'tm-item',
                            content: resp.data[i].notices[j].attendName,
                            attendDesc: resp.data[i].notices[j].attendDesc,
                            label: resp.data[i].notices[j].arrTime,
                            read: resp.data[i].notices[j].isread !== 0,
                            imgUrl:resp.data[i].notices[j].imgUrl,
                            src :'img/attendance/Pictures_Day.png'
                        });
                     }
                    
                 }
            }
                 
              //   console.log(list);
                 $scope.events = list;
                 Requester.setCheckInNoticeRead(toset).then(function(resp){
                     console.log('标记到校通知为已读');
                     console.log(resp);
                 });

        }, function (error) {
            console.log('error message:' + error.data.message);
        });
    };
 
    //查看照片
    $scope.viewImg = function (url) {
        console.log(url);
        var newUrl = url.replace('/bre', '');
        if ($scope.largeImgUrl !== newUrl) {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
            });
        }
        $scope.largeImgUrl = newUrl;
        ionicImageView.showViewModal({
            allowSave: true
        }, [$scope.largeImgUrl]);
    };

});
