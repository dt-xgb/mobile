/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('todayCtrl', function ($scope, ionicTimePicker, NoticeService, Constant, BroadCast, UserPreference, ionicImageView) {
        $scope.isAllDay = UserPreference.get("TodayClassType", "");
        $scope.startTime = UserPreference.getObject("TodayClassStartTime");
        $scope.endTime = UserPreference.getObject("TodayClassEndTime");
        $scope.deviceList = [];
        $scope.selected = {};

        if (isEmptyObject($scope.startTime)) {
            $scope.startTime = {
                value: undefined,
                text: '点击设置起始时间'
            };
        }
        if (isEmptyObject($scope.endTime)) {
            $scope.endTime = {
                value: undefined,
                text: '点击设置结束时间'
            };
        }

        $scope.loadData = function (reset) {
            if (Constant.debugMode) console.log('load start ' + reset);
            if (reset)
                $scope.page = 1;
            else if (NoticeService.todayListHasMore)
                $scope.page++;
            else
                return;

            NoticeService.getTodayClass($scope.page, $scope.startTime.value, $scope.endTime.value, $scope.selected.tid);
        };

        $scope.page = 1;
        $scope.listHasMore = NoticeService.todayListHasMore;
        $scope.list = UserPreference.getArray('today_class_list_cache');
        $scope.loadData(true);

        $scope.$on(BroadCast.TODAY_CLASS_LIST_REV, function (a, rst) {
            if (rst && rst.result) {
                $scope.list = NoticeService.todayClassList;
                $scope.deviceList = NoticeService.deviceList;
                $scope.selected.tid = NoticeService.currentDeviceID;
            }
            $scope.listHasMore = NoticeService.todayListHasMore;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });

        $scope.deviceChange = function () {
            $scope.loadData(true);
        };

        $scope.optionChange = function (data) {
            UserPreference.set('TodayClassType', data);
            if (data === '') {
                $scope.startTime = {value: undefined, text: '点击设置起始时间'};
                UserPreference.setObject('TodayClassStartTime', $scope.startTime);
                $scope.endTime = {value: undefined, text: '点击设置结束时间'};
                UserPreference.setObject('TodayClassEndTime', $scope.endTime);
                $scope.loadData(true);
            }
        };

        $scope.showTimeVal = (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60));
        $scope.showTimePicker = function (arg) {
            var ipObj1 = {
                inputTime: $scope.showTimeVal,
                callback: function (val) {
                    if (typeof (val) === 'undefined') {
                        if (arg === 0)
                            $scope.startTime = {value: undefined, text: '点击设置起始时间'};
                        else
                            $scope.endTime = {value: undefined, text: '点击设置结束时间'};
                    } else {
                        $scope.showTimeVal = val;
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        if (arg === 0) {
                            $scope.startTime = {value: time + ':00', text: time};
                            UserPreference.setObject('TodayClassStartTime', $scope.startTime);
                        }
                        else {
                            $scope.endTime = {value: time + ':59', text: time};
                            UserPreference.setObject('TodayClassEndTime', $scope.endTime);
                        }

                        //page reset to 0 when time range change
                        $scope.loadData(true);
                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

        $scope.openModal = function (picture) {
            ionicImageView.showViewModal({
                allowSave: true
            }, [picture]);
        };

        $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
            $scope.$broadcast('scroll.refreshComplete');
        });
    });
