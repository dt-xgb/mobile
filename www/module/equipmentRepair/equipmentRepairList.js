angular.module('app.controllers')
    .controller('equipmentRepairListCtrl', function ($scope, $q, $ionicModal, $state, Constant, CameraHelper, $timeout, Requester, $ionicLoading, toaster, UserPreference, BroadCast) {

        $scope.$on("$ionicView.enter", function (event, data) {
            console.log('enter');
            $scope.isFixed = true;
            $scope.page = 1; //当前页面
            $scope.isMoreData = true;
            $scope.repairList = UserPreference.getArray('repair_record_list') ? UserPreference.getArray('repair_record_list') : [];
            $scope.getRepairRecordList();
            $scope.repairImgs = [];
            $scope.selected = {
                terminalAddress: '',
                orderDescribe: '',
                maintainImgs: [],
                assetNumber: ''
            };

            $scope.isWeiXin = isWeixin();
            $scope.placeholder = $scope.isWeiXin ? '资产ID，非必填' : "资产ID，非必填，支持扫码";

        });


        $scope.$on("$ionicView.loaded", function (event, data) {
            console.log('load');

        });

        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $ionicLoading.hide();
            $scope.requestCount = 0;
            $scope.isLoading = false;
            $scope.isMoreData = false;
        });

        //获取报修记录列表
        $scope.getRepairRecordList = function () {
            Requester.getRepairRecordList(0, $scope.page).then(function (res) {
                if (res.result) {
                    $scope.repairList = res.data.data.content;
                    UserPreference.setObject('repair_record_list', $scope.repairList);
                    // if ($scope.page>1 && res.data.content.length < Constant.reqLimit) {
                    //     $scope.isMoreData = false;
                    // }
                } else {
                    $scope.isMoreData = false;
                }
            }).finally(function () {

                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 0;
            $scope.getRepairRecordList($scope.selectType);
        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            Requester.getRepairRecordList($scope.selectType, $scope.page).then(function (res) {
                if (res.result) {
                    if (res.data.data.content && res.data.data.content.length > 0) {
                        res.data.data.content.forEach(function (item) {
                            $scope.repairList.push(item);
                        });
                    }
                    //关闭刷新
                    if (!res.data.data.content || res.data.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                } else {
                    $scope.isMoreData = false;
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //查看维修详情
        $scope.goToRepairDetail = function (repairRecordId) {

            $state.go('repairDetail', {
                repairRecordId: repairRecordId
            });
        };

        /**
         * modal
         */
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'equipmentRepair/postRepairInfo.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {

            $scope.modal = modal;
        });

        $scope.openModal = function () {
            $scope.requestCount = 0;
            $scope.isLoading = false;
            $scope.modal.show();
        };

        $scope.closeModal = function () {
            $scope.modal.hide();
        };

        $scope.selectImg = function () {
            //if(!isWeixin())
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        // console.log('repair imgs');
                        // console.log(resp);
                        Array.prototype.push.apply($scope.repairImgs, resp);
                        $scope.$apply();
                    } else {
                        $scope.repairImgs.push(
                            "data:image/jpeg;base64," + resp
                        );
                    }
                }, 9 - $scope.repairImgs.length);
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }

        };

        // input形式打开系统系统相册
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.repairImgs.push($scope.testImg);

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.repairImgs.splice(index, 1);
        };

        function onImageResized(resp) {
           
            $scope.selected.maintainImgs.push(resp);
        }

        //提交维修记录
        $scope.commitRepair = function () {
            var result = checkStringIsIncludeSpecialChar($scope.selected.assetNumber);
            if (!result ||($scope.selected.assetNumber&& $scope.selected.assetNumber.length > 30)){
                toaster.warning({
                    title: "提交失败",
                    body: '资产ID不存在',
                    timeout: 3000
                });
                return;
            }

            if (isEmojiCharacter($scope.selected.orderDescribe)) {
                $ionicLoading.show({
                    template: '请不要输入表情字符',
                    duration: 1500
                });
                return;
            }
            $scope.selected.maintainImgs = [];
            var promiseArr = [];
            $scope.repairImgs.forEach(function (img) {
                promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized));
            });
            $scope.isLoading = true;
           // $scope.requestCount++;
            
            $q.all(promiseArr).then(function () {

                // if ($scope.requestCount > 0) {
                    $ionicLoading.show({
                        noBackdrop: true,
                        template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                    });
                    // console.log($scope.selected);
                    Requester.addEquipmentRepairRecord($scope.selected).then(function (res) {
                        if (res.result) {
                            console.log('add repair record succeed');
                            toaster.success({
                                title: "提交成功",
                                body: "成功报修，请耐心等待管理员处理",
                                timeout: 3000
                            });
                            $ionicLoading.hide();
                            $scope.isLoading = false;
                            $scope.closeModal();
                            $scope.repairImgs = [];
                            $scope.selected = {
                                terminalAddress: '',
                                orderDescribe: '',
                                maintainImgs: []
                            };
                            $scope.requestCount = 0;
                        } else {
                            $ionicLoading.hide();
                            $scope.requestCount = 0;
                            $scope.isLoading = false;
                            toaster.warning({
                                title: "提交失败",
                                body: res.message,
                                timeout: 3000
                            });
                        }
                    }).then(function () {
                        $scope.page = 1;
                        $scope.getRepairRecordList($scope.selectType);
                    }).finally(function(){
                        setTimeout(function(){
                            $scope.isLoading = false;
                        },5000);
                    });
                
                // } else {
                //     // alert('count:'+$scope.requestCount);
                // }

            });

        };

        //扫码
        $scope.scanningView = function (event) {
            $scope.isIos = ionic.Platform.isIOS();
            if ($scope.isIos) {
                $scope.getAssetsIdByScanning();
            } else {
                var permissions = cordova.plugins.permissions;
                permissions.hasPermission(permissions.CAMERA, function (status) {
                    if (!status.hasPermission) {
                        var errorCallback = function () {};
                        permissions.requestPermission(
                            permissions.CAMERA,
                            function (status) {
                                if (!status.hasPermission) {
                                    errorCallback();
                                } else {
                                    $scope.getAssetsIdByScanning();
                                }
                            }, errorCallback);
                    } else
                        $scope.getAssetsIdByScanning();
                }, null);
            }


        };


        $scope.getAssetsIdByScanning = function () {
            window.scanning.startScanning(function (result) {
                $scope.selected.assetNumber = '';
                Requester.getAssetsIdByScan(result).then(function (res) {
                    if (res.result) {
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.selected.assetNumber = res.data;
                            });

                        }, 200);
                    } else {
                        toaster.warning({
                            title: "获取失败",
                            body: res.message,
                            timeout: 3000
                        });
                    }

                });

            }, function (error) {
                // alert('error');
            }, {});
        };

    });