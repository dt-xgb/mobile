/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('orderCtrl', function ($scope, $ionicHistory, Constant, $ionicModal, Requester, $state) {

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.loadData(true);
        });

        $scope.loadData = function (reset) {
            if (reset)
                $scope.page = 1;
            else
                $scope.page++;
            Requester.getAllOrders($scope.page).then(function (resp) {
                $scope.listHasMore = !resp.data.last;
                if (reset) {
                    $scope.orders =  resp.data.content;
                } else {
                    for (var i = 0; i < resp.data.content.length; i++)
                        $scope.orders.push(resp.data.content[i]);
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        $scope.goBack = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('tabsController.settingPage');
        };

        $scope.getStatusTxt = function (status) {
            return getOrderStatusTxt(status);
        };

        $scope.getStatusIcon = function (status) {
            return getOrderStatusIcon(status);
        };

        $scope.getChannelName = function (status) {
            return getPayChannelName(status);
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'order/orderDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $scope.openDetail = function (item) {
            console.log(item);
            $scope.detailItem = item;
            $scope.modal.show();
        };

        $scope.closeDetailView = function () {
            $scope.modal.hide();
        };

        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

    });