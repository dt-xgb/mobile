angular.module('app.controllers')
  .controller('teaReservationListCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, $ionicPopup, UserPreference) {

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      $scope.isWeiXin = isWeixin();
      $scope.page = 1;
      $scope.getTeacherReservationList();
      if (Constant.debugMode) {
        $scope.link = "https://test17.xgenban.com/adweb/?isApp=1&isWin=1&debug=true#/visitor";
      } else {
        $scope.link = "https://xgenban.com/adweb/?isApp=1&isWin=1&debug=false#/visitor";
      }


    });

    $scope.$on("$ionicView.loaded", function (event, data) {

    });

    var UIPath = '';
    if (Constant.debugMode) UIPath = 'module/';
    //去分享预约链接
    $scope.reservationLink = function () {
      $scope.sharePopup = $ionicPopup.show({
        templateUrl: UIPath + 'others/enterSchoolReservation/shareAlert.html',
        //  template:'<div><div style="text-align:center;">www.baidu.com</div><div style="height:36px;width:80px;text-align:center;">复制</div></div>',
        title: '<p >预约链接</p>',
        cssClass: 'approval_alert', //'customAlert', // String, The custom CSS class name
        scope: $scope
      });
    };
    $scope.copyOrShare = function (type) {
      //1:copy 2:share
      if (type === 1) {
        // var Url1 = document.getElementById("reservationLink");
        // Url1.select(); //选择对象  
        // var tag = document.execCommand("Copy"); //执行浏览器复制命令  
        // if (tag) {
        //   toaster.success({
        //     title: "",
        //     body: "复制成功",
        //     // timeout: 3000
        // });
        // }

        if (window.cordova) {
          cordova.plugins.clipboard.copy($scope.link, function (rest) {}, function (error) {
            alert('failure');

          });
        }
        toaster.success({
          title: "",
          body: "复制成功",
          // timeout: 3000
        });
      } else {
        if (window.cordova) {
          window.share.share($scope.link, '入校预约', 'text/plain', function (succeded) {
            //$state.go('');
            console.log('分享成功');
            // alert('succeed');
          }, function (failed) {
            alert('failed');
          });
        }
      }
      $scope.sharePopup.close();
    };

    $scope.cancelChoose = function () {
      $scope.sharePopup.close();
    };

    //查看预约详情
    $scope.goToDetail = function (item) {
      $state.go('reservationDetail', {
        itemId: item.id
      });
    };

    //下拉刷新
    $scope.refreshData = function () {
      $scope.page = 1;
      $scope.getTeacherReservationList();
    };
    //上拉加载
    $scope.loadMore = function () {
      $scope.page++;
      $scope.getMoreDataList();
    };

    //request --
    $scope.getTeacherReservationList = function () {
      Requester.getTeacherReservationList($scope.page).then(function (rest) {
        if (rest.result) {
          $scope.list = rest.data.content;
        } else {
          $scope.isMoreData = true;
          toaster.warning({
            title: "温馨提示",
            body: rest.message
          });
        }
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    };

    //request --加载更多
    $scope.getMoreDataList = function () {
      Requester.getTeacherReservationList($scope.page).then(function (rest) {
        if (rest.result) {
          var data = rest.data.content;
          for (var i = 0; i < data.length; i++) {
            $scope.list.push(data[i]);
          }
          if (!data || (data && data.length < Constant.reqLimit)) {
            $scope.isMoreData = true;
          }
        } else {
          $scope.isMoreData = true;
          toaster.warning({
            title: "温馨提示",
            body: rest.message
          });
        }
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    };
  });