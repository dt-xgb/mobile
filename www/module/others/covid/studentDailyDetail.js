angular.module('app.controllers')
    .controller('studentDailyDetailCtrl', function ($scope, Constant, Requester, toaster, UserPreference,$ionicPopup, ionicImageView) {
        $scope.selected = {
            tempHead: 36,
            tempTail: 5
        };
        $scope.tempHeads = [];
        $scope.tempTails = [];
        for(var i=35;i<=45;i++){
            $scope.tempHeads.push({
                key: i,
                value: i
            });
        }
        for(var j=0;j<=9;j++){
            $scope.tempTails.push({
                key: j,
                value: j
            });
        }

        $scope.fullScreenView = function(url){
            if(url) {
                ionicImageView.showViewModal({
                    allowSave: true
                }, [url]);
            }
        };

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.user = UserPreference.get('DefaultChildID');
          console.log('user');
            $scope.list=[];
            $scope.getStudentCheckRecords();
        });


        $scope.getStudentCheckRecords = function(){
            Requester.getStudentCheckRecords($scope.user).then(function(res){
                var data = res.data;
                var list = [];
                for(var i=0;i<data.length;i++) {
                    data[i].date = data[i].dateTime.substr(0, 10);
                    data[i].time = data[i].dateTime.substr(11, 5);

                    var j=0;
                    for (;j<list.length; j++) {
                        if(list[j].date == data[i].date) {
                            list[j].events.push(data[i]);
                            break;
                        }
                    }
                    if (j == list.length){
                        var day = {
                            date: data[i].date,
                            events: [data[i]]
                        };
                        list.push(day);
                    }
                }
                $scope.list = list;
            });
        };


        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $scope.openSelectModal = function(){
            $scope.selectModel = $ionicPopup.show({
                templateUrl: UIPath + 'others/covid/tempInput.html',
                scope: $scope,
                cssClass: 'approval_alert'
            });
        };

        $scope.cancelChoose = function(){
            $scope.selectModel.close();
        };

        $scope.submit = function(){
            Requester.reportTemp($scope.user, $scope.selected.tempHead+'.'+$scope.selected.tempTail).then(function(res){
                if(res.result) {
                    toaster.success({title: '体温上报成功!', body: '',timeout:'3000'});
                    $scope.cancelChoose();
                    $scope.getStudentCheckRecords();
                }
            });
        };
    });