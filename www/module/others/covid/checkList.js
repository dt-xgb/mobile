angular.module('app.controllers')
    .controller('covidCheckListCtrl', function ($scope, $stateParams, $state, Requester, toaster,$rootScope) {

        $scope.$on("$ionicView.enter", function (event, data) {
            var classId = $stateParams.id?$stateParams.id:$rootScope.rootWxCovidClassId;
            if(isWeixin()){
                $rootScope.rootWxCovidClassId = classId;
            }
            $scope.list = [];
            Requester.getCheckedList(classId).then(function(res){
                $scope.list = res.data.content;
            });
        });

        $scope.goDetail = function (id, name) {
            $state.go('covidCheckDetail', {id: id, stuName: name});
        };

    });