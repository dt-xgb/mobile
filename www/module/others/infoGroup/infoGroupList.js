angular.module('app.controllers')
  .controller('infoGroupListCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference) {

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      $scope.page = 1;
      $scope.getGroupNewsInfoList();
    });

    $scope.$on("$ionicView.loaded", function (event, data) {

    });
    //添加分组
    $scope.addGroup = function () {
      $state.go('addInfoGroup');
    };
    //查看详情
    $scope.goToDetail = function (item) {
      $state.go('infoGroupDetail',{itemId:item.id});
    };

    //下拉刷新
    $scope.refreshData = function () {
      $scope.page = 1;
      $scope.getGroupNewsInfoList();
    };
    //上拉加载
    $scope.loadMore = function () {
      $scope.page++;
      $scope.getMoreDataList();
    };

    //request --获取列表
    $scope.getGroupNewsInfoList = function () {
      Requester.getGroupNewsInfoList($scope.page).then(function (rest) {
        if (rest.result) {
          console.log('group info list');
          $scope.list = rest.data.content;
        } else {
           $scope.isMoreData = true;
          toaster.warning({
            title: "温馨提示",
            body: rest.message
          });
        }
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    };
    //request --加载更多
    $scope.getMoreDataList = function () {
      Requester.getGroupNewsInfoList($scope.page).then(function (rest) {
        if (rest.result) {
          var data = rest.data.content;
          for (var i = 0; i < data.length; i++) {
            $scope.list.push(data[i]);
          }
          if (!data || (data && data.length < Constant.reqLimit)) {
            $scope.isMoreData = true;
          }
        } else {
           $scope.isMoreData = true;
          toaster.warning({
            title: "温馨提示",
            body: rest.message
          });
        }
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    };

  });