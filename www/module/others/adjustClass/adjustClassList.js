angular.module('app.controllers')
    .controller('adjustClassListCtrl', function ($scope, Constant, $state, $ionicModal, Requester, UserPreference, toaster, ionicDatePicker, $ionicPopup, $ionicListDelegate) {

        $scope.$on('$ionicView.beforeEnter', function (event, data) {
            // $scope.list = [1, 2, 3, ];
            $scope.selected = {
                classId: '',
                className: '',
                courseDay1: '',
                recordDay1: '',
                courseDay2: '',
                recordDay2: '',
                section1: null,
                section2: null,
                teacherId1: UserPreference.getArray('user').id,
                teacherId2: '',
                subjectId1: null,
                subjectId2: null,
                subjectName1: '',
                subjectName2: '',
                teacherName2: '',
                sections1: [],
                sections2: [],
            };

            //获取当前日期时间戳
            $scope.currentTamp = getCurrentDateTamp();

            $scope.getAdjustCourseList();
        });



        $scope.$on('$ionicView.loaded', function (event, data) {
            $scope.classes = UserPreference.getArray('class');
        });

        //选择班级
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $scope.selectClass = function () {
            $scope.selectClassPopup = $ionicPopup.show({
                template: '<div ng-repeat="item in classes " ng-click="selectOneClass(item)" ng-style="{\'border-bottom\':$index==classes.length-1?\'none\':\' 1px solid #ddd\'}" style="padding: 10px 0px; border-radius: 0;text-align:center;">{{item.value}}</div>',
                // templateUrl: UIPath + 'others/adjustClass/selectClassAlert.html',
                title: '<div style="color:white !important;">选择班级</div>',
                cssClass: 'project_alert',
                scope: $scope,
            });

        };

        $scope.cancelChoose = function () {
            $scope.selectClassPopup.close();
        };
        $scope.selectOneClass = function (item) {
            $scope.selected.classId = item.key;
            $scope.selected.className = item.value;
            //切换班级后 重新获取节次
            if ($scope.selected.recordDay1) {
                $scope.getCourseByTeacherOneDay($scope.selected.recordDay1);
            }

            if ($scope.selected.recordDay2) {
                $scope.getCourseByClassAndRemoveThisTeacher($scope.selected.recordDay2);
            }
            $scope.selectClassPopup.close();
        };

        //选择对调日期
        $scope.selectDate = function (type) {
            if (!$scope.selected.classId) {
                toaster.warning({
                    body: '请先选择班级'
                });
                return;
            }
            setTimeout(function () {
                if (window.cordova) cordova.plugins.Keyboard.close();
                var ipObj1 = {
                    callback: function (val) {
                        if (typeof (val) !== 'undefined') {
                            if (type === 'start') {
                                var date1 = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                                var startTamp1 = Date.parse(new Date(date1.replace(/\-/g, "/"))) / 1000;
                                // console.log('satrt tamp:' + startTamp1);
                                if (startTamp1 < $scope.currentTamp) {
                                    toaster.warning({
                                        body: '参与调课的课程时间不能早于当前时间',
                                        timeout: 3000
                                    });
                                    // return;
                                } else {
                                    $scope.selected.recordDay1 = date1;
                                    $scope.getCourseByTeacherOneDay(date1);
                                }

                            } else {
                                var date2 = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                                var startTamp2 = Date.parse(new Date(date2.replace(/\-/g, "/"))) / 1000;
                                if (startTamp2 < $scope.currentTamp) {
                                    toaster.warning({
                                        body: '参与调课的课程时间不能早于当前时间',
                                        timeout: 3000
                                    });
                                } else {
                                    $scope.selected.recordDay2 = date2;
                                    $scope.getCourseByClassAndRemoveThisTeacher(date2);
                                }

                            }

                        }
                    }
                };
                ionicDatePicker.openDatePicker(ipObj1);
            }, 100);
        };


        //选择节次
        $scope.changeSection = function (type) {
            $scope.sectionArr = [];
            $scope.selectType = type;
            // console.log('section2:');
            // console.log($scope.selected.sections2);
            if (!$scope.selected.classId) {
                toaster.warning({
                    body: '请先选择班级',
                    timeout: 3000
                });
                return;
            }
            if (type === 'start') {
                if (!$scope.selected.courseDay1) {
                    toaster.warning({
                        body: '请先选择调课日期',
                        timeout: 3000
                    });
                    return;
                }
                if ($scope.selected.sections1.length === 0||!$scope.selected.sections1) {
                    toaster.warning({
                        body: '该班该时间没有课程',
                        timeout: 3000
                    });
                    return;
                }
            }

            if (type === 'end') {
                if (!$scope.selected.courseDay2) {
                    toaster.warning({
                        body: '请先选择被调课日期',
                        timeout: 3000
                    });
                    return;
                }
                if ($scope.selected.sections2.length === 0||!$scope.selected.sections2) {
                    toaster.warning({
                        body: '该班该时间没有课程',
                        timeout: 3000
                    });
                    return;
                }
            }

            $scope.sectionArr = type === "start" ? $scope.selected.sections1 : $scope.selected.sections2;


            $scope.selectSectionPopup = $ionicPopup.show({
                template: '<div ng-repeat="item in  sectionArr" ng-click="selectOneSection(item)" ng-style="{\'border-bottom\':$index==sectionArr.length-1?\'none\':\' 1px solid #ddd\'}" style="padding: 10px 0px; border-radius: 0;text-align:center;">{{item.section}}</div>',
                // templateUrl: UIPath + 'others/adjustClass/selectClassAlert.html',
                title: '<div style="color:white !important;">选择节次</div>',
                cssClass: 'project_alert',
                scope: $scope,
            });
        };

        $scope.selectOneSection = function (item) {
            if ($scope.selectType === 'start') {
                $scope.selected.section1 = item.section;
                $scope.selected.teacherId1 = item.teacherId;
                $scope.selected.subjectName1 = item.subjectName;
                $scope.selected.subjectId1 = item.subjectId;

            } else {
                $scope.selected.section2 = item.section;
                $scope.selected.teacherName2 = item.teacherName;
                $scope.selected.teacherId2 = item.teacherId;
                $scope.selected.subjectName2 = item.subjectName;
                $scope.selected.subjectId2 = item.subjectId;
            }

            $scope.selectSectionPopup.close();
        };

        //下拉刷新
        $scope.refreshData = function () {
            $scope.getAdjustCourseList();
        };
        //撤销调课通知
        $scope.cancelItem = function (item, event) {
            // console.log(event);
            event.stopPropagation();
            $ionicListDelegate.closeOptionButtons();
            $scope.cancelAdjustCourse(item.id);

        };


        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'others/adjustClass/createAdjustClassModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.adjustClassModal = modal;
        });
        //发起调课
        $scope.createAdjustClassModal = function () {
            $scope.adjustClassModal.show();
        };
        //返回
        $scope.hideAdjustClassModal = function () {
            $scope.adjustClassModal.hide();
        };

        //提交
        $scope.commit = function () {
            if (!$scope.selected.classId || !$scope.selected.courseDay1 || !$scope.selected.section1 || !$scope.selected.courseDay2 || !$scope.selected.section2) {
                toaster.warning({
                    body: '课程或对调课程不能为空',
                    timeout: 3000
                });
                return;
            }

            $scope.addCourseAdjust();

        };



        //request --获取列表
        $scope.getAdjustCourseList = function () {
            Requester.getAdjustCourseList($scope.selected.teacherId1).then(function (rest) {
                if (rest.result) {
                    // console.log('调课记录列表');
                    $scope.list = rest.data;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };
        //request -- 获取老师指定日期课表
        $scope.getCourseByTeacherOneDay = function (date1) {
            Requester.getCourseByTeacherOneDay($scope.selected).then(function (rest) {
                if (rest.result) {
                    // console.log('指定日期课程');
                    $scope.selected.courseDay1 = date1;
                    $scope.selected.sections1 = rest.data?rest.data:[];
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //request --除去操作老师以外的指定课程表
        $scope.getCourseByClassAndRemoveThisTeacher = function (date2) {
            Requester.getCourseByClassAndRemoveThisTeacher($scope.selected).then(function (rest) {
                if (rest.result) {
                    //console.log('其它老师指定日期课程');

                    $scope.selected.courseDay2 = date2;
                    $scope.selected.sections2 = rest.data? rest.data:[];

                    //console.log($scope.selected.sections2);
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //requester 撤销调课
        $scope.cancelAdjustCourse = function (itemId) {
            Requester.cancelAdjustCourse(itemId).then(function (rest) {
                if (rest.result) {
                    toaster.success({
                        title: "",
                        body: "撤销成功",
                        timeout: 3000
                    });
                    $scope.getAdjustCourseList();

                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }

            });
        };

        //requester 发起调课
        $scope.addCourseAdjust = function () {
            Requester.addCourseAdjust($scope.selected).then(function (rest) {
                if (rest.result) {
                    $scope.getAdjustCourseList();
                    toaster.success({
                        title: "",
                        body: "发起调课成功",
                        timeout: 3000
                    });
                    $scope.selected = {
                        teacherId1: UserPreference.getArray('user').id,
                        sections1: [],
                        sections2: [],
                    };
                    $scope.adjustClassModal.hide();
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };
    });