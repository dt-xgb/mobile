angular.module('app.controllers')
    .controller('evaluateStudentDetailCtrl', function ($scope, $state, Requester, $stateParams, $rootScope, toaster,UserPreference) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
          
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
 
            $scope.stuId = $stateParams.studentId ? $stateParams.studentId : $rootScope.rootWxStudentId; 
            $scope.taskId = $stateParams.taskId ? $stateParams.taskId : $rootScope.rootWxTaskId;   
            $scope.totalScore =  $stateParams.totalScore;//?$stateParams.totalScore:$rootScope.rootWxTotalScore;
           // $scope.title = $stateParams.title?$stateParams.title:$rootScope.rootWxEvaluateDetailTitle;
            if(isWeixin()){
                $scope.totalScore = $stateParams.totalScore?$stateParams.totalScore==='0'?'':$stateParams.totalScore:$rootScope.rootWxTotalScore;
                $rootScope.rootWxTotalScore = $scope.totalScore;
                $rootScope.rootWxStudentId = $scope.stuId;
                $rootScope.rootWxTaskId = $scope.taskId;
                $rootScope.rootWxEvaluateDetailTitle = $scope.title;
            }
            $scope.user = UserPreference.getObject('user');
           // console.log( ' $scope.totalScore:'+ $stateParams.totalScore );
            if($scope.user.rolename===4){
                $scope.useName =  UserPreference.get('DefaultChildName');
                $scope.userLogo = UserPreference.get('DefaultChildLogo')?UserPreference.get('DefaultChildLogo')==='null'?'img/icon/default_student.png':UserPreference.get('DefaultChildLogo'):'img/icon/default_student.png';
                $scope.userPhonenumber =  $scope.user.student[0].phoneNumber;
            }else{
                $scope.useName = $stateParams.userName;
                $scope.userLogo = $stateParams.userLogo;
                $scope.userPhonenumber = $stateParams.userPhonenumber;
            }
            $scope.title =  $scope.useName + '('+$scope.userPhonenumber+')';
            $scope.getStudentAssessDetail();

        });

        //--request 查看详情
        $scope.getStudentAssessDetail = function () {
            Requester.getStudentAssessDetail($scope.taskId, $scope.stuId).then(function (resp) {
                if (resp.result) {
                    $scope.list = resp.data;
                    //console.log($scope.list);
             
                } else {
                    toaster.error({
                        title: resp.message,
                        body: ''
                    });
                }
            });
        };

        
    });