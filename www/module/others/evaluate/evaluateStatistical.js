angular.module('app.controllers')
    .controller('evaluateStatisticalCtrl', function ($scope, Constant, $stateParams,$rootScope,$state, Requester, UserPreference, toaster, $ionicLoading, $timeout, $ionicPopup) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {

            $scope.class_id = $stateParams.classId ? $stateParams.classId : $rootScope.rootWxClassId;
            $rootScope.rootWxClassId = $scope.class_id;
            $scope.taskId = $stateParams.taskId ? $stateParams.taskId : $rootScope.rootWxTaskId;
            $rootScope.rootWxTaskId = $scope.taskId;
            $scope.assessTitle =  $stateParams.assessTitle?  $stateParams.assessTitle: $rootScope.rootWxAssessTitle;
            if(isWeixin()) $rootScope.rootWxAssessTitle = $scope.assessTitle;
            $scope.getClassStudentScoresList();
        });

        $scope.$on("$ionicView.loaded", function (event, data) {

        });

        //更新班级id
        $scope.loadData = function () {
            $scope.recordClassId = $scope.selected.class_id;

        };

        //详情
        $scope.goToDetail = function (item) {
            $state.go('evaluateStudentDetail',{studentId:item.studentId,taskId: $scope.taskId,totalScore:item.score,userName:item.studentName,userLogo:item.logo,userPhonenumber:item.phoneNumber});
        };

        //--request 评价结果列表
        $scope.getClassStudentScoresList = function () {
            Requester.getClassStudentScoresList($scope.class_id, $scope.taskId).then(function (resp) {
                if (resp.result) {
                    $scope.list = resp.data;
                    // for(var i=0;i< $scope.list.length;i++){
                       
                    //     var stu = $scope.list[i];
                    //     if (stu.score === '0'||stu.score==='0.00'||stu.score==='0.0') {
                    //         stu.score = 0;
                    //     } else if (parseInt(stu.score) > 0) {
                    //         stu.score = parseInt(stu.score);
                    //     }
                    // }      
                    //console.log($scope.list);
                   
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }

            });
        };



    });