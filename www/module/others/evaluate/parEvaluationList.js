angular.module('app.controllers')
    .controller('parEvaluationListCtrl', function ($scope, Constant, $state, Requester, UserPreference, toaster) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.selected = {};
            $scope.selected.class_id = UserPreference.get('DefaultClassID');
            $scope.stuId =  UserPreference.get('DefaultChildID');
            console.log('class id:' + $scope.selected.class_id);
            
            $scope.getMyChildAssessList();
        });

        $scope.$on("$ionicView.loaded", function (event, data) {

        });

        $scope.goToSelectEvaluateDetail = function (item) {
           var score = item.score;
           if(isWeixin()) score = item.score?item.score:'0';
            $state.go('evaluateStudentDetail',{studentId:$scope.stuId,taskId:item.taskId,totalScore:score ,title:item.title});
        };

         //下拉刷新
         $scope.refreshData = function () {
            $scope.getMyChildAssessList();
        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.loadMoreData();
        };

        //--request 家长获取学生评分列表
        $scope.getMyChildAssessList = function () {
            $scope.page = 1;
            Requester.getMyChildAssessList($scope.page,$scope.stuId).then(function (resp) {
                if (resp.result) {

                    $scope.list = resp.data.content;
                  
                } else {
                    toaster.error({
                        title: resp.message,
                        body: ''
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        // 加载更多
        $scope.loadMoreData = function () {
            Requester.getMyChildAssessList($scope.page,$scope.stuId).then(function (resp) {
                if (resp.result) {
                    var data = resp.data.content;

                    for (var i = 0; i < data.length; i++) {
                        $scope.list.push(data[i]);
                    }
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }

                } else {
                    toaster.error({
                        title: resp.message,
                        body: ''
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

    });