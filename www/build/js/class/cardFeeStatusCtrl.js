/**
 * 班级押金缴费状态
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('cardFeeStatusCtrl', ['$scope', 'BroadCast', 'Constant', 'UserPreference', '$state', 'Requester', '$ionicScrollDelegate', function ($scope, BroadCast, Constant, UserPreference, $state, Requester, $ionicScrollDelegate) {
        if (window.cordova) MobclickAgent.onEvent('app_card_fee_status');
        $scope.onSelectionChange = function () {
            $scope.loadData();
        };

        $scope.switchTab = function (index) {
            $scope.activeIndex = index;
            $scope.listFilter = {
                pay: (index === 0)
            };
        };

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.classes = UserPreference.getObject("user").classes;
            $scope.selected = {
                id: $scope.classes[0].id
            };
            $scope.switchTab(0);
            $scope.loadData();
        });

        $scope.loadData = function () {
            $scope.list = [];
            Requester.getClassCardPayStatus($scope.page, $scope.selected.id).then(function (resp) {
                if (resp.result) {
                    var list = resp.data;
                    $scope.payCount = 0;
                    $scope.unPayCound = 0;
                    for (var i = 0; i < list.length; i++) {
                        var pay = (list[i].status === 1);
                        if (pay)
                            $scope.payCount++;
                        else
                            $scope.unPayCound++;
                        $scope.list.push({
                            name: list[i].studentName,
                            phoneNumber: list[i].phoneNumber,
                            icon: list[i].logo,
                            pay: pay,
                            time: pay ? list[i].timeSuccess : ''
                        });
                    }
                    $ionicScrollDelegate.resize();
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
        };
    }]);