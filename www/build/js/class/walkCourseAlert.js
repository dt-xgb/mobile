angular.module('app.controllers')
    .controller('walkCourseAlertCtrl', ['$scope', 'Constant', 'UserPreference', '$stateParams', '$ionicModal', 'NoticeService', '$rootScope', 'toaster', 'BroadCast', '$ionicPopup', 'Requester', '$timeout', function ($scope, Constant, UserPreference, $stateParams, $ionicModal, NoticeService, $rootScope, toaster, BroadCast, $ionicPopup, Requester, $timeout) {

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.classId = $stateParams.classId;
            $scope.weekNum = $stateParams.weekNum;
            $scope.sectionNum = $stateParams.sectionNum;
            $scope.getNeccWalkCourse();
            
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.walkCourseList = [];//走班课数组
           
        });

        //获取指定节次走班课
        $scope.getNeccWalkCourse = function () {
            Requester.getWalkCourseBySection($scope.classId, $scope.weekNum ,  $scope.sectionNum ).then(function (result) {
             
                console.log(result);
                if (result.result) {
                    $scope.walkCourseList = result.data;
                }

            });
        };


    }]);

