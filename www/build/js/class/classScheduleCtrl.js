/**
 * 设置值日生和查询课表
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('classScheduleCtrl', ['$scope', '$state', 'Constant', 'UserPreference', '$stateParams', '$ionicModal', 'NoticeService', '$rootScope', 'toaster', 'BroadCast', '$ionicPopup', 'Requester', '$timeout', function ($scope, $state, Constant, UserPreference, $stateParams, $ionicModal, NoticeService, $rootScope, toaster, BroadCast, $ionicPopup, Requester, $timeout) {
        $scope.isTeacher = function () {
            return $scope.userRole == Constant.USER_ROLES.TEACHER;
        };
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
          
            $scope.currentDate = new Date().Format("yyyy-MM-dd hh:mm:ss");
            $scope.isIos = ionic.Platform.isIOS()&&!isWeixin();
            $scope.selectedDayofWeek = new Date($scope.currentDate.replace(/\s+/g, 'T').concat('.000+08:00')).getDay();
            if ($scope.selectedDayofWeek == '0') {
                // var bar = document.getElementById("day-bar");
                // bar.scrollLeft = bar.scrollWidth;
            }
            $scope.userRole = UserPreference.getObject('user').rolename;
            $scope.classes = UserPreference.getArray('class');
           // $scope.isTeacher = $scope.userRole == '3'? true :false;
            $scope.isHeadTeacher = UserPreference.getBoolean('isHeadTeacher');
            $scope.headerTeacherClassIds = UserPreference.getArray('HeadTeacherClassId');
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.statueTitle = "编辑";
            $scope.isEdit = false;

            $scope.funcType = $stateParams.type ? $stateParams.type : $rootScope.WXScheduleType;
            if (isWeixin())
                $rootScope.WXScheduleType = $scope.funcType;

            $scope.selected = {};
            $scope.bgImg = "url('../img/bg_func.png')";
            if ($scope.isTeacher()) {
                $scope.selected.class_id = $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].key : '';
                if ($scope.isHeadTeacher)
                    $scope.checkInClassIsHeadTeacher($scope.selected.class_id);

            } else
                $scope.selected.class_id = UserPreference.get('DefaultClassID', '');

            if ($scope.selected.class_id && $scope.selected.class_id !== '') {
                if ($scope.funcType == 'duty') {
                    $scope.maxScheduleDay = 7;
                    $scope.memberList = [];
                    $scope.listFilter = {};
                    $scope.weeks = [{
                        weekCnName: '周一',
                        weekEnName: 'mon'
                    },
                    {
                        weekCnName: '周二',
                        weekEnName: 'tues'
                    },
                    {
                        weekCnName: '周三',
                        weekEnName: 'wed'
                    },
                    {
                        weekCnName: '周四',
                        weekEnName: 'thur'
                    },
                    {
                        weekCnName: '周五',
                        weekEnName: 'fri'
                    }, {
                        weekCnName: '周六',
                        weekEnName: 'sat'
                    }, {
                        weekCnName: '周日',
                        weekEnName: 'sun'
                    }
                    ];
                    NoticeService.getClassDuty($scope.selected.class_id);
                    if (window.cordova) MobclickAgent.onEvent('app_duty_schedule');
                } else if ($scope.funcType == 'course') {
                    $scope.maxScheduleDay = 5;
                    $scope.scheduleTime = [];
                    $scope.scheduleList = [];
                    $scope.sectionTimes = [];
                    $scope.weeks = []; //周的数组
                    NoticeService.getClassSchedule($scope.selected.class_id);
                    if (window.cordova) MobclickAgent.onEvent('app_class_schedule');
                }
            }
            //科目列表
            $scope.projectList = [];
            $scope.projectList = UserPreference.getArray('project_list');
            $scope.projectArr = [];
            $scope.selected.class_name = $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].value : '';
            $scope.getProjectListRequest();
            $scope.selectIsOpenPermissions();//检测是否开通新高考走班
        });



        $scope.save = function () {
            for (var i = 0; i < $scope.editList.length; i++) {
                if ($scope.editMode == 'add') {
                    if ($scope.editList[i].selected) {
                        $scope.editList[i].isOnDuty = true;
                        $scope.dutyList.push($scope.editList[i]);
                        if (Constant.debugMode) console.log($scope.editList[i]);
                    }
                } else {
                    if ($scope.editList[i].selected) {
                        $scope.editList[i].isOnDuty = false;
                        for (var j = $scope.dutyList.length; j--;) {
                            if ($scope.dutyList[j].stu_id === $scope.editList[i].stu_id) {
                                $scope.dutyList.splice(j, 1);
                            }
                        }
                    }
                }
                $scope.editList[i].selected = false;
            }
            $scope.memberList = $scope.editList;
            for (var k = 0; k < $scope.dutyListAll.length; k++) {
                if ($scope.selectedDayofWeek == $scope.dutyListAll[k].day)
                    $scope.dutyListAll[k].students = $scope.dutyList;
            }
            var ids = [];
            for (var p = 0; p < $scope.dutyList.length; p++)
                ids.push($scope.dutyList[p].stu_id);
            NoticeService.updateClassMembers({
                'class_id': $scope.selected.class_id,
                'day_index': $scope.selectedDayofWeek,
                'stu_ids': ids
            });
            $scope.modal.hide();
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'class/editOnDutyStudents.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $scope.showEditMode = function (editMode) {
            $scope.editMode = editMode;
            $scope.listFilter.isOnDuty = editMode != 'add';
            $scope.selectedCount = 0;
            $scope.editList = angular.copy($scope.memberList);
           // console.log($scope.editList);
            $scope.modal.show();
        };

        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.onItemClick = function (item) {
            if (item.selected)
                $scope.selectedCount--;
            else
                $scope.selectedCount++;
            item.selected = !item.selected;
        };

        //选择对应的班级
        $scope.onSelectClass = function (classes) {

            if ($scope.selected.class_id && $scope.selected.class_id !== '') {
                if ($scope.funcType === 'duty') {
                    $scope.selected.class_id = classes.key;
                    $scope.selected.class_name = classes.value;
                    $scope.memberList = [];
                    $scope.listFilter = {};
                    NoticeService.getClassDuty($scope.selected.class_id);
                    $scope.classAlert.close();
                } else if ($scope.funcType === 'course') {

                    if ($scope.isEdit) {
                        //是否是编辑状态
                        $scope.saveAlert = $ionicPopup.show({
                            template: '<div>您编辑的课程信息还没有保存，是否保存</div>',
                            title: '温馨提示',
                            scope: $scope,
                            buttons: [{
                                text: '取消',
                                type: 'button-cancel',
                                onTap: function (e) {
                                    $scope.projectArr = []; //保存成功 清空数组
                                    $scope.selected.class_id = classes.key;
                                    $scope.selected.class_name = classes.value;
                                    $scope.checkInClassIsHeadTeacher($scope.selected.class_id);
                                    NoticeService.getClassSchedule($scope.selected.class_id);
                                    $scope.classAlert.close();
                                }
                            },
                            {
                                text: '<b>保存</b>',
                                type: 'button-xgreen',
                                onTap: function (e) {
                                    //保存课程表
                                    Requester.fixScheduleProject($scope.selected.class_id, $scope.projectArr).then(function (rest) {
                                        if (rest.result) {
                                            $scope.isEdit = false;
                                            $scope.projectArr = []; //保存成功 清空数组
                                            $scope.selected.class_id = classes.key;
                                            $scope.selected.class_name = classes.value;
                                            $scope.checkInClassIsHeadTeacher($scope.selected.class_id);
                                            NoticeService.getClassSchedule($scope.selected.class_id);
                                            $scope.classAlert.close();
                                            toaster.success({
                                                title: "保存成功",
                                                body: "修改课表成功"
                                            });
                                        }
                                    }).then(function () {

                                    });

                                }
                            }
                            ]
                        });
                    } else {
                        $scope.selected.class_id = classes.key;
                        $scope.selected.class_name = classes.value;
                        $scope.checkInClassIsHeadTeacher($scope.selected.class_id);
                        NoticeService.getClassSchedule($scope.selected.class_id);
                        $scope.classAlert.close();
                    }


                }
                $scope.refreshData();
            }


        };

        //弹出班级选择框
        $scope.onSelectionChange = function (item) {

            $scope.classAlert = $ionicPopup.show({
                template: '<div ng-repeat="item in classes " ng-click="onSelectClass(item)" ng-style="{\'border-bottom\':$index==classes.length-1?\'none\':\' 1px solid #ddd\'}" style="padding: 10px 0px; border-radius: 0;text-align:center;">{{item.value}}</div>',
                scope: $scope,
                title: '选择班级',
                cssClass: 'project_alert',
            });

            console.log($scope.selected.class_id);


        };

        $scope.getClassTime = function (index) {
            for (var i = 0; i < $scope.sectionTimes.length; i++) {
                if ($scope.sectionTimes[i].sectionNum == index)
                    return $scope.sectionTimes[i].onTime + ' - ' + $scope.sectionTimes[i].offTime;
            }
            return ' ';
        };

        $scope.getClassInterval = function(index){
            for (var i = 0; i < $scope.sectionTimes.length; i++) {
                if ($scope.sectionTimes[i].sectionNum == index)
                    return $scope.sectionTimes[i].sectionStatus===0?'上午':$scope.sectionTimes[i].sectionStatus===1?'下午':'晚上';
            }
            return '';
        } ;

        $scope.refreshData = function(){
            $scope.$broadcast('scroll.refreshComplete');
        };
        function getDutyLists() {
            for (var i = 0; i < $scope.dutyListAll.length; i++) {
                if ($scope.selectedDayofWeek == $scope.dutyListAll[i].day)
                    $scope.dutyList = $scope.dutyListAll[i].students;
            }
            for (var j = 0; j < $scope.memberList.length; j++) {
                var member = $scope.memberList[j];
                member.isOnDuty = false;

                for (var k = 0; k < $scope.dutyList.length; k++) {
                    if (member.stu_id == $scope.dutyList[k].stu_id) {
                        member.isOnDuty = true;
                        break;
                    }
                }
            }

            //$scope.$apply();
        }

        $scope.go2Day = function (day) {
            // $scope.selectedDayofWeek = day;
            $scope.selectedDayofWeek = day === 7 ? '0' : String(day);
            if ($scope.funcType == 'duty') {
                getDutyLists();
            }
        };

        $scope.$on(BroadCast.CLASS_MEMBER_REV, function (arg, data) {
            if (data.result) {
                $scope.memberList = data.data;
                //getDutyLists();
            } else
                toaster.error({
                    title: data.message,
                    body: ''
                });
        });

        $scope.$on(BroadCast.CLASS_DUTY_REV, function (arg, data) {
            if (data.result) {
                $scope.dutyListAll = data.data;
                console.log('duty data');
                console.log( $scope.dutyListAll);
                setTimeout(function(){
                    getDutyLists();
                },100);
               
                setTimeout(function(){
                    NoticeService.getClassMembers($scope.selected.class_id);
                },100);
               
               
            } else
                toaster.error({
                    title: data.message,
                    body: ''
                });
        });

        $scope.$on(BroadCast.CLASS_SCHEDULE_REV, function (arg, data) {
            if (data.result) {
                $scope.sectionTimes = data.data.sectionTimes;
                $scope.AMAcount = 0; //上午节次数
                $scope.PMAcount = 0; //下午节次数
                for (var l = 0; l < $scope.sectionTimes.length; l++) {
                    var block = $scope.sectionTimes[l];
                    if (block.sectionStatus === 0) {
                        $scope.AMAcount++;
                    } else if (block.sectionStatus === 1) {
                        $scope.PMAcount++;
                    } else if (block.sectionStatus === 2) {
                        $scope.EMAcount++;
                    }
                }
                var list = data.data.courses;
                $scope.scheduleList = [];
                $scope.maxScheduleDay = data.data.maxWorkDays;
                $scope.maxSectionNum = data.data.maxSectionNum;
                $scope.weeks = []; //清空数组
                for (var i = 1; i <= $scope.maxScheduleDay; i++) {
                    for (var j = 1; j <= $scope.maxSectionNum; j++) {
                        $scope.scheduleList.push({
                            'weekNum': i,
                            'sectionNum': j,
                            'subjectName': '___'
                        });
                    }

                    $scope.weeks.push({
                        weekCnName: i == 1 ? '周一' : i == 2 ? '周二' : i == 3 ? '周三' : i == 4 ? '周四' : i == 5 ? '周五' : i == 6 ? '周六' : '周日',
                        weekEnName: i == 1 ? 'mon' : i == 2 ? 'tues' : i == 3 ? 'wed' : i == 4 ? 'thur' : i == 5 ? 'fri' : i == 6 ? 'sat' : 'sun'
                    });
                }

                for (var k = 0; k < list.length; k++) {
                    var day = list[k].weekNum;
                    var section = list[k].sectionNum;
                    if ($scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1]) {
                        $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].subjectName = list[k].subjectName;
                        $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].teacherName = list[k].teacherName;
                        $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].walkingNcee = list[k].walkingNcee;
                        $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].walkingClassName = list[k].walkingClassName;
                        $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].walkingSubName = list[k].walkingSubName;
                        $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].siteName = list[k].siteName;
                        $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].className = list[k].className;
                    }
                }
            } else
                toaster.error({
                    title: data.message,
                    body: ''
                });
        });

        $scope.$on(BroadCast.CLASS_DUTY_MODIFIED, function (arg, data) {
            console.log('update duty succeed');
            if (!data.result)
                toaster.error({
                    title: data.message,
                    body: ''
                });
        });

        $scope.selectWeekDay = function (index) {
            var result;
            result = index == 7 ? 0 : index;
            return result;
        };


        //进入课程页面就获取科目列表
        $scope.getProjectListRequest = function () {
            Requester.getProjectList($scope.selected.class_id).then(function (res) {
                if (res.result) {
                    $scope.projectList = res.data;
                    UserPreference.setObject('project_list', res.data);

                } else {

                }

            });
        };
        //编辑课程表
        $scope.editSchedule = function () {

            //$scope.isEdit = !$scope.isEdit;

            if (!$scope.isEdit) {
                $scope.isEdit = true;

            } else {
                //保存课程表
                Requester.fixScheduleProject($scope.selected.class_id, $scope.projectArr).then(function (rest) {
                    if (rest.result) {
                        $scope.isEdit = false;
                        $scope.projectArr = []; //保存成功 清空数组
                        toaster.success({
                            title: "保存成功",
                            body: "修改课表成功"
                        });
                    }
                }).then(function () {
                    //获取课程列表
                    NoticeService.getClassSchedule($scope.selected.class_id);
                });
            }
        };

        //获取学科列表
        $scope.getProjectList = function (project) {
            console.log(project);
            Requester.checkExistWalkCourse($scope.selected.class_id, project.weekNum, project.sectionNum).then(function (res) {
                if (res.result) {
                    if (res.data.exist) {
                        var myPopup = $ionicPopup.show({
                            template: '<div style="line-height:60px;height:60px;">该节次有走班课还未结束，不能更改</div>',
                            title: '<p style="color:white;">温馨提示</p>',
                            cssClass: 'project_alert',

                        });

                        $timeout(function () {
                            myPopup.close();
                        }, 2500);
                    } else {
                        $scope.selectProject = project;
                        $scope.projectPopup = $ionicPopup.show({
                            template: '<img src="img/repair/close.png" style="float:right;position:absolute;top:12px;right:12px;width:15px;height:15px;" ng-click="cancelChoose()"><div ng-repeat="item in projectList" ng-click="onProjectSelected(item,selectProject)" style="padding: 10px 0px; border-radius: 0;text-align:center;border-bottom:1px solid #ddd;">{{item.name}}</div>',
                            title: '<p style="color:white;">选择科目</p>',
                            cssClass: 'project_alert',
                            scope: $scope,
                        });
                    }

                } else {
                    console.log('check walk course failure');

                }
            });



        };


        //选择完科目后
        $scope.onProjectSelected = function (item, project) {

            $scope.scheduleList.forEach(function (resultProject) {
                if ($scope.selectWeekDay(resultProject.weekNum) == $scope.selectedDayofWeek && resultProject.sectionNum == $scope.selectProject.sectionNum) {
                    resultProject.subjectName = item.name;
                    return;
                }
            });

            if ($scope.projectArr && $scope.projectArr.length > 0) {
                $scope.projectArr.forEach(function (sunbjectItem) {
                    if (sunbjectItem.classId == $scope.selected.class_id && sunbjectItem.sectionNum == $scope.selectProject.sectionNum && $scope.selectWeekDay(sunbjectItem.weekNum) == $scope.selectWeekDay($scope.selectProject.weekNum)) {
                        //在这些都相等情况下
                        if (sunbjectItem.subjectId == item.id) {
                            return;
                        } else {
                            sunbjectItem.id = item.id;
                        }
                    } else {
                        $scope.projectArr.push({
                            // classId: $scope.selected.class_id,
                            sectionNum: $scope.selectProject.sectionNum,
                            weekNum: $scope.selectProject.weekNum,
                            subjectId: item.id
                        });
                    }
                });
            } else {
                $scope.projectArr.push({
                    // classId: $scope.selected.class_id,
                    sectionNum: $scope.selectProject.sectionNum,
                    weekNum: $scope.selectProject.weekNum,
                    subjectId: item.id
                });
            }


            $scope.projectPopup.close();
        };

        $scope.cancelChoose = function () {
            console.log('关闭');
            $scope.projectPopup.close();
        };

        //检测该老师在该班是否为班主任
        $scope.checkInClassIsHeadTeacher = function (currentClassId) {

            $scope.isCurrentHeadTeacher = false;
            if ($scope.isHeadTeacher && $scope.headerTeacherClassIds) {
                for (var i = 0; i < $scope.headerTeacherClassIds.length; i++) {
                    if (currentClassId == $scope.headerTeacherClassIds[i]) {
                        $scope.isCurrentHeadTeacher = true;
                        break;
                    } else {
                        $scope.isCurrentHeadTeacher = false;
                        $scope.isEdit = false;
                    }
                }
            }

        };

        //检测当前节次是否为走班选课
        $scope.checkIsWalkCourse = function (weekNum, sectionNum) {
            Requester.checkExistWalkCourse($scope.selected.class_id, weekNum, sectionNum).then(function (res) {
                if (res.result) {
                    console.log('check walk course succeed');
                } else {

                }
            });
        };


        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'class/walkCourseAlert.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.walkCourseModal = modal;
        });
        //查看所有走班课
        $scope.readAllWalkCourse = function (item) {
        
            $state.go('walkCourseAlert',{classId:$scope.selected.class_id,weekNum:item.weekNum,sectionNum:item.sectionNum});

            // $scope.walkCourseModal.show();

        };
        //关闭走班课列表弹框
        $scope.closeWalkCourseAlert = function () {
            // $scope.walkCoursePop.close();
            $scope.walkCourseModal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.walkCourseModal.remove();
        });

        //查询 是否开通了新高考走班
        $scope.selectIsOpenPermissions = function () {
            Requester.checkSchoolHaveNceeAuth($scope.schoolId).then(function (result) {

                $scope.isOpenXgk = result.data;
            });
        };
        $scope.getDeviceType = function () {
            var result = 'android';
            $scope.isIos = ionic.Platform.isIOS() && !isWeixin();
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }

            } else {
             result = 'android';
            }
            return result;
        };


    }]);