/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('myClassScheduleCtrl', ['$scope', 'Constant', '$state', 'Requester', 'NoticeService', 'toaster', '$window', '$ionicLoading', 'UserPreference', function ($scope, Constant, $state, Requester, NoticeService, toaster, $window, $ionicLoading, UserPreference) {
        if (window.cordova) MobclickAgent.onEvent('app_teaching_schedule');
        $scope.currentDate = new Date().Format("yyyy-MM-dd hh:mm:ss");
        $scope.selectedDayofWeek = new Date($scope.currentDate.replace(/\s+/g, 'T').concat('.000+08:00')).getDay();

        if ($scope.selectedDayofWeek == '0') {
            // var bar = document.getElementById("day-bar");
            // bar.scrollLeft = bar.scrollWidth;
        }
        $scope.maxScheduleDay = 5;
        $scope.scheduleTime = [];
        $scope.scheduleList = [];
        $scope.sectionTimes = [];
        $scope.weeks = [];
       
      $scope.classes = UserPreference.getArray('class');
        if ($scope.classes && $scope.classes.length > 0) {
            Requester.getTeachingSchedule().then(function (data) {
                if (data.result) {
                    $scope.sectionTimes = data.data.sectionTimes;
                    $scope.AMAcount = 0; //上午节次数
                    $scope.PMAcount = 0; //下午节次数
                    for (var l = 0; l < $scope.sectionTimes.length; l++) {
                        var block = $scope.sectionTimes[l];
                        if (block.sectionStatus === 0) {
                            $scope.AMAcount++;
                        } else if (block.sectionStatus === 1) {
                            $scope.PMAcount++;
                        } else if (block.sectionStatus === 2) {
                            $scope.EMAcount++;
                        }
                    }
                    //console.log('$scope.AMAcount:'+$scope.AMAcount+'PMAcount:'+$scope.PMAcount);
                    var list = data.data.courses;
                    $scope.scheduleList = [];

                    $scope.maxScheduleDay = data.data.maxWorkDays;
                    $scope.maxSectionNum = data.data.maxSectionNum;

                    for (var i = 1; i <= $scope.maxScheduleDay; i++) {
                        for (var j = 1; j <= $scope.maxSectionNum; j++) {
                            $scope.scheduleList.push({
                                'weekNum': i,
                                'sectionNum': j,
                                'subjectName': '——',
                            });

                        }
                        $scope.weeks.push({
                            weekCnName: i == 1 ? '周一' : i == 2 ? '周二' : i == 3 ? '周三' : i == 4 ? '周四' : i == 5 ? '周五' : i == 6 ? '周六' : '周日',
                            weekEnName: i == 1 ? 'mon' : i == 2 ? 'tues' : i == 3 ? 'wed' : i == 4 ? 'thur' : i == 5 ? 'fri' : i == 6 ? 'sat' : 'sun'
                        });
                    }
                    // console.log('课程表');
                    // console.log($scope.scheduleList);
                    for (var k = 0; k < list.length; k++) {
                        var day = list[k].weekNum;
                        var section = list[k].sectionNum;
                        if ($scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1]) {
                            $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].subjectName = list[k].subjectName;
                            $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].addressInfo = list[k].addressInfo;
                            $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].walkingNcee = list[k].walkingNcee;
                            $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].walkingClassName = list[k].walkingClassName;
                            $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].walkingSubName = list[k].walkingSubName;
                            $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].siteName = list[k].siteName;
                            $scope.scheduleList[(day - 1) * $scope.maxSectionNum + section - 1].className = list[k].className;
                        }
                    }
                } else
                    toaster.error({
                        title: data.message,
                        body: ''
                    });
            });
        }
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.isIos = ionic.Platform.isIOS() && !isWeixin();

        });

        $scope.getClassTime = function (index) {
            for (var i = 0; i < $scope.sectionTimes.length; i++) {
                if ($scope.sectionTimes[i].sectionNum == index)
                    return $scope.sectionTimes[i].onTime + ' - ' + $scope.sectionTimes[i].offTime;
            }
            return ' ';
        };
        $scope.getClassInterval = function(index){
           // console.log($scope.sectionTimes);
            for (var i = 0; i < $scope.sectionTimes.length; i++) {
                if ($scope.sectionTimes[i].sectionNum === index)
                    return $scope.sectionTimes[i].sectionStatus===0?'上午':$scope.sectionTimes[i].sectionStatus===1?'下午':'晚上';
            }
            return '';
        } ;

        $scope.go2Day = function (day) {
            $scope.selectedDayofWeek = day === 7 ? '0' : String(day);

        };

        $scope.goClassSchedule = function () {
            $state.go('classSchedule', {
                type: 'course'
            });
        };

        $scope.selectWeekDay = function (index) {
            var result;
            result = index == 7 ? 0 : index;
            return result;
        };

        //判断文字是否过长
        $scope.isBigLength = function (text) {
            var result;
            var width = textWidth(text, 15);
            var screenWidth = $window.innerWidth;
            if (width >= screenidth - 110) {
                result = true;
            }
            return result;
        };
        //文字过长时 显示全部
        $scope.showAllContent = function () {
            $ionicLoading.show({
                template: $scope.schoolName,
                noBackdrop: true,
                duration: 2000
            });
        };

        $scope.getDeviceType = function () {
            var result = 'android';
            // console.log('window.screen.width:' + window.screen.width);
            // console.log('window.screen.height:' + window.screen.height);
            $scope.isIos = ionic.Platform.isIOS() && !isWeixin();
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }

            } else {
                result = 'android';
            }
            return result;
        };

       
    }]);