/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('leaveRequestCtrl', ['$scope', 'BroadCast', 'Constant', 'UserPreference', '$state', 'Requester', '$ionicModal', 'ionicDatePicker', 'toaster', 'ionicTimePicker', function ($scope, BroadCast, Constant, UserPreference, $state, Requester, $ionicModal, ionicDatePicker, toaster, ionicTimePicker) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.loading = true;
            $scope.userRole = UserPreference.getObject('user').rolename;
            $scope.isTeacher = (String($scope.userRole) === Constant.USER_ROLES.TEACHER);
            if ($scope.isTeacher)
                $scope.pageTitle = '学生请假';
            else
                $scope.pageTitle = '请假';
            $scope.loadData(true);
        });

        $scope.loadData = function (reset) {
            if (reset) {
                $scope.list = [];
                $scope.page = 1;
            }
            else
                $scope.page++;
            if (!$scope.isTeacher) {
                Requester.getLeaveTypeList()
                    .then(function (resp) {
                        $scope.leaveTypes = resp.data;
                    });
            }
            Requester.getLeaveList($scope.userRole, $scope.page)
                .then(function (resp) {
                    var arr = resp.data.content;
                    for (var i = 0; i < arr.length; i++) {
                        var tmp = {
                            id: arr[i].id,
                            remark: arr[i].add_desc,
                            createTime: arr[i].create_time,
                            startDate: arr[i].start_date,
                            endDate: arr[i].end_date,
                            read: arr[i].is_received,
                            desc: arr[i].desc,
                            showFullText: false
                        };
                        if ($scope.isTeacher) {
                            tmp.className = arr[i].className;
                            tmp.stuName = arr[i].student_name;
                        }
                        $scope.list.push(tmp);
                    }
                    $scope.listHasMore = arr.length >= Constant.reqLimit;
                })
                .finally(function () {
                    $scope.loading = false;
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'class/newLeaveReq.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            var time = formatTimeWithoutSecends(new Date().getTime() / 1000, "yyyy-MM-dd ");
            $scope.selected = {
                startDate: time + "08:00:00",
                endDate: time + "18:00:00"
            };
            $scope.startTS = Date.parse($scope.selected.startDate.replace(/\-/g, "/"));
            $scope.endTS = Date.parse($scope.selected.endDate.replace(/\-/g, "/"));
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.doRev = function (item, $event) {
            $event.stopPropagation();
            Requester.updateLeaveRequest(item.id).then(function (resp) {
                if (resp.result) {
                    item.read = 1;
                }
            });
        };

        $scope.commit = function () {
            if ($scope.startTS >= $scope.endTS) {
                toaster.warning({title: '请假开始时间须小于结束时间！', body: ''});
                return;
            }
            $scope.selected.familyId = UserPreference.getObject('user').id;
            $scope.selected.studentId = UserPreference.get('DefaultChildID');
            Requester.leaveRequest($scope.selected).then(function (resp) {
                if (resp.result) {
                    toaster.success({title: '您已成功提交请假信息！', body: ''});
                    $scope.loadData(true);
                    $scope.closeModal();
                    if (window.cordova) MobclickAgent.onEvent('app_request_off');
                }
                else
                    toaster.error({title: resp.message, body: ''});
            });
        };

        $scope.expandLeave = function (item) {
            item.showFullText = !item.showFullText;
        };

        $scope.showDatePicker = function (arg) {
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {
                        if (arg === 0) {
                            if ((new Date().getTime() - val) / 1000 >= 24 * 3600) {
                                toaster.warning({title: '请假开始时间不能早于今天！', body: ''});
                                return;
                            }
                            $scope.startTS = val;
                        }
                        else if (arg === 1) {
                            $scope.endTS = val;
                        }
                        $scope.showTimePicker(arg);
                    }
                }
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.showTimePicker = function (arg) {
            var defaultTime = 0;
            if(arg === 0)
                defaultTime = 8 * 3600;
            else
                defaultTime = 18 * 3600;
            var ipObj1 = {
                inputTime: defaultTime,
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {
                        if (arg === 0) {
                            $scope.startTS = val * 1000 + $scope.startTS;
                            $scope.selected.startDate = formatTimeWithoutSecends($scope.startTS / 1000);
                        }
                        else {
                            $scope.endTS = val * 1000 + $scope.endTS;
                            $scope.selected.endDate = formatTimeWithoutSecends($scope.endTS / 1000);
                        }
                    }
                }
            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

    }]);