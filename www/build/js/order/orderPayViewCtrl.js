/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('orderPayViewCtrl', ['$scope', '$ionicHistory', 'Constant', '$stateParams', 'Requester', 'toaster', '$ionicModal', '$ionicLoading', 'UserPreference', '$ionicPopup', '$state', '$rootScope', function ($scope, $ionicHistory, Constant, $stateParams, Requester, toaster, $ionicModal, $ionicLoading, UserPreference, $ionicPopup, $state,$rootScope) {

        function getGoodsInfo(good) {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            Requester.getGoodInfo(good.id, good.categoryId, UserPreference.get('DefaultSchoolID')).then(function (resp) {
                if (resp.result) {
                    good.info = resp.data;
                    if (resp.data.price) {
                        $scope.orderPriceTotal += resp.data.price;
                    }
                } else {
                    toaster.error({title: data.message, body: ''});
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                }, 200);
            });
        }

        function getOrderInfo(orderSerial) {
            Requester.getOrderInfo(orderSerial).then(function (resp) {
                if (resp.result) {
                    $scope.detailItem = resp.data;
                } else {
                    toaster.error({title: data.message, body: ''});
                }
            });
        }

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'order/payResult.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };

        $ionicModal.fromTemplateUrl(UIPath + 'order/orderDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modalDetail = modal;
        });

        $scope.openDetail = function () {
            getOrderInfo($scope.orderSerial);
            $scope.closeModal();
            $scope.modalDetail.show();
        };

        $scope.closeDetailView = function () {
            $scope.modalDetail.hide();
            $scope.goBack();
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
            $scope.modalDetail.remove();
        });

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.needRepay = true;
            $scope.isWexin = isWeixin();
            $scope.goods = $stateParams.goods?$stateParams.goods: $rootScope.WXOrderPayViewGoods;
            if( $scope.isWexin)
            $rootScope.WXOrderPayViewGoods=$scope.goods;
            $scope.orderPriceTotal = 0;
            for (var i = 0; i < $scope.goods.length; i++) {
                var good = $scope.goods[i];
                getGoodsInfo(good);
            }
        });

        $scope.goBack = function () {
            if ($ionicHistory.backView())
                $ionicHistory.goBack();
            else
                $state.go('tabsController.mainPage');
        };

        $scope.getStatusTxt = function (status) {
            return getOrderStatusTxt(status);
        };

        $scope.getStatusIcon = function (status) {
            return getOrderStatusIcon(status);
        };

        $scope.getChannelName = function (status) {
            return getPayChannelName(status);
        };

        function onPaySuccess() {
            $scope.payRstImg = 'img/success.png';
            $scope.payRstDesc = '您已成功支付本订单！';
            $scope.payRst = '支付成功';
            $scope.needRepay = false;
            $scope.openModal();
        }

        function onPayFail(needWait) {
            if (needWait) {
                $scope.payRstImg = 'img/fail.png';
                $scope.payRstDesc = '支付结果获取中，请稍后...';
                $scope.payRst = '支付完成';
                $scope.needRepay = false;
            } else {
                $scope.payRstImg = 'img/fail.png';
                $scope.payRstDesc = '抱歉，本次支付未完成！';
                $scope.payRst = '支付失败';
                $scope.needRepay = true;
            }
            $scope.openModal();
        }

        function start2Pay(type, orderSerial) {
            if (orderSerial) {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                Requester.newOrderPayment(type, orderSerial).then(function (resp) {
                    if (resp.result) {
                        if (type === 0) {
                            //alipay
                            if (resp.data.tradeStatus.toUpperCase() === 'SUCCESS' && resp.data.aliAppRequestStr) {
                                var uri;
                                if (device.platform.toLowerCase() == 'ios') {
                                    uri = 'alipay://';
                                } else {
                                    uri = 'com.eg.android.AlipayGphone';
                                }

                                appAvailability.check(
                                    uri, // URI Scheme
                                    function () {
                                        cordova.plugins.alipay.payment(resp.data.aliAppRequestStr, function success(e) {
                                            onPaySuccess();
                                        }, function error(e) {
                                            if (e.resultStatus === '9000' || e.resultStatus === '6004') {
                                                onPayFail(true);
                                                setTimeout(function () {
                                                    Requester.isPaymentSuccess().then(function (resp) {
                                                        if (String(resp.data.tradeCode) === '1')
                                                            onPaySuccess();
                                                        else if (String(resp.data.tradeCode) === '3')
                                                            onPayFail();
                                                    });
                                                }, 5000);
                                            }
                                            else
                                                onPayFail();
                                        });
                                    },
                                    function () {
                                        $ionicPopup.alert({
                                            title: '提示',
                                            template: '未检测到支付宝应用，请安装支付宝或使用其它方式支付。',
                                            okText: '确定',
                                            okType: 'button-balanced'
                                        });
                                    }
                                );
                            }
                        } else if (type === 1) {
                            //wechat
                            if (resp.data.tradeStatus.toUpperCase() === 'SUCCESS' && resp.data.wxAppRequestVo) {
                                Wechat.isInstalled(function (installed) {
                                    if (installed) {
                                        var req = {
                                            "partnerid": resp.data.wxAppRequestVo.mchId,
                                            "timestamp": resp.data.wxAppRequestVo.timeStamp,
                                            "noncestr": resp.data.wxAppRequestVo.nonceStr,
                                            "prepayid": resp.data.wxAppRequestVo.prePayId,
                                            "sign": resp.data.wxAppRequestVo.paySign
                                        };
                                        Wechat.sendPaymentRequest(req, function () {
                                            //show success
                                            onPaySuccess();
                                        }, function (reason) {
                                            onPayFail();
                                        });
                                    }
                                    else {
                                        $ionicPopup.alert({
                                            title: '提示',
                                            template: '未检测到微信应用，请安装微信或使用其它方式支付。',
                                            okText: '确定',
                                            okType: 'button-balanced'
                                        });
                                    }
                                }, function (reason) {
                                });
                            }
                            else
                                toaster.warning({title: resp.message, body: resp.data.msg});
                        }
                    } else {
                        toaster.error({title: resp.message, body: ''});
                    }
                }).finally(function () {
                    setTimeout(function () {
                        $ionicLoading.hide();
                    }, 1000);
                });
            }
        }

        $scope.reqPayWith = function (type) {
            if (!$scope.needRepay) {
                toaster.success({title: '您已成功支付。', body: ''});
                return;
            }
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            var childID = UserPreference.get('DefaultChildID');
            var childName = UserPreference.get('DefaultChildName');
            Requester.isUserPurchased(1, childID).then(function (resp) {
                if (resp.result) {
                    if (resp.data.isContains)
                        toaster.success({title: '您的小孩' + childName + '已交过押金', body: ''});
                    else {
                        $ionicLoading.show({
                            noBackdrop: true,
                            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                        });
                        Requester.newOrder('学生卡押金-' + childName, [{
                            goodId: $scope.goods[0].id,
                            number: $scope.goods[0].count
                        }], childID, UserPreference.get('DefaultSchoolID'), '平安校园')
                            .then(function (resp) {
                                if (resp.result) {
                                    $scope.orderSerial = resp.data.orderSerial;
                                    start2Pay(type, resp.data.orderSerial);
                                }
                                else
                                    toaster.error({title: resp.message, body: ''});
                            }).finally(function () {
                            $ionicLoading.hide();
                        });
                    }
                }
            }).finally(function () {
                $ionicLoading.hide();
            });
        };

    }]);