/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('postDetailCtrl', ['$scope', '$ionicHistory', '$stateParams', 'Constant', '$ionicModal', '$sce', 'Requester', 'ionicImageView', '$state', '$ionicPopup', '$ionicScrollDelegate', '$location', '$rootScope', 'SavePhotoTool', function ($scope, $ionicHistory, $stateParams, Constant, $ionicModal, $sce, Requester, ionicImageView, $state, $ionicPopup, $ionicScrollDelegate, $location, $rootScope, SavePhotoTool) {
        $rootScope.$on('saveImgUrl', function (event, url) {
            SavePhotoTool.savePhoto(url);
        });

        function removeStyle(nodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].children.length > 0) {
                    removeStyle(nodes[i].children);
                }
                nodes[i].removeAttribute('style');
                if (nodes[i].nodeName === 'IMG') {
                    nodes[i].setAttribute('ng-click', 'openModal(\'' + nodes[i].getAttribute('src') + '\')');
                }
            }
        }

        function getTextContent() {
            if ($scope.news.source !== 1) {
                var div = document.createElement('div');
                div.innerHTML = $scope.news.text;
                removeStyle(div.children);
                $scope.news.text = div.innerHTML;
            }
        }

        function getCommentsAndFavors(cid) {
            if ($scope.news && $scope.news.id) {
                if (cid) {
                    $scope.loadComments(true, cid);
                } else {
                    $scope.loadComments(true);
                }
                $scope.loadFavorList();
            }
        }

        $scope.loadFavorList = function () {
            Requester.getCampusNewsFavorList($scope.news.id).then(function (resp) {
                $scope.favorList = resp.data;
                $scope.news.praiseNum = $scope.favorList.length;
            });
        };

        $scope.switchCommentTab = function (tab) {
            $scope.favorSelected = tab;
            $ionicScrollDelegate.resize();
        };

        $scope.deleteComment = function (item) {
            var confirmPopup = $ionicPopup.confirm({
                title: '提示',
                template: '确认删除' + item.commentsPersion + '的评论？',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    Requester.deleteCampusComment(item.id).then(function (rst) {
                        if (rst && rst.result) {
                            var i = 0;
                            for (; i < $scope.commentList.length; i++) {
                                if ($scope.commentList[i].id === item.id) {
                                    $scope.commentList.splice(i, 1);
                                }
                            }
                            $scope.news.commentsNum--;
                        }
                    });
                }
            });
        };

        $scope.loadComments = function (reset, cid) {
            if (reset === true) {
                $scope.page = 1;
            } else if ($scope.listHasMore)
                $scope.page++;
            else
                return;
            Requester.getCampusNewsCommentList($scope.news.id, $scope.page, cid).then(function (resp) {
                if (reset)
                    $scope.commentList = resp.data.content;
                else
                    Array.prototype.push.apply($scope.commentList, resp.data.content);
                $scope.listHasMore = !resp.data.last;
            }).finally(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                if (cid) {
                    setTimeout(function () {
                        $location.hash('commentIndex' + cid);
                        $ionicScrollDelegate.anchorScroll();
                    }, $scope.listHasMore.length * 5);
                }
            });
        };

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.isWeinXin = isWeixin();
            $scope.listHasMore = false;

            $stateParams.post = $stateParams.post ? $stateParams.post : $rootScope.WXRootPost;
            $stateParams.comment = $stateParams.comment ? $stateParams.comment : $rootScope.WXRootComment;
            if(isWeixin()){
                $rootScope.WXRootPost = $stateParams.post;
                $rootScope.WXRootComment = $stateParams.comment;
            }
           
            $scope.picUrls = [];

            if ($stateParams.post) {
                $scope.news = $stateParams.post;

                getTextContent();
                getCommentsAndFavors();

                $scope.news.imageUrls.forEach(function (url) {
                    $scope.urlItem = {
                        thumb: '',
                        src: ''
                    };
                    $scope.urlItem.thumb = url;
                    $scope.urlItem.src = url;
                    $scope.picUrls.push($scope.urlItem);
                });


            } else if ($stateParams.comment) {
                $scope.comment = $stateParams.comment;
                Requester.getCampusNewsById($scope.comment.newsId).then(function (resp) {
                    $scope.news = resp.data[0];

                    $scope.news.imageUrls.forEach(function (url) {
                        $scope.urlItem = {
                            thumb: '',
                            src: ''
                        };
                        $scope.urlItem.thumb = url;
                        $scope.urlItem.src = url;
                        $scope.picUrls.push($scope.urlItem);
                    });
                    
                    getTextContent();
                    if ($scope.comment.type === '0')
                        getCommentsAndFavors($scope.comment.id);
                    else
                        getCommentsAndFavors();
                });
            } else {
                $ionicHistory.goBack();
            }

            if (window.cordova) MobclickAgent.onEvent('app_view_news');
        });

        $scope.viewDoc = function (url) {
            $state.go('pdfView', {
                url: url
            });
        };


        $scope.videoUrl = function (url) {
            return $sce.trustAsResourceUrl(url);
        };

        //点赞
        $scope.favorNews = function () {
            if ($scope.news.isFavor === true) {
                Requester.unfavorCampus($scope.news.id).then(function () {
                    $scope.loadFavorList();
                });
                if ($scope.news.praiseNum > 0)
                    $scope.news.praiseNum--;
            } else {
                Requester.favorCampus($scope.news.id).then(function () {
                    $scope.loadFavorList();
                });
                $scope.news.praiseNum++;
            }
            $scope.news.isFavor = !$scope.news.isFavor;
        };

        //评论
        $scope.showCommentInput = function () {
            $ionicPopup.show({
                template: '<textarea type="text" ng-model="news.commentContent" style="min-height: 100px;">',
                title: '请输入评论内容',
                scope: $scope,
                buttons: [{
                        text: '取消'
                    },
                    {
                        text: '<b>评论</b>',
                        type: 'button-xgreen',
                        onTap: function (e) {
                            if (!$scope.news.commentContent) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                return $scope.news.commentContent;
                            }
                        }
                    }
                ]
            }).then(function (res) {
                if (res && res.trim() !== '') {
                    $scope.news.commentContent = undefined;
                    Requester.addCampusComment($scope.news.id, res).then(function (resp) {
                        $scope.loadComments(true);
                        $scope.news.commentsNum++;
                    });
                }
            });
        };

        $scope.getReviewStatus = function (status) {
            if (status == Constant.NEWS_STATUS.TEACHER_REVIEW.key)
                return Constant.NEWS_STATUS.TEACHER_REVIEW.text;
            else if (status == Constant.NEWS_STATUS.TEACHER_IGNORE.key)
                return Constant.NEWS_STATUS.TEACHER_IGNORE.text;
            else
                return Constant.NEWS_STATUS.TEACHER_REVIEW_PASS.text;
        };

        $scope.openArrModal = function (index) {
            ionicImageView.showViewModal({
                allowSave: true
            }, $scope.news.imageUrls, index);
        };

        $scope.openModal = function (pic) {
            ionicImageView.showViewModal({
                allowSave: true
            }, [pic]);
        };
    }]);