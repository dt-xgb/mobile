/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('noticeDetailCtrl', ['$scope', '$stateParams', '$sce', 'Requester', 'ionicImageView', '$state', '$rootScope', 'UserPreference', function ($scope, $stateParams, $sce, Requester, ionicImageView, $state, $rootScope, UserPreference) {
        function removeStyle(nodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].children.length > 0) {
                    removeStyle(nodes[i].children);
                }
                nodes[i].removeAttribute('style');
                if (nodes[i].nodeName === 'IMG') {
                    nodes[i].setAttribute('ng-click', 'openModal(\'' + nodes[i].getAttribute('src') + '\')');
                }
            }
        }

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.news = $stateParams.notice ? $stateParams.notice : $rootScope.rootWxNotice;          
             $scope.detailId = $scope.news.id;
            $scope.getNoticeDetail();
           
            if (isWeixin()) {
                $rootScope.rootWxNotice = $scope.news;
                if (!$scope.news || ($scope.news && !$scope.news.text)) {
                    $state.go('tabsController.mainPage');
                    return;
                }
            }
               
            if (window.cordova) MobclickAgent.onEvent('app_view_news');
        });


        $scope.viewDoc = function (url) {
            $state.go('pdfView', {
                url: url
            });
        };

        $scope.videoUrl = function (url) {
            return $sce.trustAsResourceUrl(url);
        };

        $scope.openModal = function (picture) {
            ionicImageView.showViewModal({
                allowSave: true
            }, [picture]);
        };

        $scope.openArrModal = function (index) {

            ionicImageView.showViewModal({
                allowSave: true
            }, $scope.news.imageList, index);
        };

        //requester --get detail
        $scope.getNoticeDetail = function () {
            Requester.getNoticeDetail($scope.detailId).then(function (res) {
                if (res.result) {
                    $scope.news = res.data;
                    $rootScope.rootNoticeType = res.data.akey;

                    if ($scope.news.source !== 1) {
                        var div = document.createElement('div');
                        div.innerHTML = $scope.news.text;
                        removeStyle(div.children);
                        $scope.news.text = div.innerHTML;
                    }
                    Requester.setNoticeRead($scope.news.id);
                    // if ($scope.myNews.alreadyRead !== true||$scope.myNews.alreadyRead==undefined) {
                    //     Requester.setNoticeRead($scope.news.id);
                    // }
                } else {
                    console.log('failure');
                }

            });
        };


        $scope.goTolinkPage = function (linkUrl) {
            if (window.cordova && ionic.Platform.isIOS()) {
                cordova.plugins.browsertab.isAvailable(function (result) {
                    if (result) {
                        cordova.plugins.browsertab.openUrl(linkUrl).then(function (rest) {
                            console.log(rest);
                        });
                    } else {
                        ordova.InAppBrowser.open(linkUrl, '_system', 'location=no,toolbar=no,toolbarposition=top,closebuttoncaption=关闭');
                    }
                });
            }

        };
    }]);