angular.module('app.controllers')
    .controller('attendanceStaticCtrl', ['$scope', '$state', '$timeout', 'UserPreference', 'Requester', 'toaster', '$ionicLoading', function ($scope, $state, $timeout, UserPreference, Requester, toaster, $ionicLoading) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
          
            $scope.studentId = UserPreference.get('DefaultChildID');
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $scope.getChildAttendStatic();
            
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
           
        });

        //获取统计表
        $scope.getStatisticalEchat = function (data1 ,data2) {
            $timeout(function () {
                var chartDiv = document.getElementById('attendance_static_echat');
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(chartDiv);
                myChart.resize('350px', '350px');

                var option = {
                    color: ['#2bcab2', '#eee'],
                    grid: {
                        y2: 0,
                    },
                    series: [{
                        // name:'考勤统计',
                        type: 'pie',
                        avoidLabelOverlap: false,
                        hoverAnimation: false,
                        silent: true,
                        radius: ['58%', '70%'],
                        itemStyle: { //图形样式
                            normal: { //normal 是图形在默认状态下的样式；emphasis 是图形在高亮状态下的样式，比如在鼠标悬浮或者图例联动高亮时。
                                label: { //饼图图形上的文本标签
                                    show: false //平常不显示
                                },
                                labelLine: { //标签的视觉引导线样式
                                    show: false //平常不显示
                                }
                            },

                        },

                        data: [{
                                value: data1,
                                name: '',
                                // itemStyle: {
                                //     emphasis: {color: '#000'}
                                //   }
                            },
                            {
                                value: data2,
                                name: '',
                                // itemStyle: {
                                //     emphasis: {color: '#000'}
                                //   }
                            }
                        ]
                    }]
                };

                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);

            }, 20);
        };

        //下拉刷新
        $scope.refreshData = function(){
            $scope.getChildAttendStatic();
        };

        //request 获取30天考勤列表
        $scope.getChildAttendStatic = function () {
            Requester.getChildAttendStatic($scope.studentId).then(function (resp) {
                if (resp.result) {
                //   console.log('--- static');
                //   console.log(resp);
                $ionicLoading.hide();
                if(resp.data&&resp.data.length>0){
                    $scope.list = dataDeal(resp.data);
                    $scope.restData = {
                        startDate : $scope.list[0].attendDay,
                        endDate :$scope.list[$scope.list.length-1].attendDay,
                        attLateDay:$scope.list[0].attLateDay?$scope.list[0].attLateDay:0,
                        attAskLeaveDay:$scope.list[0].attAskLeaveDay?$scope.list[0].attAskLeaveDay:0,
                        attNotreachedDay:$scope.list[0].attNotreachedDay?$scope.list[0].attNotreachedDay:0,
                        attActualDay:$scope.list[0].attActualDay?$scope.list[0].attActualDay:0,//出勤
                        attShouldDay:$scope.list[0]. attShouldDay//应到

                    };
                    var attActualDay = $scope.restData.attActualDay?$scope.restData. attActualDay:0;
                    var attShouldDay = $scope.restData.attShouldDay?$scope.restData.attShouldDay:1;//应到不能为在0
                   
                    $scope.getStatisticalEchat(attActualDay,attShouldDay );
                    
                }else{
                    $scope.list = []; 
                }
                

                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
                setTimeout(function () {
                    $ionicLoading.hide();
                }, 5000);
            });
        };

        //将相同天数的不同时段的 重新组装
        function dataDeal(data) {
            var listArr = [];
            data.forEach(function (el, index) {
                for (var i = 0; i < listArr.length; i++) {
                    // 对比相同的字段key，相同放入对应的数组
                    if (listArr[i].attendDay === el.attendDay) {
                        listArr[i].group.push({
                            attTimeSlot:el.attTimeSlot,//考勤时段
                            inTimeStr:el.inTimeStr,//到校时间
                            outTimeStr:el.outTimeStr,//离校时间
                            attStatus:el.attStatus//考勤状态
                        });
                        return;
                    }
                }
                // 第一次对比没有参照，放入参照
                listArr.push({
                    id:el.id,
                    attendDay: el.attendDay,
                    week:el.week,
                    attActualDay:el.attActualDay,//出勤天数
                    attAskLeaveDay:el.attAskLeaveDay,//请假天数
                    attLateDay:el.attLateDay,//迟到天数
                    attNotreachedDay:el.attNotreachedDay,//缺勤天数
                    attShouldDay:el.attShouldDay,//应到天数
                    group: [{
                        attTimeSlot:el.attTimeSlot,//考勤时段
                        inTimeStr:el.inTimeStr,//到校时间
                        outTimeStr:el.outTimeStr,//离校时间
                        attStatus:el.attStatus//考勤状态
                    }]
                });
            });
            return listArr;
        }



    }]);