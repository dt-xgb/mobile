/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('checkInNoticeBabyCtrl', ['$scope', 'Constant', '$ionicHistory', '$state', 'Requester', 'UserPreference', '$ionicModal', '$ionicPopup', 'toaster', '$ionicLoading', 'ionicImageView', function ($scope, Constant, $ionicHistory, $state, Requester, UserPreference, $ionicModal, $ionicPopup, toaster, $ionicLoading, ionicImageView) {
        //接送通知
        $scope.goBack = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('tabsController.mainPage');
        };
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'attendanceKid/checkInCardManage.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.viewImg = function (url) {
            if (url&&url!=="null") {
                var newUrl = url.replace('/bre', '');
                if ($scope.largeImgUrl !== newUrl) {
                    $ionicLoading.show({
                        noBackdrop: true,
                        template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
                    });
                }
                $scope.largeImgUrl = newUrl;
                ionicImageView.showViewModal({
                    allowSave: true
                }, [$scope.largeImgUrl]);
            }
        };

        $scope.showBindCardView = function () {
            $ionicPopup.show({
                template: '<input type="text" ng-model="input.card" maxlength="10">',
                title: '绑定接送卡',
                subTitle: '请输入接送卡卡面上编号',
                scope: $scope,
                buttons: [
                    {text: '取消'},
                    {
                        text: '<b>保存</b>',
                        type: 'button-balanced',
                        onTap: function (e) {
                            if (!$scope.input.card || $scope.input.card.trim().length < 1) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                $scope.input.card = $scope.input.card.toUpperCase();
                                for (var j = 0; j < $scope.cards.length; j++) {
                                    if ($scope.input.card == $scope.cards[j]) {
                                        toaster.warning({title: '该卡片已被绑定！', body: ''});
                                        return null;
                                    }
                                }
                                Requester.bindStudentCard($scope.input.card).then(function (resp) {
                                    if (resp.result) {
                                        $scope.cards.push($scope.input.card);
                                        var tmp = '';
                                        for (var j = 0; j < $scope.cards.length; j++) {
                                            tmp += $scope.cards[j];
                                            tmp += ',';
                                        }
                                        tmp = tmp.substr(0, tmp.length - 1);
                                        var stuid = UserPreference.get('DefaultChildID');
                                        var user = UserPreference.getObject('user');
                                        for (var i = 0; i < user.student.length; i++) {
                                            if (stuid == user.student[i].id) {
                                                user.student[i].stucardid = tmp;
                                                UserPreference.setObject('user', user);
                                                break;
                                            }
                                        }

                                        $scope.input = {card: ''};
                                        toaster.success({title: '绑定接送卡成功!', body: '',timeout:'3000'});
                                        $scope.closeModal();

                                    } else
                                        toaster.error({title: resp.message, body: ''});
                                });
                                return $scope.input.card;
                            }
                        }
                    }
                ]
            });
        };

        $scope.unbindCard = function (card) {
            var confirmPopup = $ionicPopup.confirm({
                title: '温馨提示',
                template: '确认删除该接送卡？',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    Requester.unbindStudentCard(card, $scope.schoolid).then(function (resp) {
                        if (resp.result) {
                            var tmp = '';
                            for (var j = 0; j < $scope.cards.length; j++) {
                                if (card != $scope.cards[j]) {
                                    tmp += $scope.cards[j];
                                    tmp += ',';
                                }
                            }
                            tmp = tmp.substr(0, tmp.length - 1);
                            if (tmp === '')
                                $scope.cards = [];
                            else
                                $scope.cards = tmp.split(',');
                            var stuid = UserPreference.get('DefaultChildID');
                            var user = UserPreference.getObject('user');
                            for (var i = 0; i < user.student.length; i++) {
                                if (stuid == user.student[i].id) {
                                    user.student[i].stucardid = tmp;
                                    UserPreference.setObject('user', user);
                                    break;
                                }
                            }
                            toaster.success({title: '删除接送卡成功!', body: ''});
                        }
                    });
                }
            });
        };

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.loading = true;
            var stuid = UserPreference.get('DefaultChildID');
            var user = UserPreference.getObject('user');
            $scope.input = {
                card: ''
            };
            $scope.cards = [];
            for (var i = 0; i < user.student.length; i++) {
                if (stuid == user.student[i].id && user.student[i].stucardid) {
                    $scope.schoolid = user.student[i].school.id;
                    if (user.student[i].stucardid === '')
                        $scope.cards = [];
                    else
                        $scope.cards = user.student[i].stucardid.split(',');
                    break;
                }
            }
            Requester.getChildCheckInRecords(stuid).then(function (resp) {
                $scope.noticeList = resp.data;
                var toset = [];
                for (var i = 0; i < $scope.noticeList.length; i++) {
                    if ($scope.noticeList[i].isread === 0)
                        toset.push($scope.noticeList[i]);
                }
                Requester.setCheckInNoticeRead(toset);
            }).finally(function () {
                $scope.loading = false;
            });
        });
    }]);