angular.module('app.controllers')
    .controller('attendanceTodayCtrl', ['$scope', 'BroadCast', 'Constant', 'UserPreference', '$state', 'Requester', '$ionicHistory', '$ionicPopup', '$ionicModal', 'ionicTimePicker', '$ionicScrollDelegate', 'toaster', 'ngIntroService', '$ionicLoading', function ($scope, BroadCast, Constant, UserPreference, $state, Requester, $ionicHistory, $ionicPopup, $ionicModal, ionicTimePicker, $ionicScrollDelegate, toaster, ngIntroService, $ionicLoading) {
        if (window.cordova) MobclickAgent.onEvent('app_today_checkin');
        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.classes = UserPreference.getArray('class');
            $scope.selected = {
                resignType: 0
            };
            $scope.selected.class_id = $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].key : '';
            $scope.activeIndex = 0;
            $scope.listFilter = { attendance: true };
            $scope.loadData();
            $scope.timeBtnTxt = '选择时间';
            $scope.checkInTypes = [{ key: 0, name: '已到校' }, { key: 1, name: '已离校' }, { key: 2, name: '已请假' }];
            $scope.groups = [{ key: 0, name: '已到校', index: 3, filter: { attendance: true } },
            { key: 1, name: '未到校', index: 0, filter: { notAttendance: true } },
            { key: 2, name: '已请假', index: 2, filter: { askLeave: true } },
            { key: 3, name: '未离校', index: 1, filter: { notLeave: true } },
            { key: 4, name: '已离校', index: 4, filter: { leave: true } }];
            $scope.ignorePermission = true;
        });

        function getStuLeaveInfo(sid) {
            Requester.getStudentTodayLeaveInfo(sid).then(function (resp) {
                if (resp.result) {
                    var confirmPopup = $ionicPopup.confirm({
                        title: '提示',
                        template: '确定要撤销该学生今日的请假吗？',
                        cancelText: '取消',
                        okText: '确认',
                        okType: 'button-balanced'
                    });
                    confirmPopup.then(function (res) {
                        if (res) {
                            Requester.removeLeaveReq(resp.data.id).then(function (response) {
                                if (response.result) {
                                    $scope.loadData();
                                    $scope.closeDetailModal();
                                } else
                                    toaster.error({ title: response.message, body: '' });
                            });
                        }
                    });
                }
            });

        }

        $scope.onHeadTeacherClicked = function (leave, sid) {
            //if ($scope.isHeadTeacher) {
            if (leave) {
                getStuLeaveInfo(sid);
            } else {
                $scope.showChangeModel(sid);
            }
            //}
        };

        $scope.onLeaveStudentClicked = function (item) {
            if (item.askLeave) {
                getStuLeaveInfo(item.studentId);
            }
        };

        $scope.loadData = function () {
            $scope.isHeadTeacher = false;
            var classes = UserPreference.getObject('user').classes;
            for (var j = 0; classes && j < classes.length; j++) {
                if ($scope.selected.class_id == classes[j].id) {
                    $scope.isHeadTeacher = true;
                    break;
                }
            }
            // setTimeout(function () {
            //     if ($scope.isHeadTeacher && !UserPreference.getBoolean("ShowAttendanceTodayTip")) {
            //         var IntroOptions = {
            //             steps: [
            //                 {
            //                     element: document.querySelector('#focusItem'),
            //                     intro: "<strong>点击这里，可对学生的今日考勤进行变更</strong></br><strong>您也可以在学生列表中点击学生信息进入补签</strong>"
            //                 }],
            //             showStepNumbers: false,
            //             showBullets: false,
            //             showButtons: false,
            //             exitOnOverlayClick: true,
            //             exitOnEsc: true
            //         };
            //         ngIntroService.setOptions(IntroOptions);
            //         ngIntroService.start();
            //         UserPreference.set("ShowAttendanceTodayTip", true);
            //     }
            // }, 300);
            Requester.getTodayAttendance($scope.selected.class_id)
                .then(function (resp) {
                    if (Constant.debugMode) console.log(resp);
                    if (resp.result) {
                        $scope.list = resp.data.content;
                        $scope.indexCount = [0, 0, 0, 0, 0];
                        for (var i = 0; i < $scope.list.length; i++) {
                            if ($scope.list[i].attendance)
                                $scope.indexCount[0]++;
                            if ($scope.list[i].notAttendance)
                                $scope.indexCount[1]++;
                            if ($scope.list[i].askLeave)
                                $scope.indexCount[2]++;
                            if ($scope.list[i].notLeave)
                                $scope.indexCount[3]++;
                            if ($scope.list[i].leave)
                                $scope.indexCount[4]++;
                        }
                    }
                })
                .finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/changeCheckInRecord.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            modal.el.className = modal.el.className + " second-modal";
            $scope.modal = modal;
        });
        $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/chooseStudentCheckIn.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            modal.el.className = modal.el.className + " third-modal";
            $scope.stuModal = modal;
        });
        $scope.closeModal = function () {
            $scope.modal.hide();
            $scope.closeDetailModal();
            for (var i = 0; i < $scope.list.length; i++) {
                $scope.list[i].isChecked = false;
            }
        };
        $scope.closeChooseModal = function () {
            $scope.stuModal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
            $scope.stuModal.remove();
            $scope.detailModal.remove();
        });

        $scope.showChooseMode = function () {
            $scope.stuModal.show();
        };

        $ionicModal.fromTemplateUrl(UIPath + 'attendanceClass/checkInStudentDetail.html', {
            scope: $scope
        }).then(function (modal) {
            modal.el.className = modal.el.className + " first-modal";
            $scope.detailModal = modal;
        });
        $scope.openDetailModal = function (item) {
            var day = formatTimeWithoutSecends(new Date().getTime() / 1000, "yyyy-MM-dd");
            $scope.detailTitle = day + '学生进出校明细';
            $scope.noContentTip = '暂无进出校记录';
            $scope.detail = {
                logo: Constant.IM_USER_AVATAR
            };
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
            });
            Requester.getStuInOutRecords(day, item.studentId, $scope.selected.class_id).then(function (resp) {
                $scope.detail = {
                    list: resp.data.attendRecords,
                    studentName: resp.data.studentName,
                    askLeave: resp.data.askLeave,
                    logo: resp.data.logo,
                    id: resp.data.studentId
                };
            }).finally(function () {
                $ionicLoading.hide();
            });
            $scope.detailModal.show();
        };
        $scope.closeDetailModal = function () {
            $scope.detailModal.hide();
        };

        $scope.getBtnLabel = function () {
            if ($scope.detail)
                return $scope.detail.askLeave ? '撤销请假' : '补签';
        };

        $scope.showTimePicker = function () {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {
                    } else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        $scope.timeBtnTxt = time;
                        var currentDate = formatTimeWithoutSecends(new Date().getTime() / 1000, "yyyy-MM-dd ");
                        $scope.selected.time = currentDate + time + ':00';
                        console.log('select-time:'+ $scope.selected.time);
                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

        $scope.showChangeModel = function (sid) {
            ngIntroService.exit();
            for (var i = 0; sid && i < $scope.list.length; i++) {
                if ($scope.list[i].studentId == sid) {
                    $scope.list[i].isChecked = true;
                    break;
                }
            }
            $scope.modal.show();
        };

        $scope.toggle = function (group, length) {
            if (length > 0) {
                group.show = !group.show;
                $ionicScrollDelegate.resize();
            }
        };

        $scope.save = function () {
            if ($scope.selected.resignType !== 2 && !$scope.selected.time) {
                var title = $scope.selected.resignType==0?'请选择到校时间':'请选择离校时间';
                toaster.warning({ title: title, body: '' });
                return;
            }
            var to = [];
            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].isChecked)
                    to.push({
                        id: $scope.list[i].studentId,
                        time: $scope.selected.time ? $scope.selected.time : formatTimeWithoutSecends(new Date().getTime() / 1000, "yyyy-MM-dd hh:mm:ss"),
                        type: $scope.selected.resignType
                    });
            }
            if (to.length > 0) {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                Requester.reCheckIn(to).then(function (resp) {
                    if (resp.result) {
                        $scope.loadData();
                        $scope.closeModal();
                    } else
                        toaster.error({ title: resp.message, body: '' });
                }).finally(function () {
                    $ionicLoading.hide();
                });
            } else
                toaster.warning({ title: '请选择待补签学生', body: '' });
        };

        $scope.about = function () {
            $ionicPopup.alert({
                title: '今日考勤状态说明',
                template: '1、已到校：今日已有进校记录的学生</br>2、未到校：当前仍未打卡入校的学生，不包括已请假的学生</br>3、已请假：今日有请假记录的学生</br>4、未离校：截止现在学生入校后还没有离开的学生',
                okText: '确认',
                okType: 'btn-follower'
            });
        };

        $scope.switchTab = function (index) {
            $scope.activeIndex = index;
            switch (index) {
                case 0:
                    $scope.listFilter = { attendance: true };
                    break;
                case 1:
                    $scope.listFilter = { notAttendance: true };
                    break;
                case 2:
                    $scope.listFilter = { askLeave: true };
                    break;
                case 3:
                    $scope.listFilter = { notLeave: true };
                    break;
                case 4:
                    $scope.listFilter = { leave: true };
                    break;
            }
        };

        $scope.goBack = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('tabsController.mainPage');
        };

    }]);

