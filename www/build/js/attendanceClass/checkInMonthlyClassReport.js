angular.module('app.controllers')
.controller('checkInMonthlyClassReportCtrl', ['$scope', 'BroadCast', 'Constant', '$ionicHistory', '$state', 'Requester', 'UserPreference', function ($scope, BroadCast, Constant, $ionicHistory, $state, Requester, UserPreference) {

    function onSelectedDateChange() {
        var firstDay = new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), 1).getDate();
        var lastDay = new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth() + 1, 0).getDate();

        var firstDayEpoch = resetHMSM(new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), firstDay)).getTime();
        var lastDayEpoch = resetHMSM(new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), lastDay)).getTime();

        $scope.disableGoPre = (firstDayEpoch - 86400000) <= $scope.fromDate;
        $scope.disableGoNext = (lastDayEpoch + 86400000) >= $scope.toDate;
        $scope.loadData();
    }

    function initDate(start, end) {
        $scope.currentDate = resetHMSM(new Date());
        if (start && end) {
            $scope.fromDate = resetHMSM(new Date(start)).getTime();
            var endDate = resetHMSM(new Date(end));
            if (endDate.getTime() < $scope.currentDate.getTime()) {
                $scope.currentDate = endDate;
            }
        }
        else {
            //学期始于2月和8月
            if ($scope.currentDate.getMonth() >= 1 && $scope.currentDate.getMonth() <= 6)
                $scope.fromDate = resetHMSM(new Date($scope.currentDate.getFullYear(), 1, 1)).getTime();
            else if ($scope.currentDate.getMonth() === 0)
                $scope.fromDate = resetHMSM(new Date($scope.currentDate.getFullYear() - 1, 7, 1)).getTime();
            else
                $scope.fromDate = resetHMSM(new Date($scope.currentDate.getFullYear(), 7, 1)).getTime();
        }
        $scope.toDate = resetHMSM(new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth(), new Date($scope.currentDate.getFullYear(), $scope.currentDate.getMonth() + 1, 0).getDate())).getTime();
        onSelectedDateChange();
    }

    $scope.onSelectionChange = function () {
        UserPreference.set("DefaultClassID", $scope.selected.class_id);
        onSelectedDateChange();
    };

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
        $scope.classes = UserPreference.getArray('class');
        var user = UserPreference.getObject('user');
        $scope.hasCheckInPermission = user.AUTH_ATTEND === 1;
        $scope.selected = {
            class_id: UserPreference.get('DefaultClassID', $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].key : '')
        };
        $scope.currentDate = resetHMSM(new Date());
        Requester.getSemesterInfo($scope.selected.class_id).then(function (resp) {
            if (resp.result) {
                $scope.noCalendar = false;
                $scope.schoolyear = resp.data.schoolyear;
                $scope.schoolterm = resp.data.schoolterm;
                $scope.schoolTime = resp.data.beginDateStr + ' ~ ' + resp.data.endDateStr;
                var start = resp.data.beginDate && resp.data.beginDate.length >= 10 ? resp.data.beginDate.substr(0, 10) : void 0;
                var end = resp.data.endDate && resp.data.endDate.length >= 10 ? resp.data.endDate.substr(0, 10) : void 0;
                initDate(start, end);
            }
            else {
                if (resp.code == -1)
                    $scope.noCalendar = true;
            }
        });
    });

    function handleCheckInRecords(resp) {
        if (resp.result) {
            $scope.list = resp.data;
            $scope.noCalendar = false;
        }
        else {
            if (resp.code == -1)
                $scope.noCalendar = true;
            $scope.list = [];
        }
    }

    $scope.loadData = function () {
        var date = $scope.currentDate.getFullYear() + '-' + ($scope.currentDate.getMonth() > 8 ? $scope.currentDate.getMonth() + 1 : '0' + ($scope.currentDate.getMonth() + 1));
        Requester.getClassMonthlyCheckInInfo(date, $scope.selected.class_id).then(function (resp) {
            handleCheckInRecords(resp);
        }).finally(function () {
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

    $scope.prevMonth = function () {
        if ($scope.currentDate.getMonth() === 1) {
            $scope.currentDate.setFullYear($scope.currentDate.getFullYear());
        }
        $scope.currentDate.setMonth($scope.currentDate.getMonth() - 1);
        onSelectedDateChange();
    };

    $scope.nextMonth = function () {
        if ($scope.currentDate.getMonth() === 11) {
            $scope.currentDate.setFullYear($scope.currentDate.getFullYear());
        }
        $scope.currentDate.setDate(1);
        $scope.currentDate.setMonth($scope.currentDate.getMonth() + 1);
        onSelectedDateChange();
    };


    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };
}]);

