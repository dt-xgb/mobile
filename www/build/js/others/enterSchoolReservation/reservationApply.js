
angular.module('app.controllers')
    .controller('reservationApplyCtrl', ['$scope', 'Constant', '$state', '$ionicPopup', '$ionicModal', '$ionicLoading', 'Requester', 'toaster', 'UserPreference', 'ionicDatePicker', 'ionicTimePicker', '$ionicHistory', function ($scope, Constant, $state, $ionicPopup, $ionicModal, $ionicLoading, Requester, toaster, UserPreference, ionicDatePicker, ionicTimePicker,$ionicHistory) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.user = UserPreference.getObject('user');
            $scope.userAccount = UserPreference.get('account', '');
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.selected = {
                followersNo: null,
                plateNo: '',
                teacherId: null,
                teacherName: '',
                visitCause: '',
                visitTime: '',
                schoolId: $scope.schoolId,
                visitorIdCardNo: '',
                visitorName: $scope.user.name,
                visitorPhoneNo: $scope.userAccount.substr(1,11)?$scope.userAccount.substr(1,11):'',//$scope.user.student[0].phoneNumber,
                search_name: '',
                studentId:UserPreference.get('DefaultChildID')
            };
            $scope.requestCount = 0;
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.visitorList = [];
        });

        $scope.$on("$ionicView.loaded", function (event, data) {

        });

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $scope.selectVisitor = function () {
            $scope.visitorPopup = $ionicPopup.show({
                templateUrl: UIPath + 'others/enterSchoolReservation/selectVisitorAlert.html',
                //  template:'<div><div style="text-align:center;">www.baidu.com</div><div style="height:36px;width:80px;text-align:center;">复制</div></div>',
                title: '<p ></p>',
                cssClass: 'approval_alert', //'customAlert', // String, The custom CSS class name
                scope: $scope
            });
        };

        $scope.cancelChoose = function () {
            $scope.visitorPopup.close();
        };

        //搜索姓名
        $scope.searchName = function () {
            // $scope.isSearch = ;
            $scope.getReservationTeacherInfo($scope.selected.search_name);

        };

        $scope.selectVistor = function (item, index) {
            $scope.selected.index = index;
            $scope.selected.teacherId = item.teacherId;
            $scope.selected.teacherName = $scope.selected.search_name;
            $scope.visitorPopup.close();
        };

        $scope.selectTimeModal = function () {
            setTimeout(function () {
                if (window.cordova) cordova.plugins.Keyboard.close();
                var ipObj1 = {
                    callback: function (val) {
                        if (typeof (val) !== 'undefined') {
                            $scope.nowTime = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                            $scope.showTimePicker();

                        }
                    }
                };
                ionicDatePicker.openDatePicker(ipObj1);
            }, 100);
        };

        //选择时间
        $scope.showTimePicker = function () {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {} else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        // $scope.timeBtnTxt = time;
                        if (time)
                            $scope.selected.visitTime = $scope.nowTime + ' ' + time ;
                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

        //提交申请
        $scope.commitApply = function () {
            if (!$scope.selected.teacherId) {
                toaster.warning({
                    // title: "温馨提示",
                    body: '请选择拜访对象'
                });
                return;
            }
            if (!$scope.selected.visitTime) {
                toaster.warning({
                    // title: "温馨提示",
                    body: '请选择拜访时间'
                });
                return;
            }
            var my_date = new Date();
            var currentTamp = Date.parse(my_date) / 1000; //精确到秒 当前时间 时间戳
            var visitStamp = Date.parse(new Date($scope.selected.visitTime)) / 1000;
            if (visitStamp<=currentTamp) {
                toaster.warning({
                    // title: "温馨提示",
                    body: '拜访时间不能早于当前时间'
                });
                return;
            }

            if(!(/^1(3|4|5|6|7|8|9)\d{9}$/.test($scope.selected.visitorPhoneNo))){ 
                toaster.warning({
                    // title: "温馨提示",
                    body: '手机号码有误'
                });
                return;
            } 
            if( $scope.requestCount === 0)
            $scope.addIntoSchoolReservation();
            $scope.requestCount ++;

        };

        //request --提交预约
        $scope.addIntoSchoolReservation = function () {
            Requester.addIntoSchoolReservation($scope.selected).then(function (rest) {
                $scope.requestCount = 0;
                if (rest.result) {
                    $ionicLoading.hide();
                    $ionicHistory.goBack();
                    toaster.success({
                        title: "",
                        body: "添加成功",
                        timeout: 3000
                    });
                } else {
                    $ionicLoading.hide();
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //request --获取教师列表
        $scope.getReservationTeacherInfo = function (teacherName) {
            Requester.getReservationTeacherInfo($scope.schoolId, teacherName).then(function (rest) {
                if (rest.result) {
                    $scope.isSearch = true;
                    $scope.visitorList = rest.data;
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };
    }]);