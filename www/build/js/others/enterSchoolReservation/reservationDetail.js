angular.module('app.controllers')
    .controller('reservationDetailCtrl', ['$scope', 'Constant', '$state', '$ionicModal', 'Requester', 'toaster', 'UserPreference', '$stateParams', '$rootScope', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference, $stateParams, $rootScope) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.isTeacher = String(UserPreference.getObject('user').rolename) === Constant.USER_ROLES.TEACHER;
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.itemId = $stateParams.itemId ? $stateParams.itemId : $rootScope.WXReservationItemId;
            if (isWeixin()) {
                $rootScope.WXReservationItemId = $scope.itemId;
            }
            $scope.detail = {};
            if ($scope.isTeacher) {
                $scope.getTeacherReservationDetail();
            } else {
                $scope.getParentReservationDetail();
            }

        });

        $scope.$on("$ionicView.loaded", function (event, data) {

        });

        $scope.agreeOrRefuse = function (type) {
            // type 2:agree 3;refuse
            $scope.status = type;
            $scope.agreeOrRefuseReservation();
        };

        //request --获取详情 parent
        $scope.getParentReservationDetail = function () {
            Requester.getParentReservationDetail($scope.itemId).then(function (rest) {
                if (rest.result) {
                    $scope.detail = rest.data;
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //request --获取详情 teacher
        $scope.getTeacherReservationDetail = function () {
            Requester.getTeacherReservationDetail($scope.itemId).then(function (rest) {
                if (rest.result) {
                    $scope.detail = rest.data;
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };
        //request --审核
        $scope.agreeOrRefuseReservation = function () {
            Requester.agreeOrRefuseReservation($scope.itemId, $scope.status).then(function (rest) {
                if (rest.result) {
                    var statusStr = $scope.status===2?'您已同意预约':'您已拒绝预约';
                    toaster.success({
                        title: "",
                        body: statusStr,
                        timeout: 3000
                    });
                    $scope.getTeacherReservationDetail();
                    
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };
    }]);