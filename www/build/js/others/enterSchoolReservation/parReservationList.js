angular.module('app.controllers')
   .controller('parReservationListCtrl', ['$scope', 'Constant', '$state', '$ionicModal', 'Requester', 'toaster', 'UserPreference', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference) {

      $scope.$on("$ionicView.beforeEnter", function (event, data) {
         $scope.page = 1;
         $scope.studentId = UserPreference.get('DefaultChildID');
         $scope.getParentReservationList();
         
         console.log("$scope.studentId:"+$scope.studentId);
      });

      $scope.$on("$ionicView.loaded", function (event, data) {

      });

      //查看详情
      $scope.goToDetail = function (item) {
         $state.go('reservationDetail', {
            itemId: item.id
         });
      };

      //发起预约
      $scope.reservationApply = function () {
         $state.go('reservationApply');
      };

      //下拉刷新
      $scope.refreshData = function () {
         $scope.page = 1;
         $scope.getParentReservationList();
      };
      //上拉加载
      $scope.loadMore = function () {
        $scope.page ++;
        $scope.getMoreDataList();
      };

      //request --
      $scope.getParentReservationList = function () {
         Requester.getParentReservationList($scope.page,$scope.studentId).then(function (rest) {
            if (rest.result) {
               $scope.list = rest.data.content;
            } else {
              $scope.isMoreData = true;
               toaster.warning({
                  title: "温馨提示",
                  body: rest.message
               });
            }
         }).finally(function () {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
         });
      };

      //request --加载更多
      $scope.getMoreDataList = function () {
         Requester.getParentReservationList($scope.page,$scope.studentId).then(function (rest) {
            if (rest.result) {
               var data = rest.data.content;
               for (var i = 0; i < data.length; i++) {
                  $scope.list.push(data[i]);
               }
               if (!data || (data && data.length < Constant.reqLimit)) {
                  $scope.isMoreData = true;
               }
            } else {
               $scope.isMoreData = true;
               toaster.warning({
                  title: "温馨提示",
                  body: rest.message
               });
            }
         }).finally(function () {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
         });
      };
   }]);