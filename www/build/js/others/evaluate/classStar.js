angular.module('app.controllers')
    .controller('classStarCtrl', ['$scope', 'Constant', '$state', '$ionicModal', 'UserPreference', 'NoticeService', 'ionicDatePicker', 'BroadCast', 'Requester', 'toaster', '$ionicPopup', '$ionicListDelegate', '$ionicScrollDelegate', '$ionicLoading', function ($scope, Constant, $state, $ionicModal, UserPreference, NoticeService, ionicDatePicker, BroadCast, Requester, toaster, $ionicPopup, $ionicListDelegate, $ionicScrollDelegate, $ionicLoading) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {

        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            //$scope.list = [1, 2, 3, 4];
            $scope.page = 1;
            $scope.selected = {
                add_search: '', //搜索,
                startDate: '',
                endDate: '',
                students: [],
                studentids: ''
            };
            $scope.classes = UserPreference.getArray('HeadTeacherClasses');
            //$scope.cleess= UserPreference.getArray('class');

            $scope.selected.class_id = $scope.classes && $scope.classes.length > 0 ? $scope.classes[0].id : '';
            $scope.selected.class_id = $scope.getMinOfArray();
            NoticeService.getClassMembers($scope.selected.class_id);
            $scope.getClassStarList();
        });

        //下拉刷新
        $scope.refreshData = function () {
            $scope.getClassStarList();
        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.getMoreDataList();
        };

        //更新班级id
        $scope.loadData = function () {
            $scope.recordClassId = $scope.selected.class_id;
            NoticeService.getClassMembers($scope.selected.class_id);
            $scope.getClassStarList();
        };

        //获取学生列表
        $scope.$on(BroadCast.CLASS_MEMBER_REV, function (arg, data) {
            if (data.result) {
                $scope.studentList = data.data;
                // console.log($scope.studentList);
            } else
                toaster.error({
                    title: data.message,
                    body: ''
                });
        });

        //删除班级之星
        $scope.deleteItem = function (item, $event) {
            $event.stopPropagation();
            var confirmPopup = $ionicPopup.confirm({
                title: '删除确认',
                template: '确认删除 ' + item.startDate + '-' + item.endDate + '班级之星 ?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    Requester.deleteClassStar($scope.selected.class_id, item.id).then(function (resp) {
                        if (resp.result) {
                            toaster.success({
                                body: '删除成功'
                            });
                            $ionicScrollDelegate.$getByHandle('delegateHandler').resize();
                            $scope.getClassStarList();
                        } else {
                            toaster.error({
                                title: resp.message,
                                body: ''
                            });
                        }
                    });
                }
                $ionicListDelegate.closeOptionButtons();
            });
        };


        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'others/evaluate/classStarModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.classStarModal = modal;
        });
        $ionicModal.fromTemplateUrl(UIPath + 'others/evaluate/chooseStarModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.chooseStarModal = modal;
        });
        //打开设置班级之星modal
        $scope.openClassStarModal = function () {

            $scope.classStarModal.show();
        };
        //选择时间
        $scope.selectTime = function (arg) {
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {
                        if (arg === 'start') {
                            $scope.selected.startDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                        } else if (arg === 'end') {
                            $scope.selected.endDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);

                        }
                    }
                }
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        //关闭设置班级之星modal
        $scope.hideClassStarModal = function () {
            $scope.classStarModal.hide();
        };
        //保存班级之星设置
        $scope.saveClassStarSet = function () {
            for (var i = 0; i < $scope.studentList.length; i++) {
                var stu = $scope.studentList[i];
                var count = 0;
                if (stu.isChecked) {
                    if ($scope.selected.students.length > 0) {
                      
                        for (var j = 0; j < $scope.selected.students.length; j++) {
                            if (stu.stu_id == $scope.selected.students[j]) {
                                break;
                            } else {
                                count++;
                            }
                        }
                        if (count === $scope.selected.students.length) {
                            $scope.selected.students.push(stu.stu_id);
                        }

                    } else {
                       
                        $scope.selected.students.push(stu.stu_id);
                    }

                }
                // if (stu.isChecked) {
                //     $scope.selected.students.push(stu.stu_id);
                // }
            }
            if (!$scope.selected.startDate) {
                toaster.warning({
                    title: "",
                    body: '请选择开始日期'
                });
                return;
            }
            if (!$scope.selected.endDate) {
                toaster.warning({
                    title: "",
                    body: '请选择结束日期'
                });
                return;
            }
            var time1 = $scope.selected.startDate.replace(/\-/g, "/");
            var timestamp1 = Date.parse(new Date(time1)) / 1000;
            var time2 = $scope.selected.endDate.replace(/\-/g, "/");
            var timestamp2 = Date.parse(new Date(time2)) / 1000;
            if (timestamp1 > timestamp2) {
                toaster.warning({
                    title: "",
                    body: '结束日期不能早于开始日期'
                });
                return;
            }
            if ($scope.selected.students.length === 0) {
                toaster.warning({
                    title: "",
                    body: '您还未选择班级之星'
                });
                return;
            }
            $scope.selected.studentids = $scope.selected.students.join(',');
            // $ionicLoading.show({
            //     noBackdrop: true,
            //     template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            // });

            $scope.isLoading = true;
            $scope.setClassStar();

        };

        //打开选择班级学生的modal
        $scope.openChooseStarModal = function () {

            // $scope.getContactTree($scope.selected.class_id);
            $scope.chooseStarModal.show();

        };

        //关闭选择班级学生的modal
        $scope.hideChooseStarModal = function () {
            $scope.chooseStarModal.hide();
        };

        //保存选择的学生
        $scope.saveSelectStudent = function () {
            // console.log('stu list');
            // console.log($scope.studentList);
            $scope.chooseStarModal.hide();
        };

        // request --获取班级之星列表
        $scope.getClassStarList = function () {
            $scope.page = 1;
            Requester.findClassStarList($scope.selected.class_id, $scope.page).then(function (rest) {
                if (rest.result) {
                    // console.log(rest);
                    $scope.list = rest.data;
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //上拉加载
        $scope.getMoreDataList = function () {
            Requester.findClassStarList($scope.selected.class_id, $scope.page).then(function (rest) {
                
                if (rest.result) {
                    var data = rest.data;
                    for (var i = 0; i < data.length; i++) {
                        $scope.list.push(data[i]);
                    }
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }

                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }

            }).finally(function () {
                setTimeout(function(){
                    $scope.isLoading = false;
                },5000);
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //设置班级之星
        $scope.setClassStar = function () {
            console.log($scope.selected);
            Requester.setClassStar($scope.selected).then(function (resp) {
                $scope.isLoading = false;
                if (resp.result) {
                    toaster.success({
                        title: "",
                        body: "设置成功",
                        timeout: 3000
                    });
                    $scope.selected.startDate = '';
                    $scope.selected.endDate = '';
                    $scope.selected.studentids = '';
                    $scope.selected.students = [];
                    // $scope.studentList = [];
                    for (var i = 0; i < $scope.studentList.length; i++) {
                        $scope.studentList[i].isChecked = false;
                    }
                    $scope.classStarModal.hide();
                } else {
                    toaster.warning({
                        title: "",
                        body: resp.message
                    });
                }
                return resp.result;
            }).then(function (result) {
                if (result) {
                    $scope.page = 1;
                    $scope.getClassStarList();
                }

            }).finally(function () {
                setTimeout(function(){
                    $scope.isLoading = false;
                },5000);
            });
        };


        // $scope.getContactTree = function (classid) {
        //     $scope.studentList = [];
        //     // console.log('classid:'+classid);
        //     var friends = ChatService.friendsMap[classid];
        //     for (var i = 0; i < friends.length; i++) {
        //         var p = friends[i];
        //         if (p.Category === 's') {
        //             $scope.studentList.push(p);
        //             continue;
        //         }
        //     }
        // };

        $scope.getMinOfArray = function () {
            var min = 0;
            if ($scope.classes && $scope.classes.length > 0) {
                min = $scope.classes[0].id;
                for (var i = 0; i < $scope.classes.length; i++) {
                    if ($scope.classes[i].id < min) {
                        min = $scope.classes[i].id;
                    }
                }
            }
            return min;

        };

    }]);