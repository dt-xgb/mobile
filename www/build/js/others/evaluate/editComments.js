angular.module('app.controllers')
    .controller('editCommentsCtrl', ['$scope', 'Constant', '$state', 'Requester', 'UserPreference', '$ionicPopup', '$timeout', 'toaster', '$rootScope', function ($scope, Constant, $state, Requester, UserPreference, $ionicPopup, $timeout, toaster, $rootScope) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.select = {
                index: 1,
                addOptions: [{
                    name: '礼貌待人',
                    score: 1
                }, {
                    name: '团队合作',
                    score: 1
                }, {
                    name: '遵守纪律',
                    score: 1
                }, {
                    name: '积极思考',
                    score: 1
                }, {
                    name: '举手问答',
                    score: 1
                }, {
                    name: '帮助他人',
                    score: 1
                }],
                reduceOptions: [{
                    name: '不守纪律',
                    score: 1
                }, {
                    name: '追跑打闹',
                    score: 1
                }, {
                    name: '没交作业',
                    score: 1
                }, {
                    name: '上课走神',
                    score: 1
                }, {
                    name: '迟到早退',
                    score: 1
                }, {
                    name: '不讲卫生',
                    score: 1
                }],
                scores: [1, 2, 3, 4],
                addScore: 1,
                reduceScore: 1
            };
        });

        $scope.$on("$ionicView.loaded", function (event, data) {

        });
        //减少选项
        $scope.reduceOption = function (index, type) {

            var myPopup = $ionicPopup.show({
                template: '<div style="text-align:center;">您确定要删除吗?</div>',
                title: '小跟班提示',
                scope: $scope,
                buttons: [{
                        text: '取消',
                        type: 'button-xlightgrey'
                    },
                    {
                        text: '<b>确定</b>',
                        type: 'button-xgreen',
                        onTap: function (e) {
                            // e.preventDefault();
                            console.log('close');
                            if (type === 'a') {
                                //加分项
                                $scope.select.addOptions.splice(index, 1);
                            } else if (type === 'r') {
                                //减分项
                                $scope.select.reduceOptions.splice(index, 1);
                            }
                        }
                    },
                ]
            });


        };
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        //增加选项
        $scope.addOption = function (type) {
            var title;
            $scope.optionType = type;
            if (type === 'a') {
                //加分项
                title = '添加加分类型';
            } else if (type === 'r') {
                //减分项
                title = '添加减分类型';
            }

            $scope.addOptionModal = $ionicPopup.show({
                templateUrl: UIPath + 'others/evaluate/addOptionModal.html',
                title: '<p style="color:white;">' + title + '</p>',
                scope: $scope,
                cssClass: 'approval_alert'
            });
        };

        //确定
        $scope.commit = function () {

        };

        $scope.closeOptionModal = function () {
            $scope.addOptionModal.close();
        };

        //选择分值
        $scope.chooseScoreOption = function (score) {
            $scope.select.addScore = score;
        };

    }]);