angular.module('app.controllers')
    .controller('evaluateOptionsCtrl', ['$scope', '$state', 'Constant', '$ionicModal', '$stateParams', '$rootScope', 'BroadCast', 'Requester', 'toaster', function ($scope, $state, Constant, $ionicModal, $stateParams, $rootScope, BroadCast, Requester, toaster) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            document.body.addEventListener('touchmove', function (e) {

                if (document.activeElement && ionic.Platform.isIOS())
                    document.activeElement.blur();
            }, {
                passive: false
            });

        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.class_id = $stateParams.classId ? $stateParams.classId : $rootScope.rootWxClassId;
            $rootScope.rootWxClassId = $scope.class_id;
            $scope.taskId = $stateParams.taskId ? $stateParams.taskId : $rootScope.rootWxTaskId;
            $rootScope.rootWxTaskId = $scope.taskId;
            $scope.assessTitle = $stateParams.assessTitle ? $stateParams.assessTitle : $rootScope.rootWxAssessTitle;
            $rootScope.rootWxAssessTitle = $scope.assessTitle;
           // console.log('$scope.class_id:' + $scope.class_id);
            $scope.getAssessOptionList();

        });


        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'others/evaluate/evaluateStuModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.evaluateStuModal = modal;
        });
        $scope.showEvaluateModal = function (item) {
            $scope.itemTitle = item.itemName;
            $scope.maxScore = item.maxScore;
            $scope.minScore = item.minScore;
            $scope.itemId = item.itemId;
            $scope.getOneAssessOptionClassScoreList(item.itemId);
            $scope.evaluateStuModal.show();
        };
        //隐藏modal
        $scope.hideEvaluateStuModal = function () {
            $scope.evaluateStuModal.hide();
        };

        //查看评价统计
        $scope.evaluateResult = function () {
            $state.go('evaluateStatistical', {
                classId: $scope.class_id,
                taskId: $scope.taskId,
                assessTitle: $scope.assessTitle
            });
        };



        //提交学生评价
        $scope.commitStudentEvaluate = function () {
            //console.log($scope.studentList);
            $scope.selected = {
                scores: []
            };
            console.log($scope.studentList);
            for (var i = 0; i < $scope.studentList.length; i++) {
                var stu = $scope.studentList[i];

                if (!stu.score && stu.score !== 0) {
                    toaster.warning({
                        title: '',
                        body: stu.studentName + '未评分',
                        timeout: 3000
                    });
                    return;
                } else if (stu.score > $scope.maxScore || stu.score < $scope.minScore || !isInteger(stu.score)) {
                    toaster.warning({
                        title: '',
                        body: stu.studentName + '的评分值应该为' + $scope.minScore + '到' + $scope.maxScore + '之间的整数',
                        timeout: 3000
                    });
                    return;
                } else {
                    $scope.selected.scores.push({
                        studentId: stu.studentId,
                        score: parseInt(stu.score)
                    });
                }
            }

            console.log($scope.selected);
            $scope.commitStudentAssessScore();

        };
        // --request 查询指定班级的评分项
        $scope.getAssessOptionList = function () {
            Requester.getAssessOptionList($scope.class_id, $scope.taskId).then(function (resp) {
                if (resp.result) {

                    $scope.list = dataDeal(resp.data);


                } else {
                    toaster.error({
                        title: resp.message,
                        body: ''
                    });
                }
            });
        };

        //--request 获取评价任务指定评价项的班级评分列表
        $scope.getOneAssessOptionClassScoreList = function (itemId) {
            Requester.getOneAssessOptionClassScoreList($scope.class_id, $scope.taskId, itemId).then(function (resp) {
                if (resp.result) {
                    $scope.studentList = resp.data;
                    // console.log('$scope.studentList');
                    // console.log($scope.studentList);
                    $scope.studentList.forEach(function (stu) {
                        if (stu.score === '0'||stu.score==='0.00'||stu.score==='0.0') {
                            stu.score = 0;
                        } else if (parseInt(stu.score) > 0) {
                            stu.score = parseInt(stu.score);
                        }



                    });

                } else {
                    toaster.error({
                        title: resp.message,
                        body: ''
                    });
                }
            });
        };

        //--提交评分
        $scope.commitStudentAssessScore = function () {
            Requester.commitStudentAssessScore($scope.selected, $scope.taskId, $scope.itemId, $scope.class_id).then(function (resp) {
                if (resp.result) {
                    toaster.success({
                        title: "",
                        body: "评分完成",
                        timeout: 3000
                    });
                    return resp.result;
                } else {
                    toaster.error({
                        title: resp.message,
                        body: ''
                    });
                }

            }).then(function (result) {
                if (result) {
                    $scope.evaluateStuModal.hide();
                    $scope.getAssessOptionList();
                }
            });
        };

        function dataDeal(data) {
            var listArr = [];
            data.forEach(function (el, index) {
                for (var i = 0; i < listArr.length; i++) {
                    // 对比相同的字段key，相同放入对应的数组
                    if (listArr[i].typeId == el.typeId) {
                        listArr[i].group.push({
                            itemName: el.itemName,
                            itemId: el.itemId,
                            maxScore: el.maxScore,
                            minScore: el.minScore,
                            completed: el.completed
                        });
                        return;
                    }
                }
                // 第一次对比没有参照，放入参照
                listArr.push({
                    typeName: el.typeName,
                    typeId: el.typeId,
                    group: [{
                        itemName: el.itemName,
                        itemId: el.itemId,
                        maxScore: el.maxScore,
                        minScore: el.minScore,
                        completed: el.completed
                    }]
                });
            });
            return listArr;
        }

        function isInteger(obj) {
            return obj % 1 === 0;
        }

    }]);