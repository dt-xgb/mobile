angular.module('app.controllers')
    .controller('babyVideoDetailCtrl', ['$scope', 'Constant', '$state', '$sce', 'toaster', '$stateParams', '$rootScope', function ($scope, Constant, $state, $sce, toaster, $stateParams, $rootScope) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            var recordUrl = $stateParams.url?$stateParams.url:$rootScope.rootWxBabyVideoUrl;
            $scope.videoUrl = $sce.trustAsResourceUrl(recordUrl);
            if(isWeixin()){
                $rootScope.rootWxBabyVideoUrl = recordUrl;
            }
        });
    }]);