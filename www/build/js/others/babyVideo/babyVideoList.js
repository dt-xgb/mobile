angular.module('app.controllers')
    .controller('babyVideoListCtrl', ['$scope', 'Constant', '$state', '$ionicModal', 'Requester', 'toaster', 'UserPreference', '$ionicPopup', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference,$ionicPopup) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            var user = UserPreference.getObject('user');
            console.log(user);
            $scope.currentSID = UserPreference.get('DefaultChildID');
            $scope.role = String(user.rolename);
            //$scope.videoList = [];
            var authMap = user.schauthmap;
            if( $scope.role ==  Constant.USER_ROLES.TEACHER){
                $scope.videoList = user.cameraManageList&&user.cameraManageList.length>0?user.cameraManageList:[];
            }else{
                for (var i = 0; i < authMap.length; i++){
                    if($scope.currentSID == authMap[i].id){
                        $scope.videoList = authMap[i].cameraManageList&&authMap[i].cameraManageList.length>0?authMap[i].cameraManageList:[];
                    }
                }
            }
            $scope.isTeacher = user.rolename == '3'? true :false;
        });

        //支付提示
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $scope.payAlertModal = function(){
            $scope.payAlert = $ionicPopup.show({
                templateUrl: UIPath + 'others/babyVideo/payAlert.html',
                // title: '<p style="color:white;">111</p>',
                scope: $scope,
                cssClass: 'approval_alert'
            });
        };

        $scope.cancelChoose = function(){
            $scope.payAlert.close();
        };
        //开通视频权限
        $scope.openVideoPermissions = function(){
            $scope.payAlert.close();
            $state.go('babyVideoOrder');
        };

        var time_range = function (beginTime, endTime) {
            var strb = beginTime.split (":");
            if (strb.length != 2) {
                return false;
            }
            var stre = endTime.split (":");
            if (stre.length != 2) {
                return false;
            }
            var b = new Date ();
            var e = new Date ();
            var n = new Date ();
            b.setHours (strb[0]);
            b.setMinutes (strb[1]);
            e.setHours (stre[0]);
            e.setMinutes (stre[1]);
            if (b.getTime() <= e.getTime()) {
                return n.getTime () > b.getTime () && n.getTime () < e.getTime ();
            } else {
                return n.getTime () > b.getTime () ||  n.getTime () < e.getTime ();
            }
        };

        $scope.viewable = function(item){
            if (!item.babyVideoTimeIntervalStr)
                return false;
            if (item.videoStatus !== '2' && !$scope.isTeacher) {
                return false;
            }
            var times = [];
            if(item.babyVideoTimeIntervalStr.indexOf(',')>0){
                times = item.babyVideoTimeIntervalStr.split(',');
            }else{
                times.push(item.babyVideoTimeIntervalStr);
            }
            var result = false;
            for(var i=0;i<times.length;i++){
                var timeString = times[i].split('-');
                if (timeString.length != 2)
                    return false;
                else {
                    result = time_range(timeString[0], timeString[1]);
                    if(result)
                        break;
                }
            }
            return result;
        };

        $scope.onVideClick = function(item){
               // console.log(item);
                if($scope.isTeacher || item.videoStatus === '2') {
                    if($scope.viewable(item)){
                        $state.go('babyVideoDetail', {url: item.pcBroadcastUrl});
                    }else{
                        toaster.warning({
                            title: '宝宝视频',
                            body: '请在开放时段内观看'
                        });
                    }
                    
                } else {
                    $scope.payAlertModal();
                }
        };

    }]);