angular.module('app.controllers')
    .controller('babyVideoOrderCtrl', ['$scope', 'Constant', '$state', '$ionicModal', 'Requester', 'toaster', 'UserPreference', '$ionicLoading', '$ionicPopup', '$ionicHistory', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference, $ionicLoading, $ionicPopup, $ionicHistory) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.goods = {
                count: 1,
                price: 0,
                goodsName: '',
                imgUrl: ''
            };
            $scope.needRepay = true;
        });

        $scope.$on("$ionicView.loaded", function (event, data) {

            $scope.user = UserPreference.get('user');
           // console.log($scope.user);
            $scope.studentId = UserPreference.get('DefaultChildID');
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.getBabyVideoPrice();

        });

        $scope.editGoodsCount = function (type) {


            if (type === 'r') {
                if ($scope.goods.count <= 1) {
                    toaster.warning({
                        //title: MESSAGES.LOGIN_ERROR,
                        body: '不能再少了,一次至少购买1个月'
                    });
                    return;
                }
                $scope.goods.count--;
            } else {
                if ($scope.goods.count >= 5) {
                    toaster.warning({
                        //title: MESSAGES.LOGIN_ERROR,
                        body: '一次最多只能购买5个月'
                    });
                    return;
                }
                $scope.goods.count++;
            }

        };

        $scope.reqPayWith = function (type) {
            //0: alipay 1:wechat
            //验证是否已支付
            if (!$scope.needRepay) {
                toaster.success({
                    title: '您已成功支付。',
                    body: ''
                });
                return;
            }
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            var childID = UserPreference.get('DefaultChildID');
            //新增订单
            Requester.newOrder('宝宝视频在线观看费用', [{
                    goodId: 2,
                    number: $scope.goods.count,
                    orderSerial: '',
                    itemAmount: $scope.goods.price
                }], childID, $scope.schoolId, '宝宝视频', 2)
                .then(function (resp) {
                    if (resp.result) {
                        $scope.orderSerial = resp.data.orderSerial;
                        start2Pay(type, resp.data.orderSerial);
                    } else
                        toaster.error({
                            title: resp.message,
                            body: ''
                        });
                }).finally(function () {
                    $ionicLoading.hide();
                });
        };

        function start2Pay(type, orderSerial) {
            if (orderSerial) {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });

                Requester.newOrderPayment(type, orderSerial).then(function (resp) {
                    if (resp.result) {
                        if (type === 0) {
                            //alipay
                            if (resp.data.tradeStatus.toUpperCase() === 'SUCCESS' && resp.data.aliAppRequestStr) {
                                var uri;
                                if (device.platform.toLowerCase() == 'ios') {
                                    uri = 'alipay://';
                                } else {
                                    uri = 'com.eg.android.AlipayGphone';
                                }

                                appAvailability.check(
                                    uri, // URI Scheme
                                    function () {
                                        cordova.plugins.alipay.payment(resp.data.aliAppRequestStr, function success(e) {
                                            onPaySuccess();
                                        }, function error(e) {
                                            if (e.resultStatus === '9000' || e.resultStatus === '6004') {
                                                onPayFail(true);
                                                setTimeout(function () {
                                                    Requester.isPaymentSuccess().then(function (resp) {
                                                        if (String(resp.data.tradeCode) === '1')
                                                            onPaySuccess();
                                                        else if (String(resp.data.tradeCode) === '3')
                                                            onPayFail();
                                                    });
                                                }, 5000);
                                            } else
                                                onPayFail();
                                        });
                                    },
                                    function () {
                                        $ionicPopup.alert({
                                            title: '提示',
                                            template: '未检测到支付宝应用，请安装支付宝或使用其它方式支付。',
                                            okText: '确定',
                                            okType: 'button-balanced'
                                        });
                                    }
                                );
                            }
                        } else if (type === 1) {
                            //wechat
                            if (resp.data.tradeStatus.toUpperCase() === 'SUCCESS' && resp.data.wxAppRequestVo) {
                                Wechat.isInstalled(function (installed) {
                                    if (installed) {
                                        var req = {
                                            "partnerid": resp.data.wxAppRequestVo.mchId,
                                            "timestamp": resp.data.wxAppRequestVo.timeStamp,
                                            "noncestr": resp.data.wxAppRequestVo.nonceStr,
                                            "prepayid": resp.data.wxAppRequestVo.prePayId,
                                            "sign": resp.data.wxAppRequestVo.paySign
                                        };
                                        Wechat.sendPaymentRequest(req, function () {
                                            //show success
                                            onPaySuccess();
                                        }, function (reason) {
                                            console.error(reason);
                                            onPayFail();
                                        });
                                    } else {
                                        $ionicPopup.alert({
                                            title: '提示',
                                            template: '未检测到微信应用，请安装微信或使用其它方式支付。',
                                            okText: '确定',
                                            okType: 'button-balanced'
                                        });
                                    }
                                }, function (reason) {});
                            } else
                                toaster.warning({
                                    title: resp.message,
                                    body: resp.data.msg
                                });
                        }
                    } else {
                        toaster.error({
                            title: resp.message,
                            body: ''
                        });
                    }
                }).finally(function () {
                    setTimeout(function () {
                        $ionicLoading.hide();
                    }, 1000);
                });
            }

        }


        function onPaySuccess() {
            $scope.payRstImg = 'img/success.png';
            $scope.payRstDesc = '您已成功支付本订单！';
            $scope.payRst = '支付成功';
            $scope.needRepay = false;
            $scope.openModal();
            var user = UserPreference.getObject('user');
            var authMap = user.schauthmap;
            var videoList ;
            $scope.currentSID = UserPreference.get('DefaultChildID');
            for (var j = 0; j < authMap.length; j++){
                if($scope.currentSID == authMap[j].id){
                    videoList  = authMap[j].cameraManageList&&authMap[j].cameraManageList.length>0?authMap[j].cameraManageList:[];
                }
            }
      // var videoList = user.cameraManageList&&user.cameraManageList.length>0?user.cameraManageList:[];
            for(var i=0;i<videoList.length;i++){
                videoList[i].videoStatus = '2';
            }

            for (var k = 0; k < authMap.length; k++){
                if($scope.currentSID == authMap[k].id){
                    authMap[k].cameraManageList = videoList;
                }
            }
            user.schauthmap = authMap;
            UserPreference.setObject("user", user);
        }

        function onPayFail(needWait) {
            if (needWait) {
                $scope.payRstImg = 'img/fail.png';
                $scope.payRstDesc = '支付结果获取中，请稍后...';
                $scope.payRst = '支付完成';
                $scope.needRepay = false;
            } else {
                $scope.payRstImg = 'img/fail.png';
                $scope.payRstDesc = '抱歉，本次支付未完成！';
                $scope.payRst = '支付失败';
                $scope.needRepay = true;
            }
            $scope.openModal();
        }

        //支付结果提示框
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'order/payResult.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };

        $ionicModal.fromTemplateUrl(UIPath + 'order/orderDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modalDetail = modal;
        });

        //查看订单详情
        $scope.openDetail = function () {
            getOrderInfo($scope.orderSerial);
            $scope.closeModal();
            $scope.modalDetail.show();
        };

        $scope.closeDetailView = function () {
            $scope.modalDetail.hide();
            $scope.goBack();
        };

        $scope.goBack = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('babyVideoList');
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
            $scope.modalDetail.remove();
        });


        function getOrderInfo(orderSerial) {
            Requester.getOrderInfo(orderSerial).then(function (resp) {
                if (resp.result) {
                    $scope.detailItem = resp.data;
                } else {
                    toaster.error({
                        title: data.message,
                        body: ''
                    });
                }
            });
        }

        $scope.getStatusTxt = function (status) {
            return getOrderStatusTxt(status);
        };

        $scope.getStatusIcon = function (status) {
            return getOrderStatusIcon(status);
        };

        $scope.getChannelName = function (status) {
            return getPayChannelName(status);
        };

        //获取宝宝视频单价
        $scope.getBabyVideoPrice = function () {
            Requester.selectVideoPrice($scope.schoolId).then(function (rest) {
                if (rest.result) {
                    $scope.goods.price = rest.data.price;
                    $scope.getGoodInfo();
                } else {
                    toaster.error({
                        title: rest.message,
                        body: ''
                    });
                }
            });
        };

        //查询商品信息
        $scope.getGoodInfo = function () {
            Requester.getGoodInfo(2, 2, $scope.schoolId).then(function (rest) {
                if (rest.result) {
                  //  console.log(rest);
                    $scope.goods.goodsName = rest.data?rest.data.goodName:'';
                    $scope.goods.imgUrl = rest.data?rest.data.imgUrl:'';
                } else {
                    toaster.warning({
                        //title: MESSAGES.LOGIN_ERROR,
                        body: rest.message
                    });
                }
            });
        };
    }]);