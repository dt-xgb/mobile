angular.module('app.controllers')
    .controller('covidWarningDetailCtrl', ['$scope', 'Constant', '$state', 'Requester', 'toaster', '$stateParams', '$rootScope', 'UserPreference', '$ionicPopup', function ($scope, Constant, $state, Requester, toaster, $stateParams,$rootScope,UserPreference,$ionicPopup) {


        window.addEventListener('native.keyboardshow', function (keyboardParameters) {
            // console.log('keyboard open warning');
             var tObj = document.getElementById("desc");
             var sPos = tObj.value.length;
             setTimeout(function(){
                 if(ionic.Platform.isIOS())
                 setCaretPosition(tObj, sPos);
             },100);
            
         });
 
        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.warning = $stateParams.id?$stateParams.id:$rootScope.rootWxCovidWarningId;
            console.log('warning:'+$scope.warning);
            if(isWeixin()){
                $rootScope.rootWxCovidWarningId = $scope.warning;
            }
            $scope.events = [];
            $scope.isHeadTeacher = UserPreference.getBoolean('isHeadTeacher');
            Requester.getWarningDetail($scope.warning.id).then(function(res){
                console.log(res);
                $scope.events = res.data;
            });

            $scope.selectedStatus = '1';
            $scope.activeTab = 'pending';
            $scope.formData = {
                desc: ''
            };
        });

        $scope.changeStatus = function (arg) {
            if ($scope.activeTab === arg) {
                return;
            }
            $scope.activeTab = arg;
            if (arg === 'clear') {
                $scope.selectedStatus = '2';
            } else {
                $scope.selectedStatus = '1';
            }
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $scope.openModal = function(){
            $scope.selectModel = $ionicPopup.show({
                templateUrl: UIPath + 'others/covid/handleWarning.html',
                scope: $scope,
                cssClass: 'approval_alert'
            });
        };
        $scope.cancelChoose = function(){
            $scope.selectModel.close();
        };

        $scope.submit = function(){
            Requester.handleWarning($scope.warning.id, $scope.selectedStatus, $scope.formData.desc).then(function(res){
                if(res.result) {
                    toaster.success({title: '跟进成功!', body: '',timeout:'3000'});
                    $scope.cancelChoose();
                    Requester.getWarningDetail($scope.warning.id).then(function(res){
                        $scope.events = res.data;
                    });
                }else{
                    toaster.warning({title: res.message, body: '',timeout:'3000'}); 
                    $scope.selectModel.close();
                }
            });
        };
    }]);