angular.module('app.controllers')
    .controller('covidCheckDetailCtrl', ['$scope', '$stateParams', '$state', 'Requester', 'toaster', 'UserPreference', '$rootScope', function ($scope, $stateParams, $state, Requester, toaster, UserPreference,$rootScope) {

        $scope.$on("$ionicView.enter", function (event, data) {
            var rId = $stateParams.id ?$stateParams.id:$rootScope.roootWxCovidRId;
            $scope.name = $stateParams.stuName?$stateParams.stuName:$rootScope.rootWxCovidStuName;
            if(isWeixin()){
                $rootScope.roootWxCovidRId = rId;
                $rootScope.rootWxCovidStuName = $scope.name;
            }

            $scope.events=[];
            Requester.getCheckedDetail(rId).then(function(res){
                $scope.events = res.data;
            });
            
        });

    }]);