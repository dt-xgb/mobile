angular.module('app.controllers')
    .controller('dateArrangeCtrl', ['$scope', 'Constant', 'UserPreference', '$stateParams', '$ionicPopup', '$ionicModal', 'ionicDatePicker', 'ionicTimePicker', 'toaster', 'Requester', function ($scope, Constant, UserPreference, $stateParams, $ionicPopup, $ionicModal, ionicDatePicker, ionicTimePicker, toaster, Requester) {
        $scope.$on('$ionicView.beforeEnter', function (event, data) {

            $scope.footerShow = true;
            $scope.month_olympic = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            $scope.month_normal = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            $scope.month_name = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
            $scope.holder = document.getElementById('days');
            $scope.prev = document.getElementById('prev');
            $scope.next = document.getElementById('next');
            $scope.ctitle = document.getElementById('calendar-title');
            $scope.cyear = document.getElementById('calendar-year');

            //用户id
            $scope.userId = UserPreference.getObject('user').id;
            //获取当前日期
            $scope.my_date = new Date();
            $scope.currentTamp = Date.parse($scope.my_date) / 1000; //精确到秒 当前时间 时间戳

            $scope.my_year = $scope.my_date.getFullYear(); //年
            $scope.my_month = $scope.my_date.getMonth(); //月
            $scope.my_day = $scope.my_date.getDate(); //日

            $scope.currentDate = $scope.my_year + '-' + ($scope.my_month + 1) + '-' + $scope.my_day;
            $scope.selectDate = $stateParams.arrangeDate ? $stateParams.arrangeDate : $scope.currentDate;
           
            //  console.log('current time date:' +  getNowFormatDate('2018-1-1'));
            $scope.refreshDate();

            //获取日程安排列表
            $scope.arrangeDates = [];
            $scope.getAllDateArrange();

            $scope.selected = {
                date: '',
                schedule: '',
                isNotice: false,
                reminder_time: ''
            };
        });

        $scope.$on('$ionicView.loaded', function (event, data) {

        });

        //获取某年某月第一天是星期几
        $scope.dayStart = function (month, year) {
            var tmpDate = new Date(year, month, 1);
            return tmpDate.getDay();
        };
        $scope.$on("$ionicView.leave", function (event, data) {
            $scope.footerShow = false;
        });

        //编辑或删除日程安排
        $scope.editOrDeleteArrange = function (type, item) {

            if (type === 'edit') {
                $scope.isEdit = true;
                $scope.selected = {
                    date: item.date,
                    schedule: item.schedule,
                    isNotice: item.reminderFlag,
                    reminder_time: item.reminderTime
                };
                $scope.scheduleId = item.id;
                $scope.arrangeModal.show();
            } else {
                var myPopup = $ionicPopup.show({
                    template: '<div style="text-align:center;">您确定删除该条日程吗?删除后不可恢复！</div>',
                    title: '小跟班提示',
                    scope: $scope,
                    buttons: [{
                            text: '取消',
                            type: 'button-xlightgrey'
                        },
                        {
                            text: '<b>确定</b>',
                            type: 'button-xgreen',
                            onTap: function (e) {
                                // e.preventDefault();
                                $scope.deleteTeacherArrange(item.id);
                                console.log('close');

                            }
                        },
                    ]
                });
            }

        };

        //计算某年是不是闰年，通过求年份除以4的余数即可
        $scope.daysMonth = function (month, year) {
            var tmp = year % 4;
            if ((tmp == 0 && year % 100 != 0) || year % 400 === 0) {
                return $scope.month_olympic[month];
            } else {
                return $scope.month_normal[month];
            }
        };

        $scope.refreshDate = function () {
            $scope.list = [];
            var str = '';
            var totalDay = $scope.daysMonth($scope.my_month, $scope.my_year); //获取该月总天数
            var firstDay = $scope.dayStart($scope.my_month, $scope.my_year); //获取该月第一天是星期几
            $scope.getFirstDay = firstDay;
           // if(firstDay===0) firstDay = 7;
           // console.log('first day:'+firstDay);
            var myclass;
            for (var j = 0; j < firstDay; j++) {
                //   str += "<li></li>"; //为起始日之前的日期创建空白节点
                $scope.list.push('');
            }
            for (var i = 1; i <= totalDay; i++) {
                if (
                    (i < $scope.my_day &&
                        $scope.my_year == $scope.my_date.getFullYear() &&
                        $scope.my_month == $scope.my_date.getMonth()) ||
                    $scope.my_year < $scope.my_date.getFullYear() ||
                    ($scope.my_year == $scope.my_date.getFullYear() &&
                        $scope.my_month < $scope.my_date.getMonth())
                ) {
                    // myclass = " class='lightgrey'"; //当该日期在今天之前时，以浅灰色字体显示
                    // $scope.dayStatus = 'l';
                } else if (
                    i == $scope.my_day &&
                    $scope.my_year == $scope.my_date.getFullYear() &&
                    $scope.my_month == $scope.my_date.getMonth()
                ) {
                    // myclass = " class='green greenbox'"; //当天日期以绿色背景突出显示
                    $scope.dayStatus = 'g';
                } else {
                    //  myclass = " class='darkgrey'"; //当该日期在今天之后时，以深灰字体显示
                    // $scope.dayStatus = 'd';
                }
                // str += "<li" + myclass + ">" + i + "</li>"; //创建日期节点
                $scope.list.push(i);
            }
            // $scope.holder.innerHTML = str; //设置日期显示
            // $scope.ctitle.innerHTML = $scope.month_name[$scope.my_month]; //设置英文月份显示
            // $scope.cyear.innerHTML = $scope.my_year; //设置年份
            $scope.getYear = $scope.my_year;
            $scope.getMonth = $scope.month_name[$scope.my_month];
            // alert('month:'+ $scope.ctitle.innerHTML+'years:'+$scope.cyear.innerHTML);
        };

        //选中某一天
        $scope.changeSelectDay = function (item) {
            // $scope.selectDay = item;
            $scope.selectDate = $scope.my_year + '-' + ($scope.my_month + 1) + '-' + item;
             //console.log('选中某一天：'+$scope.selectDate);
            $scope.getTeacherArrangeList($scope.selectDate, $scope.selectDate);
        };
        //上一个月
        $scope.preMonth = function (e) {
            e.preventDefault();
            $scope.my_month--;
            if ($scope.my_month < 0) {
                $scope.my_year--;
                $scope.my_month = 11;
            }

            // $scope.selectDate = $scope.my_year + '-' + ($scope.my_month + 1);
            $scope.refreshDate();
        };

        //下一个月
        $scope.nextMonth = function (e) {
            e.preventDefault();

            $scope.my_month++;
            if ($scope.my_month > 11) {
                $scope.my_year++;
                $scope.my_month = 0;
            }
            //  $scope.selectDate = $scope.my_year + '-' + ($scope.my_month + 1);
            $scope.refreshDate();
            // $scope.isCurrentDay = false;
        };

        //比较日期是否相等
        $scope.isSameDate = function (dateStr1, dateStr2) {
            var result;
            result = dateToSecond(dateStr1) === dateToSecond(dateStr2) ;
            // console.log('result:' + result);
            return result;
        };

        //将2019-1-1转成 2019-01-01形式 并转成时间戳
        function dateToSecond(dstr1) {
            // var dstr = dstr1 + ' ' + '00' + ':'+'00'+':'+'00';
            // var dArr = dstr.match(/\d+/g);
            // var date = new Date();
            // date.setYear(dArr[0]);
            // date.setMonth(dArr[1] - 1);
            // date.setDate(dArr[2]);
            //  return date.getTime();
              
           var tamp = getNowFormatDate(dstr1);
          // var tamp =  Date.parse(new Date(date1.replace(/\-/g, "/")))/1000;
           return tamp;
        }


        //日历上显示当前日程安排的日期
        $scope.currentDateIsOnArrange = function (item) {
            var result = 0;
            var date = $scope.my_year + '-' + ($scope.my_month + 1) + '-' + item;
            //console.log('item:'+item);
            if(!item){
                return result;
            }
           
            //当前日期如果是选中日期
            if (date === $scope.selectDate) {
                result = 1;
            } else {
                for (var j = 0; j < $scope.arrangeDates.length; j++) {
                  //  var tamp1 =  Date.parse(new Date($scope.arrangeDates[j].replace(/\-/g, "/")))/1000;
                    if (dateToSecond(date) ===  dateToSecond($scope.arrangeDates[j])) {
                        result = 2;
                        break;
                    } else {
                        result = 0;
                    }
                }
                // return result;
            }

          return result;

        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'others/dateArrange/dateArrangeSetModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.arrangeModal = modal;
        });
        //添加日程安排
        $scope.addDateArrange = function () {
            $scope.arrangeModal.show();
        };

        //关闭添加日程安排设置
        $scope.hideSetDateArrangeModal = function () {
            $scope.arrangeModal.hide();
            if ($scope.isEdit === true) {
                $scope.selected = {
                    date: '',
                    schedule: '',
                    isNotice: false,
                    reminder_time: ''
                };
                $scope.isEdit = false;
                $scope.scheduleId = '';
            }
        };

        //选择日期
        $scope.chooseDate = function (type) {
            setTimeout(function () {
               if(window.cordova) cordova.plugins.Keyboard.close();
                var ipObj1 = {
                    callback: function (val) {
                        if (typeof (val) !== 'undefined') {
                            if (type === 'create') {
                                $scope.selected.date = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                            } else {
                                $scope.selected.reminder_time = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                                $scope.showTimePicker();
                            }

                        }
                    }
                };
                ionicDatePicker.openDatePicker(ipObj1);
            }, 100);

        };

        //选择时间
        $scope.showTimePicker = function () {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {} else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        // $scope.timeBtnTxt = time;
                        $scope.selected.reminder_time = $scope.selected.reminder_time + ' ' + time + ':00';
                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

        $scope.changeToggole = function () {
            // console.log('isNotice:'+$scope.selected.isNotice);
            //  if(!$scope.selected.isNotice) $scope.selected.reminder_time = '';
        };

        //提交
        $scope.commitDateArrange = function () {
            if (!$scope.selected.date) {
                toaster.warning({
                    title: "",
                    body: '请选择日期',
                    timeout: 3000
                });
                return;
            }
            var remindStamp = Date.parse(new Date($scope.selected.reminder_time.replace(/\-/g, "/"))) / 1000;
            if ($scope.selected.isNotice && !$scope.selected.reminder_time) {
                toaster.warning({
                    title: "",
                    body: '请设置提醒时间',
                    timeout: 3000
                });
                return;
            } else if ($scope.selected.isNotice && $scope.selected.reminder_time && remindStamp - $scope.currentTamp < 0) {
                toaster.warning({
                    title: "",
                    body: '提醒时间不能早于当前时间',
                    timeout: 3000
                });
                return;
            }


            //console.log($scope.selected);
            var scheduleId = $scope.isEdit ? $scope.scheduleId : '';
            if (!$scope.selected.isNotice) $scope.selected.reminder_time = '';
            $scope.updateTeacherArrange(scheduleId, $scope.isEdit);
            //$scope.arrangeModal.hide();
        };


        //刷新
        $scope.refreshData = function () {
            $scope.getAllDateArrange();
        };

        //requester --获取所有的日程安排
        $scope.getAllDateArrange = function () {
            Requester.getTeacherArrangeList('', '').then(function (resp) {
                if (resp.result) {
                    for (var i = 0; i < resp.data.length; i++) {
                        $scope.arrangeDates.push(resp.data[i].date);
                    }
                    return resp.result;
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }
            }).then(function (result) {
                if (result) {
                    $scope.getTeacherArrangeList($scope.selectDate, $scope.selectDate);
                }
            });
        };

        //获取日程安排列表
        $scope.getTeacherArrangeList = function (startDate, endDate) {
            Requester.getTeacherArrangeList(startDate, endDate).then(function (resp) {
                if (resp.result) {
                    $scope.arrangeList = resp.data;
                    // console.log($scope.list);
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //删除某天日程安排
        $scope.deleteTeacherArrange = function (scheduleId) {
            Requester.deleteTeacherArrange(scheduleId).then(function (resp) {
                if (resp.result) {
                    //$scope.classFeeList = resp.data;

                    $scope.getAllDateArrange();
                    toaster.success({
                        body: '删除成功'
                    });
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }
            });
        };
        //新增或修改某天日程安排
        $scope.updateTeacherArrange = function (scheduleId, modificationFlag) {
            Requester.updateTeacherArrange(scheduleId, $scope.userId, $scope.selected, modificationFlag).then(function (resp) {
                if (resp.result) {
                    var successMsg = scheduleId ? '更新成功' : '添加成功';
                    toaster.success({
                        body: successMsg
                    });
                    $scope.selected = {
                        date: '',
                        schedule: '',
                        isNotice: false,
                        reminder_time: ''
                    };
                    $scope.isEdit = false;
                    $scope.arrangeModal.hide();
                    $scope.getAllDateArrange();
                    // if(scheduleId){
                    //     $scope.getTeacherArrangeList($scope.selectDate , $scope.selectDate);
                    // }else{

                    // }


                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: resp.message
                    });
                }
            });
        };

    }]);
