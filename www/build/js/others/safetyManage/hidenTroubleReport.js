angular.module('app.controllers')
    .controller('hidenTroubleReportCtrl', ['$scope', 'Constant', '$state', '$ionicModal', '$ionicLoading', 'Requester', 'toaster', 'UserPreference', '$timeout', 'CameraHelper', '$q', '$ionicHistory', '$rootScope', function ($scope, Constant, $state, $ionicModal, $ionicLoading, Requester, toaster, UserPreference, $timeout, CameraHelper, $q, $ionicHistory,$rootScope) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.select = {
                selectManName: '',
                selectManId: 0,
                location: '',
                title: '',
                message: '',
                picdatas: []
            };
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.reportImgs = [];
        });

        $scope.$on("$ionicView.loaded", function (event, data) {

        });

        function onImageResized(resp) {
            $scope.select.picdatas.push(resp);
        }
        //提交隐患
        $scope.commitTrouble = function () {
            $scope.select.picdatas = [];
            var promiseArr = [];
            for(var i=0;i< $scope.reportImgs.length;i++ ){
                var img = $scope.reportImgs[i];
                promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized)); 
            }
            $scope.isLoading = true;
            $q.all(promiseArr).then(function () {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                $scope.addHidenTrouble();

            });
        };

        //选择图片
        $scope.selectImg = function () {
          
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.reportImgs, resp);
                        $scope.$apply();
                    } else {
                        $scope.reportImgs.push(
                            "data:image/jpeg;base64," + resp
                        );
                    }
                }, 9 - $scope.reportImgs.length);
            } else {
                //web 端
                var input = document.getElementById("capture101");
                input.click();
            }
        };

        // input形式打开系统系统相册
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.reportImgs.push($scope.testImg);

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.reportImgs.splice(index, 1);
        };

       
        //request --隐患上报
        $scope.addHidenTrouble = function () {
            Requester.addHidenTrouble($scope.select,$scope.schoolId).then(function (rest) {
                if (rest.result) {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    $rootScope.$broadcast('ADD_HidenTrouble_SUCCEED', 'succeed');
                    $ionicHistory.goBack();
                    toaster.success({
                        title: "",
                        body: "添加成功",
                        timeout: 3000
                    });
                   
                    $scope.reportImgs = [];
                } else {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                }, 5000);
            });
        };



    }]);