angular.module('app.controllers')
    .controller('exerciseRecordCtrl', ['$scope', 'Constant', '$state', '$ionicModal', 'CameraHelper', 'Requester', '$stateParams', '$q', 'toaster', '$ionicLoading', '$timeout', '$rootScope', 'BroadCast', function ($scope, Constant, $state, $ionicModal,CameraHelper, Requester, $stateParams, $q, toaster, $ionicLoading, $timeout,$rootScope,BroadCast) {
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.list = [];
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
           
            $scope.startTime = String($stateParams.startTime)?String($stateParams.startTime):$rootScope.WXDrillItemStartTime;
            $scope.endTime = $stateParams.endTime?$stateParams.endTime:$rootScope.WXDrillItemEndTime;
            $scope.intervalTime = $scope.getTimeDistant($scope.startTime, $scope.endTime);
            $scope.selected = {
                remarks: '',
                picdatas: []
            };

            $scope.drillId = $stateParams.itemId?$stateParams.itemId:$rootScope.WXDrillItemId ; //演练id
            if(isWeixin()){
                $rootScope.WXDrillItemStartTime =  $scope.startTime;
                $rootScope.WXDrillItemEndTime = $scope.endTime;
                $rootScope.WXDrillItemId =  $scope.drillId;
            }

            $scope.reportImgs = [];

            $scope.page = 1;

            $scope.getDrillRedordDetail();
            
        });

        //添加记录
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'others/safetyManage/addExerciseRecord.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.recordModal = modal;
        });
        $scope.addRecordModal = function () {
            $scope.recordModal.show();
        };

        $scope.hideModal = function () {
            $scope.recordModal.hide();
        };


        //选择图片
        $scope.selectImg = function () {
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.reportImgs, resp);
                        $scope.$apply();
                    } else {
                        $scope.reportImgs.push(
                            "data:image/jpeg;base64," + resp
                        );
                    }
                }, 9 - $scope.reportImgs.length);
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }
        };

        // input形式打开系统系统相册
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.reportImgs.push($scope.testImg);

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.reportImgs.splice(index, 1);
        };
        function onImageResized(resp) {
            $scope.selected.picdatas.push(resp);
        }
        //提交演练记录
        $scope.commitAddExerciseRecord = function () {
           
            $scope.selected.picdatas = [];
            var promiseArr = [];
            $scope.reportImgs.forEach(function (img) {
                promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized));
            });
            $scope.isLoading = true;
            $q.all(promiseArr).then(function () {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                $scope.addDrillRecord();

            });

        };
        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.isLoading = false;
            $scope.isMoreData = true;
            $ionicLoading.hide();
        });

        //加载更多
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getDrillRedordDetail();
        };

        //加载更多
        $scope.loadMore = function () {
            $scope.page++;
            $scope.getMoreData();
        };

        // 计算时长的
        $scope.getTimeDistant = function (start, end) {
            var result = '--';
            if (!end || !start) {
                return result;
            }
            //获取当前时间的时间戳
            var startTamp = Date.parse(new Date(start.replace(/\-/g, "/"))) / 1000;
            var endtamp = Date.parse(new Date(end.replace(/\-/g, "/"))) / 1000;
            var interval = (endtamp - startTamp) / 60;
            result = interval + '分钟';
            return result;
        };

        //request ---演练记录详情
        $scope.getDrillRedordDetail = function () {

            Requester.getDrillRedordDetail($scope.drillId, $scope.page).then(function (rest) {
                if (rest.result) {
                    $scope.list = rest.data.content;

                    $scope.list.forEach(function (item) {
                        var picUrls = [];
                        item.picUrls = [];
                        picUrls = item.strImageUrls ? item.strImageUrls.split(',') : [];
                        picUrls.forEach(function (url) {
                            item.picUrls.push({
                                thumb: url,
                                src: url
                            });
                        });
                    });
                } else {
                     $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });

        };

        //request --加载更多
        $scope.getMoreData = function () {
            Requester.getDrillRedordDetail($scope.drillId, $scope.page).then(function (rest) {
                if (rest.result) {
                    var data = rest.data.content;
                    for (var i = 0; i < data.length; i++) {
                        //var item = data[i];
                        $scope.list.push(data[i]);
                    }
                    $scope.list.forEach(function (item) {
                        var picUrls = [];
                        item.picUrls = [];
                        picUrls = item.strImageUrls ? item.strImageUrls.split(',') : [];
                        picUrls.forEach(function (url) {
                            item.picUrls.push({
                                thumb: url,
                                src: url
                            });
                        });
                    });
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //request --添加演练记录
        $scope.addDrillRecord = function () {
            Requester.addDrillRecord($scope.drillId, $scope.selected).then(function (rest) {
                if (rest.result) {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    $scope.recordModal.hide();
                    toaster.success({
                        title: "",
                        body: "添加成功",
                        timeout: 3000
                    });
                    $scope.selected = {
                        remarks: '',
                        picdatas: []
                    };
                    $scope.reportImgs = [];
                    $scope.page = 1;
                    $scope.getDrillRedordDetail();
                } else {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                }, 5000);
            });
        };

      
    }]);