angular.module('app.controllers')
    .controller('exerciseNoticeDetailCtrl', ['$scope', 'Constant', '$state', '$ionicModal', '$stateParams', 'Requester', 'toaster', '$rootScope', function ($scope, Constant, $state, $ionicModal, $stateParams, Requester, toaster, $rootScope) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.drillId = $stateParams.itemId ? $stateParams.itemId : $rootScope.WXDrillItemId;
            $scope.title = $stateParams.title ? $stateParams.title : $rootScope.WXDrillItemTitle;
            if (isWeixin()) {
                $rootScope.WXDrillItemId = $scope.drillId;
                $rootScope.WXDrillItemTitle = $scope.title;
            }
            $scope.getDrillDetail();
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            // $scope.drillId = ''; //演练id
            $scope.selected = {
                drillTime: '',
                detail: {}
            };
        });
        //查看方案详情
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'others/safetyManage/exerciseProgramDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.programDetailModal = modal;
        });
        $scope.goToProgramDetail = function () {
           // $scope.programDetailModal.show();
        };
        //关闭演练方案详情
        $scope.hideProgramModal = function () {
            $scope.programDetailModal.hide();
        };

        //request --演练详情
        $scope.getDrillDetail = function () {
            Requester.getDrillDetail($scope.drillId).then(function (rest) {
                if (rest.result) {
                    $scope.selected.detail = rest.data;
                    $scope.selected.programName = rest.data.drillPlanName;
                    $scope.programId = rest.data.tempDrillPlanId;
                    return rest.result;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).then(function(result){
                  if(result){
                    //$scope.getDrillProgramDetail($scope.programId); 
                  }
            });
        };

        //request --查看演练方案详情
        $scope.getDrillProgramDetail = function (programId) {
            Requester.getDrillProgramDetail(programId).then(function (rest) {
                if (rest.result) {
                    $scope.programDetailText = rest.data.content;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };
    }]);