/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('introduceCtrl', ['$scope', '$state', 'UserPreference', 'Constant', function ($scope, $state, UserPreference, Constant) {

        $scope.options = {
            loop: false,
            speed: 500
        };

        $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
            // data.slider is the instance of Swiper
            $scope.slider = data.slider;
        });

        $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
            // console.log('Slide change is beginning');
        });

        $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
            
            $scope.activeIndex =data.slider.activeIndex;
            $scope.previousIndex = data.slider.previousIndex;
           
           
        });

        $scope.enter = function () {
            UserPreference.set(Constant.version, true);
            $state.go(UserPreference.get('PreIntroView', 'login'));
        };
        
       $scope.swipeToLeft = function(){
           
        UserPreference.set(Constant.version, true);
        $state.go(UserPreference.get('PreIntroView', 'login'));
       };
       
    //    $scope.slideHasChanged = function(index){
    //    $scope.recordSliderIndex = index;
    //    };
       
       
    }]);