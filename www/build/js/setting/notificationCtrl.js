/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('notificationCtrl', ['$scope', '$ionicHistory', 'PushService', 'UserPreference', 'toaster', 'MESSAGES', '$state', '$cordovaInAppBrowser', '$rootScope', '$ionicLoading', function ($scope, $ionicHistory, PushService, UserPreference, toaster, MESSAGES, $state, $cordovaInAppBrowser, $rootScope,$ionicLoading) {
        $scope.goBack = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('tabsController.settingPage');
        };

        $scope.revStatus = UserPreference.getObject('PushSetting');

        PushService.getUserNotificationSettings(function (result) {
            if (result === 0) {
                // 系统设置中已关闭应用推送。
                $scope.revStatus.checked = false;
                toaster.warning({title: MESSAGES.PUSH_ON_FAIL_REASON, body: ''});
            } else if (result > 0) {
                // 系统设置中打开了应用推送。
            }
        });

        if (isEmptyObject($scope.revStatus))
            $scope.revStatus = {checked: true, soundChecked: true, vibrateChecked: true};

        $scope.pushNotificationChange = function () {
            //console.log($scope.revStatus.checked);
            if ($scope.revStatus.checked) {
                PushService.resumePush();
                PushService.getUserNotificationSettings(function (result) {
                    if (result === 0) {
                        // 系统设置中已关闭应用推送。
                        $scope.revStatus.checked = false;
                        setTimeout(function () {
                            toaster.warning({title: MESSAGES.PUSH_ON_FAIL, body: MESSAGES.PUSH_ON_FAIL_REASON});
                        }, 0);
                    } else if (result > 0) {
                        // 系统设置中打开了应用推送。
                    }
                });
            }
            else
                PushService.stopPush();
            UserPreference.setObject('PushSetting', $scope.revStatus);
        };

        $scope.pushNotificationBehaveChange = function () {
            if ($scope.revStatus.soundChecked && $scope.revStatus.vibrateChecked) {
                //allow both
                PushService.setBasicNotification("all");
            }
            else if (!$scope.revStatus.soundChecked && $scope.revStatus.vibrateChecked) {
                //allow only vibrate
                PushService.setBasicNotification("vibrate");
            }
            else if ($scope.revStatus.soundChecked && !$scope.revStatus.vibrateChecked) {
                //allow only sound
                PushService.setBasicNotification("sound");
            }
            else {
                PushService.setBasicNotification(null);
            }
            UserPreference.setObject('PushSetting', $scope.revStatus);
        };

        $scope.goInstruction = function () {
            var options = {
                location: 'no',
                clearcache: 'yes',
                toolbar: 'no',
                hidden: 'no'
            };
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $cordovaInAppBrowser.open('http://xgenban.com/wmdp/mobile/msgtips1.html', '_blank', options)
                .then(function (event) {
                    // success
                    console.log('open succeed');
                    $ionicLoading.hide();
                    $cordovaInAppBrowser.show();
                })
                .catch(function (event) {
                    // error
                    console.log(event);
                    $ionicLoading.hide();
                    $cordovaInAppBrowser.close();
                });
                setTimeout(function(){
                    $ionicLoading.hide();
                },3000);
                
                
        };

        $rootScope.$on('$cordovaInAppBrowser:loadstart', function (e, event) {
            var url = event.url;
            console.log('---strat');
            var iOut = url.indexOf("CloseNewsTab");
            if (iOut >= 0) {
                $ionicLoading.hide();
                $cordovaInAppBrowser.close();
               
            }
        });

        $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event){
            // console.log('---stop');
            $ionicLoading.hide();
          });
        

        $scope.isAndroid = function () {
            return ionic.Platform.isAndroid();
        };
    }]);