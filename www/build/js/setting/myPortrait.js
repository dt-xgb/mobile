angular.module('app.controllers')
    .controller('myPortraitCtrl', ['$scope', 'ionicImageView', '$ionicModal', 'Constant', 'CameraHelper', 'BroadCast', 'Requester', 'toaster', 'UserPreference', '$ionicLoading', '$timeout', function ($scope, ionicImageView, $ionicModal, Constant, CameraHelper, BroadCast, Requester, toaster, UserPreference, $ionicLoading, $timeout) {

        $scope.sampleImgs = [{
            url: 'img/others/smile_over.jpg',
            lab: '过度微笑'
        }, {
            url: 'img/others/chuckled.jpg',
            lab: '抿嘴'
        }, {
            url: 'img/others/frown.jpg',
            lab: '皱眉'
        }, {
            url: 'img/others/background_notclean.jpg',
            lab: '背景不干净'
        }, {
            url: 'img/others/reflective.jpg',
            lab: '眼睛反光'
        }, {
            url: 'img/others/keep_out.jpg',
            lab: '镜框遮挡'
        }, {
            url: 'img/others/wear_hat.jpg',
            lab: '戴帽子'
        }];

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.selected = {
                isChecked: false
            };
            $scope.myInfo = UserPreference.getObject('myPortraitInfo');
            $scope.userId = UserPreference.getObject('user').id;
            $scope.remarkArr = UserPreference.getArray('RemarkReadStatus') ? UserPreference.getArray('RemarkReadStatus') : [];
            $scope.getTeacherFaceRecognition();
        });

        //点击图片放大
        $scope.viewImages = function (item, index, $event) {
            var url = 'img/icon/person.png';
            if (item.userImgSrc) url = item.userImgSrc;
            var urls = [];
            index = 0;
            urls.push(url);
            $event.stopPropagation();
            ionicImageView.showViewModal({
                allowSave: false
            }, urls, index);
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'setting/attentionModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.attentionModal = modal;
        });

        //选择图片 
        $scope.selectImg = function () {
            var isRead = $scope.getReadStatus();
            if (isRead === 'read') {
                if (window.cordova) {
                    CameraHelper.selectImage('portrait', {
                        allowEdit: false,
                        width:800,
                        height:800
                    });
                } else {
                    var input = document.getElementById("capture1");
                    input.click();
                }
            } else {
                $scope.selected.isChecked = false;
                $scope.attentionModal.show();
            }
        };

        $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
            if (rst && rst.which === 'portrait') {
                //上传图片
                // $scope.sendFeedbackRequest('1', rst.source);
                $scope.uploadUserPortrait($scope.userId, rst.source);
            }
        });

        //web端选择文件
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result;
                    $ionicLoading.show({
                        noBackdrop: true,
                        template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                    });

                    $scope.uploadUserPortrait($scope.userId, $scope.testImg);
                }, 100);
            };
        };

        //我已阅读
        $scope.haveKnow = function () {
            $scope.setReadStatus();
            $scope.attentionModal.hide();
            if (window.cordova) {
                CameraHelper.selectImage('portrait', {
                    allowEdit: false,
                    width:800,
                    height:800
                });
            } else {
                var input = document.getElementById("capture1");
                input.click();
            }
        };

        $scope.hideAttentionModal = function () {
            $scope.attentionModal.hide();
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.attentionModal.remove();
        });

        $scope.refreshData = function(){
            $scope.getTeacherFaceRecognition();
        };

        // request --获取我的人像
        $scope.getTeacherFaceRecognition = function () {
            Requester.getTeacherFaceRecognition($scope.userId).then(function (resp) {
                if (resp.result) {
                    $scope.myInfo = resp.data;
                    UserPreference.setObject('myPortraitInfo', $scope.myInfo);
                    
                } else {
                    toaster.error({
                        title: resp.message,
                        body: ''
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
              });
        };

        //requester ---上传
        $scope.uploadUserPortrait = function (userId, base64Str) {
            Requester.uploadUserPortrait(userId, base64Str).then(function (resp) {
                $ionicLoading.hide();
                if (resp.result) {
                    toaster.success({
                        body: '上传成功'
                    });
                    $scope.getTeacherFaceRecognition();
                } else {
                    toaster.error({
                        title: resp.message,
                        body: ''
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                }, 3000);

            });
        };

        $scope.getTypeStr = function (type) {
            var result = type === 0 ? '未采集' : type === 1 ? '人像提取中' : type === 2 ? '采集完成' : '采集失败';
            return result;

        };

        //存状态数组
        $scope.setReadStatus = function () {
            var readMark = $scope.selected.isChecked ? 'read' : 'unread';
            var count = 0;
            if ($scope.remarkArr && $scope.remarkArr.length > 0) {
                for (var i = 0; i < $scope.remarkArr.length; i++) {
                    //如果用户已存在则是更改状态
                    if ($scope.userId === $scope.remarkArr[i].key) {
                        $scope.remarkArr[i].value = readMark;
                        break;
                    } else {
                        count++;
                    }
                }
                if ($scope.remarkArr.length === count) {
                    $scope.remarkArr.push({
                        key: $scope.userId,
                        value: readMark
                    });
                }

            } else {
                $scope.remarkArr.push({
                    key: $scope.userId,
                    value: readMark
                });

            }

            UserPreference.setObject('RemarkReadStatus', $scope.remarkArr);
        };

        //取状态数组
        $scope.getReadStatus = function () {
            var result;
            var statusArr = UserPreference.getArray('RemarkReadStatus');
            if (statusArr && statusArr.length > 0) {
                for (var i = 0; i < statusArr.length; i++) {
                    if (statusArr[i].key === $scope.userId && statusArr[i].value === 'read') {
                        result = 'read';
                        break;
                    } else {
                        result = 'unread';
                    }
                }

            } else {
                result = 'unread';
            }
            return result;
        };

    }]);