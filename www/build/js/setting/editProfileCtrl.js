/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('editProfileCtrl', ['$scope', 'Constant', '$rootScope', '$ionicHistory', '$stateParams', 'UserPreference', 'SettingService', 'BroadCast', 'toaster', 'MESSAGES', '$ionicLoading', '$state', 'Requester', 'CameraHelper', '$timeout', function ($scope, Constant, $rootScope, $ionicHistory, $stateParams, UserPreference, SettingService, BroadCast, toaster, MESSAGES, $ionicLoading, $state, Requester, CameraHelper, $timeout) {
        $scope.isWeiXin = isWeixin();
        $scope.visibleArea = $stateParams.content?$stateParams.content: $rootScope.WXRootEditProFileContent;
        if(isWeixin())
        $rootScope.WXRootEditProFileContent = $scope.visibleArea;
        $scope.user = UserPreference.getObject("user");
        $scope.defaultChildID = UserPreference.get('DefaultChildID');
        $scope.toChangeChild1 = {};
        $scope.rolename = String($scope.user.rolename);

        console.log($scope.defaultChildID);
        if ($scope.rolename == Constant.USER_ROLES.PARENT) {
            for (var i = 0; i < $scope.user.student.length; i++) {
                if ($scope.user.student[i].id == $scope.defaultChildID) {
                    $scope.toChangeChild = $scope.user.student[i];
                    break;
                }
            }
        }


        console.log($scope.toChangeChild);

        switch ($scope.visibleArea) {
            case 'name':
                $scope.pageTitle = "修改姓名";
                break;
            case 'password':
                $scope.pageTitle = "修改密码";
                break;
            case 'children':
                $scope.pageTitle = "子女信息";
                break;
            case 'instruction':
                $scope.pageTitle = "修改个人简介";
                break;

        }

        $scope.pwd = {
            _old: '',
            _new: '',
            _confirm: ''
        };
        $scope.save = function () {
            switch ($scope.visibleArea) {
                case 'name':
                    SettingService.editInfo('name', $scope.user.name);
                    break;
                case 'password':
                    var reg = /^[a-zA-Z0-9]+$/;
                    if (!$scope.pwd._old.match(reg) || !$scope.pwd._new.match(reg)) {
                        toaster.warning({
                            title: MESSAGES.REMIND,
                            body: MESSAGES.LOGIN_PASSWORD_PATTERN
                        });
                        return;
                    }
                    if ($scope.pwd._new != $scope.pwd._confirm) {
                        toaster.warning({
                            title: MESSAGES.REMIND,
                            body: MESSAGES.PASSWORD_CONFIRM_ERROR
                        });
                        return;
                    }
                    SettingService.changePwd($scope.pwd._old, $scope.pwd._new);
                    break;
                case 'instruction':
                    SettingService.editInfo('instruction', $scope.user.instruction);
                    break;
            }
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
        };

        $scope.setDefaultChild = function (child) {
            $scope.defaultChildID = child.id;
            UserPreference.set('DefaultChildID', child.id);
            UserPreference.set('DefaultClassID', child.classno.id);
            UserPreference.set('DefaultClassName', child.classno.className);
            UserPreference.set('DefaultChildName', child.student_name);
            UserPreference.set('DefaultSchoolName', child.school.schoolName);
            UserPreference.set('DefaultSchoolID', child.school.id);
            UserPreference.set('DefaultChildLogo', child.logo);
            UserPreference.setObject('DefaultChild', child);
        };

        $scope.goBack = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            if ($scope.visibleArea === 'children') {
                $state.go('tabsController.settingPage');
            } else {
                $state.go('useProfile');
            }
        };

        $scope.selectImage = function (child) {
            $scope.toChangeChild = child;
            if (!isWeixin()) {
                CameraHelper.selectImage('av', {
                    allowEdit: true
                });
            } else {
                var input = document.getElementById("childCapture");
                input.click();
            }

        };

        //web端选择文件
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result;
                    var data = {
                        which: 'av',
                        source: $scope.testImg
                    };
                    $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, data);

                }, 100);
            };
        };

        $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {

            if (rst && rst.which === 'av') {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                Requester.modifyStuInfo($scope.toChangeChild.id, 'logo', rst.source.substr(rst.source.indexOf('base64,') + 7)).then(function (resp) {
                    if (resp.result) {
                        toaster.success({
                            title: '修改成功'
                        });

                        $scope.toChangeChild.logo = rst.source;
                        for (var i = 0; i < $scope.user.student.length; i++) {
                            if ($scope.user.student[i].id === $scope.toChangeChild.id) {
                                $scope.user.student[i] = $scope.toChangeChild;
                                UserPreference.setObject('user', $scope.user);
                                UserPreference.setObject('DefaultChild', $scope.user.student[i]);
                                break;
                            }

                        }
                    }
                }).finally(function () {
                    $ionicLoading.hide();
                });
            }
        });

        $scope.$on(BroadCast.EDIT_INFO, function (a, rst) {
            if (rst && rst.result) {
                toaster.success({
                    title: MESSAGES.SAVE_SUCCESS,
                    body: MESSAGES.INFO_UPDATED
                });
                $scope.goBack();
            } else
                toaster.error({
                    title: MESSAGES.REMIND,
                    body: rst.message
                });
            $ionicLoading.hide();
        });

        $scope.$on(BroadCast.PASSWORD_CHANGE, function (a, rst) {
            if (rst && rst.result) {
                toaster.success({
                    title: MESSAGES.REMIND,
                    body: MESSAGES.PWD_UPDATED
                });
                $scope.goBack();
            } else
                toaster.error({
                    title: MESSAGES.REMIND,
                    body: rst.message
                });
            $ionicLoading.hide();
        });

        $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
            $ionicLoading.hide();
        });
    }]);