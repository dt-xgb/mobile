angular.module('app.controllers')
    .controller('addApprovalCtrl', ['$http', '$scope', '$ionicModal', 'Constant', 'ionicImageView', '$stateParams', 'CameraHelper', '$rootScope', 'Requester', '$timeout', '$q', 'ionicDatePicker', 'ionicTimePicker', 'toaster', 'UserPreference', '$ionicHistory', '$ionicLoading', function ($http, $scope, $ionicModal, Constant, ionicImageView, $stateParams, CameraHelper, $rootScope, Requester, $timeout, $q, ionicDatePicker, ionicTimePicker, toaster, UserPreference, $ionicHistory,$ionicLoading) {
        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.templateId = $stateParams.templateId ? $stateParams.templateId : $rootScope.rootWxTemplateId;
            $scope.isLoading = false;
            $scope.radioSelect = {
                option: 1
            };

            $scope.templateId = parseInt($scope.templateId);
            $rootScope.rootWxTemplateId = $scope.templateId;
            $scope.files = [];
            $scope.unloadImgs = []; //未加载时图片
            $scope.templateDetail = {};
            $scope.controls = []; //组件数组

            $scope.radiosOptions = []; //单选框内容
            $scope.boxOptions = []; //复选框数组

            $scope.schoolId = parseInt(UserPreference.get('DefaultSchoolID'));
            $scope.userId = UserPreference.getObject('user').id;
            $scope.select = {
                approvalManId: null,
                attaches: [],
                chains: [],
                checkboxes: [],
                comment: '',
                currUserId: null,
                dateRanges: [],
                dates: [],
                fulltexts: [],
                inputs: [],
                notifierIds: [],
                radios: [],
                title: '',
                receiveMen: [],
                timeList :[]

            };
            $scope.sel = {};
            $scope.approvalImgs = [];
            $scope.radioSelectMen = [];
            $scope.boxSelectMen = [];
            if ($scope.templateId != 0) {
                $scope.getTeacherApprovalTemplateDetail();
            } else {
                $scope.getSchoolDepartmentList();
            }



        });

        function onImageResized(resp) {

        }

        // 新建审批-选择图片 
        $scope.selectImg = function () {
           //$scope.uploadNum = 0;
            $scope.files = [];
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        $scope.receiveImgs = resp;
                        //$scope.mulpFiles = [];
                        if(iOSDevice()){
                            resp.forEach(function(file,index){
                                console.log('index:'+index);
                                setTimeout(function(){
                                    $scope.uploadSinglePicture(file,index);
                                },300*(index+1));  
                            });
                    
                        }else{
                            $scope.delQueue(resp).then(function(result){
                     
                            });
                        }
                       
                      
                    } else {

                        var base64 = "data:image/jpeg;base64," + resp;
                        var file = dataURLtoFile(base64, 'image1.jpeg');
                        $scope.uploadSinglePicture(file);

                    }
                }, 9 - $scope.select.attaches.length,'mult');
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }
        };
        $scope.preViewImages = function (index) {

            var origin = [];
            $scope.select.attaches.forEach(function (attache) {
                if (attache.attachUrl)
                    origin.push(attache.attachUrl);
                else
                    origin.push(attache);
            });
            ionicImageView.showViewModal({
                allowSave: false
            }, origin, index);
        };
        // input形式打开系统系统相册
        $scope.getFile = function (files) {
            var file = files[0];

            $timeout(function () {
                $scope.unloadImgs.push({
                    attachName: '图片',
                    attachUrl: 'img/approval/not_load.jpg' //res.data.url ? res.data.url : res.data.link
                });
                $scope.uploadSinglePicture(file);
            }, 50);
        };

        $scope.uploadMorePicture = function () {
            // console.log('$scope.files2');
            // console.log($scope.files);
            $scope.uploadMultipartPicture($scope.files);
        };

        //单图 上传
        $scope.uploadSinglePicture = function (file,index) {
            Requester.uploadPictureByFileType(file,index).then(function (res) {
                //$scope.unloadImgs = [];
                if (res.result) {
                   // $scope.receiveImgs.push(img);
                    $scope.select.attaches.push({
                        attachName: '图片',
                        attachUrl: res.data.url ? res.data.url : res.data.link
                    });

                } else {
                    toaster.warning({
                        title: "上传图片失败",
                        body: res.message
                    });
                }
            });
        };

        //多图 上传
        $scope.uploadMultipartPicture = function (files) {
            Requester.uploadMorePictureFileType(files).then(function (res) {
                if (res.result) {
                    console.log('multipart upload succeed');
                    console.log(res);
                    for (var i = 0; i < res.data.length; i++) {
                        $scope.select.attaches.push({
                            attachName: '图片'+i*3,
                            attachUrl: res.data[i].url ? res.data[i].url : res.data[i].link
                        });
                    }
                }
            });
        };

        // 移除图片
        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.select.attaches.splice(index, 1);
            $scope.unloadImgs.splice(index, 1);
        };


        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        //选择审批人s -单选 && 抄送人c （多选）
        $ionicModal.fromTemplateUrl(UIPath + 'approvalAdmin/selectPersonModal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {

            $scope.selectPersonModal = modal;
        });

        $scope.selectApprovalMan = function (type) {

            if (type === 's') {
                $scope.approvertitle = '选择审批人';
            } else if (type === 'c') {
                $scope.approvertitle = '选择抄送人';
            }
            $scope.approvalManType = type;
            $scope.selectPersonModal.show();
        };
        //关闭选择审批/抄送人modal
        $scope.hideSelectPersonModal = function () {
            $scope.selectPersonModal.hide();
        };

        //选择审批联系人
        $scope.chooseSMan = function (man, index) {
             //console.log($scope.select.receiveMen);
            $scope.radioSelectMen.push(man);
            $scope.selectPersonModal.hide();
        };
        //选择抄送人
        $scope.chooseCMan = function (man, index) {
            // man.isBoxChecked = !man.isBoxChecked;
        };
        //确定选择审批或抄送的人
        $scope.saveApprovalMan = function () {

            var count = 0;
            for (var i = 0; i < $scope.select.receiveMen.length; i++) {
                if ($scope.select.receiveMen[i].isBoxChecked) {
                    if ($scope.boxSelectMen.length == 0) {
                        $scope.boxSelectMen.push($scope.select.receiveMen[i]);
                    } else {
                        for (var j = 0; j < $scope.boxSelectMen.length; j++) {
                            if ($scope.boxSelectMen[j].teacherId == $scope.select.receiveMen[i].teacherId) {
                                count++;
                                continue;
                            }
                        }
                        if (count == 0) {
                            $scope.boxSelectMen.push($scope.select.receiveMen[i]);
                        }
                    }

                }
            }

            $scope.selectPersonModal.hide();
        };

        //移除审批人
        $scope.removeApprovalMan = function (index, man) {

            $scope.radioSelectMen.splice(index, 1);

            for(var i=0;i<$scope.select.receiveMen.length;i++){
                $scope.select.receiveMen[i].checked= false;
            }
            // console.log($scope.select.receiveMen);

        };

        //移除抄送人
        $scope.removeReceiveMan = function (index) {
            //console.log(index);
            $scope.boxSelectMen.splice(index, 1);
        };

        //选择时间
        $scope.chooseDate = function (item, arg) {

            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {
                        if (arg == 0) {
                            item.date = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                        } else if (arg == 1) {
                            item.startDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                            $scope.showTimePicker(item, arg);
                        } else if (arg == 2) {
                            item.endDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                            $scope.showTimePicker(item, arg);
                        } else if(arg==3){

                        }
                       
                    }
                }
            };
            if(arg!=3){
                ionicDatePicker.openDatePicker(ipObj1);
            }else{
                $scope.showTimePicker(item, arg);
            }
            
        };
        $scope.showTimePicker = function (item, arg) {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {} else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        $scope.timeBtnTxt = time;
                        if (arg == 0) {
                            item.date = item.date + ' ' + time + ':00';
                        } else if (arg == 1) {
                            item.startDate = item.startDate + ' ' + time + ':00';
                        } else if (arg == 2) {
                            item.endDate = item.endDate + ' ' + time + ':00';
                            // console.log('end date :'+ item.endDate);
                        }else if(arg ===3){
                            //时间控件
                            item.time = time+':00';
                        }


                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

        //单选框点击事件
        $scope.chooseRadioChoose = function (option, item, index) {

            //$scope.radioSelect.option = item.id + option + index;
            item.selectOption = item.id + option;
            item.checkOption = option;
        };

        //提交新建的审批
        $scope.commitApply = function () {
            $scope.select.chains = [];
            $scope.select.notifierIds = [];
            $scope.select.inputs = [];
            $scope.select.fulltexts = [];
            $scope.select.dates = [];
            $scope.select.dateRanges = [];
            $scope.select.radios = [];
            $scope.select.checkboxes = [];
            $scope.select.timeList = [];
            // console.log('---- controlls');
            // console.log($scope.controls);
            for (var j = 0; j < $scope.controls.length; j++) {
                var ctrl = $scope.controls[j];
                if (ctrl.type == 3 && !ctrl.date && ctrl.required) {
                    toaster.warning({
                        title: "",
                        body: '请选择' + ctrl.name + '日期'
                    });
                    return;
                } else if (ctrl.type == 4 && ctrl.required && (!ctrl.startDate || !ctrl.endDate)) {
                    var str = !ctrl.startDate ? '开始时间' : !ctrl.endDate ? '结束时间' : '时段';
                    toaster.warning({
                        title: "",
                        body: '请选择' + ctrl.name + str
                    });
                    return;
                } else if (ctrl.type == 5 && ctrl.required && !ctrl.checkOption) {
                    toaster.warning({
                        title: "",
                        body: ctrl.name + '未做出选择'
                    });
                    return;
                } else if (ctrl.type == 6 && ctrl.required) {
                    var count = 0;
                    for (var k = 0; k < ctrl.options.length; k++) {
                        if (ctrl.options[k].checked == false) {
                            count++;
                            continue;
                        }

                    }
                    if (count == ctrl.options.length) {
                        toaster.warning({
                            title: "",
                            body: ctrl.name + '未做出选择'
                        });
                        return;
                    }
                }else if(ctrl.type == 7&&!ctrl.time&&ctrl.required){
                    toaster.warning({
                        title: "",
                        body: '请选择' + ctrl.name + '时间'
                    });
                    return;
                }
            }
            if ($scope.radioSelectMen.length == 0) {
                toaster.warning({
                    title: "",
                    body: '请添加审批人'
                });
                return;
            }


            //审批人
            for (var i = 0; i < $scope.radioSelectMen.length; i++) {
                $scope.select.chains.push({
                    level: i + 1,
                    nextUserId: i == $scope.radioSelectMen.length - 1 ? null : $scope.radioSelectMen[i + 1].teacherId,
                    prevUserId: i == 0 ? null : $scope.radioSelectMen[i - 1].teacherId,
                    userId: $scope.radioSelectMen[i].teacherId
                });
            }
            //当前审批人
            $scope.select.currUserId = $scope.radioSelectMen[0].teacherId;
            //抄送人
            $scope.boxSelectMen.forEach(function (man) {
                $scope.select.notifierIds.push(man.teacherId);
            });

            for (var h = 0; h < $scope.controls.length; h++) {
                var item = $scope.controls[h];

                if (item.type == 1) {
                    //单行文本
                    $scope.select.inputs.push({
                        id: item.id,
                        idx: item.idx,
                        name: item.name,
                        text: item.text,
                        required: item.required
                    });
                } else if (item.type == 2) {
                    //多行文本
                    $scope.select.fulltexts.push({
                        id: item.id,
                        idx: item.idx,
                        name: item.name,
                        fulltext: item.fulltext,
                        required: item.required
                    });

                } else if (item.type == 3) {
                    //日期
                    $scope.select.dates.push({
                        id: item.id,
                        idx: item.idx,
                        name: item.name,
                        date: item.date,
                        required: item.required
                    });

                } else if (item.type == 4) {
                    //日期区间
                    $scope.select.dateRanges.push({
                        id: item.id,
                        name: item.name,
                        idx: item.idx,
                        endDate: item.startDate,
                        startDate: item.endDate,
                        required: item.required
                    });

                } else if (item.type == 5) {
                    //单选框
                    $scope.select.radios.push({
                        checkOption: item.checkOption,
                        name: item.name,
                        id: item.id,
                        idx: item.idx
                    });
                } else if (item.type == 6) {
                    //多选框
                    var checkOptions = [];
                    for (var l = 0; l < item.options.length; l++) {
                        if (item.options[l].checked) {
                            checkOptions.push(item.options[l].value);
                        }
                    }
                    $scope.select.checkboxes.push({
                        name: item.name,
                        id: item.id,
                        idx: item.idx,
                        checkOptions: checkOptions
                    });
                } else if(item.type == 7){
                    $scope.select.timeList.push({
                        id: item.id,
                        idx: item.idx,
                        name: item.name,
                        time: item.time,
                        required: item.required
                    });
                }
            }
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
           $scope.isLoading = true;

            Requester.addNewApproval($scope.schoolId, $scope.userId, $scope.templateId, $scope.select).then(function (data) {

                if (data.result) {
                    toaster.success({
                        title: "提交成功",
                        body: "您的申请已提交，请等待审核",
                        timeout: 3000
                    });
                    $rootScope.$broadcast('ADD_APPROVAL_SUCCEED', 'succeed');
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    $ionicHistory.goBack();
                   
                } else {
                    $scope.isLoading = false;
                    $ionicLoading.hide();
                    toaster.warning({
                        title: "",
                        body: data.message
                    });
                }

            }).finally(function(){
                setTimeout(function(){
                    //$scope.isLoading = false;
                    $scope.isLoading = false;
                    $ionicLoading.hide();
                },5000);
            });

        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.selectPersonModal.remove();
        });


        //选择的部门改变时
        $scope.onSelectionChange = function (partment) {

            //$scope.getDepartmentPersons($scope.sel.part);
        };

        //request - 获取学校部门列表
        $scope.getSchoolDepartmentList = function () {
            Requester.getSchoolDepartmentList($scope.schoolId).then(function (data) {
                if (data.result) {
                    $scope.parts = data.data;
                    $scope.sel.part = $scope.parts[0].id;
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
                return data;

            }).then(function (data) {
                if (data)
                    $scope.getDepartmentPersons();
            });
        };

        //request--查询部门人员
        $scope.getDepartmentPersons = function (departmentId) {
            Requester.getDepartmentPersons($scope.schoolId, departmentId).then(function (data) {
                if (data.result) {
                    // console.log(data);
                    $scope.select.receiveMen = data.data;
                }
            });
        };

        //request --查询老师指定模板
        $scope.getTeacherApprovalTemplateDetail = function () {
            Requester.getTeacherApprovalTemplateDetail($scope.templateId).then(function (resp) {
                if (resp.result) {
                    $scope.templateDetail = resp.data;
                    //控件列表
                    if (resp.data.controls && resp.data.controls.length > 0) {
                        $scope.controls = resp.data.controls;
                        //按照序列重新排列 冒泡排序
                        for (var a = 0; a < $scope.controls.length; a++) {
                            for (var b = a + 1; b < $scope.controls.length; b++) {
                                if ($scope.controls[a].idx > $scope.controls[b].idx) {
                                    var s = $scope.controls[a];
                                    $scope.controls[a] = $scope.controls[b];
                                    $scope.controls[b] = s;
                                } else {
                                    continue;
                                }
                            }
                        }
                     
                        //if(ctrl.)
                        for (var k = 0; k < resp.data.controls.length; k++) {
                            var ctrl = resp.data.controls[k];

                            if (ctrl.type == 1) {
                                //单行文本
                                ctrl.text = '';
                            } else if (ctrl.type == 2) {
                                //多行文本
                                ctrl.fulltexts = '';
                            } else if (ctrl.type == 3) {
                                //日期
                                ctrl.date = '';
                            } else if (ctrl.type == 4) {
                                //日期区间
                                ctrl.endDate = '';
                                ctrl.startDate = '';
                            } else if (ctrl.type == 5) {
                                //单选框
                                var optionsArr1 = ctrl.options.split(",");
                                ctrl.options = optionsArr1;
                                ctrl.checkOption = '';
                                ctrl.selectOption = ''; //
                            } else if (ctrl.type == 6) {
                                //多选框
                                var optionsArr3 = ctrl.options.split(",");
                                var optionsArr4 = [];
                                for (var l = 0; l < optionsArr3.length; l++) {
                                    optionsArr4.push({
                                        checked: false,
                                        value: optionsArr3[l] // option
                                    });
                                }
                                ctrl.options = optionsArr4;
                                ctrl.checkOptions = [];
                            } 
                            else if(ctrl.type == 7){
                               //时间控件
                               ctrl.time = '';

                            }
                        }
                    }

                    if (resp.data.chains && resp.data.chains.length > 0) {
                        resp.data.chains.forEach(function (smen) {
                            $scope.radioSelectMen.push({
                                teacherId: smen.userId,
                                teacherName: smen.userName
                            });
                        });
                    }
                    if (resp.data.notifiers && resp.data.notifiers.length > 0) {
                        resp.data.notifiers.forEach(function (cmen) {
                            $scope.boxSelectMen.push({
                                teacherId: cmen.notifierId,
                                teacherName: cmen.notifierName
                            });
                        });
                    }
                } else {
                    toaster.warning({
                        title: "",
                        body: resp.message
                    });
                }

                return resp.result;
            }).then(function (result) {
                // console.log('---result:' + result);
                if (result) {
                    //  $scope.getSchoolDepartmentList();
                }

            });
        };


        $scope.resizeImg = function (longSideMax, url) {

            var $q = angular.injector(['ng']).get('$q');
            var defer = $q.defer();
            var when = $q.when();
            var tempImg = new Image();
            tempImg.src = url;

            tempImg.onload = function () {
                // Get image size and aspect ratio.
                var targetWidth = tempImg.width;
                var targetHeight = tempImg.height;
                var aspect = tempImg.width / tempImg.height;

                // Calculate shorter side length, keeping aspect ratio on image.
                // If source image size is less than given longSideMax, then it need to be
                // considered instead.
                if (tempImg.width > tempImg.height) {
                    longSideMax = Math.min(tempImg.width, longSideMax);
                    targetWidth = longSideMax;
                    targetHeight = longSideMax / aspect;
                } else {
                    longSideMax = Math.min(tempImg.height, longSideMax);
                    targetHeight = longSideMax;
                    targetWidth = longSideMax * aspect;
                }

                // Create canvas of required size.
                var canvas = document.createElement('canvas');
                canvas.width = targetWidth;
                canvas.height = targetHeight;

                var ctx = canvas.getContext("2d");
                // Take image from top left corner to bottom right corner and draw the image
                // on canvas to completely fill into.
                ctx.drawImage(this, 0, 0, tempImg.width, tempImg.height, 0, 0, targetWidth, targetHeight);
                // $scope.files.push(file2);
                // console.log($scope.receiveImgs);
                // console.log('receive imgs');
                var res = canvas.toDataURL("image/jpeg");
                var file2 = dataURLtoFile(res, 'image1.jpeg');
            };

        };

        //单图上传
        $scope.uploadPictureByFileType = function (file,index) {
            var fd = new FormData();
            if(!index) index = 0;
            fd.append('file', file);
            fd.append('filename', 'image'+index*0+'.jpeg');
            var url = Constant.ServerUrl.substr(0, Constant.ServerUrl.length - 4) + '/admin/fw/common/upload';
                return $http({
                    method: 'POST',
                    url: url,
                    data: fd,
                    headers: {
                        'Content-Type': undefined, //' multipart/form-data',
                        // 'Content-Disposition' :"inline;filename=hello.jpg "
                    },
                    transformRequest: angular.identity
                }).then(function (response) {
                    //上传成功的操作
                    var res = response.data;
                    $scope.select.attaches.push({
                        attachName: '图片',
                        attachUrl: res.data.url ? res.data.url : res.data.link
                    });
                }, function (error) {
                    console.log('failure');
                    $q.reject(error);
                });
            
        };

        $scope.delQueue = function(ids) {
            var promise = $q.when();
            var longSideMax = Constant.CAPTURE_IMAGE_RANGE;
            ids.forEach(function(id,index1) {
                var tempImg = new Image();
                tempImg.src = id;
                tempImg.onload = function () {
                    // Get image size and aspect ratio.
                    var targetWidth = tempImg.width;
                    var targetHeight = tempImg.height;
                    var aspect = tempImg.width / tempImg.height;
                    if (tempImg.width > tempImg.height) {
                        longSideMax = Math.min(tempImg.width, longSideMax);
                        targetWidth = longSideMax;
                        targetHeight = longSideMax / aspect;
                    } else {
                        longSideMax = Math.min(tempImg.height, longSideMax);
                        targetHeight = longSideMax;
                        targetWidth = longSideMax * aspect;
                    }
    
                    // Create canvas of required size.
                    var canvas = document.createElement('canvas');
                    canvas.width = targetWidth;
                    canvas.height = targetHeight;
    
                    var ctx = canvas.getContext("2d");
                    // Take image from top left corner to bottom right corner and draw the image
                    // on canvas to completely fill into.
                    ctx.drawImage(this, 0, 0, tempImg.width, tempImg.height, 0, 0, targetWidth, targetHeight);
   
                    var res = canvas.toDataURL("image/jpeg");
                    var file2 = dataURLtoFile(res, 'image1.jpeg');
                    promise = promise.then(function() {
                        setTimeout(function(){

                        },300*index1);
                        return $scope.uploadPictureByFileType(file2,index1);
                    });
                };
               
            });
            return promise;
        };
        

    }]);
