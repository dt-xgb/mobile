angular.module('app.controllers')
   .controller('approvalDetailCtrl', ['$scope', 'Constant', 'UserPreference', '$stateParams', '$rootScope', 'BroadCast', '$ionicLoading', 'Requester', '$ionicPopup', 'DownloadFile', 'toaster', '$cordovaFileOpener2', function ($scope, Constant, UserPreference, $stateParams, $rootScope, BroadCast, $ionicLoading, Requester, $ionicPopup, DownloadFile, toaster, $cordovaFileOpener2) {
      $scope.$on("$ionicView.enter", function (event, data) {
         $scope.archiveId = $stateParams.archiveId ? $stateParams.archiveId : $rootScope.WXArchiveId;
         $scope.selectIndex = $stateParams.selectIndex ? $stateParams.selectIndex : $rootScope.WXSelectIndex;
         $scope.userId = UserPreference.getObject('user').id;
         $scope.imgCount = 0;
         if (isWeixin()) {
            $rootScope.WXArchiveId = $scope.archiveId;
            $rootScope.WXSelectIndex = $scope.selectIndex;
         }

         //提交审批
         $scope.select = {
            result: null,
            suggest: null
         };

         $scope.getApprovalArchiveDeatil();
      });

      //提交审批
      var UIPath = '';
      if (Constant.debugMode) UIPath = 'module/';
      $scope.showApprovalAlert = function () {
         console.log('show alert');
         $scope.approvalAlert = $ionicPopup.show({
            templateUrl: UIPath + 'approvalAdmin/approvalAlert.html',
            title: '<p style="color:white;">审批</p>',
            scope: $scope,
            cssClass: 'approval_alert'
         });
      };

      //同意或拒绝
      $scope.agreeOrRefuse = function (status) {
         //1 同意 3 拒绝
         $scope.select.result = status;

      };

      //取消或提交
      $scope.cancleOrCommitApproval = function (type) {
         if (type == 0) {
            $scope.approvalAlert.close();
         } else {
            //确定提交
            if (!$scope.select.result) {
               toaster.warning({
                  title: '',
                  body: '请选择审批结果'
               });
               return;
            }
            $scope.commitApproval();
         }
      };

      //查看文件
      $scope.openFile = function (url) {
         if (isWeixin()) {
            alert('公众号不支持打开文档，如需查看请移步到”小跟班“app');
            return;
         }
         
        // url = 'https://test17.xgenban.com/wmdp/comup/201805/18/19603499049049307.pdf';
         DownloadFile.readFile(url);
      };

      $scope.getNameWidth = function (nameStr) {
         var width = textWidth(nameStr, 16) + 50 + 'px';
         return width;
      };
      $scope.currentApprovalStatus = function (progress) {
         var result;
         result = progress == 'PASS' ? '通过' : progress == 'APPROVAL' ? '审批中' : progress == 'FAIL' ? '不通过' : '';
         return result;
      };

      // request--查询详情
      $scope.getApprovalArchiveDeatil = function () {
         $scope.picUrls = [];//图片数组
         $scope.docs = [];//文件数组
         $scope.controls = [];
         $scope.inputs = [];//单行文本控件数组
         $scope.fulltexts = [];//多行文本控件数组
         $scope.dates = [];//日期控件数组
         $scope.dateRanges = [];//日期区间数组
         $scope.radios = [];//单选框控件数组
         $scope.checkboxes = [];//多选框控件数组
         $scope.timeList = [];//时间控件数组

         Requester.getApprovalArchiveDeatil($scope.archiveId).then(function (data) {
            if (data.result) {
               $scope.select.detail = data.data;
               $scope.inputs = data.data.controls.inputs;
               $scope.fulltexts =  data.data.controls.fulltexts;
               $scope.dates = data.data.controls.dates;
               $scope.dateRanges = data.data.controls.dateRanges;
               $scope.radios =  data.data.controls.radios;
               $scope.checkboxes =  data.data.controls.checkboxes;
               $scope.timeList = data.data.controls.timeList;
               $scope.inputs.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     text:item.text,
                     type:1,
                     index:item.idx?item.idx:1
                  });
               });
               
               $scope.fulltexts.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     fulltext:item.fulltext,
                     type:2,
                     index:item.idx?item.idx:2
                  });
               });

               $scope.dates.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                      date:item.date,
                     type:3,
                     index:item.idx?item.idx:3
                  });
               });

               $scope.dateRanges.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     startDate:item.startDate,
                     endDate:item.endDate,
                     type:4,
                     index:item.idx?item.idx:4
                  });
               });

               $scope.radios.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     checkOption:item.checkOption,
                     type:5,
                     index:item.idx?item.idx:5
                  });
               });

               $scope.checkboxes.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     checkOptions:item.checkOptions,
                     type:6,
                     index:item.idx?item.idx:6
                  });
               });

               $scope.timeList.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     time:item.time,
                     type:7,
                     index:item.idx?item.idx:7
                  });
               });

     
               for (var i = 0; i < data.data.attaches.length; i++) {
                  var url = data.data.attaches[i].attachUrl;
                  var fileType = url.slice(url.lastIndexOf('.') + 1);
                  console.log('filetype:'+fileType);
                  if (fileType === 'png' || fileType === 'jpg' || fileType === 'jpeg' || fileType === 'bmp' || fileType.length>10) {
                     $scope.picUrls.push({
                        src: url,
                        thumb: url
                     });
                  }else {
                     $scope.docs.push({
                        attachUrl:url,
                        attachName:data.data.attaches[i].attachName
                     });
                  }
               }
              

            } else {
               console.log('detail failed');
            }
         });
      };

      // request --审核
      $scope.commitApproval = function () {
         Requester.commitApproval($scope.userId, $scope.archiveId, $scope.select).then(function (data) {
            if (data.result) {
               toaster.success({
                  title: "审批成功",
                  body: "您的审批已提交",
                  timeout: 3000
               });
               $scope.getApprovalArchiveDeatil();
               $scope.approvalAlert.close();
            } else {
                toaster.warning({
                   title:'审批失败',
                   body:data.message
                });
            }

         });
      };
   }]);