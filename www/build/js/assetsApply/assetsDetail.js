angular.module('app.controllers')
    .controller('assetsDetailCtrl', ['$scope', '$ionicModal', 'Constant', '$stateParams', '$rootScope', 'Requester', function ($scope, $ionicModal, Constant, $stateParams, $rootScope, Requester) {
       
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.assetsId = $stateParams.assetsId ? $stateParams.assetsId : $rootScope.WXAssetsRecordId;
            if(isWeixin())
            $rootScope.WXAssetsRecordId = $scope.assetsId;
            $scope.hasGot = Math.pow(2, 30);
            $scope.cancelStatus = Math.pow(2, 31)-1;
            $scope.queryAssetsIds();
            $scope.getAssetsDetail();
        });
        //即将进入
        $scope.$on("$ionicView.enter", function (event, data) {

        });

        $scope.readAssetsId = function (event) {
            if (event.status == $scope.hasGot) {
                $scope.modal.show();
            }
        };

        //资产详情Request
        $scope.getAssetsDetail = function () {
            $scope.events = [];
            Requester.assetsApplyDetail($scope.assetsId).then(function (res) {
                if (res.result) {
                    $scope.assetsDetail = res.data;
                    console.log('detail');
                    console.log($scope.assetsDetail);
                    if (res.data.progress && res.data.progress.length > 0) {
                        for (var i = 0; i < res.data.progress.length; i++) {

                            var assetsProgress = res.data.progress[i];
                            var checkStatus = assetsProgress.orderProgress;
                            var progressName = checkStatus == 1 ? '提交申请' : checkStatus == 2 ? '正在审核' : checkStatus == 4 ? '拒绝申请' : checkStatus == 8 ? '审核通过' : checkStatus == $scope.hasGot ? '已领取' : checkStatus==$scope.cancelStatus? '已取消':'审核通过';
                            var prefix = checkStatus == 1 ? '申请人:' : checkStatus == 4 ? '审批人:' : checkStatus == 8 ? '审批人:' : checkStatus == $scope.hasGot ? '发放人:' : '审批人:';
                            var afterFix = assetsProgress.userDepartment ? '(' + assetsProgress.userDepartment + '）' : '';
                            if (checkStatus != 2) {
                                $scope.events.push({
                                    badgeIconClass: 'tm-icon',
                                    title: progressName,
                                    operateMan: prefix + ' ' + assetsProgress.userName + afterFix,
                                    text: checkStatus == $scope.hasGot ? '查看资产ID' : assetsProgress.progressDescribe,
                                    date: assetsProgress.operateTime.substr(0, 10),
                                    status: assetsProgress.orderProgress
                                });
                            }

                        }
                    }


                } else {

                }
            });
        };

        //已申领的资产id 
        $scope.queryAssetsIds = function () {
            Requester.queryAssetsApplyIds($scope.assetsId).then(function (res) {
                if (res.result) {
                    $scope.assetsApplyIds = res.data;
                }
            });
        };


        /**
         * modal 查看资产id
         */
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'assetsApply/assetsIds.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {

            $scope.modal = modal;
        });

        $scope.closeModal = function () {
            $scope.modal.hide();
        };
    }]);