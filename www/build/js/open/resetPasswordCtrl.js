/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('resetPwdCtrl', ['$scope', '$ionicHistory', 'AccountService', 'BroadCast', 'UserPreference', 'Constant', '$state', '$ionicPopup', 'toaster', 'MESSAGES', function ($scope, $ionicHistory, AccountService, BroadCast, UserPreference, Constant, $state, $ionicPopup, toaster, MESSAGES) {
        $scope.goBack = function () {
            if ($scope.step > 1)
                $scope.step--;
            else
                $state.go('login');
        };

        $scope.step = 1;
        $scope.input = {
            smscode: '',
            phone: '',
            retype: 't'
        };
        $scope.btnText = '获取验证码';

        function updateCoolDownText() {
            if ($scope.cd > 0) {
                $scope.cd--;
                $scope.btnText = $scope.cd;
            }
            else if ($scope.cooldown) {
                clearInterval($scope.cooldown);
                $scope.canReqSMS = true;
                $scope.cd = Constant.SMS_REQ_INTERVAL;
                $scope.btnText = '获取验证码';
            }
            $scope.$digest();
        }

        var lastSmsTime = UserPreference.get("LastSendSMSTime", 0);
        var diff = Math.round(new Date().getTime() / 1000) - lastSmsTime;
        $scope.canReqSMS = (diff >= Constant.SMS_REQ_INTERVAL);
        if ($scope.canReqSMS)
            $scope.cd = Constant.SMS_REQ_INTERVAL;
        else {
            $scope.cd = Constant.SMS_REQ_INTERVAL - diff;
            $scope.cooldown = setInterval(updateCoolDownText, 1000);
        }

        $scope.getSmsCode = function () {
            if ($scope.canReqSMS) {
                $scope.canReqSMS = false;
                AccountService.getSmsCode($scope.input.phone, 'forgetpass', $scope.input.retype);
            }
        };

        $scope.doReset = function () {
            var reg = /^[a-zA-Z0-9]+$/;
            if (!$scope.input.newpassword.match(reg) || !$scope.input.pwd.match(reg)) {
                toaster.warning({title: MESSAGES.REMIND, body: MESSAGES.LOGIN_PASSWORD_PATTERN});
                return;
            }
            if ($scope.input.pwd != $scope.input.newpassword) {
                toaster.warning({title: MESSAGES.REMIND, body: MESSAGES.PASSWORD_CONFIRM_ERROR});
                return;
            }
            AccountService.resetPwd(angular.copy($scope.input));
        };

        $scope.switchStep = function (s) {
            if (s === 2) {
                var opt = angular.copy($scope.input);
                opt.smstype = 'forgetpass';
                AccountService.checkCode(opt).then(function (resp) {
                    //console.log(resp);
                    if (resp.data.result) {
                        $scope.students = resp.data.data;
                        $scope.step = s;
                    }
                    else {
                        toaster.warning({title: MESSAGES.REMIND, body: resp.data.message});
                    }
                });
            }
        };

        $scope.$on(BroadCast.SMS_SENT, function (event, data) {
            if (data) {
                UserPreference.set("LastSendSMSTime", Math.round(new Date().getTime() / 1000));
                $scope.cooldown = setInterval(updateCoolDownText, 1000);
                if (!data.result) {
                    toaster.warning({title: MESSAGES.REMIND, body: data.message});
                }
            }
            if (Constant.debugMode) console.log(data);
        });

        $scope.$on(BroadCast.PASSWORD_RESET, function (event, data) {
            if (data && data.result) {
                var confirmPopup = $ionicPopup.alert({
                    title: '您的密码已重置',
                    template: '请再次尝试登录。',
                    okText: '确认',
                    okType: 'button-balanced'
                });
                confirmPopup.then(function () {
                    $state.go('login');
                });
            }
            if (Constant.debugMode) console.log(data);
        });
    }]);