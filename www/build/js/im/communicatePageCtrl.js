/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('communicatePageCtrl', ['UserPreference', '$scope', '$ionicScrollDelegate', '$state', 'ChatService', 'BroadCast', 'Constant', '$ionicModal', '$ionicPopup', 'ngIntroService', 'toaster', 'MESSAGES', '$rootScope', 'closePopupService', function (UserPreference, $scope, $ionicScrollDelegate, $state, ChatService, BroadCast, Constant, $ionicModal, $ionicPopup, ngIntroService, toaster, MESSAGES, $rootScope, closePopupService) {
        $scope.contactView = false;
        $scope.classes = UserPreference.getArray('class');
        var user = UserPreference.getObject('user');
        $scope.userRole = String(user.rolename);
        $scope.isTeacher = $scope.userRole === Constant.USER_ROLES.TEACHER;
        $scope.switchView = function (contactV) {
            $scope.contactView = contactV;
            $ionicScrollDelegate.resize();
            setTimeout(function () {
                if ($scope.contactView && $scope.showTip && $scope.classes.length > 1) {
                    var IntroOptions = {
                        steps: [
                            {
                                element: document.querySelector('#select_class'),
                                intro: "<strong>若您任教多个班级，可以在此处切换</strong>"
                            }],
                        showStepNumbers: false,
                        showBullets: false,
                        showButtons: false,
                        exitOnOverlayClick: true,
                        exitOnEsc: true,
                        doneLabel: '好的'
                    };
                    ngIntroService.setOptions(IntroOptions);
                    ngIntroService.start();
                    $scope.showTip = false;
                    UserPreference.set("ShowChatTip", true);
                }
            }, 300);
        };

        $scope.selectAllFiltered = function (list, isChecked) {
            var changeCount = 0;
            for (var i = 0; i < list.length; i++) {
                if (list[i].isChecked !== isChecked) {
                    list[i].isChecked = isChecked;
                    changeCount++;
                }
            }
            $scope.selectionChanged(isChecked, changeCount);
        };

        $scope.toggle = function (group, length) {
            if (length > 0) {
                group.show = !group.show;
                $ionicScrollDelegate.resize();
            }
        };
        $scope.sel = {};
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicModal.fromTemplateUrl(UIPath + 'im/newConversation.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });

        function initAddView() {
            $scope.selCount = 0;
            $scope.modalTitle = '发起聊天';
            for (var i = 0; i < $scope.addModeContactTree.length; i++) {
                for (var j = 0; j < $scope.addModeContactTree[i].list.length; j++) {
                    $scope.addModeContactTree[i].list[j].isChecked = false;
                }
            }
            $scope.modal.show();
        }

        $scope.openModal = function () {
            if ($scope.userRole == Constant.USER_ROLES.TEACHER && $scope.classes.length > 1) {
                $scope.classChooser = $ionicPopup.show({
                    template: '<div class="item" ng-repeat="item in classes" ng-click="onAddModeClassSelected(item)">{{item.value}}</div>',
                    title: '请选择聊天对象',
                    scope: $scope
                });
                closePopupService.register($scope.classChooser);
            }
            else {
                $scope.addModeContactTree = $scope.ContactTree;
                initAddView();
            }
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.saveConversation = function () {
            var arr = [];
            var name = '群聊:';
            for (var i = 0; i < $scope.addModeContactTree.length; i++) {
                for (var j = 0; j < $scope.addModeContactTree[i].list.length; j++) {
                    var p = $scope.addModeContactTree[i].list[j];
                    if (p.isChecked) {
                        arr.push(p.Member_Account);
                        if (arr.length < 2) {
                            name += p.Name;
                            name += ',';
                        }
                        else if (arr.length === 2)
                            name += p.Name;
                    }
                }
            }
            if (arr.length === 1) {
                var info = ChatService.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, arr[0], $scope.sel.class.key);
                var obj = {
                    SessionType: webim.SESSION_TYPE.C2C,
                    SessionId: arr[0],
                    SessionNick: info.name,
                    SessionImage: info.image
                };
                $scope.modal.hide();
                $state.go('communicateDetail', {obj: obj});
            }
            else {
                if (name.length > 10) {
                    name = name.substr(0, 10);
                    name += '..';
                }

                $scope.sel.tmpGroupName = name;

                var myPopup = $ionicPopup.show({
                    template: '<input type="text" placeholder="群名需在10字以内" maxlength="10" ng-model="sel.tmpGroupName">',
                    title: '群名称设置',
                    scope: $scope,
                    buttons: [{
                        text: '取消'
                    },
                        {
                            text: '<b>确定</b>',
                            type: 'button-balanced',
                            onTap: function (e) {
                                if ($scope.sel.tmpGroupName && $scope.sel.tmpGroupName.length > 0) {
                                    ChatService.createCustomGroup($scope.sel.tmpGroupName, $scope.sel.class, arr).then(function (resp) {
                                        //console.log(resp);
                                        if (resp.data.result) {
                                            ChatService.getContacts();
                                            $scope.modal.hide();

                                            var rsp = JSON.parse(resp.data.data);
                                            var obj = {
                                                GroupId: rsp.groupId,
                                                TypeEn: 'Private',
                                                Name: $scope.sel.tmpGroupName
                                            };
                                            $scope.conversationDetail(obj);
                                            myPopup.close();
                                            if (window.cordova) MobclickAgent.onEvent('app_private_group');
                                        }
                                    });
                                } else {
                                    e.preventDefault();
                                }
                            }
                        }
                    ]
                });
            }
        };

        $scope.onAddModeClassSelected = function (key) {
            if (key && $scope.classChooser) {
                $scope.sel.class = key;
                $scope.classChooser.close();
                getContactTree(key.key, true);
                initAddView();
            }
        };

        $scope.selectionChanged = function (isChecked, count) {
            if (count === undefined)
                count = 1;
            if (isChecked)
                $scope.selCount = $scope.selCount + count;
            else
                $scope.selCount = $scope.selCount - count;
            if ($scope.selCount > 0)
                $scope.modalTitle = '已选择' + $scope.selCount + '人';
            else {
                $scope.selCount = 0;
                $scope.modalTitle = '发起聊天';
            }
        };

        function getContactTree(classid, addMode) {
            $scope.cachedCID = angular.copy(classid);
            console.log( $scope.cachedCID );
            var friends = ChatService.friendsMap[classid];
           console.log('friends:');
           console.log(friends);
            var groups = ChatService.groups;
            var filterGroups = [];
            for (var j in groups) {
                if (groups[j].ClassID == classid)
                    filterGroups.push(groups[j]);
            }
            if (friends && friends.length > 0) {
                $scope.emptyContacts = false;
                var teachers = [], students = [], parents = [], others = [];
               
                for (var i in friends) {
                    var p = friends[i];
                    switch (p.Category) {
                        case 's':
                            students.push(p);
                            break;
                        case 't':
                            teachers.push(p);
                            break;
                        case 'f':
                            parents.push(p);
                            break;
                        default:
                            others.push(p);
                            break;
                    }
                }
                var contactTree;
                if (classid === 'schoolTeacher'||($scope.isTeacher&& !$scope.classes)||($scope.isTeacher&& $scope.classes&&$scope.classes.length<=0)) {
                    contactTree = [
                        {
                            name: '老师',
                            list: teachers,
                            num: teachers.length,
                            icon: 'img/icon/chat_teacher.png',
                            isGroup: false
                        },
                        {
                            name: '我的群组',
                            list: filterGroups,
                            num: filterGroups.length,
                            icon: 'img/icon/chat_group.png',
                            isGroup: true
                        }
                    ];
                }
                else {
                    contactTree = [
                        {
                            name: '老师',
                            list: teachers,
                            num: teachers.length,
                            icon: 'img/icon/default_teacher.png',
                            isGroup: false
                        },
                        {
                            name: '学生',
                            list: students,
                            num: students.length,
                            icon: 'img/icon/default_student.png',
                            isGroup: false
                        },
                        {
                            name: '家长',
                            list: parents,
                            num: parents.length,
                            icon: 'img/icon/default_parent.png',
                            isGroup: false
                        },
                        {
                            name: '我的群组',
                            list: filterGroups,
                            num: filterGroups.length,
                            icon: 'img/icon/chat_group.png',
                            isGroup: true
                        }
                    ];
                }

                // console.log('contacts:');
                // console.log( contactTree);
                if (addMode)
                    $scope.addModeContactTree = contactTree;
                $scope.ContactTree = contactTree;
            }
            else {
                $scope.emptyContacts = true;
                if (addMode)
                    $scope.addModeContactTree = [];
                $scope.ContactTree = [];
            }
        }

        $scope.$on(BroadCast.DIC_REV, function (a, rst) {
            if (rst === 'class')
                $scope.classes = UserPreference.getArray(rst);
        });

        $scope.$on('$ionicView.beforeEnter', function () {

            if (window.cordova&&ionic.Platform.isIOS()) {
                cordova.plugins.Keyboard.disableScroll(true);
            }
            $scope.userID = UserPreference.getObject('user').id;
            $scope.userRole = UserPreference.getObject('user').rolename;

            if ($scope.userRole != Constant.USER_ROLES.SCHOOL) {
                $scope.classes = UserPreference.getArray('class');
                $scope.sel.class = {
                    key: UserPreference.get('DefaultClassID', undefined)
                };

                if ($scope.userRole == Constant.USER_ROLES.TEACHER) {
                    $scope.classes.push({
                        key: 'schoolTeacher',
                        value: '学校同事'
                    });
                }
                for (var i = 0; i < $scope.classes.length; i++) {
                    if ($scope.sel.class.key == $scope.classes[i].key) {
                        $scope.sel.class = $scope.classes[i];
                        break;
                    }
                }
                if ($scope.userRole == Constant.USER_ROLES.TEACHER) {
                    if (!$scope.sel.class.key) {
                        $scope.sel.class = $scope.classes && $scope.classes.length > 0 ? $scope.classes[0] : {};
                        if ($scope.sel.class.key !== 'schoolTeacher')
                            UserPreference.set("DefaultClassID", $scope.sel.class.key);
                    }
                    $scope.showTip = !UserPreference.getBoolean("ShowChatTip");
                }
                else {
                    $scope.showTip = false;
                }
            }

            
        });

        $scope.refreshContacts = function (contactView) {
            if (contactView)
                ChatService.getContacts();
            else
                ChatService.getRecentContacts($scope.sel.class.key);
        };

        $scope.$on('$ionicView.enter', function () {
            if ($rootScope.ContactProfileChanged === true)
                ChatService.getContacts();
            else if ($scope.sel.class.key != $scope.cachedCID) {
                getContactTree($scope.sel.class.key);
            }
            ChatService.getRecentContacts($scope.sel.class.key);
        });

        $scope.loading = true;
        setTimeout(function () {
            ChatService.getRecentContacts($scope.sel.class.key);
        }, 5000);

        $scope.onSelectionChange = function () {
            if ($scope.sel.class) {
                if ($scope.sel.class.key !== 'schoolTeacher')
                    UserPreference.set("DefaultClassID", $scope.sel.class.key);
                getContactTree($scope.sel.class.key);
            }
        };

        $scope.conversationDetail = function (item, isConversation) {
            if (isConversation) {
                $state.go('communicateDetail', {obj: item});
            }
            else {
                if (item.Member_Account) {
                    var obj = {
                        SessionId: item.Member_Account
                    };
                    $state.go('contactProfile', {obj: obj});
                }
                else
                    $state.go('communicateDetail', {obj: item});
            }
        };

        $scope.$on(BroadCast.IM_RECENT_CONTACTS, function (event, resp) {
            if (resp) {
                $scope.conversations = resp;
                $scope.$digest();
            }
            $scope.$broadcast('scroll.refreshComplete');

            $scope.loading = false;
        });

        $scope.$on(BroadCast.IM_REV_CONTACTS, function (event, data) {
            if (data)
                getContactTree($scope.sel.class.key);
            else
                toaster.error({title: MESSAGES.REMIND, body: MESSAGES.CONTACT_FETCH_FAIL});
            $scope.$broadcast('scroll.refreshComplete');
        });

        $scope.$on(BroadCast.IM_NEW_MESSAGE, function (event, newMsgList) {
            $scope.conversations = ChatService.conversations;
            $scope.$digest();
        });

        $scope.removeHTMLTag = function (str) {
            str = str.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
            str = str.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
            // str = str.replace(/\n[\s| | ]*\r/g, '\n'); //去除多余空行
            str = str.replace(/&nbsp;/ig, '');//去掉&nbsp;
            return str;
        };


    }]);
