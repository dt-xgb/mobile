/**

 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('communicateDetailCtrl', ['$ionicModal', 'CameraHelper', '$ionicPopover', 'Constant', '$scope', '$state', '$stateParams', 'ChatService', '$ionicActionSheet', '$ionicPopup', '$ionicScrollDelegate', '$timeout', '$interval', '$ionicHistory', 'BroadCast', '$rootScope', 'toaster', 'MESSAGES', 'UserPreference', 'ionicImageView', function ($ionicModal, CameraHelper, $ionicPopover, Constant, $scope, $state, $stateParams,
        ChatService, $ionicActionSheet, $ionicPopup, $ionicScrollDelegate, $timeout, $interval,
        $ionicHistory, BroadCast, $rootScope, toaster, MESSAGES, UserPreference, ionicImageView) {
        $scope.goBack = function () {
            if ($ionicHistory.backView())
                $ionicHistory.goBack();
            else
                $state.go('tabsController.mainPage');
        };

        if(window.cordova){
           // cordova.plugins.Keyboard.disableScroll(false);
        }

        $scope.user = {};
        $scope.messages = [];

        $scope.input = {
            message: ''
        };

        $scope.convertMsgtoHtml = function (msg) {
            return convertMsgtoHtml(msg);
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';
        $ionicPopover.fromTemplateUrl(UIPath + 'im/emotions.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.popover = popover;
        });

        $scope.hidePopover = function () {
            if ($scope.popover)
                $scope.popover.hide();
        };

        $scope.emotions = webim.Emotions;

        $scope.showEmotions = function ($event) {
            setTimeout(function () {
                if (window.cordova)
                    cordova.plugins.Keyboard.close();
                $scope.popover.show($event);
            }, 100);
        };

        $scope.selectEmotion = function (e) {
            if ($scope.input.message)
                $scope.input.message += e;
            else
                $scope.input.message = e;
        };

        $ionicModal.fromTemplateUrl(UIPath + 'im/chatGroupInfo.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function (edit) {
            if (edit)
                $scope.editModal.hide();
            else
                $scope.modal.hide();
        };


        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
            $scope.editModal.remove();
        });

        $scope.$on('popover.hidden', function () {
            // Execute action
        });

        $ionicModal.fromTemplateUrl(UIPath + 'im/newConversation.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.editModal = modal;
        });
        $scope.showEditMode = function (mode) {
            getContactTree();
            $scope.selCount = 0;
            if (mode === 'add') {
                $scope.editFilter = {
                    isMember: false
                };
                $scope.constantTitle = '添加群成员';
            } else {
                $scope.editFilter = {
                    isMember: true
                };
                $scope.constantTitle = '移除群成员';
            }
            $scope.modalTitle = $scope.constantTitle;
            $scope.editModal.show();
        };

        $scope.selectAllFiltered = function (list, isChecked) {
            var changeCount = 0;
            for (var i = 0; i < list.length; i++) {
                if (list[i].isChecked !== isChecked) {
                    list[i].isChecked = isChecked;
                    changeCount++;
                }
            }
            $scope.selectionChanged(isChecked, changeCount);
        };

        function getContactTree() {
            var friends = ChatService.friendsMap[$scope.classID];
            if (friends && friends.length > 0) {
                var teachers = [],
                    students = [],
                    parents = [],
                    others = [];
                for (var i in friends) {
                    var p = friends[i];
                    p.isMember = false;
                    p.isChecked = false;
                    for (var j = 0; j < $scope.groupInfo.MemberList.length; j++) {
                        if (p.Member_Account == $scope.groupInfo.MemberList[j].Member_Account) {
                            p.isMember = true;
                            break;
                        }
                    }
                    switch (p.Category) {
                        case 's':
                            students.push(p);
                            break;
                        case 't':
                            teachers.push(p);
                            break;
                        case 'f':
                            parents.push(p);
                            break;
                        default:
                            others.push(p);
                            break;
                    }
                }
                $scope.addModeContactTree = [{
                        name: '老师',
                        list: teachers,
                        num: teachers.length,
                        icon: 'img/icon/chat_teacher.png',
                        isGroup: false
                    },
                    {
                        name: '学生',
                        list: students,
                        num: students.length,
                        icon: 'img/icon/chat_student.png',
                        isGroup: false
                    },
                    {
                        name: '家长',
                        list: parents,
                        num: parents.length,
                        icon: 'img/icon/chat_parent.png',
                        isGroup: false
                    }
                ];
                console.log($scope.addModeContactTree);
            }
        }

        function getGroupDetail(init) {
            ChatService.getGroupInfo($scope.SessionId).then(
                function (resp) {
                    // console.log('group info');
                    // console.log(resp);
                    $scope.groupInfo = resp.GroupInfo[0];
                    $scope.isGroupAdmin = ($scope.groupInfo.Owner_Account == $scope.userID);
                    if (init) {
                        webim.MsgStore.delSessByTypeId($scope.SessionType, $scope.SessionId);
                        $scope.session = undefined;
                        $scope.NextMsgSeq = $scope.groupInfo.NextMsgSeq - 1;
                        ChatService.getGroupHistoryMsgs($scope.SessionId, $scope.NextMsgSeq);
                    }
                },
                function (error) {
                    $scope.doneLoading = true;
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,
                        body: MESSAGES.CONNECT_ERROR_MSG
                    });
                }
            );
        }

        $scope.toggle = function (group, length) {
            if (length > 0) {
                group.show = !group.show;
                $ionicScrollDelegate.resize();
            }
        };

        $scope.onImageClick = function (pic) {
            ionicImageView.showViewModal({
                allowSave: true
            }, [pic]);
        };

        $scope.selectionChanged = function (isChecked, count) {
            if (count === undefined)
                count = 1;
            if (isChecked)
                $scope.selCount = $scope.selCount + count;
            else
                $scope.selCount = $scope.selCount - count;
            if ($scope.selCount > 0)
                $scope.modalTitle = '已选择' + $scope.selCount + '人';
            else {
                $scope.selCount = 0;
                $scope.modalTitle = $scope.constantTitle;
            }
        };

        $scope.saveConversation = function () {
            var arr = [];
            for (var i = 0; i < $scope.addModeContactTree.length; i++) {
                for (var j = 0; j < $scope.addModeContactTree[i].list.length; j++) {
                    var p = $scope.addModeContactTree[i].list[j];
                    if (p.isChecked) {
                        arr.push(p.Member_Account);
                    }
                }
            }
            if (arr.length > 0) {
                ChatService.modifyGroupMember($scope.groupInfo.GroupId, arr, $scope.editFilter.isMember).then(function (resp) {
                    getGroupDetail();
                    $scope.closeModal('edit');
                });
            }
        };

        var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
        var txtInput = angular.element(document.getElementsByName("chatInput"));

        $scope.$on('$ionicView.loaded', function () {
            if ($stateParams.obj) {
                $scope.user = ChatService.loginInfo;
                $scope.userID = $scope.user.identifier;
                $scope.SessionId = $stateParams.obj.SessionId;
                $scope.isGroupAdmin = false;
                $scope.isHeadTeacher = UserPreference.getBoolean('isHeadTeacher');
                if ($scope.SessionId) {
                    //会话列表进入
                    $scope.SessionType = $stateParams.obj.SessionType;
                    $scope.SessionName = $stateParams.obj.SessionNick;
                    $scope.SessionImage = $stateParams.obj.SessionImage;
                } else {
                    //通讯录进入
                    if ($stateParams.obj.TypeEn) {
                        //群
                        $scope.SessionId = $stateParams.obj.GroupId;
                        $scope.SessionType = webim.SESSION_TYPE.GROUP;
                        $scope.SessionName = $stateParams.obj.Name;
                    } else {
                        //私聊
                        $scope.SessionId = $stateParams.obj.Member_Account;
                        $scope.SessionType = webim.SESSION_TYPE.C2C;
                        $scope.SessionName = $stateParams.obj.Name;
                        $scope.SessionImage = $stateParams.obj.Icon;
                    }
                }

                if ($scope.SessionType == webim.SESSION_TYPE.C2C) {
                    $scope.msgKey = '';
                    $scope.msgLastTime = 0;
                    $scope.doneLoading = false;
                    ChatService.getC2CHistoryMsgs($scope.SessionId, $scope.msgLastTime, $scope.msgKey);
                } else {
                    $scope.doneLoading = false;
                    var arr = $scope.SessionId.split('_');
                    if (arr && arr.length > 1) {
                        $scope.classID = arr[arr.length - 1];
                    }
                    getGroupDetail(true);
                }
            }
            //console.log($scope);
        });

        $scope.changeGroupName = function () {
            if ($scope.groupInfo.Type === 'Private') {
                $scope.groupInfo.tmpName = angular.copy($scope.groupInfo.Name);
                var myPopup = $ionicPopup.show({
                    template: '<input type="text" placeholder="群名需在10字以内" maxlength="10" ng-model="groupInfo.tmpName">',
                    title: '群名称设置',
                    scope: $scope,
                    buttons: [{
                            text: '取消'
                        },
                        {
                            text: '<b>确定</b>',
                            type: 'button-balanced',
                            onTap: function (e) {
                                if ($scope.groupInfo.tmpName && $scope.groupInfo.tmpName.length > 0) {
                                    ChatService.modifyGroupInfo($scope.groupInfo.GroupId, $scope.groupInfo.tmpName).then(
                                        function (resp) {
                                            $rootScope.ContactProfileChanged = true;
                                            $scope.groupInfo.Name = $scope.groupInfo.tmpName;
                                            $scope.SessionName = $scope.groupInfo.tmpName;
                                            myPopup.close();
                                        }
                                    );
                                } else {
                                    e.preventDefault();
                                }
                            }
                        }
                    ]
                });
            }
        };

        $scope.getC2CImage = function (id) {
            if ($scope.SessionType == webim.SESSION_TYPE.C2C)
                return $scope.SessionImage;
            else {
                //var result =
                var info = ChatService.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, id, $scope.classID);
               // console.log(info);
                return info ? info.image ?info.image:info.type==='s'?'img/icon/default_student.png':info.type==='f'?'img/icon/default_parent.png': 'img/icon/default_teacher.png':'img/icon/default_teacher.png';
            }
        };

        $scope.getC2CName = function (id, max) {
            var limit = 5;
            if (max)
                limit = max;
            if ($scope.SessionType == webim.SESSION_TYPE.C2C)
                return '';
            else {
                var info = ChatService.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, id, $scope.classID);
                var name = info ? info.name : id;
                return name.length > limit ? name.substr(0, limit) : name;
            }
        };

        $scope.quitGroup = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: '确认退出该群？',
                template: '退出后群内的聊天记录将一并清空。',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    ChatService.quitGroup($scope.groupInfo.GroupId).then(function (resp) {
                        if (resp.ErrorCode === 0) {
                            $rootScope.ContactProfileChanged = true;
                            $scope.closeModal();
                            $scope.goBack();
                        }
                    });
                }
            });
        };

        $scope.destroyGroup = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: '确认解散该群？',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    ChatService.destroyGroup($scope.groupInfo.GroupId, $scope.groupInfo.Owner_Account).then(function (resp) {
                        if (resp.result) {
                            $rootScope.ContactProfileChanged = true;
                            $scope.closeModal();
                            $scope.goBack();
                        }
                    });
                }
            });
        };

        $scope.$on('$ionicView.leave', function () {
            if ($scope.session)
                webim.setAutoRead($scope.session, false, false);
            if ($scope.SessionType != webim.SESSION_TYPE.C2C) {
                webim.MsgStore.delSessByTypeId($scope.SessionType, $scope.SessionId);
                $scope.session = undefined;
            }

        });

        $scope.loadMore = function () {
            if ($scope.SessionType == webim.SESSION_TYPE.C2C)
                ChatService.getC2CHistoryMsgs($scope.SessionId, $scope.msgLastTime, $scope.msgKey);
            else
                ChatService.getGroupHistoryMsgs($scope.SessionId, $scope.NextMsgSeq);
        };

        function getSessionFromMsgIfNotExist() {
            if (!$scope.session && $scope.messages.length > 0) {
                $scope.session = $scope.messages[0].getSession();
                webim.setAutoRead($scope.session, true, true);
            }
        }

        $scope.$on(BroadCast.IM_NEW_MESSAGE, function (event, newMsgList) {
            for (var j in newMsgList) {
                var newMsg = newMsgList[j];
                if (newMsg.getSession().id() == $scope.SessionId) {
                    for (var i = 0; i < $scope.messages.length; i++) {
                        if ($scope.messages[i].random == newMsg.random) {
                            $scope.messages.splice(i, 1);
                            break;
                        }
                    }
                    $scope.messages.push(newMsg);
                    getSessionFromMsgIfNotExist();
                }
            }
            $scope.$digest();
            $timeout(function () {
                viewScroll.scrollBottom(true);
            }, 100);
            if ($scope.session)
                webim.setAutoRead($scope.session, true, true);
        });

        function insertTimeLabelToMsg(i) {
            for (; i >= 0; i--) {
                var msg = $scope.messages[i];
                var msgDay = formatTimeWithoutSecends(msg.time).substr(8, 2);
                var now = formatTimeWithoutSecends(new Date().getTime() / 1000).substr(8, 2);
                if (i > 0) {
                    var preMsg = $scope.messages[i - 1];
                    var preMsgDay = formatTimeWithoutSecends(preMsg.time).substr(8, 2);
                    if (now != msgDay) {
                        if (msgDay != preMsgDay)
                            msg.timeLabel = formatTimeWithoutSecends(msg.time);
                        else
                            msg.timeLabel = undefined;
                    } else if (msg.time - preMsg.time >= 60 * 5)
                        msg.timeLabel = formatTimeWithoutSecends(msg.time).substr(11, 5);
                    else
                        msg.timeLabel = undefined;
                } else {
                    if (now != msgDay) {
                        msg.timeLabel = formatTimeWithoutSecends(msg.time);
                    } else
                        msg.timeLabel = formatTimeWithoutSecends(msg.time).substr(11, 5);
                }
            }
        }

        function createSessionIfNotExist() {
            $scope.session = webim.MsgStore.sessByTypeId($scope.SessionType, $scope.SessionId);
            if (!$scope.session)
                $scope.session = new webim.Session($scope.SessionType, $scope.SessionId, $scope.SessionName, $scope.SessionImage, Math.round(new Date().getTime() / 1000));
            webim.setAutoRead($scope.session, true, true);
        }

        function onRevHistoryMsg(data) {
            if (!Array.isArray(data)) {
                console.log('message data is not array');
                $scope.doneLoading = true;
                $scope.$broadcast('scroll.refreshComplete');
                return;
            }
            var i;
            if ($scope.messages.length > 0) {
                for (var j = data.length - 1; j >= 0; j--) {
                    $scope.messages.unshift(data[j]);
                }
                i = data.length;
            } else {
                $scope.messages = data;
                i = $scope.messages.length - 1;
                getSessionFromMsgIfNotExist();
            }

            insertTimeLabelToMsg(i);
            if ($scope.messages.length == data.length) {
                $timeout(function () {
                    viewScroll.scrollBottom(true);
                }, 1000);
            }
            $scope.doneLoading = true;
            $scope.$broadcast('scroll.refreshComplete');
        }

        $scope.$on(BroadCast.IM_C2C_HISTORY_MESSAGE, function (event, data) {
            if (data) {
                if ($scope.msgKey === '')
                    $scope.messages = [];
                $scope.msgKey = data.MsgKey;
                $scope.msgLastTime = data.LastMsgTime;
            }
            onRevHistoryMsg(data.MsgList);
        });


        $scope.$on(BroadCast.IM_GROUP_HISTORY_MESSAGE, function (event, data) {
            var filter = [];
            if (data.length > 0) {
                $scope.NextMsgSeq = data[0].seq - 1;
                for (var i = 0; i < data.length; i++) {
                    // var senderId = data[i].fromAccount;
                    // if (!senderId || senderId == '@TIM#SYSTEM' ) {
                    // 	continue;
                    // }
                    if (convertMsgtoHtml(data[i]) !== '')
                        filter.push(data[i]);
                }
            }
            onRevHistoryMsg(filter);
        });

        $scope.$on(BroadCast.IM_MSG_SENT, function (event, msg) {
            if (!msg)
                return;
            if ($scope.SessionType == webim.SESSION_TYPE.C2C) {
                if (msg.type === 'image') {
                    var message = msg.msg;
                    for (var i = 0; i < $scope.messages.length; i++) {
                        if ($scope.messages[i].random == message.random) {
                            $scope.messages.splice(i, 1);
                            break;
                        }
                    }
                    $scope.messages.push(message);
                    $scope.$digest();
                    $timeout(function () {
                        viewScroll.scrollBottom(true);
                    }, 100);
                }
            }
        });

        $scope.$on(BroadCast.IM_MSG_SENDING, function (event, msg) {
            if (!msg)
                return;
            $scope.input.message = '';
            var title = UserPreference.getObject("user").name;
            var pushMessage = '给您发送了一条消息',
                to = [];
            var msgStr = convertMsgtoPushStr(msg.msg);
            if (msgStr !== "") {
                pushMessage = msgStr;
            }
            if ($scope.SessionType == webim.SESSION_TYPE.C2C || msg.type === 'image') {
                var message = angular.copy(msg.msg);
                if (msg.type === 'image') {
                    var text_obj = new webim.Msg.Elem.Text("图片发送中..");
                    message.addText(text_obj);
                    pushMessage = '[图片]';
                }
                console.log(message);
                $scope.messages.push(message);
                $timeout(function () {
                    viewScroll.scrollBottom(true);
                }, 100);
            }
            if ($scope.SessionType == webim.SESSION_TYPE.GROUP) {
                for (var j = 0; j < $scope.groupInfo.MemberList.length; j++) {
                    if ($scope.groupInfo.MemberList[j].Member_Account != $scope.userID)
                        to.push($scope.groupInfo.MemberList[j].Member_Account);
                }
            } else {
                to.push($scope.SessionId);
            }
            ChatService.sendPush(to, title, pushMessage);
        });

        $scope.sendMessage = function () {

            keepKeyboardOpen();
            if (!$scope.session)
                createSessionIfNotExist();
            if ($scope.input.message && $scope.input.message.length > 0) {
                ChatService.sendMessage($scope.session, $scope.input.message);
                $scope.isSending = true;
            }
            $timeout(function () {
                $scope.isSending = false;
            }, 1000);
        };

        $scope.selectImg = function () {
            if (!$scope.isSending) {
                $timeout(function () {
                    if (window.cordova)
                        cordova.plugins.Keyboard.close();
                    $ionicScrollDelegate.resize();
                    CameraHelper.selectImage('chat');
                }, 10);
            }
        };

        $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
            if (rst && rst.which === 'chat') {
                ChatService.uploadImageAndSend($scope.session, rst.source);
            }
        });

        // this keeps the keyboard open on a device only after sending a message, it is non obtrusive
        function keepKeyboardOpen() {
            console.log('keepKeyboardOpen');
            $timeout(function () {
                $ionicScrollDelegate.resize();
                viewScroll.scrollBottom(true);
            }, 100);
            txtInput.one('blur', function () {
                console.log('textarea blur, focus back on it');
                txtInput[0].focus();
            });
        }

        $scope.onMessageHold = function (e, itemIndex, message) {
            console.log('onMessageHold');
            // $ionicActionSheet.show({
            // 	buttons: [{
            // 		text: '复制'
            // 	}, {
            // 		text: '删除消息'
            // 	}],
            // 	buttonClicked: function(index) {
            // 		switch (index) {
            // 			case 0: // Copy Text
            // 				//cordova.plugins.clipboard.copy(message.text);
            //
            // 				break;
            // 			case 1: // Delete
            // 				// no server side secrets here :~)
            // 				$scope.messages.splice(itemIndex, 1);
            // 				$timeout(function() {
            // 					viewScroll.resize();
            // 				}, 0);
            //
            // 				break;
            // 		}
            //
            // 		return true;
            // 	}
            // });
        };

        // this prob seems weird here but I have reasons for this in my module, secret!
        $scope.viewProfile = function (id) {
            var obj = {};
            if (id === $scope.user.identifier) {
                obj.self = true;
            }
            obj.SessionId = id;
            $state.go('contactProfile', {
                obj: obj
            });
        };

        //get default headImg
        $scope.getDefaultHeadImg = function () {
            console.log($scope.groupInfo.MemberList);
            ChatService.getUserDetail(id).then(function (res) {

                if (res.result) {
                    $scope.groupInfo.MemberList[i].defaultImg = res.data.rolename === 4 ? 'img/icon/default_parent.png' : res.data.rolename === 2 ? 'img/icon/default_student.png' : 'img/icon/default_teacher.png';
                }
            });

        };
    }]);
