/**
 * Created by hewz on 2018/4/11.
 */
angular.module('app.controllers')
    .controller('parScoresCtrl', ['$scope', '$ionicHistory', 'Requester', 'UserPreference', '$ionicScrollDelegate', '$window', '$ionicPopup', 'Constant', '$timeout', 'BroadCast', function ($scope, $ionicHistory, Requester, UserPreference, $ionicScrollDelegate, $window, $ionicPopup, Constant, $timeout,BroadCast) {

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.isIos = ionic.Platform.isIOS()&&!isWeixin();

        });
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            //定义变量
            $scope.selected = true;
            $scope.pageNum = 1;
            $scope.selectExamSubject = 0;
            $scope.examRes = {};
            $scope.isMoreData = true;
            $scope.isLoadingSucceed = false;//加载中
          
            $scope.examList = [];
            //考试成绩
            $timeout(function () {
                $scope.getScoresRequest($scope.pageNum);
            },100);

        });
        $scope.$on("$ionicView.enter", function (event, data) {
            // $scope.getExamEchat();

        });
        //离开页面时  将所有未读的 标记为已读状态
        $scope.$on("$ionicView.leave", function (event, data) {
            $scope.selected = true;
            $scope.fixExamState();

        });
        $scope.$on(BroadCast.CONNECT_ERROR,function(){
            $scope.isMoreData = false;
         }) ;
        
        //家长获取学生的考试成绩
        $scope.getScoresRequest = function (pageNum) {
            var user = UserPreference.getObject('user');
            $scope.userRole = String(user.rolename);
            $scope.stuId = $scope.userRole === Constant.USER_ROLES.PARENT ? UserPreference.get('DefaultChildID') : String(user.id);
            $scope.examArr = [];
            $scope.subjectArr = [];
            $scope.isLoadingSucceed = false;
            Requester.selectTestScoresList($scope.userRole, '0', pageNum, $scope.stuId).then(function (resp) {
                console.log(resp);
            //   $scope.$broadcast('scroll.refreshComplete');
            //   $scope.$broadcast('scroll.infiniteScrollComplete');
                if (resp.result) {

                    $scope.isLoadingSucceed = true;
                    $scope.examRes = resp;
                    $scope.examArr = resp.data.content;
                    $scope.examArr.forEach(function (exam) {
                        exam.isNewExam = false;//是否是新增考试
                        //推送过来的考试 与考试列表的 进行对比 如果没有 存在则为新增
                        $scope.examList.push(exam);
                    });

                    //上拉加载的没有更多数据
                    if ($scope.pageNum > 1 && $scope.examArr.length < 20) {
                        console.log('当前页面：'+$scope.pageNum);
                        $scope.isMoreData = false;
                    }

                    $scope.examList.forEach(function (exam) {
                        $scope.subjectArr = exam.subjectScores;
                        $scope.noScoresNum = 0;//初始化定义无分数的科目个数
                        exam.examTotalMark = '';
                        $scope.subjectArr.forEach(function (subject) {
                            if ((subject.score === '--' || subject.score == null || subject.score == '') && subject.subjectId != 0) {
                                $scope.noScoresNum++;
                            }
                            if (subject.subjectId == 0) {
                                exam.examTotalMark = subject.score;
                            }
                        });
                        exam.noScoresNum = $scope.noScoresNum;
                    });

                } else {
                    $scope.isMoreData = false;
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            //    $scope.isMoreData = false;
            });
        };

        //将 所有标记为已读
        $scope.fixExamState = function () {
            $scope.examList.forEach(function (exam) {
                if (exam.readed == false) {
                    //遍历考试数组，将未读的改为已读
                    Requester.fixExamReadState(exam.examId, $scope.stuId, true).then(function (resp) {
                        if (resp.result) {
                            console.log('fix  state succeed');
                        }
                    });
                }

            });

        };


        //下拉刷新
        $scope.loadData = function () {
            $scope.examList = [];
            $scope.isMoreData = true;
            $scope.getScoresRequest(1);
            //$scope.$broadcast('scroll.refreshComplete');
        };
        //上拉加载
        $scope.loadMore = function () {
            $scope.pageNum++;
            $scope.getScoresRequest($scope.pageNum);

        };

        //head bar tab click
        $scope.chooseTypeClick = function (index) {
            if (index == 1) {
                $scope.selected = true;
                //考试成绩
                $scope.examList = [];
                $scope.pageNum = 1;
                $scope.getScoresRequest($scope.pageNum);
            } else {
                //成长分析
                $scope.selected = false;
                //成长分析
                $scope.allExamArr = [];//成长分析所有考试数组(含总分)
                $scope.allSubjectExams = [];//所有科目考试
                $scope.selectSubjectExams = [];//当前tab 选择的科目考试数组
                $scope.analysisExamArr = [];
                $scope.isLoadingSucceed = true;//加载中
                Requester.getStudentGrowthAnalysis($scope.stuId).then(function (resp) {
                    if (resp.result) {
                        $scope.isLoadingSucceed = false;
                        console.log(resp);
                        $scope.allExamArr = resp.data;//带总分的 考试数组
                        $scope.allExamArr.forEach(function (exam) {
                            if (exam.subjectId != 0) {
                                $scope.allSubjectExams.push(exam);//不带总分学科的考试数组
                            }
                            if (exam.subjectName.length <= 2) {

                                exam.subjectWidth = exam.subjectName.length * (21.5 - exam.subjectName.length * 0.5);
                            } else {
                                exam.subjectWidth = exam.subjectName.length * (19.5 - exam.subjectName.length * 0.5);
                            }

                        });
                        console.log('examScore :');
                        console.log($scope.allSubjectExams);
                        //默认选择总分
                        if ($scope.allSubjectExams.length > 1) {
                            $scope.selectSubjectExams = $scope.allExamArr[0].examScores;

                        } else if($scope.allSubjectExams.length==1){
                            $scope.selectSubjectExams = $scope.allSubjectExams[0].examScores;
                        }else{
                            $scope.selectSubjectExams = [];
                        }
                        return $scope.selectSubjectExams;
                    } else {
                        //$scope.isMoreData = false;
                        return;
                    }

                }).then(function (selectSubjectArr) {
                    $scope.initEchatData(selectSubjectArr);

                }).finally(function () {
                    $scope.isLoadingSucceed = false;
                   // $scope.isMoreData = false;
                });

                //5秒后 自动消失
                $timeout(function () {
                    $scope.isLoadingSucceed = false;
                   // $scope.isMoreData = false;
                }, 5000);

            }


        };

        //成长分析
        $scope.chooseSubjectStatistical = function (index,examName) {
            $scope.legendName = examName;
            $scope.selectExamSubject = index;
            $scope.selectSubjectExams = $scope.allExamArr[index].examScores;

            //初始化相关数组
            $scope.initEchatData($scope.selectSubjectExams);

            var currentBtn = document.getElementById('subject' + index);
            var currentX = currentBtn.offsetLeft;

            if (index < $scope.allExamArr.length - 1) {
                var nextIndex = index + 1;
                var nextBtn = document.getElementById('subject' + nextIndex);
                var nextX = nextBtn.offsetLeft;
                //下一个按钮的尾部坐标
                var nextButWidth = $scope.allExamArr[nextIndex].subjectWidth + nextX + 85;
                //获取屏幕宽度
                $scope.screenWidth = $window.innerWidth;
                if (nextButWidth > $scope.screenWidth) {
                    $ionicScrollDelegate.$getByHandle('small').scrollTo(currentX, 0);
                }
            }


        };

        //初始化图表 和 相关数据的数组
        $scope.initEchatData = function (selectSubjectArr) {
            $scope.XdataArr = [];//图表的X轴数组
            $scope.totalScoreArr = [];//总分数据
            $scope.avgScoreArr = [];//平均分数组
            selectSubjectArr.forEach(function (exam, index) {
                var score = exam.score == '--' || exam.score == null ? 0 : exam.score;
                var classAvgScore = exam.classAvgScore == '--' || exam.classAvgScore == null ? 0 : exam.classAvgScore;
                if (index < 5) {
                    var subExamName = '';
                    if(exam.examName.length<8){
                        subExamName = exam.examName;
                    }else{
                        subExamName = exam.examName.substr(0,7)+'...';
                    }
                    $scope.XdataArr.push(subExamName);//只存放最近的五条数据
                    $scope.totalScoreArr.push(score);
                    $scope.avgScoreArr.push(classAvgScore);
                }
            });
            var chartDiv = document.getElementById('score_echat');
            if (selectSubjectArr.length > 1) {
                //考试数目 大于1 才初始化表格
                chartDiv.style.display = 'block';
            } else {
                chartDiv.style.display = 'none';
            }
            $scope.getExamEchat();

        };

        $scope.scroll = function () {
           
        };

        //封装echat
        $scope.getExamEchat = function () {

            $timeout(function () {
                var chartDiv = document.getElementById('score_echat');
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(chartDiv);
                myChart.resize('350px', '350px');

                // 指定图表的配置项和数据
                var option = {
                    title: {
                        text: ''
                    },
                    color: ['#2bcab2', '#f19149'],
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
                    legend: {
                        data: ['总分', '平均分'],
                        top: '3%'
                    },
                    grid: {
                        left: '5%',
                        right: '5%',
                        bottom: '3%',
                        top: '14%',
                        containLabel: true,

                    },
                    xAxis: {
                        data: $scope.XdataArr,
                        axisTick: {
                            alignWithLabel: true
                        },
                        axisLabel: {
                            interval: 0,
                            rotate: 30,
                            

                        }
                    },
                    yAxis: {},
                    series: [{
                        name: '总分',
                        type: 'line',
                        smooth: 'true',
                        data: $scope.totalScoreArr
                    }, {
                        name: '平均分',
                        type: 'line',
                        smooth: 'true',
                        data: $scope.avgScoreArr
                    },
                    ]
                };

                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);

            }, 10);


        };

        //查看考试名称
        $scope.showExamName = function (examName) {
            var myPopup = $ionicPopup.show({
                title: examName,
                scope: $scope,
                cssClass: 'wl-alert'
            });
            $timeout(function () {
                myPopup.close(); //close the popup after 3 seconds for some reason
            }, 1000);

        };

        //nav back
        $scope.goBack = function () {

            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $ionicHistory.goBack();
        };



    }]);