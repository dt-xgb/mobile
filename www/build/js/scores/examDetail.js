/**
 * Created by hewz on 2018/4/14.
 */
angular.module('app.controllers')
    .controller('examDetailCtrl',['$scope', '$ionicHistory', 'Requester', 'UserPreference', '$ionicScrollDelegate', '$stateParams', '$window', '$rootScope', function ($scope,$ionicHistory,Requester,UserPreference,$ionicScrollDelegate,$stateParams,$window,$rootScope){


        $scope.selecteExamSubject = 0;

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.isIos = ionic.Platform.isIOS()&&!isWeixin();
            $scope.selectExamSubjectIndex = 0;
            $scope.examId = $stateParams.examId?$stateParams.examId: $rootScope.WXRootExamId;
            $scope.classId = $stateParams.classId? $stateParams.classId:  $rootScope.WXRootClassId;
            $rootScope.WXRootExamId = $scope.examId;
            $rootScope.WXRootClassId = $scope.classId;
            $scope.subjectExamDetailArr = [];//初始化学科考试详情数组
            $scope.selectExamSubject = {};//所选择的考试，默认显示数组第一个
           
            //接口返回数据
            Requester.getClassStudentExamListDetail($scope.classId,$scope.examId ).then(function (res) {
                console.log(res);
                if(res.result){
                    $scope.subjectExamDetailArr= res.data;
                  
                    $scope.selectExamSubject =   $scope.subjectExamDetailArr[0];
                    $scope.subjectExamDetailArr.forEach(function (subjectExam) {
                     
                        if(subjectExam.subjectName.length<=2){
                            subjectExam.subjectWidth = subjectExam.subjectName.length*(21.5 - subjectExam.subjectName.length*0.5);
                        }else {
                            subjectExam.subjectWidth = subjectExam.subjectName.length*(19.5 - subjectExam.subjectName.length*0.5);
                        }
                    });
                }

            });


        });

        $scope.$on("$ionicView.enter", function (event, data) {


        });

        //选择考试科目
       $scope.chooseSubjectStatistical = function (index) {
           $scope.selectExamSubjectIndex = index;
           $scope.selectExamSubject =   $scope.subjectExamDetailArr[index];

           var currentBtn=document.getElementById('subject'+index);
           var currentX = currentBtn.offsetLeft;
           if(index< $scope.subjectExamDetailArr.length-1){
               var nextIndex = index+1;
               var nextBtn=document.getElementById('subject'+nextIndex);
               var nextX = nextBtn.offsetLeft;

               //下一个按钮的尾部坐标
               var nextButWidth = $scope.subjectExamDetailArr[index+1].subjectWidth+nextX+85;
               //获取屏幕宽度
               $scope.screenWidth=$window.innerWidth;
               if(nextButWidth> $scope.screenWidth){
                   $ionicScrollDelegate.$getByHandle('small').scrollTo(currentX,0);
               }
           }


       };


       //nav back
        $scope.goBack = function () {
            // $ionicHistory.nextViewOptions({
            //     disableBack: true
            // });
            $ionicHistory.goBack();
        };

        $scope.getDeviceType = function () {
            var result = 'android';
            $scope.isIos = ionic.Platform.isIOS() && !isWeixin();
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }

            } else {
             result = 'android';
            }
            return result;
        };

    }]);