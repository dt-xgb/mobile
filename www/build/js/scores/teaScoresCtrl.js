/**
 * Created by hewz on 2018/4/14.
 */
angular.module('app.controllers')
    .controller('teaScoresCtrl', ['$scope', '$ionicHistory', '$state', 'Requester', 'UserPreference', 'toaster', '$ionicLoading', '$timeout', '$ionicPopup', 'BroadCast', function ($scope, $ionicHistory, $state, Requester, UserPreference, toaster, $ionicLoading, $timeout, $ionicPopup,BroadCast) {


        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.isLoadingSucceed = false;
            //数据请求
            console.log('enter');
            $scope.pageNum = 1;//初始化 初始页面
            $scope.isMoreData = true;//默认先显示 上拉加载控件
            //初始化考试列表数组
            $scope.examList = [];
            $scope.getScoresRequest($scope.pageNum);
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            console.log('load');
            $scope.classes = UserPreference.getArray('class');
            $scope.selected = {
                resignType: 0
            };
            $scope.selected.class_id  =   $scope.recordClassId ?$scope.recordClassId: $scope.classes && $scope.classes.length > 0 ?  $scope.classes[0].key : '';
          
        });

        //根据不同班级 筛选不同数据
        $scope.loadData = function () {
            //选择班级后 需要重新设置
            $scope.examList = [];//初始化考试列表数组
            $scope.pageNum = 1;//初始化 初始页面
            $scope.isMoreData = true;//默认先显示 上拉加载控件
            $scope.recordClassId = $scope.selected.class_id;
            $scope.getScoresRequest($scope.pageNum);


        };
        //下拉刷新
        $scope.refreshData = function () {
            $scope.examList = [];
            $scope.isMoreData = true;
            $scope.getScoresRequest(1);
            
        };
        //上拉加载
        $scope.loadMore = function () {
            // // console.log('加载了---');
            $scope.pageNum++;
            $scope.getScoresRequest($scope.pageNum);

        };
        $scope.$on(BroadCast.CONNECT_ERROR,function(){
            $scope.isMoreData = false;
         }) ;

        //老师获取学生的考试成绩
        $scope.getScoresRequest = function (pageNum) {
            var user = UserPreference.getObject('user');
            $scope.stuId = UserPreference.get('DefaultChildID');
            $scope.userRole = String(user.rolename);
            $scope.examArr = [];
            $scope.subjectArr = [];
          //  $ionicLoading.show();
            $scope.isLoadingSucceed = false;
            Requester.selectTestScoresList($scope.userRole, $scope.selected.class_id, pageNum, '').then(function (resp) {
                //console.log('请求。。');
              
                $ionicLoading.hide();
                if (resp.result) {
                    $scope.examArr = resp.data.content;
                    $scope.isLoadingSucceed = true;
                    $scope.examArr.forEach(function (exam) {
                        $scope.examList.push(exam);
                    });
                   
                    if ($scope.pageNum > 1 && $scope.examArr.length < 20) {
                        $scope.isMoreData = false;
                    }

                    $scope.examList.forEach(function (exam) {
                        $scope.subjectArr = exam.subjectScores;
                        $scope.noScoresNum = 0;//初始化定义无分数的科目个数
                        $scope.subjectNameArr = [];//存放科目的名字数组
                        exam.examTotalMark = '';
                        exam.subjectScores.forEach(function (subject) {
                            if (subject.subjectId != 0) {
                                $scope.subjectNameArr.push(subject.subjectName);
                            }
                            if ((subject.score === '--' ||subject.score==null||subject.score=='') && subject.subjectId != 0) {
                                $scope.noScoresNum++;
                            }
                            if (subject.subjectId == 0) {
                                exam.examTotalMark = subject.score;
                            }
                        });
                        exam.subjectStr = $scope.subjectNameArr.join(',');
                        exam.noScoresNum = $scope.noScoresNum;
                          
                    });

                } else {
                   
                    toaster.warning({ title: "温馨提示", body: "系统异常" });
                    $scope.isMoreData = false;
                }


            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
                //$scope.isMoreData = false;
                $ionicLoading.hide();
            });

        };

       


        //查看详情或通知
        $scope.readDetailAndNotice = function (index, examId) {
            if (index == 1) {
                //查看详情
                $state.go('examDetail', { classId: $scope.selected.class_id, examId: examId });

            } else {
                //通知家长
                $ionicLoading.show();
                Requester.sendExamNotice(examId, $scope.selected.class_id).then(function (resp) {
                    $ionicLoading.hide();
                    if (resp.result) {
                        var myPopup = $ionicPopup.show({
                            title: '已成功发送至学生家长',
                            scope: $scope,
                            cssClass: 'wl-alert'
                        });

                        $timeout(function () {
                            myPopup.close(); //close the popup after 3 seconds for some reason
                        }, 1000);

                    }

                }).finally(function () {
                    $ionicLoading.hide();
                });

            }

        };


        //nav back
        $scope.goBack = function () {

            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $ionicHistory.goBack();
        };
    }]);