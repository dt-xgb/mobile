angular.module('app.controllers')
.controller('bannerArticleListCtrl',['$state', '$scope', '$stateParams', 'Requester', 'toaster', 'BroadCast', function($state,$scope,$stateParams,Requester,toaster,BroadCast){

    $scope.$on("$ionicView.beforeEnter", function (event, data){
        $scope.isMoreData = true;
        
    });

    $scope.$on("$ionicView.enter", function (event, data) {
        $scope.isLike = false;//是否喜欢，初始值应从服务器获取
       $scope.url = $stateParams.url;
       $scope.bannerId = $stateParams.Id;
      
       $scope.page = 1;
       $scope.articleList = [];
       $scope.getArticleList();
       
   });

   $scope.$on("$ionicView.loaded", function (event, data){
   
   });

 
    //请求文章列表
    $scope.getArticleList = function () {
        //获取article列表
        Requester.getArticleList($scope.page).then(function (res) {

           if(res.result){
              res.data.content.forEach(function(ele) {
                $scope.articleList.push(ele);
              });
              if(res.data.content.length<10&&$scope.page>1){
                  $scope.isMoreData = false;
                }
           }else {
             toaster.warning({ title: "温馨提示", body: "系统异常" });
             $scope.isMoreData = false;
           }
           
        }, function (err) {
           
        }).finally(function () {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    };
    $scope.$on(BroadCast.CONNECT_ERROR,function(){
        $scope.isMoreData = false;
     }) ;

     $scope.loadData = function(){
        $scope.page = 1;
        $scope.articleList = [];
        $scope.getArticleList();
     };

    //获取更多
    $scope.loadMore = function(){
        $scope.page ++;
        $scope.getArticleList();
       
    };

    //查看阅读详情
    $scope.goToArticleDetail = function (articleCode, articleId) {
     $state.go('bannerArticle',{code:articleCode,Id:articleId});
    };

}]);