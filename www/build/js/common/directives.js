angular.module('app.directives', [])
    .directive('resizeFootBar', ['$ionicScrollDelegate', '$rootScope', function ($ionicScrollDelegate, $rootScope) {
        return {
            replace: false,
            link: function (scope, iElm, iAttrs, controller) {
                scope.$on("taResize", function (e, ta) {
                    if (!ta) return;
                    var scroller = document.body.querySelector('#userMessagesView .scroll-content');
                    var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
                    var taHeight = ta[0].offsetHeight;
                    var newFooterHeight = taHeight + 10;
                    newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;
                    iElm[0].style.height = newFooterHeight + 'px';
                    scroller.style.bottom = newFooterHeight + 'px';
                    viewScroll.scrollBottom();
                });
            }
        };
    }])
    .directive('ngFocus', [function () {
        var FOCUS_CLASS = "ng-focused";
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$focused = false;
                element.bind('focus', function (evt) {
                    element.addClass(FOCUS_CLASS);
                    scope.$apply(function () {
                        ctrl.$focused = true;
                    });
                }).bind('blur', function (evt) {
                    element.removeClass(FOCUS_CLASS);
                    scope.$apply(function () {
                        ctrl.$focused = false;
                    });
                });
            }
        };
    }])
    .directive('compile', ['$compile', function ($compile) {
        return function (scope, element, attrs) {
            scope.$watch(function (scope) {
                    return scope.$eval(attrs.compile);
                },
                function (value) {
                    element.html(value);
                    $compile(element.contents())(scope);
                }
            );
        };
    }])
    .directive('backImg', function () {
        return function (scope, element, attrs) {
            element.css({
                'background-image': 'url(' + attrs.backImg + ')',
                'background-size': 'cover'
            });
        };
    })
    .directive('pwCheck', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function () {
                    scope.$apply(function () {
                        var v = elem.val() === $(firstPassword).val();
                        ctrl.$setValidity('pwmatch', v);
                    });
                });
            }
        };
    }])
    .directive('passwordToggle', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            scope: {},
            link: function (scope, elem, attrs) {
                scope.tgl = function () {
                    elem.attr('type', (elem.attr('type') === 'text' ? 'password' : 'text'));
                };
                var lnk = angular.element('<a ng-click="tgl()"></a>');
                $compile(lnk)(scope);
                elem.wrap('<div class="password-toggle"/>').after(lnk);
            }
        };
    }])
    .directive('phoneCheck', function () {
        return {
            require: 'ngModel',
            scope: false,
            link: function (scope, elm, attrs, ctrl) {
                elm.bind('keyup', function () {
                    scope.getChildren();
                });
            }
        };
    })
    .directive('backBtn', function () {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                nav: '@navTo'
            },
            template: [
                '<button class="button button-icon icon " ng-click="goBack()">' +
                '<img src="img/icon/icon-arrow_left.png" style="max-width: 25px;max-height:25px;padding-top: 7px;" alt=""/>' +
                '</button>'
            ].join(''),
            controller: ['$scope', '$ionicHistory', '$state', '$rootScope', function ($scope, $ionicHistory, $state, $rootScope) {

                $scope.goBack = function () {
                    if ($scope.nav) {
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go($scope.nav);
                    } else {
                        if ($ionicHistory.backView())
                            // $ionicHistory.goBack();
                            $rootScope.$ionicGoBack();

                        else {
                            console.log('back view is null, go mainPage');
                            $state.go('tabsController.mainPage');
                        }
                    }
                };
            }]
        };
    })
    .directive('imageonload', ['$ionicLoading', function ($ionicLoading) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, ctrl) {
                element.bind('load', function () {
                    $ionicLoading.hide();
                });
                element.bind('error', function () {
                    $ionicLoading.hide();
                });
            }
        };
    }])
    .directive('itemEnd', function () {
        //用于将某些地方将元素放在容器的右边
        return {
            restrict: 'A',
            link: function (scope, element, attrs, ctrl) {
                element.css({
                    'float': 'right'
                });

            }
        };
    })
    .directive('emotionPicker', function () {
        return {
            restrict: 'EA',
            // replace:true,
            // transclude:true,
            scope: false,
            template: '<div style="height:195px;border-top: 1px solid #cfcece;width:100%;">' +
                '<div style="padding:10px; height:195px;width:100%;">' +
                '<ion-slides>' +
                '<ion-slide-page ng-repeat="item in items" style="height:195px;width:100%;">' +
                '<span ng-repeat="emotion in item"  ng-click="setValue(emotion)" style="display: block;float: left;width: 12.5%; height: 42px; font-size: 1.2em;line-height: 42px;text-align: center;margin-bottom: 10px;">' +
                '{{emotion}}' +
                '</span>' +
                '</ion-slide-page>' +
                '</ion-slides>' +
                '</div>' +
                '</div>',
            link: function (scope, element, attrs) {
                var EMOJIS =
                    "😀 😃 😄 😁 😆 😅 😂 🤣 😊 😇 🙂 😉 😌 😭 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 🤡 🤠 😏 😒 😞 😔 😟 😕 🙁 ☹️" +
                    " 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 🤤 😓 😪 😴 🙄 🤔 🤥 😬 🤐 🤢 🤧 😷 🤒 🤕 😈 👿 👹 👺" +
                    " 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊ 🤛 🤜 🤞 ✌️ 🤘 👌 👈 👉 👆 👇 ☝️ ✋ 🤚 🖐 🖖" +
                    " 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 🕶 🌂 ☂️";
                var EmojiArr = EMOJIS.split(" ");
                var groupNum = Math.ceil(EmojiArr.length / 24);
                scope.items = [];

                for (var i = 0; i < groupNum; i++) {
                    scope.items.push(EmojiArr.slice(i * 24, (i + 1) * 24));
                }
                //设置值
                // scope.setValue = function (emotion) {
                //     // console.log('点击了:' + emotion);
                //     scope.localValue += emotion;
                // };
            }
        }
        .directive('popupKeyBoardShow', [function ($rootScope, $ionicPlatform, $timeout, $ionicHistory, $cordovaKeyboard) {
                return {
                    link: function (scope, element, attributes) {
                        window.addEventListener('native.keyboardshow', function (e) {
                            angular.element(element).parent().parent().css({
                                'margin-top': '-' + 350 + 'px' //这里80可以根据页面设计，自行修改
                            });
                        });

                        window.addEventListener('native.keyboardhide', function (e) {
                            angular.element(element).parent().parent().css({
                                'margin-top': 0
                            });
                        });
                    }
                };
            }]);
           

    });
