angular.module('app.routes', [])
    .config(['$stateProvider', '$urlRouterProvider', 'Constant', function ($stateProvider, $urlRouterProvider, Constant) {
        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the module can be in.
        // Each state's controller can be found in controllers.js
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'module/';

        $stateProvider
            .state('tabsController.mainPage', {
                url: '/mainPage',
                views: {
                    'mainTab': {
                        templateUrl: UIPath + 'main/mainPage.html',
                        controller: 'mainPageCtrl'
                    }
                }
            })
            .state('tabsController.schoolPage', {
                url: '/schoolPage',
                views: {
                    'schoolTab': {
                        templateUrl: UIPath + 'campus/schoolPage.html',
                        controller: 'schoolPageCtrl'
                    }
                }
            })
            .state('tabsController.communicatePage', {
                url: '/communicatePage',
                views: {
                    'communicateTab': {
                        templateUrl: UIPath + 'im/communicatePage.html',
                        controller: 'communicatePageCtrl'
                    }
                }
            })
            .state('tabsController.settingPage', {
                url: '/settingPage',
                views: {
                    'settingTab': {
                        templateUrl: UIPath + 'setting/settingPage.html',
                        controller: 'settingPageCtrl'
                    }
                }
            })
            .state('tabsController', {
                url: '/tab',
                templateUrl: UIPath + 'tabs/tabsController.html',
                controller: 'tabsCtrl',
                abstract: true
            })
            .state('postDetail', {
                url: '/postDetail',
                templateUrl: UIPath + 'campus/postDetail.html',
                controller: 'postDetailCtrl',
                params: {
                    post: null,
                    comment: null,
                    index: null
                }
            })
            .state('unreadMsg', {
                url: '/unreadMsg',
                templateUrl: UIPath + 'campus/unreadMessages.html',
                controller: 'unreadMsgCtrl',
                params: {
                    list: []
                }
            })
            .state('noticePage', {
                url: '/noticePage',
                templateUrl: UIPath + 'notice/noticePage.html',
                controller: 'noticePageCtrl',
                params: {
                    type: null
                }
            })
            .state('noticeDetail', {
                url: '/noticeDetail',
                templateUrl: UIPath + 'notice/noticeDetail.html',
                controller: 'noticeDetailCtrl',
                params: {
                    notice: {}
                }
            })
            .state('communicateDetail', {
                url: '/communicateDetail',
                templateUrl: UIPath + 'im/communicateDetail.html',
                controller: 'communicateDetailCtrl',
                params: {
                    obj: null
                }
            })
            .state('contactProfile', {
                url: '/contactProfile',
                templateUrl: UIPath + 'im/contactProfile.html',
                controller: 'contactProfileCtrl',
                params: {
                    obj: null,
                    self: false
                }
            })
            .state('introduce', {
                url: '/introduce',
                templateUrl: UIPath + 'setting/introduce.html',
                controller: 'introduceCtrl'
            })

            .state('useProfile', {
                url: '/useProfile',
                templateUrl: UIPath + 'setting/useProfile.html',
                controller: 'userProfileCtrl'
            })
            .state('editProfile', {
                url: '/editProfile',
                templateUrl: UIPath + 'setting/editProfile.html',
                controller: 'editProfileCtrl',
                params: {
                    content: null
                }
            })
            .state('about', {
                url: '/about',
                templateUrl: UIPath + 'setting/about.html',
                controller: 'aboutCtrl'
            })
            .state('suggest', {
                url: '/suggest',
                templateUrl: UIPath + 'setting/suggest.html',
                controller: 'suggestCtrl'
            })
            .state('notification', {
                url: '/notification',
                templateUrl: UIPath + 'setting/notification.html',
                controller: 'notificationCtrl'
            })
            .state('today', {
                url: '/today',
                templateUrl: UIPath + 'class/today.html',
                controller: 'todayCtrl'
            })
            .state('cardFeeStatus', {
                url: '/cardFeeStatus',
                templateUrl: UIPath + 'class/cardFeeStatus.html',
                controller: 'cardFeeStatusCtrl'
            })
            .state('classMotto', {
                url: '/classMotto',
                templateUrl: UIPath + 'class/classMotto.html',
                controller: 'classMottoCtrl'
            })
            .state('myClassSchedule', {
                url: '/myClassSchedule',
                templateUrl: UIPath + 'class/myClassSchedule.html',
                controller: 'myClassScheduleCtrl'
            })
            .state('classSchedule', {
                url: '/classSchedule',
                templateUrl: UIPath + 'class/classSchedule.html',
                controller: 'classScheduleCtrl',
                params: {
                    type: null
                }
            })
            .state('leaveRequest', {
                url: '/leaveRequest',
                templateUrl: UIPath + 'class/leaveRequest.html',
                controller: 'leaveRequestCtrl'
            })
            .state('myDynamicCourse', {
                url: '/myDynamicCourse',
                templateUrl: UIPath + 'electiveCourse/myDynamicCourse.html',
                controller: 'myDynamicCourseCtrl'
            })
            .state('chooseDynamicCourse', {
                url: '/chooseDynamicCourse',
                templateUrl: UIPath + 'electiveCourse/chooseDynamicCourse.html',
                controller: 'chooseDynamicCourseCtrl',
                params: {
                    disableID: null
                }
            })
            .state('checkInNotice', {
                url: '/checkInNotice',
                templateUrl: UIPath + 'attendanceClass/checkInNotice.html',
                controller: 'checkInNoticeCtrl'
            })
            .state('checkInNoticeBaby', {
                url: '/checkInNoticeBaby',
                templateUrl: UIPath + 'attendanceKid/checkInNoticeKid.html',
                controller: 'checkInNoticeBabyCtrl'
            })
            .state('checkInHistory', {
                url: '/checkInHistory',
                templateUrl: UIPath + 'attendanceClass/checkInHistory.html',
                controller: 'checkInHistoryCtrl',
                params: {
                    type: null,
                    course: null,
                    isWalkCourse: null
                }
            })
            .state('checkInMonthlyClassReport', {
                url: '/checkInMonthlyClassReport',
                templateUrl: UIPath + 'attendanceClass/checkInMonthlyClassReport.html',
                controller: 'checkInMonthlyClassReportCtrl'
            })

            .state('attendanceRecords', {
                url: '/attendanceRecords',
                templateUrl: UIPath + 'attendanceClass/attendanceRecords.html',
                controller: 'attendanceRecordsCtrl',
                params: {
                    day: null
                }
            })
            .state('attendanceToday', {
                url: '/attendanceToday',
                templateUrl: UIPath + 'attendanceClass/attendanceToday.html',
                controller: 'attendanceTodayCtrl'
            })
            .state('safeSchool', {
                url: '/safeSchool',
                templateUrl: UIPath + 'safeSchool/safeSchool.html',
                controller: 'safeSchoolCtrl'
            })
            .state('order', {
                url: '/order',
                templateUrl: UIPath + 'order/order.html',
                controller: 'orderCtrl'
            })
            .state('orderPayView', {
                url: '/orderPayView',
                templateUrl: UIPath + 'order/orderPayView.html',
                controller: 'orderPayViewCtrl',
                params: {
                    goods: null
                }
            })
            .state('pdfView', {
                url: '/pdfView',
                templateUrl: UIPath + 'common/pdfView.html',
                controller: 'pdfViewCtrl',
                params: {
                    url: null
                }
            })
            .state('resetPwd', {
                url: '/resetPwd',
                templateUrl: UIPath + 'open/resetPassword.html',
                controller: 'resetPwdCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: UIPath + 'open/login.html',
                controller: 'loginCtrl'
            })
            .state('register', {
                url: '/register',
                templateUrl: UIPath + 'open/register.html',
                controller: 'registerCtrl',
                params: {
                    status: null
                }
            })

            .state('parScores', {
                url: '/parScores',
                templateUrl: UIPath + 'scores/parScores.html',
                controller: 'parScoresCtrl'

            })

            .state('teaScores', {
                url: '/teaScores',
                templateUrl: UIPath + 'scores/teaScores.html',
                controller: 'teaScoresCtrl'

            })
            .state('examDetail', {
                url: '/examDetail',
                templateUrl: UIPath + 'scores/examDetail.html',
                controller: 'examDetailCtrl',
                params: {
                    classId: null,
                    examId: null
                }
            })
            .state('bannerArticle', {
                url: '/bannerArticle',
                templateUrl: UIPath + 'main/bannerArticle.html',
                controller: 'bannerArticleCtrl',
                params: {
                    code: null,
                    Id: null
                }
            })
            .state('bannerArticleList', {
                url: '/bannerArticleList',
                templateUrl: UIPath + 'main/bannerArticleList.html',
                controller: 'bannerArticleListCtrl',
                params: {

                }
            })
            .state('versionIntroduce', {
                url: '/versionIntroduce',
                templateUrl: UIPath + 'setting/versionIntroduce.html',
                controller: 'versionIntroduceCtrl'
            })
            .state('leaveMessage', {
                url: '/leaveMessage',
                templateUrl: UIPath + 'main/leaveMessage.html',
                controller: 'leaveMessageCtrl'
            })
            .state('market', {
                url: '/market',
                templateUrl: UIPath + 'main/market.html',
                controller: 'marketCtrl'
            })
            .state('equipmentRepairList', {
                url: '/equipmentRepairList',
                templateUrl: UIPath + 'equipmentRepair/equipmentRepairList.html',
                controller: 'equipmentRepairListCtrl'
            })
            .state('repairDetail', {
                url: '/repairDetail',
                templateUrl: UIPath + 'equipmentRepair/repairDetail.html',
                controller: 'repairDetailCtrl',
                params: {
                    repairRecordId: null
                }
            })
            .state('closeRepair', {
                url: '/closeRepair',
                templateUrl: UIPath + 'equipmentRepair/closeRepair.html',
                controller: 'closeRepairCtrl',
                params: {
                    repairRecordId: null
                }
            })
            .state('portraitList', {
                url: '/portraitList',
                templateUrl: UIPath + 'setting/portraitList.html',
                controller: 'portraitListCtrl'
            })
            .state('myPortrait', {
                url: '/myPortrait',
                templateUrl: UIPath + 'setting/myPortrait.html',
                controller: 'myPortraitCtrl'
            })
            .state('assetsList', {
                url: '/assetsList',
                templateUrl: UIPath + 'assetsApply/assetsList.html',
                controller: 'assetsListCtrl'

            })
            .state('assetsApplyCommit', {
                url: '/assetsApplyCommit',
                templateUrl: UIPath + 'assetsApply/assetsApplyCommit.html',
                controller: 'assetsApplyCommitCtrl'

            })
            .state('assetsDetail', {
                url: '/assetsDetail',
                templateUrl: UIPath + 'assetsApply/assetsDetail.html',
                controller: 'assetsDetailCtrl',
                params: {
                    assetsId: null
                }
            })
            .state('meetingNoticeList', {
                url: '/meetingNoticeList',
                templateUrl: UIPath + 'meetingNotice/meetingNoticeList.html',
                controller: 'meetingNoticeListCtrl'
            })
            .state('meetingRoomReservationList', {
                url: '/meetingRoomReservationList',
                templateUrl: UIPath + 'meetingNotice/meetingRoomReservationList.html',
                controller: 'meetingRoomReservationListCtrl'
            })
            .state('walkCourseAlert', {
                url: '/walkCourseAlert',
                templateUrl: UIPath + 'class/walkCourseAlert.html',
                controller: 'walkCourseAlertCtrl',
                params: {
                    classId: null,
                    weekNum: null,
                    sectionNum: null
                }
            })
            .state('approvalList', {
                url: '/approvalList',
                templateUrl: UIPath + 'approvalAdmin/approvalList.html',
                controller: 'approvalListCtrl'
            })
            .state('approvalDetail', {
                url: '/approvalDetail',
                templateUrl: UIPath + 'approvalAdmin/approvalDetail.html',
                controller: 'approvalDetailCtrl',
                params: {
                    archiveId: null,
                    selectIndex: null
                }
            })
            .state('addApproval', {
                url: '/addApproval',
                templateUrl: UIPath + 'approvalAdmin/addApproval.html',
                controller: 'addApprovalCtrl',
                params: {
                    templateId: null
                }
            })
            .state('dateArrange', {
                url: '/dateArrange',
                templateUrl: UIPath + 'others/dateArrange/dateArrange.html',
                controller: 'dateArrangeCtrl',
                params: {
                    arrangeDate: null
                }
            })
            .state('classStar', {
                url: '/classStar',
                templateUrl: UIPath + 'others/evaluate/classStar.html',
                controller: 'classStarCtrl'
            })
            .state('editComments', {
                url: '/editComments',
                templateUrl: UIPath + 'others/evaluate/editComments.html',
                controller: 'editCommentsCtrl'
            })
            .state('evaluateStatistical', {
                url: '/evaluateStatistical',
                templateUrl: UIPath + 'others/evaluate/evaluateStatistical.html',
                controller: 'evaluateStatisticalCtrl',
                params: {
                    classId: null,
                    taskId: null,
                    assessTitle: null
                }
            })
            .state('evaluateStudent', {
                url: '/evaluateStudent',
                templateUrl: UIPath + 'others/evaluate/evaluateStudent.html',
                controller: 'evaluateStudentCtrl',

            })
            .state('parEvaluationList', {
                url: '/parEvaluationList',
                templateUrl: UIPath + 'others/evaluate/parEvaluationList.html',
                controller: 'parEvaluationListCtrl'
            })
            .state('evaluateOptions', {
                url: '/evaluateOptions',
                templateUrl: UIPath + 'others/evaluate/evaluateOptions.html',
                controller: 'evaluateOptionsCtrl',
                params: {
                    classId: null,
                    taskId: null,
                    assessTitle: null
                }
            })
            .state('evaluateStudentDetail', {
                url: '/evaluateStudentDetail',
                templateUrl: UIPath + 'others/evaluate/evaluateStudentDetail.html',
                controller: 'evaluateStudentDetailCtrl',
                params: {
                    studentId: null,
                    taskId: null,
                    totalScore: null,
                    userName: null,
                    userLogo: null,
                    userPhonenumber: null,
                    title: null
                }

            })
            .state('classroomMonitList', {
                url: '/classroomMonitList',
                templateUrl: UIPath + 'others/tourClass/classroomMonitList.html',
                controller: 'classroomMonitListCtrl'
            })
            .state('chargeList', {
                url: '/chargeList',
                templateUrl: UIPath + 'others/chargeManage/chargeList.html',
                controller: 'chargeListCtrl'
            })
            .state('chargeDetail', {
                url: '/chargeDetail',
                templateUrl: UIPath + 'others/chargeManage/chargeDetail.html',
                controller: 'chargeDetailCtrl',
                params: {
                    type: null,
                    classId: null,
                    feeId: null,
                    studentId: null,
                    title: null
                }
            })
            .state('adjustClassList', {
                url: '/adjustClassList',
                templateUrl: UIPath + 'others/adjustClass/adjustClassList.html',
                controller: 'adjustClassListCtrl',

            })
            .state('attendanceStatic', {
                url: '/attendanceStatic',
                templateUrl: UIPath + 'attendanceKid/attendanceStatic.html',
                controller: 'attendanceStaticCtrl'
            })
            //入校预约
            .state('parReservationList', {
                url: '/parReservationList',
                templateUrl: UIPath + 'others/enterSchoolReservation/parReservationList.html',
                controller: 'parReservationListCtrl'
            })
            .state('reservationApply', {
                url: '/reservationApply',
                templateUrl: UIPath + 'others/enterSchoolReservation/reservationApply.html',
                controller: 'reservationApplyCtrl'
            })
            .state('teaReservationList', {
                url: '/teaReservationList',
                templateUrl: UIPath + 'others/enterSchoolReservation/teaReservationList.html',
                controller: 'teaReservationListCtrl'
            })
            .state('reservationDetail', {
                url: '/reservationDetail',
                templateUrl: UIPath + 'others/enterSchoolReservation/reservationDetail.html',
                controller: 'reservationDetailCtrl',
                params: {
                    itemId:null
                }
            })
            // //信息分组
            .state('infoGroupList', {
                url: '/infoGroupList',
                templateUrl: UIPath + 'others/infoGroup/infoGroupList.html',
                controller: 'infoGroupListCtrl'
            })
            .state('infoGroupDetail', {
                url: '/infoGroupDetail',
                templateUrl: UIPath + 'others/infoGroup/infoGroupDetail.html',
                controller: 'infoGroupDetailCtrl',
                params: {
                    itemId:null
                }
            })
            .state('addInfoGroup', {
                url: '/addInfoGroup',
                templateUrl: UIPath + 'others/infoGroup/addInfoGroup.html',
                controller: 'addInfoGroupCtrl'
            })
            //安全管理 演练和安全隐患
            .state('exerciseNoticeList',{
                url:'/exerciseNoticeList',
                templateUrl:UIPath + 'others/safetyManage/exerciseNoticeList.html',
                controller :'exerciseNoticeListCtrl'
            })
            .state('exerciseNoticeDetail',{
                url:'/exerciseNoticeDetail',
                templateUrl:UIPath + 'others/safetyManage/exerciseNoticeDetail.html',
                controller :'exerciseNoticeDetailCtrl',
                params: {
                    itemId:null,
                    title:null
                }
            })
            .state('exerciseRecord',{
                url:'/exerciseRecord',
                templateUrl:UIPath + 'others/safetyManage/exerciseRecord.html',
                controller :'exerciseRecordCtrl',
                params:{
                    itemId:null,
                    startTime:null,
                    endTime:null
                    
                }
            })
            .state('hidenTroubleList',{
                url:'/hidenTroubleList',
                templateUrl:UIPath + 'others/safetyManage/hidenTroubleList.html',
                controller :'hidenTroubleListCtrl'
            })
            .state('hidenTroubleDetail',{
                url:'/hidenTroubleDetail',
                templateUrl:UIPath + 'others/safetyManage/hidenTroubleDetail.html',
                controller :'hidenTroubleDetailCtrl',
                params: {
                    itemId:null
                }
            })
            .state('hidenTroubleReport',{
                url:'/hidenTroubleReport',
                templateUrl:UIPath + 'others/safetyManage/hidenTroubleReport.html',
                controller :'hidenTroubleReportCtrl'
            })
            .state('covidDailySummery',{
                url:'/covidDailySummery',
                templateUrl:UIPath + 'others/covid/dailySummery.html',
                controller :'covidDailySummeryCtrl' 
            })
            .state('covidCheckList',{
                url:'/covidCheckList',
                templateUrl:UIPath + 'others/covid/checkList.html',
                controller :'covidCheckListCtrl',
                params: {
                    id:null
                }
            })
            .state('covidCheckDetail',{
                url:'/covidCheckDetail',
                templateUrl:UIPath + 'others/covid/checkDetail.html',
                controller :'covidCheckDetailCtrl',
                params: {
                    id:null,
                    stuName:''
                }
            })
            .state('covidWarningDetail',{
                url:'/covidWarningDetail',
                templateUrl:UIPath + 'others/covid/warningDetail.html',
                controller :'covidWarningDetailCtrl',
                params: {
                    id: {}
                }
            })
            .state('studentDailyDetail',{
                url:'/studentDailyDetail',
                templateUrl:UIPath + 'others/covid/studentDailyDetail.html',
                controller :'studentDailyDetailCtrl' 
            })
            .state('babyVideoList',{
                url:'/babyVideoList',
                templateUrl:UIPath + 'others/babyVideo/babyVideoList.html',
                controller :'babyVideoListCtrl' 
            })
            .state('babyVideoDetail',{
                url:'/babyVideoDetail',
                templateUrl:UIPath + 'others/babyVideo/babyVideoDetail.html',
                controller :'babyVideoDetailCtrl' ,
                params:{
                    url:''
                }
            })
            .state('babyVideoOrder',{
                url:'/babyVideoOrder',
                templateUrl:UIPath + 'others/babyVideo/babyVideoOrder.html',
                controller :'babyVideoOrderCtrl' ,
                params:{
                    itemId:''
                }
            })
            .state('userProtocol',{
                url:'/userProtocol' ,
                templateUrl :UIPath + 'open/userProtocol.html',
                controller:'userProtocolCtrl'
            })

            ;

        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get("$state");
            $state.go('login');
        });

    }])
    .run(['$rootScope', '$state', 'UserPreference', 'Constant', '$location', '$ionicHistory', function ($rootScope, $state, UserPreference, Constant, $location, $ionicHistory) {
        $rootScope.$on('$stateChangeStart', function (event, next, current) {
            if (next.url === $state.current.url && (next.url !== '/postDetail' && next.url !== '/noticeDetail')) {
                if (Constant.debugMode) console.log("same page, go nowhere..");
                event.preventDefault();
            }
            if (!isWeixin() && next.url === '/login' && !UserPreference.getBoolean(Constant.version)&&!Constant.isDingDing) {
                if (Constant.debugMode) console.log("show guide pages..");
                event.preventDefault();
                $state.go('introduce');
            }
        });
        $rootScope.$on('$locationChangeSuccess', function () {
            $rootScope.actualLocation = $location.path();
        });
        $rootScope.$watch(function () {
            return $location.path();
        }, function (newLocation, oldLocation) {
            if ($rootScope.actualLocation === newLocation) {

                //  alert('请使用顶部导航栏返回上一页');
                // $ionicHistory.goBack();

            }
        });
    }]);
