
/**
 * Created by hewz on 2017/5/16.
 */
angular.module('app.requester', ['ionic'])
    .factory('Requester', ['$http', 'Constant', '$q', 'UserPreference', 'toaster', 'SavePhotoTool', '$timeout', '$injector', '$rootScope', 'BroadCast', function ($http, Constant, $q, UserPreference, toaster, SavePhotoTool, $timeout, $injector,$rootScope,BroadCast) {
        var req = {};
        req.advancedHttpRequest = function(url,method,headerStatus,params){
            var defer = $q.defer();
            var cookie = UserPreference.get('IOSCookies');
            var auth = $injector.get('AuthorizeService');
            cordova.plugin.http.setCookie(url, cookie);
            var reqStr = headerStatus === 2 ? 'urlencoded' :(method==='post'||method==='put')? 'utf8':'json';
            cordova.plugin.http.setDataSerializer(reqStr);
            cordova.plugin.http.setRequestTimeout(12.0);
            var param = {};
            if(method==='get'){
                param = getStringJson(params);
            }else{
                 param = params?headerStatus == 2? getStringJson(params):JSON.stringify(params) :{};
            }
            // console.log('param====');
            // console.log(param);
            var header = headerStatus ===2 ? { "Content-Type": 'application/x-www-form-urlencoded' } : { "Content-Type": 'application/json' };
            var options  ;
            if(method==='post'||method==='put'||method==='patch'){
               options  = {
                method:method,
                data:param,
               // params : param,
                headers:header,
                responseType:headerStatus === 2?'urlencoded':'json',
                timeout:12
              };
            }else{
                options  = {
                    method:method,
                     params : param,
                    headers:header,
                    responseType:headerStatus === 2?'urlencoded':'json',
                    timeout:12
                  };
            }
           

            cordova.plugin.http.sendRequest(url,options,function(response){
                console.log('send request response');
                console.log(response);
                try {
                    var data =  response.data;//JSON.parse(response.data);
                    console.log('url :'+url);
                    console.log(data);

                    var loginModel = {
                        //remember: true,
                        username: UserPreference.get('username', '')||UserPreference.get('account', ''),
                        password: UserPreference.get('password', '')
                    };
                    if (data.result == false && (data.code == -1||data.code == -100) && loginModel.password && loginModel.username) {
                        console.log('重新登录');
                        auth.logining = false;
                        auth.login(loginModel, function () { }, function () { });
                    }
                    $timeout(function () {
                        defer.resolve(data);
                    });
                } catch (error) {
                    console.error("JSON parsing error");
                }
            },function(error){
                // console.log('=======send error======='+url);
                // console.log(error);
                var status = JSON.parse(error.status);
                if (status) {

                    SavePhotoTool.interceptors(status);
                }
                $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                $timeout(function () {
                    defer.reject(error);
                });
            });
            return defer.promise;
        };

        function getRequest(url, headerStatus, params) {
            var defer = $q.defer();
            var cookie = UserPreference.get('IOSCookies');
            var auth = $injector.get('AuthorizeService');
            cordova.plugin.http.setCookie(url, cookie);
            var reqStr = headerStatus == 2 ? 'urlencoded' : 'json';
            cordova.plugin.http.setDataSerializer(reqStr);
            cordova.plugin.http.setRequestTimeout(12.0);
            var options = params ?  getStringJson(params) : {};
            // console.log('option===');
            // console.log(options);
            var header = headerStatus == 2 ? { "Content-Type": 'application/x-www-form-urlencoded' } : { "Content-Type": 'application/json' };
            cordova.plugin.http.get(url,
                options, header, function (response) {
                    try {
                        var data = JSON.parse(response.data);
                        var loginModel = {
                            //remember: true,
                            username: UserPreference.get('username', '')?UserPreference.get('username', ''):UserPreference.get('account', ''),
                            password: UserPreference.get('password', '')
                        };
                        
                        if (data.result == false && (data.code == -1||data.code == -100) && loginModel.password && loginModel.username) {
                            console.log('重新登录1');
                            auth.logining = false;
                            auth.login(loginModel, function () { }, function () { });
                        }
                        $timeout(function () {
                            defer.resolve(data);
                        });
                    } catch (error) {
                        console.error("JSON parsing error");
                    }
                }, function (error) {
                    // console.log('=======get error======='+url);
                    // console.log(error);
                    var status = JSON.parse(error.status);
                    if (status) {

                        SavePhotoTool.interceptors(status);
                    }
                    $timeout(function () {
                        defer.reject(error);
                    });
                });
            return defer.promise;
        }

        function postRequest(url, headerStatus, params) {
            var defer = $q.defer();
            var cookie = UserPreference.get('IOSCookies');
            var auth = $injector.get('AuthorizeService');
            cordova.plugin.http.setCookie(url, cookie);
            cordova.plugin.http.setRequestTimeout(12.0);
            var reqStr = headerStatus == 2 ? 'urlencoded' : 'utf8';
            cordova.plugin.http.setDataSerializer(reqStr);
            var options = params?headerStatus == 2? getStringJson(params):JSON.stringify(params) :{};
           
            // console.log('option===');
            // console.log(options);
            var header = headerStatus == 2 ? { "Content-Type": 'application/x-www-form-urlencoded' } : { "Content-Type": 'application/json' };
            cordova.plugin.http.post(url,
                options, header, function (response) {

                    try {
                        var data = JSON.parse(response.data);
                        var loginModel = {
                            //remember: true,
                            username: UserPreference.get('username', '')||UserPreference.get('account', ''),
                            password: UserPreference.get('password', '')
                        };

                        if (data.result == false && (data.code == -1||data.code==-100 )&& loginModel.password && loginModel.username) {
                            // console.log('重新登录');
                            auth.logining = false;
                            auth.login(loginModel, function () { }, function () { });
                        }
                        $timeout(function () {
                            defer.resolve(data);
                        });
                    } catch (error) {
                        console.error("JSON parsing error");
                    }
                }, function (error) {
                    // console.log('=======post error=======');
                    // console.log(error);
                    var status = JSON.parse(error.status);
                    if (status) {
                        SavePhotoTool.interceptors(status);
                    }
                    $timeout(function () {
                        defer.reject(error);
                    });
                });
            return defer.promise;
        }

        /**
         * 文件形式上传图片 单个
         */
        req.uploadPictureByFileType = function (file,index) {
            var fd = new FormData();
            if(!index) index=0;
            fd.append('file', file);
            fd.append('filename', 'image'+index*3+'.jpeg');
            
            var url = Constant.ServerUrl.substr(0, Constant.ServerUrl.length - 4) + '/admin/fw/common/upload';
            if(iOSDevice()){
             var defer = $q.defer();
            cordova.plugin.http.setDataSerializer('multipart');
            cordova.plugin.http.setRequestTimeout(12.0);
           
            var header =  { "Content-Type": 'multipart/form-data' } ;
            var options  = {
                method:'post',
                data:fd,
                headers:header,
                responseType:'json',
                timeout:12
              }; 
            
            cordova.plugin.http.sendRequest(url,options,function(response){
                try {
                    var data =  response.data;//JSON.parse(response.data);   
                    $timeout(function () {
                        defer.resolve(data);
                    });
                } catch (error) {
                    console.error("JSON parsing error");
                }
            },function(error){
                
                var status = JSON.parse(error.status);
                if (status) {

                    SavePhotoTool.interceptors(status);
                }
                $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                $timeout(function () {
                    defer.reject(error);
                });
            });
            return defer.promise;
              

            }else{
                return $http({
                    method: 'POST',
                    url: url,
                    data: fd,
                    headers: {
                        'Content-Type': undefined, //' multipart/form-data',
                        // 'Content-Disposition' :"inline;filename=hello.jpg "
                    },
                    transformRequest: angular.identity
                }).then(function (response) {
                    //上传成功的操作
                    return response.data;
                }, function (error) {
                    console.log('failure');
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 多图上传
         */
        req.uploadMorePictureFileType = function (files) {
            var fd = new FormData();
            files.forEach(function (file) {
                fd.append('files', file);
               // fd.append('filename', 'image.jpeg');
            });
            var url = Constant.ServerUrl.substr(0, Constant.ServerUrl.length - 4) + '/admin/fw/common/upload/batch';
            if(iOSDevice()){
                var defer = $q.defer();
                cordova.plugin.http.setDataSerializer('multipart');
                cordova.plugin.http.setRequestTimeout(12.0);
                var header =  { "Content-Type": 'multipart/form-data' } ;
                var options  = {
                    method:'post',
                    data:fd,
                    headers:header,
                    responseType:'json',
                    timeout:12
                  }; 
                
                cordova.plugin.http.sendRequest(url,options,function(response){
                    try {
                        var data =  response.data;//JSON.parse(response.data);
                        console.log('mulp data');
                       console.log(data);
                        $timeout(function () {
                            defer.resolve(data);
                        });
                    } catch (error) {
                        console.error("JSON parsing error");
                    }
                },function(error){
                    
                    var status = JSON.parse(error.status);
                    if (status) {
    
                        SavePhotoTool.interceptors(status);
                    }
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    $timeout(function () {
                        defer.reject(error);
                    });
                });
                return defer.promise;
                  
            }else{
                return $http({
                    method: 'POST',
                    url: url, //'https://test17.xgenban.com/sctserver/admin/fw/common/upload/batch',
                    data: fd,
                    headers: {
                        'Content-Type': undefined, //' multipart/form-data',
                        // 'Content-Disposition' :"inline;filename=hello.jpg "
                    },
                    transformRequest: angular.identity
                }).then(function (response) {
                    //上传成功的操作
                    // console.log("uplaod success");
                    return response.data;
                }, function (error) {
                    console.log('failure');
                    return $q.reject(error);
                });
            }
            
        };


        /***
         * 获取自己（已选）走班课
         * @param role
         * @returns {*}
         */
        req.getMyDynamicCourse = function (role) {
            if (iOSDevice()) {
                switch (String(role)) {
                    case Constant.USER_ROLES.STUDENT:
                        return getRequest(Constant.ServerUrl + "/walkcourse/getSelfClCource",1);
                    case Constant.USER_ROLES.PARENT:
                        return getRequest(Constant.ServerUrl + "/walkcourse/getChildClCource",1,{
                            stuid: UserPreference.get('DefaultChildID')
                        });            
                    case Constant.USER_ROLES.TEACHER:
                       return getRequest(Constant.ServerUrl + "/walkcourse/getTeacherClCource",1);
                }

            } else {
                switch (String(role)) {
                    case Constant.USER_ROLES.STUDENT:
                        return $http.get(Constant.ServerUrl + "/walkcourse/getSelfClCource")
                            .then(function (response) {
                                return response.data;
                            }, function (error) {
                                return $q.reject(error);
                            });
                    case Constant.USER_ROLES.PARENT:
                        return $http.get(Constant.ServerUrl + "/walkcourse/getChildClCource", {
                            params: {
                                stuid: UserPreference.get('DefaultChildID')
                            }
                        })
                            .then(function (response) {
                                return response.data;
                            }, function (error) {
                                return $q.reject(error);
                            });
                    case Constant.USER_ROLES.TEACHER:
    
                        return $http.get(Constant.ServerUrl + "/walkcourse/getTeacherClCource")
                            .then(function (response) {
                                return response.data;
                            }, function (error) {
                                return $q.reject(error);
                            });
                }

            }
        };

        /**
         * 获取可选走班课
         * @param sid
         * @returns {*}
         */
        req.getSelectableDynamicCourse = function (sid) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/walkcourse/getWalkByStudent",1,{
                stuid: sid
            });
            }else{
                return $http.get(Constant.ServerUrl + "/walkcourse/getWalkByStudent", {
                    params: {
                        stuid: sid
                    }
                }).then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
            
        };

        /**
         * 获取可选走班课子课程
         * @param wkid
         * @returns {*}
         */
        req.getSubDynamicCourse = function (wkid) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/walkcourse/getWalkSubListByWalkCourseID",1,{
                wkid: wkid
            });
            }else{
                return $http.get(Constant.ServerUrl + "/walkcourse/getWalkSubListByWalkCourseID", {
                    params: {
                        wkid: wkid
                    }
                }).then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
            
        };

        /**
         * 设置推送消息已读
         * @param noticeId
         * @returns {*}
         */
        req.setNoticeRead = function (noticeId) {
            if(iOSDevice()){
             return postRequest(Constant.ServerUrl + "/notice/readSign",2,{
                noticeId: noticeId
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/notice/readSign",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        noticeId: noticeId
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 设置到校通知已读
         * @param arr
         * @returns {*}
         */
        req.setCheckInNoticeRead = function (arr) {
            if(iOSDevice()){
                return postRequest(Constant.ServerUrl + "/attend/readup",1,arr);

            }else{
                return $http.post(Constant.ServerUrl + "/attend/readup", arr).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 选课
         * @param stuid
         * @param walksubid
         * @returns {*}
         */
        req.courseEnroll = function (stuid, walksubid) {
            if(iOSDevice()){
                return postRequest(Constant.ServerUrl + "/walkcourse/chooseWalkCource",1,{
                    stuid: stuid,
                    walksubid: walksubid
                });

            }else{
                return $http.post(Constant.ServerUrl + "/walkcourse/chooseWalkCource", {
                    stuid: stuid,
                    walksubid: walksubid
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 获取所有消息未读数
         * @param stuId
         * @returns {*}
         */
        req.getUnreadTotal = function (stuId) {
            if (!stuId)
                stuId = UserPreference.getObject('user').id;

                if(iOSDevice()){
                    return getRequest(Constant.ServerUrl + "/notice/unreadTotal",1,{
                        stuId: stuId
                    });
                }else{
                    return $http.get(Constant.ServerUrl + "/notice/unreadTotal", {
                        params: {
                            stuId: stuId
                        }
                    }) .then(function (response) {
                            return response.data;
                        }, function (error) {
                            return $q.reject(error);
                        });
                }
           
        };

        /**
         * 获取到校通知 (幼儿园)
         * @param stuId
         * @returns {*}
         */
        req.getChildCheckInRecords = function (stuId) {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/attend/getChildAttendanceInfo",1,{
                stuid: stuId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/getChildAttendanceInfo", {
                    params: {
                        stuid: stuId
                    }
                }).then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
           
        };

        /**
         * 获取到校通知（班级考勤） old
         * @param stuId
         * @returns {*}
         */
        req.getChildCheckInRecords2 = function (stuId) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/attend/getChildAdAttendanceInfo",1,{
                    studentId: stuId
                });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/getChildAdAttendanceInfo", {
                    params: {
                        studentId: stuId
                    }
                }) .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
            
        };

        /**
         * 获取到校通知（班级考勤） new
         */
        req.getChildCheckInRecords3 = function (stuId) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/attend/child/in-out",1,{
                studentId: stuId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/child/in-out", {
                    params: {
                        studentId: stuId
                    }
                }) .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
            
        };

        /**
         * 获取班级考勤月报
         * @param month
         * @param classid
         * @returns {*}
         */
        req.getClassMonthlyCheckInInfo = function (month, classid) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/attend/getClassAttendanceDetailMonthByCalendar",1,{
                classId: classid,
                month: month
            });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/getClassAttendanceDetailMonthByCalendar", {
                    params: {
                        classId: classid,
                        month: month
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                }); 
            }
            
        };

        /**
         * 获取班级考勤信息(幼儿)
         * @param month
         * @param classid
         * @returns {*}
         */
        req.getKidClassCheckInInfo = function (month, classid) {
            if(iOSDevice()){
           return getRequest(Constant.ServerUrl + "/attend/getClassAttendanceInfoMonthByCalendar",1,{
            classId: classid,
            month: month
        });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/getClassAttendanceInfoMonthByCalendar", {
                    params: {
                        classId: classid,
                        month: month
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };


        /**
         * 获取班级考勤信息（班级考勤）
         * @param month
         * @param classid
         * @returns {*}
         */
        req.getClassCheckInInfo = function (month, classid) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/attend/getClassAdAttendInMonth",1,{
                classId: classid,
                month: month,
                page: 1,
                rows: 31
            });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/getClassAdAttendInMonth", {
                    params: {
                        classId: classid,
                        month: month,
                        page: 1,
                        rows: 31
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };


        /**
         * 班级考勤时段列表
         * @param classid
         * @returns {*}
         */
        req.getClassCheckInTimeList = function (classid) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/attend/getAdAttendTimeList",1,{
                classId: classid
            });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/getAdAttendTimeList", {
                    params: {
                        classId: classid
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取考勤学生进出校记录
         * @param day
         * @param stuId
         * @param classId
         * @param type
         * @returns {*}
         */
        req.getStuInOutRecords = function (day, stuId, classId, type, attendTimeId) {
            var q;
            if (type === 'kid') {
                //幼儿园考勤
                if(iOSDevice()){
                    q = getRequest(Constant.ServerUrl + "/kind/attend/attendRecord/" + stuId,1,{
                    date: day
                });
                }else{
                    q = $http.get(Constant.ServerUrl + "/kind/attend/attendRecord/" + stuId, {
                        params: {
                            date: day
                        }
                    });
                }
                
            } else if (type === 'normal') {
                if(iOSDevice()){
                 q = getRequest(Constant.ServerUrl + "/attend/student-record/" + stuId,1,{
                    date: day,
                    attendTimeId: attendTimeId
                });
                }else{
                    q = $http.get(Constant.ServerUrl + "/attend/student-record/" + stuId, {
                        params: {
                            date: day,
                            attendTimeId: attendTimeId
                        }
                    });
                }
               
            } else {
                //平安校园
                if(iOSDevice()){
                   q = getRequest(Constant.ServerUrl + "/schlAttendanceStatistics/getStudentAttendRecord/" + stuId,1,{
                    date: day,
                    classId: classId
                });
                }else{
                    q = $http.get(Constant.ServerUrl + "/schlAttendanceStatistics/getStudentAttendRecord/" + stuId, {
                        params: {
                            date: day,
                            classId: classId
                        }
                    });
                }
               
            }
            if(iOSDevice()){
                return q;
            }else{
                return q.then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
          
        };

        /**
         * 获取班级考勤已到（幼儿园）
         * @param day
         * @param classid
         * @returns {*}
         */
        req.getClassCheckedIn = function (day, classid) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/kind/attend/getattendinfo",1,{
                classid: classid,
                day: day
            });
            }else{
                return $http.get(Constant.ServerUrl + "/kind/attend/getattendinfo", {
                    params: {
                        classid: classid,
                        day: day
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取班级考勤明细
         * @param day
         * @param classid
         * @param attendTimeId
         * @returns {*}
         */
        req.getNormalClassCheckedIn = function (day, classid, attendTimeId) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/attend/getClassAdAttendDetail",1,{
                    classId: classid,
                    date: day,
                    attendTimeId: attendTimeId
                });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/getClassAdAttendDetail", {
                    params: {
                        classId: classid,
                        date: day,
                        attendTimeId: attendTimeId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取班级考勤缺勤信息（幼儿园）
         * @param day
         * @param classid
         * @returns {*}
         */
        req.getClassUncheckedIn = function (day, classid) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/kind/attend/getunattendinfo",1,{
                classid: classid,
                day: day
            });
            }else{
                return $http.get(Constant.ServerUrl + "/kind/attend/getunattendinfo", {
                    params: {
                        classid: classid,
                        day: day
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取班级考勤请假信息（幼儿园）
         * @param day
         * @param classid
         * @returns {*}
         */
        req.getClassLeave = function (day, classid) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/kind/attend/getaskleaveinfo",1, {
                    classId: classid,
                    day: day
                });
            }else{
                return $http.get(Constant.ServerUrl + "/kind/attend/getaskleaveinfo", {
                    params: {
                        classId: classid,
                        day: day
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取走班课考勤信息
         * @param month
         * @param walksubid
         * @returns {*}
         */
        req.getCourseCheckInInfo = function (month, walksubid) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/attend/getWalkAttendanceInfoMonth",1,{
                    walksubid: walksubid,
                    month: month
                });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/getWalkAttendanceInfoMonth", {
                    params: {
                        walksubid: walksubid,
                        month: month
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取走班课考勤已到信息
         * @param day
         * @param walksubid
         * @returns {*}
         */
        req.getCourseCheckedIn = function (day, walksubid) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/attend/getattendinfo",1,{
                walksubid: walksubid,
                day: day
            });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/getattendinfo", {
                    params: {
                        walksubid: walksubid,
                        day: day
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取走班课考勤未到信息
         * @param day
         * @param walksubid
         * @returns {*}
         */
        req.getCourseUncheckedIn = function (day, walksubid) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/attend/getunattendinfo",1, {
                walksubid: walksubid,
                day: day
            });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/getunattendinfo", {
                    params: {
                        walksubid: walksubid,
                        day: day
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取请假类型
         * @param role
         * @returns {*}
         */
        req.getLeaveTypeList = function (role) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/askLeave/getAskTypeDefine",1);
            }else{
                return $http.get(Constant.ServerUrl + "/askLeave/getAskTypeDefine")
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取请假列表
         * @param role
         * @param page
         * @returns {*}
         */
        req.getLeaveList = function (role, page) {
            if(iOSDevice()){
                switch (String(role)) {
                    case Constant.USER_ROLES.PARENT:
                        return  getRequest(Constant.ServerUrl +"/askLeave/getFamilyByStu",1,{
                            studentId: UserPreference.get('DefaultChildID'),
                            page: page,
                            rows: Constant.reqLimit
                        });
                    case Constant.USER_ROLES.TEACHER:
                        return  getRequest(Constant.ServerUrl + "/askLeave/getTeaAskLeave",1,{
                            teacherId: UserPreference.getObject('user').id,
                            page: page,
                            rows: Constant.reqLimit
                        });
                }
    
            }else{
                switch (String(role)) {
                    case Constant.USER_ROLES.PARENT:
                        return $http.get(Constant.ServerUrl + "/askLeave/getFamilyByStu", {
                            params: {
                                studentId: UserPreference.get('DefaultChildID'),
                                page: page,
                                rows: Constant.reqLimit
                            }
                        })
                            .then(function (response) {
                                return response.data;
                            }, function (error) {
                                return $q.reject(error);
                            });
                    case Constant.USER_ROLES.TEACHER:
                        return $http.get(Constant.ServerUrl + "/askLeave/getTeaAskLeave", {
                            params: {
                                teacherId: UserPreference.getObject('user').id,
                                page: page,
                                rows: Constant.reqLimit
                            }
                        })
                            .then(function (response) {
                                return response.data;
                            }, function (error) {
                                return $q.reject(error);
                            });
                }
    
            }
           
        };

        /**
         * 请假
         * @param reqData
         * @returns {*}
         */
        req.leaveRequest = function (reqData) {
            if(iOSDevice()){
                return postRequest(Constant.ServerUrl + "/askLeave/add-leave",1,reqData);
            }else{
                return $http.post(Constant.ServerUrl + "/askLeave/add-leave", reqData)
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 老师更新请假状态
         * @param id
         * @returns {*}
         */
        req.updateLeaveRequest = function (id) {
            if(iOSDevice()){
           return getRequest(Constant.ServerUrl + "/askLeave/update-leaveStatus",1,{
            id: id
        });
            }else{
                return $http.get(Constant.ServerUrl + "/askLeave/update-leaveStatus", {
                    params: {
                        id: id
                    }
                }).then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
            
        };

        /**
         * 老师获取今日考勤信息
         * @param cid
         * @returns {*}
         */
        req.getTodayAttendance = function (cid) {
            if(iOSDevice()){
         return getRequest(Constant.ServerUrl + "/pattend/today/detail",1,{
            classId: cid,
            page: 1,
            rows: 500
        });
            }else{
                return $http.get(Constant.ServerUrl + "/pattend/today/detail", {
                    params: {
                        classId: cid,
                        page: 1,
                        rows: 500
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 老师获取学生今日请假讯息
         * @param sid
         * @returns {*}
         */
        req.getStudentTodayLeaveInfo = function (sid) {
            if(iOSDevice()){
           return getRequest(Constant.ServerUrl + "/askLeave/getBaseByDate",1,{
            studentId: sid
        });
            }else{
                return $http.get(Constant.ServerUrl + "/askLeave/getBaseByDate", {
                    params: {
                        studentId: sid
                    }
                }).then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
            
        };

        /**
         * 老师撤销学生请假记录
         * @param id
         * @returns {*}
         */
        req.removeLeaveReq = function (id) {
            if(iOSDevice()){
             return  postRequest(Constant.ServerUrl + "/askLeave/cancelOne",2,{
                id: id
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/askLeave/cancelOne",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        id: id
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 老师获取月度考勤信息
         * @param cid
         * @param month
         * @returns {*}
         */
        req.getMonthlyAttendance = function (cid, month) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/schlAttendanceStatistics/getMonthStatisticsofClass",1,{
                classId: cid,
                month: month,
                page: 1,
                rows: 31
            });
            }else{
                return $http.get(Constant.ServerUrl + "/schlAttendanceStatistics/getMonthStatisticsofClass", {
                    params: {
                        classId: cid,
                        month: month,
                        page: 1,
                        rows: 31
                    }
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
           
        };

        /**
         * 老师获取指定日考勤信息
         * @param cid
         * @param day
         * @returns {*}
         */
        req.getDailyAttendance = function (cid, day) {
            if(iOSDevice()){
           return getRequest(Constant.ServerUrl + "/schlAttendanceStatistics/getStatisticsByAttendStatus",1,{
            classId: cid,
            date: day
        });
            }else{
                return $http.get(Constant.ServerUrl + "/schlAttendanceStatistics/getStatisticsByAttendStatus", {
                    params: {
                        classId: cid,
                        date: day
                    }
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
          
        };

        /**
         * 家长获取到离校通知
         * @returns {*}
         */
        req.getChildArriveInfo = function () {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/safeschool/attend/clocklist",1,{
                stuid: UserPreference.get('DefaultChildID')
            });
            }else{
                return $http.get(Constant.ServerUrl + "/safeschool/attend/clocklist", {
                    params: {
                        stuid: UserPreference.get('DefaultChildID')
                    }
                }).then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
           
        };


        /**
         * 家长获取到安全预警
         * @returns {*}
         */
        req.getChildSaftyWarnings = function () {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/safeschool/attend/warnlist",1,{
                stuid: UserPreference.get('DefaultChildID')
            });
            }else{
                return $http.get(Constant.ServerUrl + "/safeschool/attend/warnlist", {
                    params: {
                        stuid: UserPreference.get('DefaultChildID')
                    }
                }).then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
            
        };

        /**
         * 家长获取安全预警设置(deprecated)
         * @param sid
         * @returns {*}
         */
        req.getChildSafetyWarnings = function (sid) {
            if(iOSDevice()){
         return getRequest(Constant.ServerUrl + "/safeschool/getparam",1,{
            parameterName: sid,
            userid: UserPreference.get('DefaultChildID')
        });
            }else{
                return $http.get(Constant.ServerUrl + "/safeschool/getparam", {
                    params: {
                        parameterName: sid,
                        userid: UserPreference.get('DefaultChildID')
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
          
        };

        /**
         * 家长设置安全预警参数(deprecated)
         * @param parameterName
         * @param parameterValue
         * @returns {*}
         */
        req.setChildSafetySettings = function (parameterName, parameterValue) {
            if(iOSDevice()){
                return postRequest(Constant.ServerUrl + "/safeschool/setparam",1,{
                    p_userid: UserPreference.get('DefaultChildID'),
                    parameterName: parameterName,
                    parameterValue: parameterValue
                });
            }else{
                return $http.post(Constant.ServerUrl + "/safeschool/setparam", {
                    p_userid: UserPreference.get('DefaultChildID'),
                    parameterName: parameterName,
                    parameterValue: parameterValue
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 班主任获取安全预警设置
         * @param sid
         * @param classId
         * @returns {*}
         */
        req.getSafetyWarnings = function (sid, classId) {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/safeschool/getclassparam",1,{
                parameterName: sid,
                classid: classId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/safeschool/getclassparam", {
                    params: {
                        parameterName: sid,
                        classid: classId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
          
        };

        /**
         * 班主任设置安全预警参数
         * @param classId
         * @param parameterName
         * @param parameterValue
         * @returns {*}
         */
        req.setSafetySettings = function (classId, parameterName, parameterValue) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/safeschool/setclassparam",1,{
                p_class_id: classId,
                parameterName: parameterName,
                parameterValue: parameterValue
            });
            }else{
                return $http.post(Constant.ServerUrl + "/safeschool/setclassparam", {
                    p_class_id: classId,
                    parameterName: parameterName,
                    parameterValue: parameterValue
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 设置安全预警已读
         * @param reqData
         * @returns {*}
         */
        req.setChildSafetyWarningsRead = function (reqData) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/safeschool/warn/readup",1,reqData);
            }else{
                return $http.post(Constant.ServerUrl + "/safeschool/warn/readup", reqData)
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 设置到校通知（平安校园下）已读
         * @param reqData
         * @returns {*}
         */
        req.setChildArriveInfoRead = function (reqData) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/safeschool/attend/readup",1,reqData);
            }else{
                return $http.post(Constant.ServerUrl + "/safeschool/attend/readup", reqData)
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
        
        };

        /**
         * 家长绑定学生卡号（幼儿）
         * @param cardNum
         * @returns {*}
         */
        req.bindStudentCard = function (cardNum) {
            if(iOSDevice()){
                 return  postRequest(Constant.ServerUrl + "/attend/bindcard",1,{
                    stuid: UserPreference.get('DefaultChildID'),
                    stucardid: cardNum
                });
            }else{
                return $http.post(Constant.ServerUrl + "/attend/bindcard", {
                    stuid: UserPreference.get('DefaultChildID'),
                    stucardid: cardNum
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
           
        };

        /**
         * 家长解绑学生卡号（幼儿）
         * @param cardNum
         * @param schoolId
         * @returns {*}
         */
        req.unbindStudentCard = function (cardNum, schoolId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/attend/delete/bindcard",1,{
                stuid: UserPreference.get('DefaultChildID'),
                stucardid: cardNum,
                schoolId: schoolId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/delete/bindcard", {
                    params: {
                        stuid: UserPreference.get('DefaultChildID'),
                        stucardid: cardNum,
                        schoolId: schoolId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 查询学期信息
         * @param classId
         * @returns {*}
         */
        req.getSemesterInfo = function (classId) {
            var schoolId;
            var user = UserPreference.getObject('user');
            if (user.rolename === 4) {
                schoolId = UserPreference.get('DefaultSchoolID') ? UserPreference.get('DefaultSchoolID') : user.schauthmap[0].schoolid;
            } else {
                schoolId = UserPreference.get('DefaultSchoolID') ? UserPreference.get('DefaultSchoolID') : user.school.id;
            }
            if(iOSDevice()){
                 return getRequest(Constant.ServerUrl + "/attend/calendar",1,{
                    classId: classId,
                    schoolId: schoolId
                });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/calendar", {
                    params: {
                        classId: classId,
                        schoolId: schoolId
                    }
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
           
        };

        /**
         * 创建订单
         * @param attach
         * @param orderItems
         * @param receiveUserId
         * @param schoolId
         * @param model
         * @returns {*}
         */
        req.newOrder = function (attach, orderItems, receiveUserId, schoolId, model, categoryId) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/pay/order/add",1,{
                attach: attach,
                schoolId: schoolId,
                orderItems: orderItems,
                receiveUserId: receiveUserId,
                model: model,
                categoryId: categoryId
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/pay/order/add",
                    data: {
                        attach: attach,
                        schoolId: schoolId,
                        orderItems: orderItems,
                        receiveUserId: receiveUserId,
                        model: model,
                        categoryId: categoryId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 预下单
         * @param channel
         * @param orderSerial
         * @returns {*}
         */
        req.newOrderPayment = function (channel, orderSerial) {
            if(iOSDevice()){
            return postRequest(Constant.ServerUrl + "/pay/createPay",2,{
                channel: channel,
                orderSerial: orderSerial
            }); 
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/pay/createPay",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        channel: channel,
                        orderSerial: orderSerial
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                }); 
            }
          
        };

        /**
         * 查询订单信息
         * @param orderSerial
         * @returns {*}
         */
        req.getOrderInfo = function (orderSerial) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/pay/order/queryOrderBySerial",1,{
                orderSerial: orderSerial
            });
            }else{
                return $http.get(Constant.ServerUrl + "/pay/order/queryOrderBySerial", {
                    params: {
                        orderSerial: orderSerial
                    }
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
            
        };

        /**
         * 查询所有订单
         * @param page
         * @returns {*}
         */
        req.getAllOrders = function (page) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/pay/order/queryOrdersByPayUser",1,{
                page: page,
                rows: Constant.reqLimit
            });
            }else{
                return $http.get(Constant.ServerUrl + "/pay/order/queryOrdersByPayUser", {
                    params: {
                        page: page,
                        rows: Constant.reqLimit
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 查询商品信息
         * @param goodId
         * @param categoryId
         * @param schoolId
         * @returns {*}
         */
        req.getGoodInfo = function (goodId, categoryId, schoolId) {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/pay/good/get",1,{
                id: goodId,
                categoryId: categoryId,
                schoolId: schoolId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/pay/good/get", {
                    params: {
                        id: goodId,
                        categoryId: categoryId,
                        schoolId: schoolId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 查询是否购买
         * @param categoryId
         * @param receiveUserId
         * @returns {*}
         */
        req.isUserPurchased = function (categoryId, receiveUserId) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/pay/order/hasCategoryGoodByReceiver",1,{
                categoryId: categoryId,
                receiveUserId: receiveUserId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/pay/order/hasCategoryGoodByReceiver", {
                    params: {
                        categoryId: categoryId,
                        receiveUserId: receiveUserId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 查询是否支付成功
         * @param orderID
         * @returns {*}
         */
        req.isPaymentSuccess = function (orderID) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/pay/queryTrade",1,{
                    outTradeNo: orderID
                });
            }else{
                return $http.get(Constant.ServerUrl + "/pay/queryTrade", {
                    params: {
                        outTradeNo: orderID
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 查询学校卡押金价格
         * @param schoolId
         * @returns {*}
         */
        req.getSchoolCardPrice = function (schoolId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/pay/order/querySchoolCardPrice",1,{
                schoolId: schoolId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/pay/order/querySchoolCardPrice", {
                    params: {
                        schoolId: schoolId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 班主任查询班级内押金缴费情况
         * @param page
         * @param classId
         * @returns {*}
         * @constructor
         */
        req.getClassCardPayStatus = function (page, classId) {
            if(iOSDevice()){
                 return getRequest(Constant.ServerUrl + "/pay/order/queryCardForegiftOrdersByClassId",1,{
                    classId: classId
                });
            }else{
                return $http.get(Constant.ServerUrl + "/pay/order/queryCardForegiftOrdersByClassId", {
                    params: {
                        classId: classId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };


        /**
         * 老师查询所教课程
         * @returns {*}
         */
        req.getTeachingSchedule = function () {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/course/byTeacher",1);
            }else{
                return $http.get(Constant.ServerUrl + "/course/byTeacher")
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
          
        };

        /**
         * 老师查询平安校园推送列表
         * @returns {*}
         */
        req.getSafeSchoolPushs = function (tid, page) {
            if(iOSDevice()){
           return getRequest(Constant.ServerUrl + "/attend/push/queryPushList",1,{
            teacherId: tid,
            page: page,
            rows: Constant.reqLimit
        });
            }else{
                return $http.get(Constant.ServerUrl + "/attend/push/queryPushList", {
                    params: {
                        teacherId: tid,
                        page: page,
                        rows: Constant.reqLimit
                    }
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
           
        };

        /**
         * 获取首页推荐文章
         * @returns {*}
         */
        req.getTopSlides = function () {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/article/top",1, {
                userId: UserPreference.getObject('user').id
            });
            }else{
                return $http.get(Constant.ServerUrl + "/article/top", {
                    params: {
                        userId: UserPreference.getObject('user').id
                    }
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
            
        };


        /**
         * 今日考勤老师补签
         * @param data
         * @returns {*}
         */
        req.reCheckIn = function (data) {
            if(iOSDevice()){
             return postRequest(Constant.ServerUrl + "/safeschool/addrecord",1,data);
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/safeschool/addrecord",
                    data: data
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };


        /**
         * 批量修改考勤信息
         * @param attendStatusStr
         * @param date
         * @param studentIds
         * @returns {*}
         */
        req.modifyCheckIn = function (attendStatusStr, date, studentIds) {
            if(iOSDevice()){
                var URL = Constant.ServerUrl + "/schlAttendanceStatistics/updateBatch" + '?'+CustomParam({
                    attendStatusStr: attendStatusStr,
                    date: date,
                    studentIds: studentIds
                });
              return postRequest(URL,2,{
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/schlAttendanceStatistics/updateBatch",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        attendStatusStr: attendStatusStr,
                        date: date,
                        studentIds: studentIds
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 家长修改学生信息
         * @param stuId
         * @param key
         * @param value
         * @returns {*}
         */
        req.modifyStuInfo = function (stuId, key, value) {
            if(iOSDevice()){
          return postRequest(Constant.ServerUrl + "/infoeditByFamily",1,{
            stuid: stuId,
            key: key,
            value: value
        });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/infoeditByFamily",
                    data: {
                        stuid: stuId,
                        key: key,
                        value: value
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 通过手机号查询子女
         * @returns {*}
         */
        req.getChildren = function (phone) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/getchildinfo/noauth",1,{
                phnumber: phone
            });
            }else{
                return $http.get(Constant.ServerUrl + "/getchildinfo/noauth", {
                    params: {
                        phnumber: phone
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 注册关联选择已有学生
         * @param stuId
         * @param phone
         * @param smscode
         * @param password
         * @returns {*}
         */
        req.selectExistedStudent2Register = function (stuId, phone, smscode, password) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/selectRegister",1,{
                stuId: stuId,
                phone: phone,
                smscode: smscode,
                password: md5(password).toUpperCase()
            });
            }else{
                return $http.post(Constant.ServerUrl + "/selectRegister", {
                    stuId: stuId,
                    phone: phone,
                    smscode: smscode,
                    password: md5(password).toUpperCase()
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 查询学生成绩列表
         * @param role
         * @param classId 老师必传
         * @param page
         * @param studentId 家长必传
         *role
         */
        req.selectTestScoresList = function (role, classId, page, studentId) {
            var param = {};
            if (role === Constant.USER_ROLES.PARENT || role === Constant.USER_ROLES.STUDENT) {
                param = {
                    page: page,
                    rows: 20,
                    studentId: studentId
                };
            } else if (role === Constant.USER_ROLES.TEACHER) {
                param = {
                    classId: classId,
                    page: page,
                    rows: 20
                };
            }
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/exam/score/list",1,param);
            }else{
                return $http.get(Constant.ServerUrl + "/exam/score/list", {
                    params: param
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

            

        };

        /**
         * 学生成长分析
         * @param studentId 学生id 家长必传
         */
        req.getStudentGrowthAnalysis = function (studentId) {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/exam/score/analysis/" + studentId,1);
            }else{
                return $http.get(Constant.ServerUrl + "/exam/score/analysis/" + studentId, {
                    params: {}
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            

        };

        /**
         * 老师查看学生考试成绩详情
         * @param classId 班级id
         * @param examId 考试Id
         */
        req.getClassStudentExamListDetail = function (classId, examId) {
            if(iOSDevice()){
          return getRequest(Constant.ServerUrl + "/exam/score/detail",1,{
            classId: classId,
            examId: examId
        });
            }else{
                return $http.get(Constant.ServerUrl + "/exam/score/detail", {
                    params: {
                        classId: classId,
                        examId: examId
                    }
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
          
        };

        /**
         * 老师推送 考试成绩 给家长
         * @param examId 考试id
         * @param classId 班级id
         */
        req.sendExamNotice = function (examId, classId) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/exam/notice/" + examId + "/" + classId,1);
            }else{
                return $http.get(Constant.ServerUrl + "/exam/notice/" + examId + "/" + classId, {
                    params: {
    
                    }
                }).then(function (response) {
                    return response.data;
    
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取校园风采未读
         * @returns {*}
         */
        req.getCampusUnread = function () {
            var params = {};
            if (String(UserPreference.getObject('user').rolename) === Constant.USER_ROLES.PARENT) {
                params.stuId = UserPreference.get('DefaultChildID');
            }

            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/campusview/unreadTotalByList",1,params);
            }else{
                return $http.get(Constant.ServerUrl + "/campusview/unreadTotalByList", {
                    params: params
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /** 修改 考试详情是否 已读状态
         *@param  studentId 学生id
         * @param examId 考试id
         * @param state 考试状态 true 为已读 false表示未读
         */
        req.fixExamReadState = function (examId, studentId, state) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/exam/score/modify/readed/" + examId + "/" + studentId,2,{
                readed: state
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/exam/score/modify/readed/" + examId + "/" + studentId,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        readed: state
                    })
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            

        };

        /**
         * 批量设置校园风采动态已读
         * @param ids comment id array
         */
        req.setCampusReadStatus = function (ids) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/campusview/readSign",1,ids);
            }else{
                return $http.post(Constant.ServerUrl + "/campusview/readSign", ids).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 校园风采添加评论
         * @param newsId
         * @param comment
         * @returns {*}
         */
        req.addCampusComment = function (newsId, comment) {
            if(iOSDevice()){
             return postRequest(Constant.ServerUrl + "/campus/add-comments",1,{
                newsId: newsId,
                comments: comment
            });
            }else{
                return $http.post(Constant.ServerUrl + "/campus/add-comments", {
                    newsId: newsId,
                    comments: comment
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 校园风采删除评论
         * @param commentId
         * @returns {*}
         */
        req.deleteCampusComment = function (commentId) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/campus/deleteComments",1,{
                    id: commentId
                });
            }else{
                return $http.get(Constant.ServerUrl + "/campus/deleteComments", {
                    params: {
                        id: commentId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 校园风采点赞
         */
        req.favorCampus = function (newsId) {
            if(iOSDevice()){
                 return postRequest(Constant.ServerUrl + "/campus/add-praise",1,{
                    newsId: newsId
                });
            }else{
                return $http.post(Constant.ServerUrl + "/campus/add-praise", {
                    newsId: newsId
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 校园风采取消点赞
         */
        req.unfavorCampus = function (newsId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/campus/deletePraise",1,{
                id: newsId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/campus/deletePraise", {
                    params: {
                        id: newsId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 校园风采获取点赞列表
         * @param newsId
         */
        req.getCampusNewsFavorList = function (newsId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/campus/getPraiseList",1,{
                newsId: newsId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/campus/getPraiseList", {
                    params: {
                        newsId: newsId
                    }
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };


        /**
         * 校园风采获取评论列表
         * @param newsId
         * @param page
         * @param commentId 有commentId时，加载之后所有评论
         * @returns {*}
         */
        req.getCampusNewsCommentList = function (newsId, page, commentId) {
            var params = {
                newsId: newsId,
                page: page,
                rows: Constant.reqLimit
            };
            if (commentId) {
                params.commentsId = commentId;
                params.page = 1;
                params.rows = 9999;
            }

            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/campus/getCommentsList",1,params);
            }else{
                return $http.get(Constant.ServerUrl + "/campus/getCommentsList", {
                    params: params
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 通过风采ID获取风采内容
         * @param id
         */
        req.getCampusNewsById = function (id) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/campusview/listById",1,{
                    id: id
                });
            }else{
                return $http.get(Constant.ServerUrl + "/campusview/listById", {
                    params: {
                        id: id
                    }
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * by wl
         * 发表用户反馈
         * content 发表的内容  若为图片类型 则为base 64字符串形式
         * contentType : 消息类型
         */
        req.publishUserFeedback = function (content, contentType) {
            var phoneSy = ionic.Platform.isIOS() ? 'iOS' : 'android';
            if(iOSDevice()){
                 return postRequest(Constant.ServerUrl + "/suggestions/public",1,{
                    appVersion: Constant.version,
                    content: content,
                    phoneOs: phoneSy,
                    contentType: contentType
                });
            }else{
                return $http.post(Constant.ServerUrl + "/suggestions/public", {
                    appVersion: Constant.version,
                    content: content,
                    phoneOs: phoneSy,
                    contentType: contentType
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                }); 
            }
           
        };

        /**
         * 标记消息为已读 用户反馈
         * newsId 消息Id
         */
        req.fixNewsReadStatus = function (newsId) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/suggestions/signRead",1,{
                id: newsId
            });
            }else{
                return $http.post(Constant.ServerUrl + "/suggestions/signRead", {
                    id: newsId
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取用户反馈消息列表
         */
        req.getFeedbackDetailList = function () {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/suggestions/detail",1,{
                endDate: '',
                startDate: ''
            });
            }else{
                return $http.get(Constant.ServerUrl + "/suggestions/detail", {
                    params: {
                        endDate: '',
                        startDate: ''
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取用户反馈 未读数
         */
        req.getNoreadNewsNumber = function () {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/suggestions/getUnreadReplySize",1);
            }else{
                return $http.get(Constant.ServerUrl + "/suggestions/getUnreadReplySize", {
                    params: {}
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 运营文章列表
         * @param page 当前页数
         * @param userId 用户id
         */
        req.getArticleList = function (page) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/article/list",1,{
                page: page,
                rows: 10,
                userId: UserPreference.getObject('user').id
            });
            }else{
                return $http.get(Constant.ServerUrl + "/article/list", {
                    params: {
                        page: page,
                        rows: 10,
                        userId: UserPreference.getObject('user').id
                    }
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 新增接口 - 文章详情
         * 文章讯息
         * @param code 文章编码
         */
        req.getNewArticleDetail = function (code) {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/article/detail",1,{
                code: code
            });
            }else{
                return $http.get(Constant.ServerUrl + "/article/detail", {
                    params: {
                        code: code
                    }
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 新增接口-运营文章操作数据
         */
        req.getArticleOperateData2 = function (code) {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/article/operateData2",1,{
                code: code
            });
            }else{
                return $http.get(Constant.ServerUrl + "/article/operateData2", {
                    params: {
                        code: code
                    }
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };


        /**
         * 运营文章详情
         * @param code 文章编码
         * @param userId 用户id
         */
        req.getArticleDetail = function (code) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/article/operationData",1,{
                code: code,
                userId: UserPreference.getObject('user').id
            });
            }else{
                return $http.get(Constant.ServerUrl + "/article/operationData", {
                    params: {
                        code: code,
                        userId: UserPreference.getObject('user').id
                    }
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 文章点赞或取消点赞
         * @param code 文章编码
         * @param userId 用户Id
         */
        req.giveArticleLikeOrUnlike = function (code) {
            if(iOSDevice()){
            return postRequest(Constant.ServerUrl + "/article/praise/toggle",2,{
                code: code,
                userId: UserPreference.getObject('user').id
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/article/praise/toggle",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        code: code,
                        userId: UserPreference.getObject('user').id
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 文章评论-点赞/取消点赞
         * @param commentId 文章评论id
         */
        req.giveCommentsLikeOrUnlike = function (commentId) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/article/comment/praise/toggle",2,{
                id: commentId,
                userId: UserPreference.getObject('user').id
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/article/comment/praise/toggle",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        id: commentId,
                        userId: UserPreference.getObject('user').id
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };
        /**
         * 给文章评论
         * @param articleId
         * @param content 
         * @param userId
         */
        req.giveArticleComment = function (code, content) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/article/comment/add",2,{
                code: code,
                content: content,
                userId: UserPreference.getObject('user').id
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/article/comment/add",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        code: code,
                        content: content,
                        userId: UserPreference.getObject('user').id
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 检查当前版本
         */
        req.getCurrentVersion = function () {
            var os = ionic.Platform.isIOS() ? 'ios' : 'android';
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/checkversion",1,{
                ostype: os,
                version: Constant.version
            });
            }else{
                return $http.get(Constant.ServerUrl + "/checkversion", {
                    params: {
                        ostype: os,
                        version: Constant.version
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 版本说明
         * @param appType 版本类型 0用户app 1管理app
         * @param version 版本号
         */
        req.getCurrentVersionIntroduce = function () {
            var os = ionic.Platform.isIOS() ? 'ios' : 'android';
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/checkversion/describe",1,{
                appType: 0,
                version: Constant.version,
                osType: os
            });
            }else{
                
                return $http.get(Constant.ServerUrl + "/checkversion/describe", {
                    params: {
                        appType: 0,
                        version: Constant.version,
                        osType: os
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };


        /**
         * 留言 
         * @param message 留言
         * @param childId 小孩id //接收用户
         * @param user 用户 留言用户
         */
        req.sendClassLeaveMessage = function (message, childId, user) {
           
            var userType = user.rolename === 2 ? 1 : 0; //学生是 1 家长为0，默认用0
            if(iOSDevice()){
                 return postRequest(Constant.ServerUrl + "/ad/msg/send",1,[{
                    msg: message,
                    touserId: childId,
                    userId: user.id,
                    userType: userType
                }]);
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/ad/msg/send",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: [{
                        msg: message,
                        touserId: childId,
                        userId: user.id,
                        userType: userType
                    }]
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                }); 
            }
           
        };

        /**
         * 登记已读
         * @param touserId 接收用户id 指的是家长
         * @param userId 留言用户id 小孩
         */
        req.markLeaveMsgReaded = function (parentId, childId) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/ad/msg/readup",1,{
                touserId: parentId,
                userId: childId
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/ad/msg/readup",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        touserId: parentId,
                        userId: childId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

            
        };

        /**
         * 获取来源于所有的学生的未读消息
         * @param parentId 家长id
         * @param studentId 学生id 
         */
        req.getUnreadLeaveMsgList = function (parentId, studentId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/ad/msg/get",1,{
                famid: parentId,
                stuid: studentId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/ad/msg/get", {
                    params: {
                        famid: parentId,
                        stuid: studentId
                    }
                }).
                    then(function (res) {
                        return res.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
            
        };

        /**
         * 获取科目列表
         * @param classId 班级id
         */
        req.getProjectList = function (classId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/course/get/stage-subject",1,{
                classId: classId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/course/get/stage-subject", {
                    params: {
                        classId: classId
                    }
                }). then(function (res) {
                        return res.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
           
        };

        /**
         * 判断班级当前节次是否有走班选课
         * @param classId 班级ID
         * @param weekNum 周几
         * @param sectionNum 节次
         */
        req.checkExistWalkCourse = function (classId, weekNum, sectionNum) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/walkcourse/exist",1,{
                classId: classId,
                weekNum: weekNum,
                sectionNum: sectionNum
            });
            }else{
                return $http.get(Constant.ServerUrl + "/walkcourse/exist", {
                    params: {
                        classId: classId,
                        weekNum: weekNum,
                        sectionNum: sectionNum
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 修改课程
         * @param projectArr 科目数组
         */
        req.fixScheduleProject = function (classId, projectArr) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/course/save/course",1, {
                classId: classId,
                courses: projectArr
            } );
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/course/save/course",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        classId: classId,
                        courses: projectArr
                    } //CustomParam(projectArr)
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 报修记录列表
         * @param condition 0,全部；1，待解决；2，已关闭
         * @param isManage true管理列表；false,我的列表
         * @param page 当前页面
         */

        req.getRepairRecordList = function (condition, page) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/maintainOrders",1, {
                condition: condition,
                isManage: false,
                row: Constant.reqLimit,
                page: page
            });
            }else{
                return $http.get(Constant.ServerUrl + "/maintainOrders", {
                    params: {
                        condition: condition,
                        isManage: false,
                        row: Constant.reqLimit,
                        page: page
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 报修记录详情
         * @param repairRecordId  报修记录id
         */
        req.getRepairRecordDetail = function (repairRecordId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/maintainOrders/" + repairRecordId,1);
            }else{
                return $http.get(Constant.ServerUrl + "/maintainOrders/" + repairRecordId, {
                    params: {
    
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 添加进度处理
         * @param repairRecordId 报修记录id
         * @param orderProgress 1:已提交;2:管理员已查看;4:正在处理;2^30:已关闭;maxinteger:已取消
         * @param progressDescribe 进度描述
         */

        req.addRepairProgress = function (repairRecordId, orderProgress, progressDescribe) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/maintainOrders/" + repairRecordId + "/progress",1,{
                orderProgress: orderProgress,
                progressDescribe: progressDescribe
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/maintainOrders/" + repairRecordId + "/progress",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        orderProgress: orderProgress,
                        progressDescribe: progressDescribe
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 添加报修记录
         * @param maintainImgs 故障图片 array
         * @param orderDescribe 故障描述
         * @param terminalAddress	设备所在地址
         */
        req.addEquipmentRepairRecord = function (params) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/maintainOrders",1,params);
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/maintainOrders",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: params
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 二维码 地址打开
         */
        req.getAssetsIdByScan = function (url) {
            if(iOSDevice()){
                return getRequest(url,1);
            }else{
                return $http({
                    method: 'get',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };


        /**
         * 资产申领列表（教师）
         */

        req.assetsApplyList = function (page) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/assets",1,{
                    row: Constant.reqLimit,
                    page: page
                });
            }else{
                return $http.get(Constant.ServerUrl + "/assets", {
                    params: {
                        row: Constant.reqLimit,
                        page: page
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 提交资产申领
         * @param applyAmount 申请数量
         * @param assetNameRefid  资产名称id
         * @param assetTypeRefid 资产类别id
         * @param  progressDescribe 申请理由
         */
        req.commitAssetsApply = function (params) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/asset/apply",1,{
                applyAmount: params.count,
                assetNameRefid: params.nameId,
                assetTypeRefid: params.categoryId,
                progressDescribe: params.content
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/asset/apply",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        applyAmount: params.count,
                        assetNameRefid: params.nameId,
                        assetTypeRefid: params.categoryId,
                        progressDescribe: params.content
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 资产分类
         */
        req.assetsCategory = function () {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/assetTypes",1);
            }else{
                return $http.get(Constant.ServerUrl + "/assetTypes", {
                    params: {
    
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 根据资产分类查询资产名称
         * @param assetTypeId 资产分类id
         */
        req.queryAssetsName = function (assetTypeId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/asset/getAssetNameByType",1, {
                assetTypeId: assetTypeId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/asset/getAssetNameByType", {
                    params: {
                        assetTypeId: assetTypeId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 添加资产申领进度（管理端）
         * @param orderId 资产申领单id
         * @param orderProgress 进度
         * @param progressDescribe 原因
         */
        req.addAssetsApplyProgress = function (orderId, orderProgress, progressDescribe) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/asset/applyProgress",1,{
                orderId: orderId,
                orderProgress: orderProgress,
                progressDescribe: progressDescribe
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/asset/applyProgress",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        orderId: orderId,
                        orderProgress: orderProgress,
                        progressDescribe: progressDescribe
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 资产申领详情
         * @param id 资产id
         */
        req.assetsApplyDetail = function (id) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/assets/" + id,1);
            }else{
                return $http.get(Constant.ServerUrl + "/assets/" + id, {
                    params: {
    
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 查询资产申领的id
         * @param assetsId 领单id
         */
        req.queryAssetsApplyIds = function (assetsId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/asset/getApplyIssue",1,{
                id: assetsId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/asset/getApplyIssue", {
                    params: {
                        id: assetsId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         *会议室已约会议列表
         * @param id 会议室id
         * @param page 页面
         */
        req.meetingRoomHaveReservationList = function (id, page) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/meetingSubscribles/getByMeetingRoom",1,{
                id: id,
                page: page,
                row: 100
            });
            }else{
                return $http.get(Constant.ServerUrl + "/meetingSubscribles/getByMeetingRoom", {
                    params: {
                        id: id,
                        page: page,
                        row: 100
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 会议通知
         */
        req.meetingNocticeList = function (page) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/meetings/joined",1,{
                page: page,
                row: Constant.reqLimit
            });
            }else{
                return $http.get(Constant.ServerUrl + "/meetings/joined", {
                    params: {
                        page: page,
                        row: Constant.reqLimit
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 发起预约
         * @param beginTime 开始时间
         * @param endTime 结束时间
         * @param meetingRoomId 会议室
         */
        req.meetingAppointment = function (params) {
            var date1 = new Date(params.startDate.replace(/-/g, '/'));
            var date2 = new Date(params.endDate.replace(/-/g, '/'));
            var startStemp = date1.getTime();
            var endStemp = date2.getTime();

            if(iOSDevice()){
             return postRequest(Constant.ServerUrl + "/meetingSubscribles",1,{
                beginTime: startStemp,
                endTime: endStemp,
                meetingContent: params.content,
                meetingRoomId: String(params.meetingRoomId)
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/meetingSubscribles",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        beginTime: startStemp,
                        endTime: endStemp,
                        meetingContent: params.content,
                        meetingRoomId: params.meetingRoomId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 选择会议室
         * 
         */
        req.getMeetingRooms = function () {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/meetings/meetingRooms",1);
            }else{
                return $http.get(Constant.ServerUrl + "/meetings/meetingRooms", {
                    params: {
    
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 我的会议预约列表
         */

        req.myMeetingReservationList = function (page) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/meetingSubscribles/getMine",1,{
                page: page,
                row: Constant.reqLimit
            });
            }else{
                return $http.get(Constant.ServerUrl + "/meetingSubscribles/getMine", {
                    params: {
                        page: page,
                        row: Constant.reqLimit
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 标记会议为已读
         * @param id 会议id
         */
        req.markMeetingReadStatus = function (id) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/meetings/signRead",1,{
                id: id
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/meetings/signRead",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        id: id
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 校验该学校是否具有新高考授权
         */
        req.checkSchoolHaveNceeAuth = function (schoolId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/course/walking-ncee/has-ncee-auth",1,{
                schoolId: schoolId,
            });
            }else{
                return $http.get(Constant.ServerUrl + "/course/walking-ncee/has-ncee-auth", {
                    params: {
                        schoolId: schoolId,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取指定节次走班课
         * @param classId 
         * @param weekNum
         * @param sectionNum
         */
        req.getWalkCourseBySection = function (classId, weekNum, sectionNum) {
            // console.log('class id:'+classId+'-weekNum:' +weekNum +'--sectionNum:'+sectionNum);
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/course/walking-ncee/week-section",1,{
                classId: classId,
                weekNum: weekNum,
                sectionNum: sectionNum
            });
            }else{
                return $http.get(Constant.ServerUrl + "/course/walking-ncee/week-section", {
                    params: {
                        classId: classId,
                        weekNum: weekNum,
                        sectionNum: sectionNum
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };


        /**
         * 获取审批管理列表-我的申请列表
         * @param  progress 状态: 1通过 2审批中 3不通过 为空表示全部
         * @param  schoolId 学校ID
         * @param submitterId 申请人ID
         */
        req.getApprovalApplyList = function (page, progress, schoolId, submitterId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/archive/list",1,{
                page: page,
                rows: Constant.reqLimit,
                progress: progress,
                schoolId: schoolId,
                submitterId: submitterId,
            });
            }else{
                return $http.get(Constant.ServerUrl + "/archive/list", {
                    params: {
                        page: page,
                        rows: Constant.reqLimit,
                        progress: progress,
                        schoolId: schoolId,
                        submitterId: submitterId,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            

        };
        /**
         * 获取经我审批列表
         * @param userId 用户Id
         * @param  progress 状态: 1通过 2审批中 3不通过 为空表示全部
         */

        req.getApprovaledOfMineList = function (page, progress, userId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/archive/list/approved",1,{
                page: page,
                rows: Constant.reqLimit,
                userId: userId,
                progress: progress
            });
            }else{
                return $http.get(Constant.ServerUrl + "/archive/list/approved", {
                    params: {
                        page: page,
                        rows: Constant.reqLimit,
                        userId: userId,
                        progress: progress
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            

        };

        /**
         * 获取通知我的列表
         */
        req.getApprovelNotifyList = function (page, userId) {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/archive/list/notify",1,{
                page: page,
                rows: Constant.reqLimit,
                userId: userId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/archive/list/notify", {
                    params: {
                        page: page,
                        rows: Constant.reqLimit,
                        userId: userId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取待我审批的列表
         */
        req.getWaitApprovalList = function (page, userId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/archive/list/to-approve",1,{
                page: page,
                rows: Constant.reqLimit,
                userId: userId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/archive/list/to-approve", {
                    params: {
                        page: page,
                        rows: Constant.reqLimit,
                        userId: userId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取申请的详情页
         * @param archiveId 申请Id
         */
        req.getApprovalArchiveDeatil = function (archiveId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/archive/detail",1,{
                archiveId: archiveId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/archive/detail", {
                    params: {
                        archiveId: archiveId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };


        /**
         * 获取老师的审批模板列表
         * @param teacherId 教师Id
         */
        req.getTeacherApprovalTemplateList = function (teacherId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/archive/templates",1,{
                teacherId: teacherId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/archive/templates", {
                    params: {
                        teacherId: teacherId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取教师审批指定模板详情
         */
        req.getTeacherApprovalTemplateDetail = function (templateId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/archive/template-detail",1,{
                templateId: templateId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/archive/template-detail", {
                    params: {
                        templateId: templateId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 查询学校部门列表
         * @param schoolId 学校id
         */
        req.getSchoolDepartmentList = function (schoolId) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/archive/department-list",1,{
                    schoolId: schoolId
                });
            }else{
                return $http.get(Constant.ServerUrl + "/archive/department-list", {
                    params: {
                        schoolId: schoolId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 查询部门人员
         */
        req.getDepartmentPersons = function (schoolId, departmentId, teacherNameLike) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/archive/department-person",1,{
                departmentId: departmentId,
                schoolId: schoolId,
                teacherNameLike: teacherNameLike
            });
            }else{
                return $http.get(Constant.ServerUrl + "/archive/department-person", {
                    params: {
                        departmentId: departmentId,
                        schoolId: schoolId,
                        teacherNameLike: teacherNameLike
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 提交审批-申请的审批操作
         * @param approverId 审批人ID
         * @param archiveId 申请ID 
         * @param result 审批结果: 1通过 3不通过 
         * @param suggest 审批意见
         */
        req.commitApproval = function (approverId, archiveId, select) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/archive/approve",1,{
                approverId: approverId,
                archiveId: archiveId,
                result: select.result,
                suggest: select.suggest
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/archive/approve",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        approverId: approverId,
                        archiveId: archiveId,
                        result: select.result,
                        suggest: select.suggest
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取授权模块列表
         * @returns {*}
         */
        req.getToolkitList = function () {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/toolkit/list",1);
            }else{
                return $http.get(Constant.ServerUrl + "/toolkit/list").then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };


        req.requestToolkit = function (phone, appId) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/toolkit/apply",2,{
                phone: phone,
                appId: appId
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/toolkit/apply",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        phone: phone,
                        appId: appId
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };
        /**
         * 新建审批提交
         * @param schoolId
         * @param  submitterId 申请人ID
         * @param templateId 模板id 
         * @param title 标题
         * @param currUserId 当前审批人id
         * @param attaches 附件 attachName-附件名称 attachUrl-附件地址
         * @param chains 审批链 level 审批链层级, 从1开始 nextUserId 后序审批人ID, 最后一个置null ,prevUserId 前序审批人ID, 第一个置null ,userId 当前审批人ID
         */
        req.addNewApproval = function (schoolId, submitterId, templateId, select) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/archive/add",1,{
                schoolId: schoolId,
                submitterId: submitterId,
                templateId: templateId,
                title: select.title,
                comment: select.comment,
                radios: select.radios,
                notifierIds: select.notifierIds,
                inputs: select.inputs,
                fulltexts: select.fulltexts,
                dates: select.dates,
                dateRanges: select.dateRanges,
                currUserId: select.currUserId,
                checkboxes: select.checkboxes,
                chains: select.chains,
                attaches: select.attaches,
                timeList: select.timeList
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/archive/add",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        schoolId: schoolId,
                        submitterId: submitterId,
                        templateId: templateId,
                        title: select.title,
                        comment: select.comment,
                        radios: select.radios,
                        notifierIds: select.notifierIds,
                        inputs: select.inputs,
                        fulltexts: select.fulltexts,
                        dates: select.dates,
                        dateRanges: select.dateRanges,
                        currUserId: select.currUserId,
                        checkboxes: select.checkboxes,
                        chains: select.chains,
                        attaches: select.attaches,
                        timeList: select.timeList
                    }
    
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         *  查询班级之星列表
         * @param class_id 班级id
         * @param page 
         */
        req.findClassStarList = function (class_id, page) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/class/getClassStarList/" + class_id,1,{
                page: page,
                rows: Constant.reqLimit
            });
            }else{
                return $http.get(Constant.ServerUrl + "/class/getClassStarList/" + class_id, {
                    params: {
                        page: page,
                        rows: Constant.reqLimit
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 设置班级之星
         * @param class_id
         * @param startDate
         * @param endDate
         * @param studentids 
         */
        req.setClassStar = function (selected) {
            if(iOSDevice()){
             return postRequest(Constant.ServerUrl + "/class/setClassStar",1,{
                classId: parseInt(selected.class_id),
                startDate: selected.startDate,
                endDate: selected.endDate,
                studentids: selected.studentids
            });
            }else{
             return $http({
                method: "post",
                url: Constant.ServerUrl + "/class/setClassStar",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    classId: parseInt(selected.class_id),
                    startDate: selected.startDate,
                    endDate: selected.endDate,
                    studentids: selected.studentids
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
            }
            
        };

        /**
         * 删除班级之星
         */
        req.deleteClassStar = function (class_id, itemId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/class/delClassStar",1,{
                cid: class_id,
                id: itemId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/class/delClassStar", {
                    params: {
                        cid: class_id,
                        id: itemId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /** 学生评价 */
        /**
         * 老师获取学生评价列表
         * @param classId
         */
        req.getStudentAssessList = function (classId, page) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/student-assess/list/class",1,{
                page: page,
                rows: Constant.reqLimit,
                classId: classId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/student-assess/list/class", {
                    params: {
                        page: page,
                        rows: Constant.reqLimit,
                        classId: classId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
         
        };

        /**
         * 老师获取某个学生评价详情
         * @param taskId 任务id
         * @param studentId 
         */
        req.getStudentAssessDetail = function (taskId, studentId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/student-assess/detail/student",1,{
                taskId: taskId,
                studentId: studentId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/student-assess/detail/student", {
                    params: {
                        taskId: taskId,
                        studentId: studentId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 家长获取学生评价列表
         * @param studentId
         */
        req.getMyChildAssessList = function (page, studentId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/student-assess/list/student",1,{
                page: page,
                rows: Constant.reqLimit,
                studentId: studentId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/student-assess/list/student", {
                    params: {
                        page: page,
                        rows: Constant.reqLimit,
                        studentId: studentId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 获取班级所有学生的评分列表--评价结果
         * @param classId
         * @param taskId 任务id
         */
        req.getClassStudentScoresList = function (classId, taskId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/student-assess/score",1,{
                taskId: taskId,
                classId: classId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/student-assess/score", {
                    params: {
                        taskId: taskId,
                        classId: classId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 新增或修改学生评分
         * @param taskId 条目id
         * @param itemId 评分项id
         * @param scores 评分数组
         * @param studentId 学生id
         * @param score 分数
         */
        req.commitStudentAssessScore = function (selected, taskId, itemId, class_id) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/student-assess/score/add-modify",1,{
                classId: parseInt(class_id),
                taskId: taskId,
                itemId: itemId,
                scores: selected.scores
            });
            }else{
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/student-assess/score/add-modify",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        classId: parseInt(class_id),
                        taskId: taskId,
                        itemId: itemId,
                        scores: selected.scores
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取评价任务指定评分项的评价列表
         * @param classId
         * @param taskId 条目id
         * @param itemId 评分项id
         */
        req.getOneAssessOptionClassScoreList = function (classId, taskId, itemId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/student-assess/score/item",1,{
                taskId: taskId,
                classId: classId,
                itemId: itemId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/student-assess/score/item", {
                    params: {
                        taskId: taskId,
                        classId: classId,
                        itemId: itemId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };
        /**
         * 获取评价项列表
         */
        req.getAssessOptionList = function (classId, taskId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/student-assess/task-items",1,{
              taskId: taskId,
              classId: classId,
          });
            }else{
                return $http.get(Constant.ServerUrl + "/student-assess/task-items", {
                    params: {
                        taskId: taskId,
                        classId: classId,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /** 收费统计 */
        /** 
         *  班级收费统计
         * */
        req.getClassFeeProject = function (classId, page) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/fee/getClassFeePorject/" + classId,1);
            }else{
                return $http.get(Constant.ServerUrl + "/fee/getClassFeePorject/" + classId, {
                    params: {
                        // page: page,
                        // rows: Constant.reqLimit 
                    }
    
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 学生收费统计
         */
        req.getListStudentStatic = function (classId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/fee/listStudentStat",1, {
                classId: classId,
                page: 1,
                rows: 100,
            });
            }else{
                return $http.get(Constant.ServerUrl + "/fee/listStudentStat", {
                    params: {
                        classId: classId,
                        page: 1,
                        rows: 100,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 某收费项 班级学生缴费情况
         */
        req.getClassFeeStudentDetail = function (feeId, classId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/fee/getClassFeeStudent/" + feeId + "/" + classId,1);
            }else{
                return $http.get(Constant.ServerUrl + "/fee/getClassFeeStudent/" + feeId + "/" + classId, {

                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 学生收费情况详情2
         */
        req.getOneStudentFeeDetail = function (stuId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/fee/listOneStudentFeeProjectStat",1,{
                stuid: stuId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/fee/listOneStudentFeeProjectStat", {
                    params: {
                        stuid: stuId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         *缴费明细 家长
         */
        req.getStudentFeeDetail = function (studentId) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/fee/getStudentFeePorject/" + studentId,1);
            }else{
                return $http.get(Constant.ServerUrl + "/fee/getStudentFeePorject/" + studentId, {

                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /** 巡课设置 */
        /**
         * 获取小孩教室实况
         */
        req.getChildrenClassroomLiveList = function (studentId) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/classroomLive/famliyByCameraInfo",1,{
                studentId: studentId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/classroomLive/famliyByCameraInfo", {
                    params: {
                        studentId: studentId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取班级同学人像列表
         * @param classId 班级id
         * @param name 学生姓名
         */
        req.getFaceRecognitionClassList = function (classId, name) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/faceRecognition/getOfClass",1,{
                classId: classId,
                name: name
            });
            }else{
                return $http.get(Constant.ServerUrl + "/faceRecognition/getOfClass", {
                    params: {
                        classId: classId,
                        name: name
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };
        /**
         * 获取老师的个人人像
         * @param userId
         */
        req.getTeacherFaceRecognition = function (userId) {
            if(iOSDevice()){
          return getRequest(Constant.ServerUrl + "/faceRecognition/getOne",1,{
            userId: userId,
        });
            }else{
                return $http.get(Constant.ServerUrl + "/faceRecognition/getOne", {
                    params: {
                        userId: userId,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 上传人像
         */
        req.uploadUserPortrait = function (userId, base64Str) {
            if(iOSDevice()){
                return postRequest(Constant.ServerUrl + "/faceRecognition/uploadImg",1,{
                    userId: userId,
                    imgData: base64Str
                });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/faceRecognition/uploadImg",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        userId: userId,
                        imgData: base64Str
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };
        /** 日程安排 */
        /**
         * 添加或修改教师日程安排
         * @param id 条目id 更新时上传
         * @param teacherId
         * @param schedule 日程安排内容
         * @param date 安排日期
         * @param reminder_time 提醒日期
         * @param  reminderFlag 是否提醒
         * @param modificationFlag false 为添加 true为修改
         */
        req.updateTeacherArrange = function (itemId, teacherId, selected, modificationFlag) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/teacher-schedule/addOrUpdate",1,{
                scheduleId: itemId,
                teacherId: teacherId,
                schedule: selected.schedule,
                date: selected.date,
                reminderTime: selected.reminder_time,
                reminderFlag: selected.isNotice,
                modificationFlag: modificationFlag
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/teacher-schedule/addOrUpdate",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        scheduleId: itemId,
                        teacherId: teacherId,
                        schedule: selected.schedule,
                        date: selected.date,
                        reminderTime: selected.reminder_time,
                        reminderFlag: selected.isNotice,
                        modificationFlag: modificationFlag
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 删除日程安排
         * @param scheduleId 条目id
         */
        req.deleteTeacherArrange = function (scheduleId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/teacher-schedule/delete",1,{
                scheduleId: scheduleId,
            });
            }else{
                return $http.get(Constant.ServerUrl + "/teacher-schedule/delete", {
                    params: {
                        scheduleId: scheduleId,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 日程安排列表
         * @param startDate 开始日期
         * @param endDate 结束日期
         */
        req.getTeacherArrangeList = function (startDate, endDate) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/teacher-schedule/list",1,{
                    startDate: startDate,
                    endDate: endDate
                });
            }else{
                return $http.get(Constant.ServerUrl + "/teacher-schedule/list", {
                    params: {
                        startDate: startDate,
                        endDate: endDate
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 考勤统计 家长
         * @param stuId 学生id
         */
        req.getChildAttendStatic = function (stuId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/kind/attend/student-attend/list",1,{
                studentId: stuId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/kind/attend/student-attend/list", {
                    params: {
                        studentId: stuId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        //调课
        /**
         * 获取老师调课记录
         */
        req.getAdjustCourseList = function (teacherId) {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/course/CourseAdjust/getByTeacher",1,{
                teacherId: teacherId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/course/CourseAdjust/getByTeacher", {
                    params: {
                        teacherId: teacherId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 发起调课
         * @param classId 班级id
         * @param courseDay1 日期大于当天 对调的日期 
         * @param courseDay2 日期大于当天 被对调的日前 
         */
        req.addCourseAdjust = function (selected) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/course/CourseAdjust/add",1,{
                classId: selected.classId,
                courseDay1: selected.courseDay1,
                courseDay2: selected.courseDay2,
                section1: selected.section1,
                section2: selected.section2,
                teacherId1: selected.teacherId1,
                teacherId2: selected.teacherId2,
                subjectId1: selected.subjectId1,
                subjectId2: selected.subjectId2
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/course/CourseAdjust/add",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        classId: selected.classId,
                        courseDay1: selected.courseDay1,
                        courseDay2: selected.courseDay2,
                        section1: selected.section1,
                        section2: selected.section2,
                        teacherId1: selected.teacherId1,
                        teacherId2: selected.teacherId2,
                        subjectId1: selected.subjectId1,
                        subjectId2: selected.subjectId2
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };
        /**
         * 撤销调课
         * @param id 调课id
         */
        req.cancelAdjustCourse = function (itemId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/course/CourseAdjust/cancel",1,{
                id: itemId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/course/CourseAdjust/cancel", {
                    params: {
                        id: itemId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 获取教师在某班某天的课表,今天的必须大于当前时间点
         * @param 调课的日期
         */
        req.getCourseByTeacherOneDay = function (selected) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/course/CourseAdjust/getCourceByClassAndTeacherAndDay",1, {
                classId: selected.classId,
                teacherId: selected.teacherId1,
                courseDay: selected.recordDay1
            });
            }else{
                return $http.get(Constant.ServerUrl + "/course/CourseAdjust/getCourceByClassAndTeacherAndDay", {
                    params: {
                        classId: selected.classId,
                        teacherId: selected.teacherId1,
                        courseDay: selected.recordDay1
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };


        /**
         * 获取某班某天的课表,今天的必须大于当前时间点,除去操作的教师任课节次
         * @param courseDay 被调课的日期
         */
        req.getCourseByClassAndRemoveThisTeacher = function (selected) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/course/CourseAdjust/getCourceByClassAndDayRemoveThisTeacher",1,{
                classId: selected.classId,
                teacherId: selected.teacherId1,
                courseDay: selected.recordDay2
            });
            }else{
                return $http.get(Constant.ServerUrl + "/course/CourseAdjust/getCourceByClassAndDayRemoveThisTeacher", {
                    params: {
                        classId: selected.classId,
                        teacherId: selected.teacherId1,
                        courseDay: selected.recordDay2
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        //应急演练
        /**
         * 演练记录列表
         * @param 
         */
        req.getDrillRecordList = function (page) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/drillRecords/list/byDrillDefine",1,{
                page: page,
                rows: 10
            });
            }else{
                return $http.get(Constant.ServerUrl + "/drillRecords/list/byDrillDefine", {
                    params: {
                        page: page,
                        rows: 10
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 演练详情 -查看详情
         * @param itemId
         */
        req.getDrillDetail = function (itemId) {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/drillRecords/detail",1,{
                id: itemId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/drillRecords/detail", {
                    params: {
                        id: itemId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 新增记录
         * @param tempDrillDefineId 演练记录id
         * @param remarks 演练记录说明
         * @param strImageUrls 图片数组
         */

        req.addDrillRecord = function (drillId, selected) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/drillRecords/save/byDrillRecords",1,{
                tempDrillDefineId: drillId,
                remarks: selected.remarks,
                picdatas: selected.picdatas,
                strImageUrls: ''
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/drillRecords/save/byDrillRecords",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        tempDrillDefineId: drillId,
                        remarks: selected.remarks,
                        picdatas: selected.picdatas,
                        strImageUrls: ''
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /***
         * 演练记录详情
         * @param drillDefineId 演练id
         * @param page  分页
         */

        req.getDrillRedordDetail = function (drillDefineId, page) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/drillRecords/list/byDrilRecords",1,{
                drillDefineId: drillDefineId,
                page: page,
                rows: 5,
            });
            }else{
                return $http.get(Constant.ServerUrl + "/drillRecords/list/byDrilRecords", {
                    params: {
                        drillDefineId: drillDefineId,
                        page: page,
                        rows: 5,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 查看方案详情
         * @param id 方案id
         */
        req.getDrillProgramDetail = function (programId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/drillRecords/drillPlan/detail",1,{
                id: programId
            }); 
            }else{
                return $http.get(Constant.ServerUrl + "/drillRecords/drillPlan/detail", {
                    params: {
                        id: programId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        //分组
        /**
         * 分组信息列表
         */
        req.getGroupNewsInfoList = function (page) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/classGroupManage/news/list",1,{
                page: page,
                rows: 10
            });
            }else{
                return $http.get(Constant.ServerUrl + "/classGroupManage/news/list", {
                    params: {
                        page: page,
                        rows: 10
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

           

        };

        /**
         * 查看分组详情
         */
        req.getGroupNewsInfoDetail = function (itemId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/classGroupManage/detail",1,{
                id: itemId
            });
       }else{
        return $http.get(Constant.ServerUrl + "/classGroupManage/detail", {
            params: {
                id: itemId
            }
        }).then(function (res) {
            return res.data;
        }, function (error) {
            return $q.reject(error);
        });
     }
            
        };

        /**
         * 获取信息类型列表
         */
        req.getFarInfoCategoryList = function () {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/classGroupManage/parentCategoryList",1);
            }else{
                return $http.get(Constant.ServerUrl + "/classGroupManage/parentCategoryList", {
                    params: {
    
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 信息子类型列表
         * @param parentId 父类id
         */
        req.getSubInfoCategoryList = function (parentId) {
            if(iOSDevice()){
          return getRequest(Constant.ServerUrl + "/classGroupManage/backstageCategoryList/byNew",1,{
            parentId: parentId
        });
            }else{
                return $http.get(Constant.ServerUrl + "/classGroupManage/backstageCategoryList/byNew", {
                    params: {
                        parentId: parentId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 发布班级范围列表
         */
        req.getGroupAttendanceClassList = function () {
            if(iOSDevice()){
             return getRequest(Constant.ServerUrl + "/classGroupManage/groupAttendance/classList",1);
            }else{
                return $http.get(Constant.ServerUrl + "/classGroupManage/groupAttendance/classList", {
                    params: {
    
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 新增班级分组信息
         */
        req.addClassInfoGroup = function (selected) {
            if(iOSDevice()){
               return postRequest(Constant.ServerUrl + "/classGroupManage/save",1,{
                parentCategory: selected.farCategoryId,
                category: selected.subCategoryId,
                classIds: selected.classIds,
                title: selected.title,
                content: selected.content,
                picdatas: selected.picdatas
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/classGroupManage/save",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        parentCategory: selected.farCategoryId,
                        category: selected.subCategoryId,
                        classIds: selected.classIds,
                        title: selected.title,
                        content: selected.content,
                        picdatas: selected.picdatas
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        //隐患管理
        /**
         * 获取 隐患列表
         * @param status 状态
         */
        req.getHidenTroubleList = function (isICreated, status, page) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/teacher/hidden-danger/teacher/list",1,{
                isICreated: isICreated,
                status: status,
                page: page,
                rows: 10
            });
            }else{
                return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/teacher/list", {
                    params: {
                        isICreated: isICreated,
                        status: status,
                        page: page,
                        rows: 10
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 获取隐患详情
         */
        req.getHidenTroubleDetail = function (hiddenDangerId) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/teacher/hidden-danger/detail",1,{
                hiddenDangerId: hiddenDangerId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/detail", {
                    params: {
                        hiddenDangerId: hiddenDangerId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取学校老师列表
         * @param schoolId 学校id
         */

        req.getSchoolTeachersList = function (schoolId) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/teacher/hidden-danger/get-teachers",1,{
                schoolId: schoolId,
                teacherName: ''
            });
            }else{
                return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/get-teachers", {
                    params: {
                        schoolId: schoolId,
                        teacherName: ''
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 添加隐患
         * @param location 隐患位置
         * @param title 隐患标题
         * @param message 隐患信息
         * @param imagesBase64 图片数组
         */
        req.addHidenTrouble = function (selected, schoolId) {
            if(iOSDevice()){
            return postRequest( Constant.ServerUrl + "/teacher/hidden-danger/teacher/upload",1,{
                location: selected.location,
                title: selected.title,
                message: selected.message,
                imagesBase64: selected.picdatas,
                schoolId: schoolId
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/teacher/hidden-danger/teacher/upload",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        location: selected.location,
                        title: selected.title,
                        message: selected.message,
                        imagesBase64: selected.picdatas,
                        schoolId: schoolId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 增加隐患处理进度
         * @param assignedPersonId 指派人
         * @param description //描述
         * @param hiddenDangerId //隐患id
         * @param status 状态
         */
        req.addHidenTroubleProgress = function (selected, itemId) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/teacher/hidden-danger/add-progress/" + selected.status,1,{
                assignedPersonId: selected.selectManId,
                description: selected.content,
                hiddenDangerId: itemId
            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/teacher/hidden-danger/add-progress/" + selected.status,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        assignedPersonId: selected.selectManId,
                        description: selected.content,
                        hiddenDangerId: itemId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 获取隐患数量
         */
        req.getHidenTroubleCount = function (isICreated) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/teacher/hidden-danger/teacher/list/count",1,{
                isICreated: isICreated
            });
            }else{
                return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/teacher/list/count", {
                    params: {
                        isICreated: isICreated
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        //预约管理
        /**
         * 获取家长端预约列表
         * 
         */
        req.getParentReservationList = function (page, studentId) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/reservation/parent-list/" + studentId,1,{
                page: page,
                rows: 10
            });
            }else{
                return $http.get(Constant.ServerUrl + "/reservation/parent-list/" + studentId, {
                    params: {
                        page: page,
                        rows: 10
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 预约详情
         * @param reservationId 预约条目id
         */
        req.getParentReservationDetail = function (itemId) {
            if(iOSDevice()){
                  return getRequest(Constant.ServerUrl + "/reservation/parent-get-detail",1,{
                    reservationId: itemId
                });
            }else{
                return $http.get(Constant.ServerUrl + "/reservation/parent-get-detail", {
                    params: {
                        reservationId: itemId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取教师信息 
         * @param schoolId 学校id
         */
        req.getReservationTeacherInfo = function (schoolId, teacherName) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/reservation/get-teacher-info",1,{
                schoolId: schoolId,
                teacherName: teacherName
            });
            }else{
                return $http.get(Constant.ServerUrl + "/reservation/get-teacher-info", {
                    params: {
                        schoolId: schoolId,
                        teacherName: teacherName
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 添加入校预约
         * @param followersNo 随访人员数量
         * @param plateNo 拜访家长车牌
         * @param visitCause 拜访事由
         * @param visitTime 拜访时间
         * @param visitorIdCardNo 拜访家长身份证号
         * @param visitorName  拜访者姓名
         * @param visitorPhoneNo 拜访者电话号码
         */
        req.addIntoSchoolReservation = function (selected) {
            if(iOSDevice()){
              return postRequest( Constant.ServerUrl + "/reservation/add",1,{
                followersNo: selected.followersNo,
                schoolId: selected.schoolId,
                plateNo: selected.plateNo,
                teacherId: selected.teacherId,
                visitCause: selected.visitCause,
                visitTime: selected.visitTime,
                visitorIdCardNo: selected.visitorIdCardNo,
                visitorName: selected.visitorName,
                visitorPhoneNo: selected.visitorPhoneNo,
                studentId: selected.studentId

            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/reservation/add",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        followersNo: selected.followersNo,
                        schoolId: selected.schoolId,
                        plateNo: selected.plateNo,
                        teacherId: selected.teacherId,
                        visitCause: selected.visitCause,
                        visitTime: selected.visitTime,
                        visitorIdCardNo: selected.visitorIdCardNo,
                        visitorName: selected.visitorName,
                        visitorPhoneNo: selected.visitorPhoneNo,
                        studentId: selected.studentId
    
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 同意或拒绝预约
         * @param status 2 同意 3拒绝
         * @param reservationId 预约条目id
         */

        req.agreeOrRefuseReservation = function (reservationId, status) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/reservation/reply-reservation",1,{
                reservationId: reservationId,
                status: status,
            });
            }else{
                return $http.get(Constant.ServerUrl + "/reservation/reply-reservation", {
                    params: {
                        reservationId: reservationId,
                        status: status,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
            // return $http({
            //     method: 'post',
            //     url: Constant.ServerUrl + "/reservation/reply-reservation" ,
            //     headers: {
            //         'Content-Type': 'application/json'
            //     },
            //     data: {
            //         reservationId: reservationId,
            //         status: status,
            //     }
            // }).then(function (response) {
            //     return response.data;
            // }, function (error) {
            //     return $q.reject(error);
            // });
        };

        /**
         * 教师获取预约列表
         */
        req.getTeacherReservationList = function (page) {
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/reservation/teacher-list",1,{
                    page: page,
                    rows: 10
                });

            }else{
                return $http.get(Constant.ServerUrl + "/reservation/teacher-list", {
                    params: {
                        page: page,
                        rows: 10
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 教师获取预约详情
         */
        req.getTeacherReservationDetail = function (itemId) {
            if(iOSDevice()){
           return getRequest(Constant.ServerUrl + "/reservation/teacher-get-detail",1,{
            reservationId: itemId
        });
            }else{
                return $http.get(Constant.ServerUrl + "/reservation/teacher-get-detail", {
                    params: {
                        reservationId: itemId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        //通知公告详情
        req.getNoticeDetail = function (detailId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/newsInfo/" + detailId,1);
            }else{
                return $http.get(Constant.ServerUrl + "/newsInfo/" + detailId, {
                    params: {
    
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };


        //宝宝视频相关api
        //查询宝宝视频观看费用价格
        req.selectVideoPrice = function (schoolId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/pay/order/querySchoolAndBabyVideoPrice",1,{
                schoolId: schoolId,
                categoryId: 2
            });
            }else{
                return $http.get(Constant.ServerUrl + "/pay/order/querySchoolAndBabyVideoPrice", {
                    params: {
                        schoolId: schoolId,
                        categoryId: 2
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        //查询疫情 今日概况
        req.getCovidSummary = function (classId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/epidemic/record/count/class_level",1,{
                classId: classId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/epidemic/record/count/class_level", {
                    params: {
                        classId: classId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        //查询疫情 体温异常名单
        req.getWarningList = function (classId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/epidemic/alarm/list",1,{
                classId: classId,
                page: 1,
                rows: 9999
            });
            }else{
                return $http.get(Constant.ServerUrl + "/epidemic/alarm/list", {
                    params: {
                        classId: classId,
                        page: 1,
                        rows: 9999
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        //查询疫情 检测明细列表
        req.getCheckedList = function (classId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/epidemic/record/list",1,{
                classId: classId,
                page: 1,
                rows: 9999
            });
            }else{
                return $http.get(Constant.ServerUrl + "/epidemic/record/list", {
                    params: {
                        classId: classId,
                        page: 1,
                        rows: 9999
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        //查询疫情 检测明细详情
        req.getCheckedDetail = function (recordId) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/epidemic/record/detail/list",1,{
                recordId: recordId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/epidemic/record/detail/list", {
                    params: {
                        recordId: recordId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        //查询疫情 告警详情
        req.getWarningDetail = function (alarmId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/epidemic/alarm/details",1,{
                alarmId: alarmId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/epidemic/alarm/details", {
                    params: {
                        alarmId: alarmId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        //查询疫情 10天内检测记录
        req.getStudentCheckRecords = function (stuId) {
            if(iOSDevice()){
              return getRequest(Constant.ServerUrl + "/epidemic/record/stu/detail/list",1,{
                stuId: stuId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/epidemic/record/stu/detail/list", {
                    params: {
                        stuId: stuId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 家长上报体温
         */
        req.reportTemp = function (stuId, temp) {
            if(iOSDevice()){
               return getRequest(Constant.ServerUrl + "/epidemic/record/save/record",1,{
                stuId: stuId,
                bodyTemperature: temp
            });
            }else{
                return $http.get(Constant.ServerUrl + "/epidemic/record/save/record", {
                    params: {
                        stuId: stuId,
                        bodyTemperature: temp
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 设置疫情告警已读
         */
        req.covidWarningRead = function (classId) {
            if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/epidemicAlarm/readSign",1,{
                classId: classId
            });
            }else{
                return $http.get(Constant.ServerUrl + "/epidemicAlarm/readSign", {
                    params: {
                        classId: classId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
        * 疫情跟进
        */
        req.handleWarning = function (alarmId, status, description) {
            if(iOSDevice()){
              return postRequest(Constant.ServerUrl + "/epidemic/alarm/save/progress",2,{
                alarmId: alarmId,
                status: status,
                description: description

            });
            }else{
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/epidemic/alarm/save/progress",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        alarmId: alarmId,
                        status: status,
                        description: description
    
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取班级值日生
         */
      //  req.getClassDuty


        return req;
    }]);
