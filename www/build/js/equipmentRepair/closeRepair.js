angular.module('app.controllers')
    .controller('closeRepairCtrl', ['$scope', 'Requester', 'toaster', '$stateParams', '$rootScope', '$ionicHistory', function ($scope, Requester,toaster,$stateParams,$rootScope,$ionicHistory) {
        $scope.$on('$ionicView.beforeEnter', function ($scope) {
            console.log('beforeEnter progress');

        });

        $scope.$on("$ionicView.enter", function (event, data) {
          
           
        
            $scope.repairRecordId = $stateParams.repairRecordId?$stateParams.repairRecordId:$rootScope.WXRepairRecordId;
            $rootScope.WXRepairRecordId = $scope.repairRecordId;
          
            //提交进度信息
            $scope.select = {
                key: 0,
                status: Math.pow(2,30),
                content: ''
            };
            //进度状态
            $scope.progressStatues = [{
                    key: 1,
                    value: '设备修好了'
                },
                {
                    key: 2,
                    value: '设备没坏不用修'
                },
                {
                    key: 3,
                    value: '设备报废了'
                },
            ];
        });

        $scope.confirm = function () {
        
            Requester.addRepairProgress($scope.repairRecordId, $scope.select.status, $scope.select.content).then(function (res) {
                if (res.result) {
                    toaster.success({
                        title: "关闭成功",
                        body: "该报修记录成功关闭",
                        timeout: 3000
                    });
                   
                  $ionicHistory.goBack();
                }
            });

        };



    }]);