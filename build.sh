#!/bin/bash
cordova clean
gulp templatecache
gulp ng_annotate
gulp useref
ionic cordova build  ios
ionic cordova build --release android
adb install -r platforms/android/build/outputs/apk/android-armv7-release.apk
